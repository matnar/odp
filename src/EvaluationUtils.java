import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import codd.model.DspEdge;
import codd.model.DspPath;
import codd.model.DspVertex;
import codd.model.Pair;

public class EvaluationUtils {

	public static List<Pair> getOutgoingEdges(int currentNodeId, Set<Pair> edges) {
		List<Pair> outgoingEdges = new ArrayList<Pair>();
		for (Pair e : edges)
			if (e.getA() == currentNodeId)
				outgoingEdges.add(e);
		return outgoingEdges;
	}

	public static ArrayList<DspPath> getPathsEndingInNode(ArrayList<DspPath> paths,
			int nodeId) {

		ArrayList<DspPath> output = new ArrayList<DspPath>();
		for (DspPath path : paths) {
			if (path.isSink(nodeId))
				output.add(path);
		}
		return output;
	}

	public static ArrayList<DspPath> computePaths(Map<Integer, DspVertex> vDsp,
			Map<Pair, DspEdge> eDsp, List<Integer> sourcesIndex) {
		/* 3. Compute paths between source and sinks */
		/* Creating paths... */
		List<Integer> frontier = new ArrayList<Integer>();

		/* Create dumb paths from source to source itself */
		ArrayList<DspPath> paths = new ArrayList<DspPath>();
		for (Integer so : sourcesIndex) {
			DspPath path = new DspPath(so);
			paths.add(path);
		}

		if (sourcesIndex.size() > 0) {
			/* Initialize graph exploration variables */
			/* Current node must be a source of the DSP Graph */
			Integer currentNode = sourcesIndex.get(0);
			boolean modified = true;

			do {

				modified = false;
				List<Pair> oEdges = getOutgoingEdges(currentNode, eDsp.keySet());

				/* Update nodes nodeTBV frontier */
				for (Pair p : oEdges) {
					frontier.add(new Integer(p.getB()));
				}

				ArrayList<DspPath> pathsToUpdate = getPathsEndingInNode(paths,
						currentNode);

				for (DspPath pathToUpdate : pathsToUpdate) {

					if (oEdges.isEmpty()) {
						continue;
					}

					paths.remove(pathToUpdate);

					for (Pair oe : oEdges) {
						DspPath newPath = pathToUpdate.clone();
						newPath.add(oe.getB());
						paths.add(newPath);
						modified = true;
					}
				}

				if (frontier.isEmpty()) {
					modified = false;
				} else {
					currentNode = frontier.remove(0);
				}
			} while (modified || !frontier.isEmpty());
		}

		return paths;
	}
}
