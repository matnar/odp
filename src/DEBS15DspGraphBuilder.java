import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codd.DspGraphBuilder;
import codd.metricsprovider.ResponseTimeProvider;
import codd.model.DspEdge;
import codd.model.DspGraph;
import codd.model.DspPath;
import codd.model.DspVertex;
import codd.model.Pair;

public class DEBS15DspGraphBuilder extends DspGraphBuilder {

	private DEBS15DspGraphBuilder(DspGraph graph) {
		super(graph);
	}

	/**
	 * Stores ratio between lambda on the edge and lambda on a reference edge.
	 */
	private Map<DspEdge, Double> edgeToLambdaCoeff = new HashMap<>();

	public static DEBS15DspGraphBuilder newInstance(double alfa, double beta) {
		/* SEQUENTIAL layered topology */
		Map<Integer, DspVertex> vDsp = new HashMap<Integer, DspVertex>();
		Map<Pair, DspEdge> eDsp = new HashMap<Pair, DspEdge>();
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();

		/* Source and sink are not replicated */

		final double jarSize = 0.3 * 1024 * 1024;// 1.4MB = 1.4 * 1024 * 1024
													// for full topology;

		/* 1. Create DspVertex */
		DspVertex v = new DspVertex(0, "datasource", 1, 2.0, 1.0);
		v.setServiceRate(beta * 280.0);
		v.setCodeImageSize(jarSize);
		v.setInternalStateImageSize(82);
		v.setMaxReplication(1);
		vDsp.put(new Integer(0), v);
		v = new DspVertex(1, "parser", 1, 2.0, 1.0);
		v.setServiceRate(beta * 230.0);
		v.setCodeImageSize(jarSize);
		v.setInternalStateImageSize(0);
		vDsp.put(new Integer(1), v);
		v = new DspVertex(2, "filterByCoordinates", 1, 2.0, 1.0);
		v.setServiceRate(beta * 250.0);
		v.setCodeImageSize(jarSize);
		v.setInternalStateImageSize(0);
		vDsp.put(new Integer(2), v);
		v = new DspVertex(3, "metronome", 1, 2.0, 1.0);
		v.setMaxReplication(1);
		v.setServiceRate(beta * 300.0); /* original 190 */
		v.setCodeImageSize(jarSize);
		v.setInternalStateImageSize(328);
		vDsp.put(new Integer(3), v);
		v = new DspVertex(4, "computeCellID", 1, 2.0, 1.0);
		v.setServiceRate(beta * 250.0);
		v.setCodeImageSize(jarSize);
		v.setInternalStateImageSize(0);
		vDsp.put(new Integer(4), v);
		v = new DspVertex(5, "countByWindow", 1, 2.0, 1.0);
		v.setServiceRate(beta * 335.0);
		v.setCodeImageSize(jarSize);
		v.setInternalStateImageSize(1376);
		vDsp.put(new Integer(5), v);
		v = new DspVertex(6, "partialRank", 1, 2.0, 1.0);
		v.setServiceRate(beta * 2300.0);
		v.setCodeImageSize(jarSize);
		v.setInternalStateImageSize(1536);
		vDsp.put(new Integer(6), v);
		v = new DspVertex(7, "globalrank", 1, 2.0, 1.0);
		v.setServiceRate(beta * 2300.0); /*
											 * we can't scale this...so we use a
											 * large service rate
											 */
		v.setCodeImageSize(jarSize);
		v.setInternalStateImageSize(480);
		v.setMaxReplication(1);
		vDsp.put(new Integer(7), v);


		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));

		/* 3. Create DspEdges to represent link between executors */
		DspEdge ij = new DspEdge(0, 1, alfa * 75, 1500);
		double refLambda = ij.getLambda();
		eDsp.put(new Pair(0, 1), ij);
		ij = new DspEdge(1, 2, alfa * 75, 1500);
		eDsp.put(new Pair(1, 2), ij);
		ij = new DspEdge(2, 3, alfa * 75, 1500);
		eDsp.put(new Pair(2, 3), ij);
		ij = new DspEdge(2, 4, alfa * 75, 1500);
		eDsp.put(new Pair(2, 4), ij);
		ij = new DspEdge(3, 5, alfa * 25, 1500);
		eDsp.put(new Pair(3, 5), ij);
		ij = new DspEdge(4, 5, alfa * 75, 1500);
		eDsp.put(new Pair(4, 5), ij);
		ij = new DspEdge(5, 6, alfa * 1500, 1500);
		eDsp.put(new Pair(5, 6), ij);
		ij = new DspEdge(6, 7, alfa * 262, 1500); 
		eDsp.put(new Pair(6, 7), ij);

		/* 4. Compute Paths */
		ArrayList<DspPath> paths = EvaluationUtils.computePaths(vDsp, eDsp, sourcesIndex);

		/* 5. Identify sinks */
		for (DspPath path : paths) {
			sinksIndex.add(path.getSink());
		}

		/* 5. Creating object DspGraph */
		String graphId = "long-topology";
		DspGraph dspGraph = new DspGraph(graphId, vDsp, eDsp);
		dspGraph.setPaths(paths);

		for (Integer so : sourcesIndex)
			dspGraph.addSource(so);

		for (Integer si : sinksIndex)
			dspGraph.addSink(si);

		DEBS15DspGraphBuilder gb = new DEBS15DspGraphBuilder(dspGraph);

		/* Stores additional information about datarate for adjusting them at run-time */
		for (DspEdge e : dspGraph.getEdges().values()) {
			gb.edgeToLambdaCoeff.put(e, e.getLambda() / refLambda);
		}

		return gb;
	}

	/**
	 * Adjusts datarate on all the DSP edges.
	 * 
	 * The adjustment is specified for edge (0,1) and proportionally
	 * applied to all the other edges.
	 * 
	 * @param lambdaDelta Adjustment delta.
	 */
	public void adjustLambdas(double lambdaDelta) {
		for (DspEdge e : dspGraph.getEdges().values()) {
			e.setLambda(e.getLambda() + lambdaDelta * edgeToLambdaCoeff.get(e));
		}
	}

	/**
	 * Prints utilization of each DSP operator.
	 */
	public void printUtilizations() {
		for (DspVertex v : dspGraph.getVertices().values()) {
			double incomingLambda = 0.0;
			for (Pair p : dspGraph.getEdges().keySet()) {
				if (p.getB() == v.getIndex())
					incomingLambda += dspGraph.getEdges().get(p).getLambda();
			}
			double u = incomingLambda / v.getServiceRate();
			System.out.println("U(" + v.getIndex() + "): " + u);
		}
	}

}
