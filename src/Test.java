import codd.DspGraphBuilder;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.OptimalSolution;
import codd.placement.*;
import codd.placement.ODDModel.MODE;
import codd.report.Report;
import codd.report.ReportException;

import java.io.IOException;


public class Test {
	

	public static void main(String[] args) {

		System.out.println("Optimal DSP Placement library");
		
		testODPBasic();
		
//		testODPBandwidth();

//		testODPBasicRelaxed();
		
//		testODPRepl();
	
	}


	@SuppressWarnings("unused")
	private static void testODPBasic(){
		/* ******* PARAMETERS ********** */
		int NRES = 5;
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 3; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.FULL_MESH;
		
		try {
			compileSolveAndReport("testODP", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
		} catch (ODDException e) {
			e.printStackTrace();
		}
	
	}
	
	
	@SuppressWarnings("unused")
	private static void testODPBandwidth(){
		/* ******* PARAMETERS ********** */
		int NRES = 75;
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 43; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.FULL_MESH;
		
		try {
			compileSolveAndReportODPBW("testODPBandwidth", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
		} catch (ODDException e) {
			e.printStackTrace();
		}
	
	}
	
	
	
	@SuppressWarnings("unused")
	private static void testODPBasicRelaxed(){
		/* ******* PARAMETERS ********** */
		int NRES = 5;
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 3; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.FULL_MESH;
		
		boolean relaxX = true;
		boolean relaxY = true;
		
		try {
			compileRelaxedSolveAndReport("testODP", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, relaxX, relaxY);
		} catch (ODDException e) {
			e.printStackTrace();
		}	
	}
	
	

	@SuppressWarnings("unused")
	private static void testODPRepl(){
		
		int NRES = 3;
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 4; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.ANSNET;
		
		WEIGHT_RESP_TIME = 0.8;
		WEIGHT_COST = .2;
		System.out.println("\n\n\nReplODP: Sequential Application (6 op.) on Ansnet (15 res) - Wr1Wc0");
		try {
			compileSolveAndReportReplODP("testReplODP", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
		} catch (ODDException e) {
			e.printStackTrace();
		}

	
	}



	
	private static void compileSolveAndReportReplODP(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType, 
			int NDSP, DspGraphBuilder.TYPE dspType, 
			double RESTRICTION_ON_VRES
			) throws ODDException {
		
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);
		
		ODDParameters params = new ODDParameters(MODE.BANDWIDTH, ODDBandwidthModel.MODE.INTERNODE_TRAFFIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);
		
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, resType, NRES);
//		rbuilder.printGraph(true);

		
		DspGraphBuilder gbuilder = new DspGraphBuilder();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(
				dspType, NDSP, 
				params);
		gbuilder.create(amp, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			gbuilder.restrictPlacement(rbuilder.getGraph(), RESTRICTION_ON_VRES);
		
//		gbuilder.restrictPlacement(rbuilder.getGraph(), 0.3);
		gbuilder.printGraph();
		
		ODDModel model = new ODDReplicatedOperatorsModel(gbuilder.getGraph(), rbuilder.getGraph(), params);

		
		
		model.compile();	
		OptimalSolution solution = model.solve();
		
		Report report = new Report(""+experiment, "output");
		try {
			report.write(idSeries, runNumber, model, gbuilder, amp, rbuilder, rmp, RESTRICTION_ON_VRES, params, solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}
	

	private static void compileSolveAndReport(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType, 
			int NDSP, DspGraphBuilder.TYPE dspType, 
			double RESTRICTION_ON_VRES
			) throws ODDException {
		
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);
		
		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(0);
		params.setWeightNetMetric(0);
		setParameters(params);
		params.setServiceRate(NOT_USED_INVERSE_REF_SERVICE_TIME);
		
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, resType, NRES);
//		rbuilder.printGraph(false);

		
		DspGraphBuilder gbuilder = new DspGraphBuilder();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		gbuilder.create(amp, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			gbuilder.restrictPlacement(rbuilder.getGraph(), RESTRICTION_ON_VRES);
//		gbuilder.printGraph();
		
		ODDModel model = new ODDBasicModel(gbuilder.getGraph(), rbuilder.getGraph(), params);

		/* XXX: ODP with network-related QoS metrics (data rate, bandwidth) */
//		ODDParameters params = new ODDParameters(MODE.BANDWIDTH, ODDBandwidthModel.MODE.INTERNODE_TRAFFIC);
//		params.setwA(0);
//		params.setwR(0);
//		params.setwZ(1.0);
//		ODDModel model = new ODDBandwidthModel(gbuilder.getGraph(), rbuilder.getGraph(), params);

// 		params.setGap(0.1);
//		model.compile();	
		OptimalSolution solution = model.solve();
		
		Report report = new Report(""+experiment, "output");
		try {
			report.write(idSeries, runNumber, model, gbuilder, amp, rbuilder, rmp, RESTRICTION_ON_VRES, params, solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}


	private static void compileSolveAndReportODPBW(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType, 
			int NDSP, DspGraphBuilder.TYPE dspType, 
			double RESTRICTION_ON_VRES
			) throws ODDException {
		
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);

		ODDParameters params = new ODDParameters(MODE.BANDWIDTH, ODDBandwidthModel.MODE.INTERNODE_TRAFFIC);
		params.setWeightAvailability(0);
		params.setWeightRespTime(0);
		params.setWeightNetMetric(1.0);
		params.setWeightCost(0);
		setParameters(params);
		params.setServiceRate(NOT_USED_INVERSE_REF_SERVICE_TIME);
		
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, resType, NRES);
//		rbuilder.printGraph(false);

		
		DspGraphBuilder gbuilder = new DspGraphBuilder();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(
				dspType, NDSP, params);
		gbuilder.create(amp, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			gbuilder.restrictPlacement(rbuilder.getGraph(), RESTRICTION_ON_VRES);
//		gbuilder.printGraph();
		
		
		ODDModel model = new ODDBandwidthModel(gbuilder.getGraph(), rbuilder.getGraph(), params);

// 		params.setGap(0.1);
//		model.compile();	
		OptimalSolution solution = model.solve();
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}

	
	private static void compileRelaxedSolveAndReport(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType,
			int NDSP, DspGraphBuilder.TYPE dspType, 
			double RESTRICTION_ON_VRES,
			boolean relaxX, boolean relaxY
			) throws ODDException{
		
		System.out.println("Running " + idSeries + ": experiment: " + experiment + ", run: " + runNumber);
		
		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(0);
		params.setWeightNetMetric(0);
		setParameters(params);
		params.setServiceRate(NOT_USED_INVERSE_REF_SERVICE_TIME);
		
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, resType, NRES);
//		rbuilder.printGraph(false);

		
		DspGraphBuilder gbuilder = new DspGraphBuilder();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		gbuilder.create(amp, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			gbuilder.restrictPlacement(rbuilder.getGraph(), RESTRICTION_ON_VRES);
//		gbuilder.printGraph();
		
		
		ODDModel model = new ODDBasicRelaxedModel(gbuilder.getGraph(), rbuilder.getGraph(), params, relaxX, relaxY);

		model.compile();
		
		// XXX: pin operators 
		model.pin(0, 0);
		model.pin(NDSP - 1, 1);	

		OptimalSolution solution = model.solve();
		
		
		Report report = new Report(""+experiment, "output");
		try {
			report.write(idSeries, runNumber, model, gbuilder, amp, rbuilder, rmp, RESTRICTION_ON_VRES, params, solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}
	
	private static void setParameters(ODDParameters params){

		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		

		params.setAvgBytePerTupleMin(1500.0);
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);

		params.setLambdaMin(0.014 / 5.0); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0); // 0.05; // 0.00005; // 0.15; // 0.0005;
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(2.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		params.setNodeAvailabilityMin(.85);
		params.setNodeAvailabilityMean(.95);
		params.setNodeAvailabilityStdDev(0.03);
		
		params.setServiceRate(0.02); // 100.0; // 15.0; // 100.0
		
	}


	/* ******* PARAMETERS ********** */
	/* ODD Weights */
	private static double WEIGHT_AVAILABILITY = 0;
	private static double WEIGHT_RESP_TIME = 1;
	private static double WEIGHT_COST = 0;
	private static double WEIGHT_NET_METRIC = 0;
	
	private static final double NOT_USED_INVERSE_REF_SERVICE_TIME = 1.0;
	
	/* ******* .PARAMETERS ********** */
	
}
