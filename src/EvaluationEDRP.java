import java.io.IOException;
import java.io.PrintWriter;

import codd.DspGraphBuilder;
import codd.EDRPParameters;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.ResVertexMultisetGenerator;
import codd.metric.ReconfigurationMetricsManager;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.EDRPSimpleApplicationMetricsProvider;
import codd.metricsprovider.EDRPSimpleResourceMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.model.DspVertex;
import codd.model.OptimalEDRPSolution;
import codd.model.OptimalMultisetSolution;
import codd.model.OptimalSolution;
import codd.model.Pair;
import codd.model.ResourceVertex;
import codd.placement.EDRPModel;
import codd.placement.EDRPStormModel;
import codd.placement.ODDBandwidthModel;
import codd.placement.ODDModel;
import codd.placement.ODDReplicatedOperatorsModel;
import codd.report.EDRPReport;
import codd.report.ODRPReport;
import codd.report.ReportException;

/**
 * Experiments for evaluating EDRP optimization model.
 */
public class EvaluationEDRP {

	private static final String INITIAL_PLACEMENT_REPORTS_FILENAME = "output";
	private static final String EDRP_REPORTS_FILENAME = "edrp_output";

	public static void main(String args[]) throws ODDException, ReportException, IOException {

		Integer iterations = null;
		Integer growRepetitions = null;
		Integer nRes = null;
		Integer rMax = null;
		Integer cMax = null;
		Double RTTcoeff = null;
		Double wtd = null;
		Double wc = null;
		Double wr = null;
		Integer tdMax = null;
		Integer maxReplication = null;
		Boolean pruning = null;
		Double mdpBeta = 0.0;

		System.out.println("args.length = " + args.length);
		
		if (args.length==1 && args[0].equalsIgnoreCase("usage")) {
			System.out.println("iter nres rmax cmax growRepetitions RTTcoeff wtd wr wc tdMax maxReplication pruning [mdp beta or 0.0]");
			System.exit(0);
		}

		if (args.length > 0)
			iterations = Integer.parseInt(args[0]);
		if (args.length > 1)
			nRes = Integer.parseInt(args[1]);
		if (args.length > 2)
			rMax = Integer.parseInt(args[2]);
		if (args.length > 3)
			cMax = Integer.parseInt(args[3]);
		if (args.length > 4)
			growRepetitions = Integer.parseInt(args[4]);
		if (args.length > 5)
			RTTcoeff = Double.parseDouble(args[5]);
		if (args.length > 6)
			wtd = Double.parseDouble(args[6]);
		if (args.length > 7)
			wr = Double.parseDouble(args[7]);
		if (args.length > 8)
			wc = Double.parseDouble(args[8]);
		if (args.length > 9)
			tdMax = Integer.parseInt(args[9]);
		if (args.length > 10)
			maxReplication = Integer.parseInt(args[10]);
		if (args.length > 11)
			pruning = Boolean.parseBoolean(args[11]);
		if (args.length > 12)
			mdpBeta = Double.parseDouble(args[12]);

		System.out.println("iterations = " + iterations);
		System.out.println("nRes = " + nRes);
		System.out.println("rMax = " + rMax);
		System.out.println("cMax = " + cMax);
		System.out.println("growRepet = " + growRepetitions);
		System.out.println("DS_RTTcoeff = " + RTTcoeff);
		System.out.println("wD = " + wtd);
		System.out.println("wR = " + wr);
		System.out.println("wC = " + wc);
		System.out.println("tdMax = " + tdMax);
		System.out.println("maxReplication = " + maxReplication);
		System.out.println("pruning = " + pruning);
		System.out.println("mdpBeta = " + mdpBeta);
	

		experiment2(mdpBeta, iterations, nRes, rMax, cMax, tdMax, RTTcoeff, wtd, wc, wr, growRepetitions, maxReplication, pruning);
	}

	/**
	 * Just a test. ODRP and EDRP are solved using same DSP and resources
	 * configurations. EDRP minimizes downtime (-> we expect the same deployment
	 * to be used)
	 */
	public static void experiment0() throws ODDException, ReportException, IOException {
		/* Prepares parameters for ODRP. */
		ResVertexMultisetGenerator.MULTISET_CARDINALITY = 1;
		final int NRES = 6;
		final int NDSP = 3;
		EDRPParameters params = new EDRPParameters(ODDBandwidthModel.MODE.INTERNODE_TRAFFIC);
		initParameters(params);
		params.setOdrpMM1(true);
		params.setWeights(0, 0.5, 0.5, 0);
		ExperimentRun run = new ExperimentRun("resp_cost", 0, 0, NRES, ResGraphBuilder.TYPE.ANSNET, NDSP,
				DspGraphBuilder.TYPE.SEQUENTIAL, 0, NRES - 1, params);

		/* Builds resources graph. */
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new EDRPSimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, run.resType, run.nRes);
		rbuilder.printGraph(false);

		/* Builds DSP graph. */
		DspGraphBuilder gbuilder = new DspGraphBuilder();
		ApplicationMetricsProvider amp = new EDRPSimpleApplicationMetricsProvider(run.dspType, run.nDsp, params);
		gbuilder.create(amp, run.dspType, run.nDsp);
		gbuilder.printGraph();

		/* Solves ODRP for initial placement. */
		OptimalMultisetSolution currentSol = solveODRP(run, rbuilder, gbuilder);
		if (!currentSol.getStatus().equals(OptimalSolution.Status.OPTIMAL)) {
			System.err.println("Found sol: " + currentSol.getStatus());
			return;
		}

		reportODRPSolution(currentSol, run, rbuilder, gbuilder, amp, rmp);
		System.gc();

		/* Given the initial placement, prepares parameters for EDRP. */
		params.updateFromSolution(currentSol);
		// params.setWeights(0, 0.0, 0, 0, 1.0);

		/* Solves EDRP and reports solution. */
		OptimalMultisetSolution edrpSol = solveEDRP(run, rbuilder, gbuilder, false, false, 2, false);
		reportEDRPSolution(edrpSol, run, rbuilder, gbuilder, amp, rmp);
	}

	/**
	 * Starting from an initial placement computed by ODRP, we progressively
	 * increment arrival rates at operators, expecting scaling actions to be
	 * performed. Then, the arrival rates are decremented back to the initial
	 * value.
	 */
	public static void experiment2(double mdpBeta, Integer iterations, Integer nres, Integer rmax, Integer cMax, Integer tdMax,
			Double dsDelayCoeff, Double wd, Double wc, Double wr, Integer growRepetitions, Integer maxReplication, Boolean pruning) throws ODDException, ReportException, IOException {
		
		/* Max replication of each operator. */
		final int MAX_MULTISET_CARDINALITY = (maxReplication == null)? 4 : maxReplication;
		ResVertexMultisetGenerator.MULTISET_CARDINALITY = 2; // for ODRP (initial placement)
		/* Steps for load growing */
		final int ITERATIONS = (iterations != null ? iterations : 10);
		/* Number of computing nodes */
		final int NRES = (nres != null ? nres : 5);
		/* Multiplier for increasing load */
		final double LAMBDA_COEFF = 3.0;
		/* Count of increase/decrease cycle repetitions (default 1) */
		final int REPEAT_GROW_COUNT = growRepetitions == null ? 1 : growRepetitions;
		/* Bandwidth to/from DS */
		final double dataStoreRate = 12.5 * 1000.0;/* 12500 KiB/s = 12.5 MiB/s = 100 Mbps is default */
		/* Reconfiguration sync time */
		final double RECONFIGURATION_SYNC_TIME = 250.0;
		/* Prune multisets generation */
		final boolean PRUNE_MULTISETS_GENERATION = (pruning == null)? true : pruning;
		
		/* wX for EDRP */
		final double EDRP_WD = (wd == null) ? 0.4 : wd;
		final double EDRP_WC = (wc == null) ? 0.3 : wc;
		final double EDRP_WR = (wr == null) ? 0.3 : wr;

		/* ------------------------------------------------ */
		/* For constrained MDP */
		double mdpLambda = 0.0;
		final boolean constrainedMDP = (mdpBeta > 0.0);
		/* ------------------------------------------------ */

		/* Total downtime during experiment */
		double totalDowntime = 0.0;

		EDRPParameters params = new EDRPParameters(ODDBandwidthModel.MODE.INTERNODE_TRAFFIC);
		initParameters(params);
		params.setOdrpMM1(true);
		params.setZmax(5000000);
		params.setCmax(cMax != null ? cMax : 30);
		params.setCmin(1);
		params.setRmax(rmax != null ? rmax : 210);
		params.setRmin(5);
		((EDRPParameters) params).setTDmax(tdMax == null ? 75000 : tdMax);

		params.setWeights(0, 0.5, 0.5, 0);
		ExperimentRun run = new ExperimentRun("experiment2", 0, 0, NRES, ResGraphBuilder.TYPE.ANSNET, 8,
				DspGraphBuilder.TYPE.SEQUENTIAL, 0, NRES - 1, params);

		/* Builds resources graph. */
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new EDRPSimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, run.resType, run.nRes);
		rbuilder.printGraph(false);

		/* Adjusts download/upload rate from/to DS. */
		for (ResourceVertex u : rbuilder.getGraph().getVertices().values()) {
			u.setDownloadRateFromDS(dataStoreRate);
			u.setUploadRateToDS(u.getDownloadRateFromDS());
			u.setDownloadRateFromLocalDS(1 * 1000 * 1000.0); /* 1 GiB/s */
			u.setUploadRateToLocalDS(u.getDownloadRateFromLocalDS());

			u.setDataStoreRTT(u.getDataStoreRTT() * (dsDelayCoeff == null ? 1.0 : dsDelayCoeff));
		}

		/* Builds DSP graph. */
		DEBS15DspGraphBuilder gbuilder = DEBS15DspGraphBuilder.newInstance(0.001, 0.001);
		gbuilder.printGraph();

		ApplicationMetricsProvider amp = new EDRPSimpleApplicationMetricsProvider(run.dspType, run.nDsp, params);
		gbuilder.getGraph().setReconfigurationSyncTime(RECONFIGURATION_SYNC_TIME);

		/* Solves ODRP for initial placement. */
		OptimalMultisetSolution currentSol = solveODRP(run, rbuilder, gbuilder);
		reportODRPSolution(currentSol, run, rbuilder, gbuilder, amp, rmp);
		System.gc();

		/* Writes parameters to file. */
		//dumpParameters(params, "experiment2");

		final double MIN_LAMBDA = gbuilder.getGraph().getEdges().get(new Pair(0, 1)).getLambda();
		final double MAX_LAMBDA = MIN_LAMBDA * LAMBDA_COEFF;

		for (int r = 1; r <= REPEAT_GROW_COUNT; r++) {
			/* even rounds get half load increment */
			double _maxLambda = (r%2==1) ? MAX_LAMBDA : MIN_LAMBDA + (MAX_LAMBDA-MIN_LAMBDA)/2.0;
			final double DELTA = (_maxLambda - MIN_LAMBDA) / (double) ITERATIONS;

			for (int i = 0; i <= ITERATIONS; i++) {
				params.setWeights(0, EDRP_WR, EDRP_WC, 0, EDRP_WD + mdpLambda);
				run.runNumber += 1;

				if (!currentSol.getStatus().equals(OptimalSolution.Status.OPTIMAL)) {
					System.err.println("Found sol: " + currentSol.getStatus());
					return;
				}

				/* Given the initial placement, prepares parameters for EDRP. */
				params.updateFromSolution(currentSol);

				/* Solves EDRP and reports solution. */
				OptimalEDRPSolution edrpSol = solveEDRP(run, rbuilder, gbuilder, false, constrainedMDP, MAX_MULTISET_CARDINALITY, PRUNE_MULTISETS_GENERATION);
				reportEDRPSolution(edrpSol, run, rbuilder, gbuilder, amp, rmp);
				totalDowntime += edrpSol.getOptTDown();
				printReconfigurationAction(edrpSol, currentSol, run.runNumber);

				/* ------------------------------------------------ */
				/* Adjust coefficient for constrained MDP */
				mdpLambda += mdpBeta * (edrpSol.getOptTDown() - params.getTDmax());
				mdpLambda = Math.max(0.0, mdpLambda); /* >= 0 */
				System.out.println("MDP lambda: " + mdpLambda);
				/* ------------------------------------------------ */

				currentSol = edrpSol;

				gbuilder.adjustLambdas(DELTA);
				System.gc();
			}

			gbuilder.adjustLambdas(-DELTA); /* avoid one more increment */

			for (int i = 0; i <= ITERATIONS; i++) {
				params.setWeights(0, EDRP_WR, EDRP_WC, 0, EDRP_WD + mdpLambda);
				run.runNumber += 1;

				if (!currentSol.getStatus().equals(OptimalSolution.Status.OPTIMAL)) {
					System.err.println("Found sol: " + currentSol.getStatus());
					return;
				}

				/* Given the initial placement, prepares parameters for EDRP. */
				params.updateFromSolution(currentSol);

				/* Solves EDRP and reports solution. */
				OptimalEDRPSolution edrpSol = solveEDRP(run, rbuilder, gbuilder, false, constrainedMDP, MAX_MULTISET_CARDINALITY, PRUNE_MULTISETS_GENERATION);
				reportEDRPSolution(edrpSol, run, rbuilder, gbuilder, amp, rmp);
				totalDowntime += edrpSol.getOptTDown();
				printReconfigurationAction(edrpSol, currentSol, run.runNumber);

				/* ------------------------------------------------ */
				/* Adjust coefficient for constrained MDP */
				mdpLambda += mdpBeta * (edrpSol.getOptTDown() - params.getTDmax());
				mdpLambda = Math.max(0.0, mdpLambda); /* >= 0 */
				System.out.println("MDP lambda: " + mdpLambda);
				/* ------------------------------------------------ */

				currentSol = edrpSol;

				gbuilder.adjustLambdas(-DELTA);
				System.gc();
			}

			gbuilder.adjustLambdas(DELTA); /* avoid one more decrement */


		}

		System.out.println("MinLambda = " + MIN_LAMBDA);
		System.out.println("MaxLambda = " + MAX_LAMBDA);
		System.out.println("Total downtime: " + totalDowntime);
	}
	
	private static void printReconfigurationAction(OptimalMultisetSolution solution, OptimalMultisetSolution previousSolution, int time)
	{
		if (solution.equals(previousSolution))
			return;
		
		boolean onlyMig = true;
		
		for (Integer iDsp : solution.getMultisetPlacements().keySet()) {
			int oldReplication = previousSolution.getMultisetPlacement(iDsp).getCardinality();
			int newReplication = solution.getMultisetPlacement(iDsp).getCardinality();
			
			if (oldReplication != newReplication) {
				onlyMig = false;
				break;
			}
		}
		
		if (onlyMig) {
			System.out.println("Migration: " + time);
		} else {
			System.out.println("Scaling: " + time);
		}
	}

	private static void reportODRPSolution(OptimalSolution solution, ExperimentRun er, ResGraphBuilder rbuilder,
			DspGraphBuilder gbuilder, ApplicationMetricsProvider amp, ResourceMetricsProvider rmp)
			throws ReportException, IOException {
		ODRPReport report = new ODRPReport("" + er.experiment, INITIAL_PLACEMENT_REPORTS_FILENAME);
		report.write(er.idSeries, er.runNumber, ODDReplicatedOperatorsModel.MODEL_TYPE, gbuilder, amp, rbuilder, rmp,
				-1.0, er.srcRes, er.snkRes, er.params, solution);
	}

	private static void reportEDRPSolution(OptimalSolution solution, ExperimentRun er, ResGraphBuilder rbuilder,
			DspGraphBuilder gbuilder, ApplicationMetricsProvider amp, ResourceMetricsProvider rmp)
			throws ReportException, IOException {
		EDRPReport report = new EDRPReport("" + er.experiment, EDRP_REPORTS_FILENAME);
		report.write(er.idSeries, er.runNumber, ODDReplicatedOperatorsModel.MODEL_TYPE, gbuilder, amp, rbuilder, rmp,
				-1.0, er.srcRes, er.snkRes, er.params, solution);
	}

	protected static OptimalMultisetSolution solveODRP(ExperimentRun er, ResGraphBuilder rbuilder,
			DspGraphBuilder gbuilder) throws ODDException {

		System.out.println("Running " + er.idSeries + ": experiment: " + er.experiment + ", run: " + er.runNumber);

		ODDModel model = new ODDReplicatedOperatorsModel(gbuilder.getGraph(), rbuilder.getGraph(), er.params);
		model.compile();

		model.pin(0, er.srcRes);
		model.pin(er.nDsp - 1, er.snkRes);

		OptimalMultisetSolution solution = (OptimalMultisetSolution) model.solve();

		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution "
				+ solution.getResolutionTime() + " ms.");
		System.out.println("\n");

		return solution;
	}

	protected static OptimalEDRPSolution solveEDRP(ExperimentRun er, ResGraphBuilder rbuilder,
			DspGraphBuilder gbuilder, boolean adjustForStorm, boolean skipDowntimeBound, int maxReplication, boolean pruneMultisets) throws ODDException {

		System.out.println("Running EDRP " + er.idSeries + ": experiment: " + er.experiment + ", run: " + er.runNumber);

		ODDModel model;

		if (adjustForStorm)
			model = new EDRPStormModel(gbuilder.getGraph(), rbuilder.getGraph(), (EDRPParameters) er.params, maxReplication, pruneMultisets);
		else
			model = new EDRPModel(gbuilder.getGraph(), rbuilder.getGraph(), (EDRPParameters) er.params, maxReplication, pruneMultisets,
					skipDowntimeBound);

		model.compile();

		model.pin(0, er.srcRes);
		model.pin(er.nDsp - 1, er.snkRes);

		OptimalEDRPSolution solution = (OptimalEDRPSolution)model.solve();

		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution "
				+ solution.getResolutionTime() + " ms.");
		System.out.println("\n");

		return solution;
	}

	private static void initParameters(ODDParameters params) {

		final double lambdaMin = 1.0 / 110.0; // 99.978; // 10.0; //
		final double serviceRate = 1.0 / 50.0; // 100.0; // 15.0; // 100.0;

		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0);
		params.setRespTimeStdDev(0.0);

		params.setAvgBytePerTupleMin(1500.0); // this is byte/ms (i.e., KB/s)
		params.setAvgBytePerTupleMean(0.0);
		params.setAvgBytePerTupleStdDev(0.0);

		params.setLambdaMin(lambdaMin); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0.05); // 0.05; // 0.00005; // 0.15; //
												// 0.0005;

		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.5);

		/* Resource Metrics Provider */

		/* XXX: meaningless if you use ANSNET ... */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(5.0);
		params.setLinkDelayStdDev(2.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.2);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);

		params.setNodeAvailResourcesMin(2.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		params.setNodeAvailabilityMin(.97); // uniform distribution
		params.setNodeAvailabilityMean(.9999999); // used as max
		params.setNodeAvailabilityStdDev(0.03);

		params.setServiceRate(serviceRate); // 100.0; // 15.0; // 100.0;

		params.setZmax(100000);
		params.setCmax(18);
		params.setCmin(1);
		params.setRmax(810);
		params.setRmin(100);
		((EDRPParameters) params).setTDmax(10000);

	}

	protected static void dumpParameters(EDRPParameters params, String tag) {
		final String PARAMS_FILENAME = String.format("parameters-%s.txt", tag);
		try {
			PrintWriter writer = new PrintWriter(PARAMS_FILENAME, "UTF-8");
			writer.print(params);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
