import codd.*;
import codd.model.*;
import codd.placement.*;
import codd.placement.ODDModel.MODE;
import codd.security.RequirementsForest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class EvaluationSDP {
	

	public static void main(String[] args) throws ODDException {

		experiment1();
	}

	/**
	 * Basic Model. 
	 * Resolution time vs DSP application graph size (Sequential, Fat)
	 */
	private static void experiment1() throws ODDException {

		List<String> reports = new ArrayList<>();

		/* Scenario A */
		double costs[] = {5.0, 6.0, 7.0, 8.0, 9.0, 10.0};
		for (double c : costs) {
			reports.add(compileSolveAndReport("sdpA", 0, 0, c, SDPModel.Objective.SECURITY, 0.0));
		}

		/* Scenario B */
		double secMin[] = {0.0, 0.9, 0.99};
		for (double c : costs) {
			for (double s : secMin) {
				reports.add(compileSolveAndReport("sdpB", 0, 0, c, SDPModel.Objective.RESPONSE_TIME, s));
			}
		}


		for (String str : reports)
			System.out.println(str);
	}



	

	
	private static String compileSolveAndReport(String idSeries, int experiment, int runNumber, double maxCost, SDPModel.Objective objective,
											  double minSecurity) throws ODDException {
		
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);

		ODDParameters params = new ODDParameters(MODE.BANDWIDTH, ODDBandwidthModel.MODE.INTERNODE_TRAFFIC);
		params.setWeightAvailability(0.0);
		params.setWeightRespTime(0.5);
		params.setWeightCost(0.5);
		params.setWeightNetMetric(0.0);
		setParameters(params);
		
		SDPResGraphBuilder rbuilder = SDPResGraphBuilder.create();
		rbuilder.printGraph(false, false);

		SDPDspGraphBuilder gbuilder = SDPDspGraphBuilder.create();
		gbuilder.printGraph();

		Set<Integer> candidatesForSource = new HashSet<>();
		candidatesForSource.add(0);
		gbuilder.getGraph().getVertices().get(0).setCandidates(candidatesForSource);

		/* Security requirements */
		RequirementsForest rf = gbuilder.createRequirements();
		//rf.print();

		params.setRmax(100);
		params.setCriticalRmax(10);
		params.setCmax(maxCost);
		params.setSecurityMinDelta(minSecurity);

		//params.setSecurityMinDelta(1.0);

		SDPModel model = new SDPModel(gbuilder.getGraph(), rbuilder.getGraph(), params, rf, objective);
		OptimalSDPSolution solution = (OptimalSDPSolution)model.solve();

		StringBuilder sb = new StringBuilder();
		sb.append(objective.name());
		sb.append(" - ");
		sb.append(maxCost);
		sb.append(" - ");
		sb.append(minSecurity);
		sb.append("\n");
		sb.append(solution.toString());

		return sb.toString();

		//Report report = new Report(""+experiment, "output");
		//try {
		//	report.write(idSeries, runNumber, model, gbuilder, null, rbuilder, null, -1, params, solution);
		//} catch (ReportException | IOException e) {
		//	e.printStackTrace();
		//}
		
		//System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		//System.out.println("\n\n");
	}

	

	
	private static void setParameters(ODDParameters params){

		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		
		params.setAvgBytePerTupleMin(1500.0);
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);

		params.setLambdaMin(0.014 / 5.0); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0); // 0.05; // 0.00005; // 0.15; // 0.0005;
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(nodeAvailResourcesMin);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		params.setNodeAvailabilityMin(.85);
		params.setNodeAvailabilityMean(.95);
		params.setNodeAvailabilityStdDev(0.03);
		
		params.setServiceRate(1.0); // NOT_USED
		
	}

	/* ******* PARAMETERS ********** */
	/* ODD Weights */
	private static double nodeAvailResourcesMin	= 2.0;
	/* ******* .PARAMETERS ********** */
	
}
