package codd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codd.model.ResourceGraph;
import codd.model.ResourceVertexMultiset;
import codd.utils.Multiset;
import codd.utils.MultisetGenerator;
import codd.utils.ResVertexMultisetFilter;

public class ResVertexMultisetGenerator {
	
	public static int MULTISET_CARDINALITY = 3;
	private static final boolean GENERATE_UP_TO_CARDINALITY = true;
	
	
	protected ResourceGraph resGraph;
	private ResVertexMultisetFilter filter = null;
	
	
	public ResVertexMultisetGenerator(ResourceGraph resGraph) {
		
		this.resGraph = resGraph;
	
	}

	public ResVertexMultisetGenerator(ResourceGraph resGraph, int multisetMaxCardinality) {
		
		this(resGraph);
		MULTISET_CARDINALITY = multisetMaxCardinality;
	}

	public ResVertexMultisetGenerator(ResourceGraph resGraph, int multisetMaxCardinality, ResVertexMultisetFilter filter) {

		this(resGraph);
		MULTISET_CARDINALITY = multisetMaxCardinality;
		this.filter = filter;
	}

	public Map<Integer, ResourceVertexMultiset> generate(){
		
		int numRes = resGraph.getVertices().values().size();
		Map<Integer, ResourceVertexMultiset> rvms = null;
		
		if (GENERATE_UP_TO_CARDINALITY){
			
			/* Compute all required multisets */
			List<Multiset> allMs = new ArrayList<Multiset>();
			for (int cardinality = 1; cardinality < MULTISET_CARDINALITY + 1; cardinality++){
				List<Multiset> ms = MultisetGenerator.generate(numRes, cardinality, filter);
				allMs.addAll(ms);
				
			}

			rvms = new HashMap<Integer, ResourceVertexMultiset>(allMs.size());

			int index = 0;
			for (Multiset m : allMs){
				ResourceVertexMultiset rvm = new ResourceVertexMultiset(index, m, this.resGraph);
				rvms.put(new Integer(index), rvm);
				index++;
			}

	
		} else {
			List<Multiset> ms = MultisetGenerator.generate(numRes, MULTISET_CARDINALITY);
			rvms = new HashMap<Integer, ResourceVertexMultiset>(ms.size());
	
			int index = 0;
			for (Multiset m : ms){
				ResourceVertexMultiset rvm = new ResourceVertexMultiset(index, m, this.resGraph);
				rvms.put(new Integer(index), rvm);
				index++;
			}
		}		
		
//		System.out.println(" == Multi-sets ========================  ");
//		for (ResourceVertexMultiset rvm : rvms.values()){
//			System.out.println(" " + rvm);
//		}
//		System.out.println(" ======================================  ");
		
		return rvms;
	}
}
