package codd.heuristics;

import codd.DspGraphBuilder;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.DspGraph;
import codd.model.OptimalSolution;
import codd.model.OptimalSolution.Status;
import codd.model.ResourceGraph;
import codd.placement.ODDBandwidthModel;
import codd.placement.ODDModel;
import codd.surrogate.SurrogateHierarchicalResourceGraphBuilder;

import java.util.HashSet;
import java.util.Set;

/**
 * If you use this heuristic, please cite:
 * M. Nardelli, V. Cardellini, V. Grassi, F. Lo Presti,
 * "Efficient Operator Placement for Distributed Data Stream Processing Applications",
 * IEEE Transactions on Parallel and Distributed Systems,
 * vol. 30, no. 8, pp. 1753–1767, 2019. doi: 10.1109/TPDS.2019.2896115.
 */
public class HierarchicalODP implements ODDModel {

	private ResGraphBuilder resGraphBuilder;
	private ResourceMetricsProvider resourceMetricProvider;
	private DspGraphBuilder dspGraphBuilder;
	private ApplicationMetricsProvider applicationMetricProvider;

	private DspGraph dspGraph;
	private ResourceGraph resGraph;

	private ODDModel model;
	private ODDParameters params;
	private OptimalSolution solution;

	private static final boolean DEBUG = false;
	private int groupingFactor; 				// default: 2;
	private int srcRes;
	private int snkRes;

	public HierarchicalODP(int NRES, ResGraphBuilder.TYPE resType, int NDSP,
						   DspGraphBuilder.TYPE dspType, double RESTRICTION_ON_VRES,
						   ODDParameters params,
						   int srcRes, int snkRes,
						   int groupingFactor) throws ODDException {

		this.params = params;
		createResGraph(NRES, resType);
		createDspGraph(NDSP, dspType, RESTRICTION_ON_VRES);

		this.solution = null;
		
		this.srcRes = srcRes;
		this.snkRes = snkRes;
		this.groupingFactor = groupingFactor;
	}

	public HierarchicalODP(DspGraph dspGraph,
						   ResourceGraph resGraph,
						   ODDParameters params,
						   int srcRes, int snkRes,
						   int groupingFactor) {

		this.params = params;
		this.dspGraph = dspGraph;
		this.resGraph = resGraph;

		this.resGraphBuilder = null;
		this.resourceMetricProvider = null;
		this.dspGraphBuilder = null;
		this.applicationMetricProvider = null;

		this.solution = null;

		this.srcRes = srcRes;
		this.snkRes = snkRes;
		this.groupingFactor = groupingFactor;

	}

	private void createResGraph(int NRES, ResGraphBuilder.TYPE resType) {
		resGraphBuilder = new ResGraphBuilder();
		resourceMetricProvider = new SimpleResourceMetricsProvider(params);
		resGraphBuilder.create(resourceMetricProvider, resType, NRES);
//		resGraphBuilder.printGraph(true);
		resGraph = resGraphBuilder.getGraph();
	}

	private void createDspGraph(int NDSP, DspGraphBuilder.TYPE dspType,
			double RESTRICTION_ON_VRES) throws ODDException {

		dspGraphBuilder = new DspGraphBuilder();
		applicationMetricProvider = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		dspGraphBuilder.create(applicationMetricProvider, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			dspGraphBuilder.restrictPlacement(resGraph, RESTRICTION_ON_VRES);
//		dspGraphBuilder.printGraph();
		dspGraph = dspGraphBuilder.getGraph();
	}

	@Override
	public void compile() {
		// not supported
	}

	public OptimalSolution solve() throws ODDException {
		
		if (solution != null)
			return solution;

//		System.out.println("Creating surrogate resource graph...");
		SurrogateHierarchicalResourceGraphBuilder sResBuilder = new SurrogateHierarchicalResourceGraphBuilder(resGraph, params);
		ResourceGraph surrogateGraph = sResBuilder.create(resGraph, groupingFactor);

//		System.out.println("[x] surrogate resource graph created!");

		/* Iterate until we are considering all resources or a solution is find */
		boolean repeat = true;
		
		while (repeat){
//			System.out.println("Surrogate graph, hierarchy level: " + surrogateGraph.getHierachyLevel());

			if (surrogateGraph.getHierachyLevel() == 0)
				repeat = false;
			
			/* 2. Compute ODD on the smallest cluster only */
//			model = new ODDBasicModel(dspGraph, surrogateGraph, params);
			model = new ODDBandwidthModel(dspGraph, surrogateGraph, params);

			if (DEBUG){
				System.out.println();
				System.out.println("Executing ODD on " + surrogateGraph.getVertices().values().size() + " resource nodes");
				System.out.println("--------------------------------------------------------------------------------");
				
			}

			model.compile();

			/* 3. Define the pinned operators */
			int srcPinRes = sResBuilder.locateResNode(surrogateGraph, srcRes);
			int snkPinRes = sResBuilder.locateResNode(surrogateGraph, snkRes);
						
			for (Integer src : dspGraph.getSources()){
				if (DEBUG)
					System.out.println("Pinning source " + src + " on node " + srcPinRes);
				model.pin(src, srcPinRes);
			}
			for (Integer snk : dspGraph.getSinks()){
				if (DEBUG)
					System.out.println("Pinning sink " + snk + " on node " + snkPinRes);
				model.pin(snk, snkPinRes);
			}	

			
			/* 4. Solve the ODD model */
			solution = model.solve();
			
			/* 5. Check solution and if needed expand the cluster of resources */
			if (Status.INVALID.equals(solution.getStatus())){
				throw new ODDException("Invalid solution status");
			}

			Set<Integer> usedRes = new HashSet<>();
			for(Integer iDsp : dspGraph.getVertices().keySet()){
				usedRes.add(solution.getPlacement(iDsp));
			}

			if (DEBUG){
	 			System.out.println(solution.getStatus());
	 			System.out.println(" xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ");
				System.out.println(solution.toString());
				System.out.println(" xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ");
			}
			surrogateGraph = sResBuilder.unfold(surrogateGraph, usedRes);

		}
		
		return solution;
	
	}

	@Override
	public void clean() {
		model.clean();
		solution = null;
	}

	@Override
	public String type() {
		return "HierarchicalODP";
	}

	@Override
	public void pin(int dspIndex, int resIndex) {
		// TODO: not supported
	}


	/* ******************************************************************* */
	/* Getters 															   */
	/* ******************************************************************* */
	public ResGraphBuilder getResGraphBuilder() {
		return resGraphBuilder;
	}

	public ResourceMetricsProvider getResourceMetricProvider() {
		return resourceMetricProvider;
	}

	public DspGraphBuilder getDspGraphBuilder() {
		return dspGraphBuilder;
	}

	public ApplicationMetricsProvider getApplicationMetricProvider() {
		return applicationMetricProvider;
	}

	public ODDModel getModel() {
		return model;
	}

}
