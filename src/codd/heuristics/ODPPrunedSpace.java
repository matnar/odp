package codd.heuristics;

import codd.DspGraphBuilder;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.distance.ObjectiveFunctionDistanceStrategy;
import codd.distance.PairwiseDistanceStrategy;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.*;
import codd.model.OptimalSolution.Status;
import codd.placement.ODDBandwidthModel;
import codd.placement.ODDModel;
import codd.utils.ClusterUtils;
import codd.utils.hierarchicalclustering.AverageLinkageStrategy;
import codd.utils.hierarchicalclustering.HierarchicalCluster;
import codd.utils.hierarchicalclustering.HierarchicalClusteringAlgorithm;

import java.util.ArrayList;
import java.util.List;


/**
 * This heuristic has been referenced to as "ODP-PS" (ODP on a Pruned Space)
 * in the paper Nardelli et al., Efficient Operator Placement for Distributed
 * Data Stream Processing Applications, IEEE Trans Parallel Distrib Syst, 2019.
 * doi: 10.1109/TPDS.2019.2896115.
 *
 * If you use this heuristic, please cite:
 * M. Nardelli, V. Cardellini, V. Grassi, F. Lo Presti,
 * "Efficient Operator Placement for Distributed Data Stream Processing Applications",
 * IEEE Transactions on Parallel and Distributed Systems,
 * vol. 30, no. 8, pp. 1753–1767, 2019. doi: 10.1109/TPDS.2019.2896115.
 */
public class ODPPrunedSpace implements ODDModel {

	private ResGraphBuilder resGraphBuilder;
	private ResourceMetricsProvider resourceMetricProvider;
	private DspGraphBuilder dspGraphBuilder;
	private ApplicationMetricsProvider applicationMetricProvider;

	private DspGraph dspGraph;
	private ResourceGraph resGraph;

	private ODDModel model;
	private ODDParameters params;
	private OptimalSolution solution;

	private static final boolean DEBUG = false;
	private int srcRes;
	private int snkRes;
	
	public ODPPrunedSpace(int NRES, ResGraphBuilder.TYPE resType, int NDSP,
						  DspGraphBuilder.TYPE dspType, double RESTRICTION_ON_VRES,
						  ODDParameters params,
						  int srcRes, int snkRes) throws ODDException {

		this.params = params;
		createResGraph(NRES, resType);
		createDspGraph(NDSP, dspType, RESTRICTION_ON_VRES);

		this.solution = null;
		
		this.srcRes = srcRes;
		this.snkRes = snkRes;
	}

	public ODPPrunedSpace(DspGraph dspGraph,
					  ResourceGraph resGraph,
					  ODDParameters params,
					  int srcRes, int snkRes) {

		this.params = params;
		this.srcRes = srcRes;
		this.snkRes = snkRes;

		this.dspGraph = dspGraph;
		this.resGraph = resGraph;

		this.resGraphBuilder = null;
		this.resourceMetricProvider = null;
		this.dspGraphBuilder = null;
		this.applicationMetricProvider = null;

		this.solution = null;

	}

	private void createResGraph(int NRES, ResGraphBuilder.TYPE resType) {
		resGraphBuilder = new ResGraphBuilder();
		resourceMetricProvider = new SimpleResourceMetricsProvider(params);
		resGraphBuilder.create(resourceMetricProvider, resType, NRES);
		// rbuilder.printGraph(false);
		resGraph = resGraphBuilder.getGraph();
	}

	private void createDspGraph(int NDSP, DspGraphBuilder.TYPE dspType,
			double RESTRICTION_ON_VRES) throws ODDException {

		dspGraphBuilder = new DspGraphBuilder();
		applicationMetricProvider = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		dspGraphBuilder.create(applicationMetricProvider, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			dspGraphBuilder.restrictPlacement(resGraph, RESTRICTION_ON_VRES);
		// gbuilder.printGraph();
		dspGraph = dspGraphBuilder.getGraph();

	}

	@Override
	public void compile() {
		// not supported
	}

	public OptimalSolution solve() throws ODDException {
		
		if (solution != null)
			return solution; 

		/* 1. Identify the smallest cluster containing source and sink 
		 * 		- Clusters are computed using the ObjectiveFunctionDistanceStrategy */

		HierarchicalCluster treeRoot = createClusters();
// 		ClusterUtils.showClusters(cluster);
//		ClusterUtils.printCluster(treeRoot);
 		HierarchicalCluster c = ClusterUtils.getSmallestClusterContaining(treeRoot, this.srcRes, this.snkRes);


		/* Iterate until we are considering all resources or a solution is find */
 		while (solution == null || !(Status.OPTIMAL.equals(solution.getStatus()) || Status.FEASIBLE.equals(solution.getStatus()))){

			/* 2. Compute ODD on the smallest cluster only */
	 		/* 2.1. Extract the resource sub graph */
			List<Integer> nodesId;
			if (c != null)
				nodesId = getContainedNodesIndex(c);
			else
				nodesId = new ArrayList<>();
//	 		ResourceGraph resExcerpt = resGraphBuilder.extractSubGraph(nodesId);
	 		ResourceGraph resExcerpt = ResGraphBuilder.extractSubGraph(resGraph, nodesId);

	 		/* 2.2. Preliminary check on graph */
	 		// XXX: to be implemented: valutare se il quantitativo di risorse aggregato e' sufficiente
	 		
	 		/* 2.3. Create an ODD model on the excerpt resource graph only */
// 			model = new ODDBasicModel(dspGraph, resExcerpt, params);
			model = new ODDBandwidthModel(dspGraph, resExcerpt, params);
	
			if (DEBUG){
				System.out.println();
				System.out.println("Executing ODD on " + resExcerpt.getVertices().values().size() + " resource nodes");
				System.out.println("--------------------------------------------------------------------------------");
				
			}
			
			/* 3. Compile the ODD model */
			model.compile();	

			/* 4. Define the pinned operators */
			for (Integer src : dspGraph.getSources()){
				if (DEBUG)
					System.out.println("Pinning source " + src + " on node " + srcRes);
				model.pin(src, srcRes);
			}
			for (Integer snk : dspGraph.getSinks()){
				if (DEBUG)
					System.out.println("Pinning sink " + snk + " on node " + snkRes);
				model.pin(snk, snkRes);
			}	
			
			/* 5. Solve the ODD model */
			solution = model.solve();
			
			/* 6. Check solution and if needed expand the cluster of resources */
			if (Status.INVALID.equals(solution.getStatus())){
				
				if (treeRoot.equals(c)){
					if (DEBUG)
						System.out.println("Already at the root");
					break;
				}else{
					if (DEBUG)
						System.out.println("Expanding the cluster");
					c = ClusterUtils.getSmallestClusterContaining(treeRoot, c);
				}
			}
		}
 		if (DEBUG)
 			System.out.println(solution.getStatus());

		return solution;
	
	}

	@Override
	public void clean() {
		model.clean();
		solution = null;
	}

	@Override
	public String type() {
		return "ODP-PS";
	}

	@Override
	public void pin(int dspIndex, int resIndex) {
		// TODO: not supported
	}

	private HierarchicalCluster createClusters(){

		HierarchicalClusteringAlgorithm alg = new HierarchicalClusteringAlgorithm();
        DistanceStruct dist = computePairwiseDistances();
		return alg.performClustering(dist.getDistances(), dist.getNames(), new AverageLinkageStrategy());

	}

	private List<Integer> getContainedNodesIndex(HierarchicalCluster hierarchicalCluster){
		
		List<Integer> cn = new ArrayList<>();
		
		if (hierarchicalCluster.countLeafs() == 1 && hierarchicalCluster.getChildren().size() == 0){
			// this hierarchicalCluster represents a leaf
			cn.add(new Integer(hierarchicalCluster.getName()));
		}
		
		for (HierarchicalCluster child : hierarchicalCluster.getChildren()){
			List<Integer> recCn = getContainedNodesIndex(child);
			cn.addAll(recCn);
		}
		return cn;
		
	}
	
	private DistanceStruct computePairwiseDistances(){

		PairwiseDistanceStrategy distanceComputer = new ObjectiveFunctionDistanceStrategy(resGraph, params);

		int numRes = resGraph.getVertices().values().size();
		double[][] distances = new double[numRes][numRes];
		String[] names = new String[numRes];
		
		int i = 0;
		for (Integer uIndex : resGraph.getVertices().keySet()){
			
			names[i] = Integer.toString(uIndex);
			
			int j = 0;
			for (Integer vIndex : resGraph.getVertices().keySet()){

				ResourceEdge e = resGraph.getEdges().get(new Pair(uIndex, vIndex));
				
				distances[i][j] = distanceComputer.getDistance(e);
				
				j++;
			}

			i++;
			
		}

		return new DistanceStruct(distances, names);
	}
	

	/* ******************************************************************* */
	/* Getters 															   */
	/* ******************************************************************* */
	public ResGraphBuilder getResGraphBuilder() {
		return resGraphBuilder;
	}

	public ResourceMetricsProvider getResourceMetricProvider() {
		return resourceMetricProvider;
	}

	public DspGraphBuilder getDspGraphBuilder() {
		return dspGraphBuilder;
	}

	public ApplicationMetricsProvider getApplicationMetricProvider() {
		return applicationMetricProvider;
	}

	public ODDModel getModel() {
		return model;
	}

	private class DistanceStruct{
		private double[][] distances; 
		private String[] names;
		
		DistanceStruct(double[][] distances, String[] names) {
			super();
			this.distances = distances;
			this.names = names;
		}

		double[][] getDistances() {
			return distances;
		}
		String[] getNames() {
			return names;
		}
	}
	
}
