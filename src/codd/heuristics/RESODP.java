package codd.heuristics;

import codd.*;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.DspGraph;
import codd.model.OptimalSolution;
import codd.model.ResourceGraph;
import codd.placement.ODDBandwidthRelaxedModel;
import codd.placement.ODDBasicRelaxedModel;
import codd.placement.ODDModel;

import java.util.HashSet;
import java.util.Set;


/**
 * If you use this heuristic, please cite:
 * M. Nardelli, V. Cardellini, V. Grassi, F. Lo Presti,
 * "Efficient Operator Placement for Distributed Data Stream Processing Applications",
 * IEEE Transactions on Parallel and Distributed Systems,
 * vol. 30, no. 8, pp. 1753–1767, 2019. doi: 10.1109/TPDS.2019.2896115.
 */
public class RESODP implements ODDModel {

	private ResGraphBuilder resGraphBuilder;
	private ResourceMetricsProvider resourceMetricProvider;
	private DspGraphBuilder dspGraphBuilder;
	private ApplicationMetricsProvider applicationMetricProvider;

	private DspGraph dspGraph;
	private ResourceGraph resGraph;

	private ODDModel model;
	private OptimalSolution solution;
	private ODDParameters params;

	private static final boolean DEBUG = false;
	private int srcRes;
	private int snkRes;
	private boolean useAllCandidates;
	private int kNeighbors;
	
	public RESODP(int NRES, ResGraphBuilder.TYPE resType, int NDSP,
				  DspGraphBuilder.TYPE dspType, double RESTRICTION_ON_VRES,
				  ODDParameters params,
				  int srcRes, int snkRes, boolean useAllCandidates,
				  int kNeighbors) throws ODDException {

		this.params = params;
		createResGraph(NRES, resType);
		createDspGraph(NDSP, dspType, RESTRICTION_ON_VRES);

		this.solution = null;

		this.srcRes = srcRes;
		this.snkRes = snkRes;
		this.useAllCandidates = useAllCandidates;
		this.kNeighbors = kNeighbors;
	}

	public RESODP(DspGraph dspGraph,
				  ResourceGraph resGraph,
				  ODDParameters params,
				  int srcRes, int snkRes,
				  boolean useAllCandidates,
				  int kNeighbors) {

		this.params = params;
		this.dspGraph = dspGraph;
		this.resGraph = resGraph;

		this.resGraphBuilder = null;
		this.resourceMetricProvider = null;
		this.dspGraphBuilder = null;
		this.applicationMetricProvider = null;

		this.solution = null;

		this.srcRes = srcRes;
		this.snkRes = snkRes;
		this.useAllCandidates = useAllCandidates;
		this.kNeighbors = kNeighbors;

	}

	private void createResGraph(int NRES, ResGraphBuilder.TYPE resType) {
		resGraphBuilder = new ResGraphBuilder();
		resourceMetricProvider = new SimpleResourceMetricsProvider(params);
		resGraphBuilder.create(resourceMetricProvider, resType, NRES);
//		resGraphBuilder.printGraph(true);
		resGraph = resGraphBuilder.getGraph();
	}

	private void createDspGraph(int NDSP, DspGraphBuilder.TYPE dspType,
			double RESTRICTION_ON_VRES) throws ODDException {

		dspGraphBuilder = new DspGraphBuilder();
		applicationMetricProvider = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		dspGraphBuilder.create(applicationMetricProvider, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			dspGraphBuilder.restrictPlacement(resGraph, RESTRICTION_ON_VRES);
//		dspGraphBuilder.printGraph();
		dspGraph = dspGraphBuilder.getGraph();

	}

	@Override
	public void compile() {
		// not supported
	}

	public OptimalSolution solve() throws ODDException {
		
		if (solution != null)
			return solution; 

		/* 3. Solve Relaxed Problem (with pinned operators)
		 * 		Multiple candidate can be identified for an operator; a single
		 * 		candidate is identified for each operator using 
		 * 		ODDBasicRelaxedModel.MULTIPLE_TO_SINGLE_CANDIDATE 
		 * 		which can be set as: 
		 *		- DETERMINISTIC_LAST_MATCH, 
		 *		- BEST_FIT,
		 *		- FIRST_FIT, 
		 *		- UNIFORMLY_RANDOM, 
		 *		- X_AS_PROBABILITY
		 *	At this point, we are only interested in determining a set of 
		 *	candidates nodes and not the operator placement. All candidates nodes
		 *	are store in solution.getUsedNodes().
		 */
//		model = new ODDBasicRelaxedModel(dspGraph, resGraph, params, true, true);
		model = new ODDBandwidthRelaxedModel(dspGraph, resGraph, params, true, true);
		model.compile();
		
		/* 4. Define the pinned operators */
		for (Integer src : dspGraph.getSources()){
			if (DEBUG)
				System.out.println("Pinning source " + src + " on node " + srcRes);
			model.pin(src, srcRes);
		}
		for (Integer snk : dspGraph.getSinks()){
			if (DEBUG)
				System.out.println("Pinning sink " + snk + " on node " + snkRes);
			model.pin(snk, snkRes);
		}	

//		((ODDBasicRelaxedModel) model).setMultipleToSingleCandidateMode(ODDBasicRelaxedModel.MULTIPLE_TO_SINGLE_CANDIDATE.UNIFORMLY_RANDOM);
		((ODDBandwidthRelaxedModel) model).setMultipleToSingleCandidateMode(ODDBasicRelaxedModel.MULTIPLE_TO_SINGLE_CANDIDATE.UNIFORMLY_RANDOM);
		OptimalSolution solution = model.solve();
		
//		long expansionTime = System.currentTimeMillis();
		
		/* 4. Expansion
		 * 		Expand the set of candidates; 
		 * 		We can use all candidates identified by the previous step (3) or 
		 * 		just the set of single candidate node for each operator. 
		 * 		For each candidate node, we find the K-Nearest Neighbors using
		 * 		a deterministic or a probabilistic mode (based on the node distance).
		 * 
		 *  	We use the objective function weights to determine the distance of a node. 
		 */
		Set<Integer> resNodes = new HashSet<>();
		ResNeighborFinder rnf = new ResNeighborFinder(resGraph, params);

		/* Heuristics with single resource for operator */
		if (useAllCandidates){
			/* Heuristics with all candidate resources */
			resNodes.clear();
	 		resNodes.addAll(solution.getUsedNodes());
			
		} else {
			for (int i = 0; i < dspGraph.getVertices().size(); i++){
				resNodes.add(solution.getPlacement(i));
			}
		}

		/* retrieve neighbors */
		Set<Integer> extResNodes = rnf.findNeighbors(kNeighbors, resNodes, ResNeighborFinder.MODE.PROBABILISTIC);
		extResNodes.addAll(resNodes);

		// add pinned nodes
		extResNodes.add(srcRes);
		extResNodes.add(snkRes);
//		expansionTime = System.currentTimeMillis() - expansionTime;
		
		
		/* 5. Restrict the placement of each operator
		 * 		We now solve the ODP model with NO relaxation. 
		 * 		To speedup the computation, we restrict the set of candidate nodes for each 
		 * 		operator to the set of expanded candidate nodes (identified in (4)).
		 */
//		dspGraphBuilder.restrictPlacement(extResNodes);
		DspGraphBuilder.restrictPlacement(dspGraph, extResNodes);

		/* Compute the optimal solution only on the candidate resource nodes */
//		model = new ODDBasicRelaxedModel(dspGraph, resGraph, params, false, false);
		model = new ODDBandwidthRelaxedModel(dspGraph, resGraph, params, false, false);
		model.compile();
		
		/* Pin data source and sink */ 
		for (Integer src : dspGraph.getSources()){
			if (DEBUG)
				System.out.println("Pinning source " + src + " on node " + srcRes);
			model.pin(src, srcRes);
		}
		for (Integer snk : dspGraph.getSinks()){
			if (DEBUG)
				System.out.println("Pinning sink " + snk + " on node " + snkRes);
			model.pin(snk, snkRes);
		}	
		
		solution = model.solve();

//		long optResTime = solution.getResolutionTime();
////		long optComTime = solution.getCompilationTime();
//		double optOptR = solution.getOptR();
//		String finalPlacement = "{";
//		for (int i = 0; i < dspGraph.getVertices().size(); i++){
//			finalPlacement += solution.getPlacement(i) + ", ";
//		}
//		finalPlacement += "}";
//		
////		System.out.println("\n\n");
////		System.out.println("Relaxed resolution\n  compilation time " + relComTime + "ms; resolution time: "+ relResTime + " ms");
////		System.out.println("Expansion: " + expansionTime + "ms");
////		System.out.println("Optimal solution on reduced set\n  compilation time " + optComTime + "ms; resolution time: "+ optResTime + " ms");
////		System.out.println("Optimum of linear relaxation: R = " + relOptR + " ms;\nOptimum of ILP on reduced set: "+ optOptR + " ms");
////		System.out.println("\n\n");
//		System.out.println(kNeighbors + "; " + optOptR + "; " + 
//				relResTime + "; " + expansionTime + "; " + optResTime + "; " + 
//				resNodes.size() + "; " + extResNodes.size() + "; " + 
//				resNodes + "; exp: " + extResNodes + "; " + 
//				finalPlacement
//				);
//				
////		Report report = new Report(""+experiment, "output");
////		try {
////			report.write(idSeries, runNumber, model, gbuilder, amp, rbuilder, rmp, RESTRICTION_ON_VRES, params, solution);
////		} catch (ReportException | IOException e) {
////			e.printStackTrace();
////		}
////		
////		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
////		System.out.println("\n\n");
		
		return solution;
	
	}

	@Override
	public void clean() {
		model.clean();
		solution = null;
	}

	@Override
	public String type() {
		return "RES-ODP";
	}

	@Override
	public void pin(int dspIndex, int resIndex) {
		// TODO: not supported
	}


	/* ******************************************************************* */
	/* Getters 															   */
	/* ******************************************************************* */
	public ResGraphBuilder getResGraphBuilder() {
		return resGraphBuilder;
	}

	public ResourceMetricsProvider getResourceMetricProvider() {
		return resourceMetricProvider;
	}

	public DspGraphBuilder getDspGraphBuilder() {
		return dspGraphBuilder;
	}

	public ApplicationMetricsProvider getApplicationMetricProvider() {
		return applicationMetricProvider;
	}

	public ODDModel getModel() {
		return model;
	}
	
}
