package codd.heuristics;

import codd.*;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.*;
import codd.placement.ODDBandwidthModel;
import codd.placement.ODDModel;
import ilog.cplex.IloCplex;

import java.util.*;

/**
 * If you use this heuristic, please cite:
 * M. Nardelli, V. Cardellini, V. Grassi, F. Lo Presti,
 * "Efficient Operator Placement for Distributed Data Stream Processing Applications",
 * IEEE Transactions on Parallel and Distributed Systems,
 * vol. 30, no. 8, pp. 1753–1767, 2019. doi: 10.1109/TPDS.2019.2896115.
 */
public class GreedyLocalSearch implements ODDModel{

	private static final int FIRST = 0;
	
	private ResGraphBuilder resGraphBuilder;
	private ResourceMetricsProvider resourceMetricProvider;
	private DspGraphBuilder dspGraphBuilder;
	private ApplicationMetricsProvider applicationMetricProvider;

	private DspGraph dspGraph;
	private ResourceGraph resGraph;

	private static final boolean DEBUG = false;

	private ODDParameters params;
	
	private OptimalSolution solution;

	private int srcRes;
	private int snkRes;

	/* GreedyLocalSearch first-fit approach
	 * Resource nodes are sorted according to the 
	 * ObjectiveFunctionBasedDistanceStrategy, therefore the 
	 * closer, the better objective value */
	public GreedyLocalSearch(int NRES, ResGraphBuilder.TYPE resType, int NDSP,
							 DspGraphBuilder.TYPE dspType, double RESTRICTION_ON_VRES,
							 ODDParameters params,
							 int srcRes, int snkRes) throws ODDException {

		this.params = params;
		createResGraph(NRES, resType);
		createDspGraph(NDSP, dspType, RESTRICTION_ON_VRES);

		this.solution = null;
		
		this.srcRes = srcRes;
		this.snkRes = snkRes;
	}

	public GreedyLocalSearch(DspGraph dspGraph,
						  ResourceGraph resGraph,
						  ODDParameters params,
						  int srcRes, int snkRes) {

		this.params = params;
		this.srcRes = srcRes;
		this.snkRes = snkRes;

		this.dspGraph = dspGraph;
		this.resGraph = resGraph;

		this.resGraphBuilder = null;
		this.resourceMetricProvider = null;
		this.dspGraphBuilder = null;
		this.applicationMetricProvider = null;

		this.solution = null;

	}

	private void createResGraph(int NRES, ResGraphBuilder.TYPE resType) {
		resGraphBuilder = new ResGraphBuilder();
		resourceMetricProvider = new SimpleResourceMetricsProvider(params);
		resGraphBuilder.create(resourceMetricProvider, resType, NRES);
		// rbuilder.printGraph(false);
		resGraph = resGraphBuilder.getGraph();
	}

	private void createDspGraph(int NDSP, DspGraphBuilder.TYPE dspType,
			double RESTRICTION_ON_VRES) throws ODDException {

		dspGraphBuilder = new DspGraphBuilder();
		applicationMetricProvider = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		dspGraphBuilder.create(applicationMetricProvider, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			dspGraphBuilder.restrictPlacement(resGraph, RESTRICTION_ON_VRES);
		// gbuilder.printGraph();
		dspGraph = dspGraphBuilder.getGraph();

	}

	@Override
	public void compile() {
		// not supported
	}

	public OptimalSolution solve() throws ODDException {
		
		if (solution != null)
			return solution; 

		/* 1. Define an initial configuration 
		 * 2. LOOP: 
		 * 		- explore the neighborhood of the current configuration
		 * 		- the best neighbor is chosen to become the current configuration
		 * 3. the loop is repeated until no better configurations are available
		 * 		or a stopping criteria is met
		 */

		long elapsedTime = System.currentTimeMillis();

		int k =  resGraph.getVertices().values().size() * 2;
		ResNeighborFinder knf = new ResNeighborFinder(resGraph, params);
		List<Integer> neighbors = knf.findNeighbors(k, srcRes, snkRes, ResNeighborFinder.MODE.BESTFIT);
		
		/* First Fit approach */
		Assignments assignments = computeInitialPlacement(neighbors);

		if (DEBUG){
			for(Assignment p : assignments.getAssignments())
				System.out.println("Assignment: operator " + p.getOperator() + " assigned to resource " + p.getResource());
		}


		double initialObjValue = computeObjectiveFunction(assignments);
		Assignments bestAssignments = assignments.clone();
		double finalObjValue = initialObjValue;
		boolean improvement = true;
		while(improvement){
			improvement = false;
			assignments = improveBySwapping(assignments);
			assignments = improveBySwappingResourceNodes(knf, assignments);
	 		assignments = improveByConsideringNewNode(knf, assignments);
			
			double currentObjValue = computeObjectiveFunction(assignments);

			if (finalObjValue < currentObjValue){
				if (DEBUG)
					System.out.println("Solution improved from " + finalObjValue + " to " + currentObjValue);

				finalObjValue = currentObjValue;
				bestAssignments = assignments.clone();
				improvement = true;
			}
		}

		if (DEBUG){
			System.out.println("ObjFx: from " + initialObjValue + " . to " + finalObjValue);
			for (Assignment a : bestAssignments.getAssignments()){
				System.out.println("Operator " + a.getOperator() + " on host " + a.getResource());
			}
		}

		elapsedTime = System.currentTimeMillis() - elapsedTime;
		solution = reportSolution(bestAssignments, elapsedTime);
		return solution;
	
	}

	@Override
	public void clean() {
		solution = null;
	}

	@Override
	public String type() {
		return "LocalSearch";
	}

	@Override
	public void pin(int dspIndex, int resIndex) {
		// TODO not supported
	}

	private OptimalSolution reportSolution(Assignments assignments, long elapsedTime){

		solution = new OptimalSolution(dspGraph.getVertices().values().size());
		solution.setOptObjValue(computeObjectiveFunction(assignments));

		solution.setOptR(computeObjectiveR(assignments));
	    solution.setOptLogA(computeObjectiveLogA(assignments));
	    solution.setOptZ(computeObjectiveZ(assignments));
	    solution.setResolutionTime(elapsedTime);
	    solution.setCompilationTime(0);

	    for (Assignment a : assignments.getAssignments()) {
	  		solution.setPlacement(a.getOperator(), a.getResource());
		}

		solution.setStatus(IloCplex.Status.Feasible);

		return solution;
		
	}
	
	private Assignments improveByConsideringNewNode(ResNeighborFinder knf, Assignments assignments){

		int k =  resGraph.getVertices().values().size() * 2;
		List<Integer> newNeighbors = knf.findNeighbors(k, srcRes, snkRes, ResNeighborFinder.MODE.BESTFIT);

		/* Filter the neighbors removing the already used resources */
		for(Assignment a : assignments.getAssignments()){
			Iterator<Integer> it = newNeighbors.iterator();
			while(it.hasNext()){
				Integer neighbor = it.next();
				if (neighbor.equals(a.getResource()))
					it.remove();
			}
		}
		
		boolean allocated = false;
		boolean improvement = true;
		Assignments initialAssignments = assignments.clone();
		if (initialAssignments == null)
			return assignments;

		Iterator<Integer> resourcesToEvaluateIt = newNeighbors.iterator();
		
		while(improvement){
			
			improvement = false;
			
			while(resourcesToEvaluateIt.hasNext()){
		
				int nextResource = resourcesToEvaluateIt.next();
				Assignments newAssignments = null; 
				double initialObjValue = computeObjectiveFunction(initialAssignments);
	
				for (DspVertex operator : dspGraph.getVertices().values()){

					if (initialAssignments == null)
						continue;
					newAssignments = initialAssignments.clone();

					if (newAssignments == null)
						return initialAssignments;

					int operatorIndex = operator.getIndex();
					Integer operatorInitialPlacement = newAssignments.getPlacement(operatorIndex);
//					System.out.println("Evaluating moving " + operatorIndex + " from " + operatorInitialPlacement + " to " + nextResource);
					
					if (!isPinned(operatorIndex)){
						if (newAssignments.canHost(nextResource, operator)){
							newAssignments.unsetAssignment(operatorInitialPlacement, operator);
							newAssignments.setAssignment(nextResource, operator);
							
							/* Improve current solution by evaluating all possible swaps */
							newAssignments = improveBySwapping(newAssignments);
							
							if (hasObjFxImprovement(initialObjValue, newAssignments)){
								allocated = true;
								break;
							} else {
								allocated = false;
								newAssignments = initialAssignments.clone();
							}
						} 
					}								
				}
	
				if(allocated){
					if (DEBUG)
						System.out.println("This resource exploration round has improved the solution.");
					initialAssignments = newAssignments;
					improvement = true;
					break;
				}else{
					improvement = false;
				}
		
			} // look for an improvement considering each neighbor 

		} // repeat until an improvement is found
		
		
		return initialAssignments;
	}

	/*
	 * Swap a resource node with another neighbor. Check for improvements. 
	 */
	private Assignments improveBySwappingResourceNodes(ResNeighborFinder knf, Assignments assignments){

		int k =  resGraph.getVertices().values().size() * 2;
		List<Integer> newNeighbors = knf.findNeighbors(k, srcRes, snkRes, ResNeighborFinder.MODE.BESTFIT);
		List<Integer> usedResources = new ArrayList<>();

		/* Create list of used nodes that can be replaced */
		for(Assignment a : assignments.getAssignments()){
			if(!isPinned(a.getOperator()))
				usedResources.add(a.getResource());
		}
		/* Remove used nodes from the list of new neighbors */
		for(Assignment a : assignments.getAssignments()){
			Iterator<Integer> it = newNeighbors.iterator();
			while(it.hasNext()){
				Integer neighbor = it.next();
				if (neighbor.equals(a.getResource()))
					it.remove();
			}
		}

		
		boolean allocated = false;
		boolean improvement = true;
		Assignments initialAssignments = assignments.clone(); 
		
		while(improvement){
			
			improvement = false;

//			Iterator<Integer> resourcesToEvaluateIt = newNeighbors.iterator();
//			while(resourcesToEvaluateIt.hasNext()){
//				int nextResource = resourcesToEvaluateIt.next();

			for (int nextResource : newNeighbors){

				Assignments newAssignments = null;
				double initialObjValue = computeObjectiveFunction(initialAssignments);
				
				for (Integer resource : usedResources){

					if (initialAssignments == null)
						continue;
					newAssignments = initialAssignments.clone();

					/* Change the placement of each operator on resource */
					List<Integer> operators = newAssignments.getOperators(resource);
					boolean canRelocateAllOperators = true;
					
					/* Check if there are no pinned operators */
					for (Integer operatorIndex : operators){
						if (isPinned(operatorIndex)){
							canRelocateAllOperators = false;
							break;
						}						
					}
					if (!canRelocateAllOperators)
						continue; 
					
					/* Check if new resource has enough available resources for all the opertors */
					List<DspVertex> operatorNodes = new ArrayList<>();
					for (Integer operatorIndex : operators){
						operatorNodes.add(dspGraph.getVertices().get(operatorIndex));
					}
					if(newAssignments.canHost(nextResource, operatorNodes)){
						
						for (DspVertex operator : operatorNodes){
							newAssignments.unsetAssignment(resource, operator);
							newAssignments.setAssignment(nextResource, operator);							
						}

						newAssignments = improveBySwapping(newAssignments);
						
						if (hasObjFxImprovement(initialObjValue, newAssignments)){
							allocated = true;
							break;
						} else {
							allocated = false;
							newAssignments = initialAssignments.clone();
						}
					}
				} // evaluate improvements for each currently used resources
	
				if(allocated){
					if (DEBUG)
						System.out.println("This resource exploration (resource swapping) round has improved the solution.");
					initialAssignments = newAssignments;
					improvement = true;
					break;
				}else{
					improvement = false;
				}
			} // evaluate each currently used resources
		} // repeat until no further improvements can be found
		
		return initialAssignments;
	}


	/* Look for improvements by swapping the operator placement on currently used resources */
	private Assignments improveBySwapping(Assignments assignments){
		
		boolean allocated = false;
		boolean improvement = true;
		
		Assignments initialAssignments = assignments.clone(); 

		while(improvement){
		
			Assignments newAssignments = null; 
//			improvement = false;
			double initialObjValue = computeObjectiveFunction(initialAssignments);

			for (DspEdge e : dspGraph.getEdges().values()){

				if (initialAssignments == null)
					continue;
				newAssignments = initialAssignments.clone();

				int i = e.getFrom();
				int j = e.getTo();
				Integer iOldResource = newAssignments.getPlacement(i);
				Integer jOldResource = newAssignments.getPlacement(j);
				
				if (!iOldResource.equals(jOldResource)){

					/* try to co-locate j on res(i) */
					allocated = swapOperatorToDestinationResource(newAssignments, j, i, iOldResource, jOldResource);
					
					if (allocated && hasObjFxImprovement(initialObjValue, newAssignments)){
						break;
					} else {
//						allocated = false;
						newAssignments = initialAssignments.clone();
					}

					/* try to co-locate i on res(j) */
					allocated = swapOperatorToDestinationResource(newAssignments, i, j, jOldResource, iOldResource);
					
					if (allocated && hasObjFxImprovement(initialObjValue, newAssignments)){
						break;
					} else {
						allocated = false;
						newAssignments = initialAssignments.clone();
					}
				}
			} // for each interconnected operators

			if(allocated){
//				System.out.println("This swapping round has improved the solution.");
				initialAssignments = newAssignments;
				improvement = true;
			}else{
				improvement = false;
			}
			
		} // repeat until further improvements are found
		
		return initialAssignments;
	}

	private boolean hasObjFxImprovement(double initialObjValue, Assignments newAssignments){

		double newobjfx = computeObjectiveFunction(newAssignments);
		
		return (newobjfx > initialObjValue);
		
	}
	
	private boolean swapOperatorToDestinationResource(Assignments newAssignments, 
			int operatorToMove, int otherOperator, 
			int destinationResource, int sourceResource 
			){
		
		if (!isPinned(operatorToMove)){

			/* try colocate j on u */
			DspVertex operatorToMoveDsp = dspGraph.getVertices().get(operatorToMove);
			if (newAssignments.canHost(destinationResource, operatorToMoveDsp)){
				newAssignments.unsetAssignment(sourceResource, operatorToMoveDsp);
				newAssignments.setAssignment(destinationResource, operatorToMoveDsp);
				
				return true;
				
			} else { 
				/* swap elements */
				List<Integer> operatorsOnIResource = newAssignments.getOperators(destinationResource);
				removeFromAssignments(operatorsOnIResource, otherOperator);
				removePinnedFromAssignments(operatorsOnIResource);
				
				for (Integer ooi : operatorsOnIResource){
					DspVertex anotherOperatorOnI = dspGraph.getVertices().get(ooi);
					
					if (newAssignments.canSwap(destinationResource, operatorToMoveDsp, anotherOperatorOnI) && 
							newAssignments.canSwap(sourceResource, anotherOperatorOnI, operatorToMoveDsp)){

						newAssignments.unsetAssignment(sourceResource, operatorToMoveDsp);
						newAssignments.unsetAssignment(destinationResource, anotherOperatorOnI);
						newAssignments.setAssignment(destinationResource, operatorToMoveDsp);
						newAssignments.setAssignment(sourceResource, anotherOperatorOnI);
						
						return true;
						
					}
				}
			}
		}

		return false;

	}
	
	private void removeFromAssignments(List<Integer> operatorsOnResource, int operator){
		Iterator<Integer> it = operatorsOnResource.iterator();
		while(it.hasNext()){
			Integer n = it.next();
			if (n.equals(operator))
				it.remove();
		}
	}
	
	private void removePinnedFromAssignments(List<Integer> operatorsOnResource){
		for(Integer src : dspGraph.getSources()){
			removeFromAssignments(operatorsOnResource, src);
		}
		for(Integer snk : dspGraph.getSinks()){
			removeFromAssignments(operatorsOnResource, snk);
		}
	}
	
	private boolean isPinned(int operator){
		for(Integer src : dspGraph.getSources()){
			if (operator == src)
				return true;
		}
		for(Integer snk : dspGraph.getSinks()){
			if (operator == snk)
				return true;
		}
		return false;
	}
	
	
	private double computeObjectiveFunction(Assignments assignments){
		
		double Rmax = computeObjectiveR(assignments);
		double logA = computeObjectiveLogA(assignments);
		double Z = computeObjectiveZ(assignments);

		
		/* ******************************************************************************
		 *  Objective
		 ********************************************************************************/	
		double obj;
		double objRExpr;
		double objAExpr;
		double objZExpr;
		objRExpr = (params.getRmax() - Rmax) * params.getWeightRespTime() / (params.getRmax() - params.getRmin());
		objAExpr = (logA - Math.log(params.getAmin())) * params.getWeightAvailability() / (Math.log(params.getAmax()) - Math.log(params.getAmin()));
		objZExpr = (params.getZmax() - Z) * params.getWeightNetMetric() / (params.getZmax() - params.getZmin());
		obj 	 = objRExpr + objAExpr + objZExpr;

//		System.out.println("Rmax  = " + Rmax);
//		System.out.println("logA  = " + logA + "(A="+Math.exp(logA) + ")");
//		System.out.println("termR = " + objRExpr);
//		System.out.println("termA = " + objAExpr);
//		System.out.println("obj   = " + obj);
		
		return obj;

	}
	
	private double computeObjectiveR(Assignments assignments){
		
		/* ******************************************************************************
		 * Response-Time		
		 ********************************************************************************/
		double Rmax = 0.0; 
		for (DspPath path : dspGraph.getPaths()) {

			/* R_computation */
			double Rpex = 0.0; 
			for(Integer operator : path.getNodesIndexes()){
				DspVertex i = dspGraph.getVertices().get(operator);
				Integer resource = assignments.getPlacement(operator);
				ResourceVertex u = resGraph.getVertices().get(resource);
				Rpex += i.getExecutionTime() / u.getSpeedup();	
//				System.out.println(".. Rpex + " + i.getExecutionTime() / u.getSpeedup() + ": " + operator + "->" + resource);
				
			}
			
			/* R_comunication*/
			double Rptx = 0.0; 
			List<Integer> sequence = path.getNodesIndexes();
			for (int k = 0; k < sequence.size() - 1; k++) {
				Integer ik = sequence.get(k);
				Integer ik1 = sequence.get(k + 1);
				Integer resk = assignments.getPlacement(ik);
				Integer resk1 = assignments.getPlacement(ik1);
				ResourceEdge eRes = resGraph.getEdges().get(new Pair(resk, resk1));
				
				Rptx += eRes.getDelay();

//				System.out.println(".. Rptx + " + eRes.getDelay() + ": (" + ik + ":" + ik1+")->(" + resk + ":" + resk1+")");
				
			}					
			
			if (Rmax < Rpex + Rptx) {
				Rmax = Rpex + Rptx; 
//				System.out.println(". Rmax updated: " + Rmax);
			}
			
		}
		
		return Rmax;

	}

	private double computeObjectiveLogA(Assignments assignments){
		
		/* ******************************************************************************
		 * Availability		
		 ********************************************************************************/
		double logA;
		
		/* A nodes */
		double logAex = 0.0;
		for (DspVertex iDsp : dspGraph.getVertices().values()) {
			
			Integer resource = assignments.getPlacement(iDsp.getIndex());
			ResourceVertex uRes = resGraph.getVertices().get(resource);
			
			logAex += Math.log(uRes.getAvailability());
//			System.out.println(".. logApex + " + Math.log(uRes.getAvailability()) + ": " + iDsp.getIndex()+ "->" + resource);

		}				
		
		/* A links */
		double logAtx = 0.0;
		
		for (DspEdge ijDsp : dspGraph.getEdges().values()) {
			int i = ijDsp.getFrom();
			int j = ijDsp.getTo();
	
			Integer u = assignments.getPlacement(i);
			Integer v = assignments.getPlacement(j);
			
			ResourceEdge uvRes = resGraph.getEdges().get(new Pair(u, v));
			
			logAtx += Math.log(uvRes.getAvailability());
//			System.out.println(".. logAtx + " + uvRes.getAvailability() + ": (" + i + ":"+j+")->(" + u + ":" + v + ")");
			
		}
		logA = logAex + logAtx;
		
		return logA;

	}
	
	private double computeObjectiveZ(Assignments assignments){
		
		/* ******************************************************************************
		 * Bandwidth: internode traffic, network utilization or elastic energy
		 ********************************************************************************/
		
		double Z = 0.0;

		for (DspEdge ij : dspGraph.getEdges().values()) {
			int i = ij.getFrom();
			int j = ij.getTo();

			/* Z = sum_{ij} Z_{ij} */
			Integer u = assignments.getPlacement(i);
			Integer v = assignments.getPlacement(j);
			
			ResourceEdge uvRes = resGraph.getEdges().get(new Pair(u, v));

			double lambdaij = ij.getLambda();
			if (u.intValue() == v.intValue())
				continue;

			if (ODDBandwidthModel.MODE.INTERNODE_TRAFFIC.equals(params.getBwMode())){
			
				Z += lambdaij;							
			
			} else if (ODDBandwidthModel.MODE.NETWORK_UTILIZATION.equals(params.getBwMode())){
				
				Z += lambdaij * uvRes.getDelay();
				
			} else if (ODDBandwidthModel.MODE.APPROX_ELASTIC_ENERGY.equals(params.getBwMode())){
				
				Z += lambdaij * uvRes.getDelay() * uvRes.getDelay();
			}
			
		}
		
		return Z;

	}
	
	
	
	private Assignments computeInitialPlacement(List<Integer> neighbors) throws ODDException{
		
		Assignments assignments = new Assignments(resGraph);
		
		/* Place source and sink */
		for (Integer src : dspGraph.getSources()){
			DspVertex iDsp = dspGraph.getVertices().get(src);
			boolean assigned = assignments.setAssignment(srcRes, iDsp);
			if (!assigned)
				throw new ODDException("Unable to assign the data source");
		}
		for (Integer snk : dspGraph.getSinks()){
			DspVertex iDsp = dspGraph.getVertices().get(snk);
			boolean assigned = assignments.setAssignment(snkRes, iDsp);
			if (!assigned)
				throw new ODDException("Unable to assign the information consumer");
		}	
		
		
		/* Place all the other operators first fit */
		Map<Integer, DspVertex> dspVertices = dspGraph.getVertices();
		if (snkRes != srcRes)
			neighbors.add(FIRST, snkRes);
		neighbors.add(FIRST, srcRes);
		
		List<Integer> operatorToBeAssigned = new ArrayList<>();
		
		/* Define the initial set of operators to be assigned, the ones directly connected 
		 * to the data source and to the final consumer (the latters are already assigned) */
		for (Integer src : dspGraph.getSources()){
			operatorToBeAssigned.addAll(getOneHopOperators(src, true));
		}
		for (Integer snk : dspGraph.getSinks()){
			operatorToBeAssigned.addAll(getOneHopOperators(snk, false));
		}	

		while (!operatorToBeAssigned.isEmpty()){
			
			int operator = operatorToBeAssigned.remove(FIRST);
			if (assignments.isAssigned(operator))
				continue;
			
			DspVertex iDsp = dspVertices.get(operator);
			boolean assigned = false;
			
			/* First fit */
			for(Integer currentResource : neighbors){
				if (assignments.canHost(currentResource, iDsp)){
					assigned = assignments.setAssignment(currentResource, iDsp);
					if (assigned)
						break;
				} 				
			}

			if (!assigned)
				throw new ODDException("Unable to assign " + operator + " to a computing node");
			
			/* Update the set of operators to be assigned */
			operatorToBeAssigned.addAll(getOneHopOperators(operator, true));
				
		}
		
		return assignments;
	}
	
	private List<Integer> getOneHopOperators(int operator, boolean direct){
		
		List<Integer> neighbors = new ArrayList<>();
		
		for (DspEdge e : dspGraph.getEdges().values()){
			
			if (direct){
				if (operator == e.getFrom()){
					neighbors.add(e.getTo());
				}
			} else {
				if (operator == e.getTo()){
					neighbors.add(e.getFrom());
				}
			}
			
		}
		
		return neighbors;
	}
	

	/* ******************************************************************* */
	/* Getters 															   */
	/* ******************************************************************* */
	public ResGraphBuilder getResGraphBuilder() {
		return resGraphBuilder;
	}

	public ResourceMetricsProvider getResourceMetricProvider() {
		return resourceMetricProvider;
	}

	public DspGraphBuilder getDspGraphBuilder() {
		return dspGraphBuilder;
	}

	public ApplicationMetricsProvider getApplicationMetricProvider() {
		return applicationMetricProvider;
	}

	private class Assignment {
		private int resource;
		private int operator;

		Assignment(int resource, int operator) {
			this.resource = resource;
			this.operator = operator;
		}

		public int getResource() {
			return resource;
		}

		public int getOperator() {
			return operator;
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof Assignment))
				return false;
			
			Assignment other = (Assignment) obj;
			return (this.resource == other.resource && this.operator == other.operator);
		}
		
		@Override
		protected Assignment clone() {
			return new Assignment(this.resource, this.operator);
		}
	}
	
	
	private class Assignments{
		
		private Map<Integer, Integer> availableResource; 
		private List<Assignment> assignments;
		private Set<Integer> assigned;
		
		private Assignments(){
			this.availableResource = new HashMap<>();
			this.assignments = new ArrayList<>();
			this.assigned = new HashSet<>();
		}
		
		Assignments(ResourceGraph resGraph) {
			
			this.availableResource = new HashMap<>();
			this.assignments = new ArrayList<>();
			for (ResourceVertex v : resGraph.getVertices().values()){
				availableResource.put(v.getIndex(),  v.getAvailableResources());
			}
			this.assigned = new HashSet<>();
		}
		
		boolean setAssignment(int resource, DspVertex operator){
			
			int requiredResources = operator.getRequiredResources();
			if (availableResource.get(resource) == null || availableResource.get(resource) < requiredResources)
				return false;
			
			availableResource.put(resource, availableResource.get(resource) - requiredResources);
			assignments.add(new Assignment(resource, operator.getIndex()));
			assigned.add(operator.getIndex());
			return true;
			
		}

		boolean unsetAssignment(int resource, DspVertex operator){
			
			Assignment a = new Assignment(resource, operator.getIndex());
			if (!assignments.contains(a))
				return false;

			int requiredResources = operator.getRequiredResources();
			assignments.remove(a);
			assigned.remove(operator.getIndex());
			availableResource.put(resource, availableResource.get(resource) + requiredResources);
			return true;
			
		}
		
		List<Assignment> getAssignments(){
			return assignments;
		}
		
		Integer getPlacement(int operator){
			
			for (Assignment a : assignments){
				if (a.getOperator() == operator)
					return a.getResource();
			}
			
			return null;
		}
		
		List<Integer> getOperators(int resource){
			
			List<Integer> operators = new ArrayList<>();
			
			for (Assignment a : assignments){
				if (a.getResource() == resource)
					operators.add(a.getOperator());
			}
			
			return operators;
		}
		
		boolean canHost(int resource, DspVertex operator){
			return (availableResource.get(resource) >= operator.getRequiredResources());
		}

		boolean canHost(int resource, List<DspVertex> operators){
			
			int allRequiredResources = 0;

			for (DspVertex operator : operators){
				allRequiredResources += operator.getRequiredResources();
			}
			
			return (availableResource.get(resource) >= allRequiredResources);

		}

		boolean canSwap(int resource, DspVertex operator, DspVertex operatorOnResource){
			
			int freeableResources = operatorOnResource.getRequiredResources();
			return (availableResource.get(resource) + freeableResources >= operator.getRequiredResources());

		}
		
		boolean isAssigned(int operator){
			return assigned.contains(operator);
		}
		
		@Override
		protected Assignments clone(){

			Assignments clone = new Assignments();
			
			for (Integer a : availableResource.keySet()){
				clone.availableResource.put(a, availableResource.get(a));
			}
			
			for (Assignment a : assignments){
				clone.assignments.add(a.clone());
			}

			clone.assigned.addAll(assigned);

			return clone;
			
		}
	}
}
