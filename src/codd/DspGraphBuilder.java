package codd;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import codd.metricsprovider.ApplicationMetricsProvider;
import codd.model.DspEdge;
import codd.model.DspGraph;
import codd.model.DspPath;
import codd.model.DspVertex;
import codd.model.Pair;
import codd.model.ResourceGraph;
import codd.model.ResourceVertex;


public class DspGraphBuilder {

	public enum TYPE {
		SEQUENTIAL, FAT, MULTILEVEL, UNKNOWN
	}

	protected TYPE type;
	protected DspGraph dspGraph;

	public DspGraphBuilder() {
		dspGraph = null;
		this.type = TYPE.UNKNOWN;
	}

	public DspGraphBuilder(DspGraph dspGraph) {
		this.dspGraph = dspGraph;
		this.type = TYPE.UNKNOWN;
	}

	public DspGraph getGraph() {
		return dspGraph;
	}

	public DspGraphBuilder create(
			// TopologyDetails topology, GeneralTopologyContext topologyContext,
			ApplicationMetricsProvider metricsProvider, TYPE type,
			int numOperators) throws ODDException {

		if (numOperators < 2)
			throw new ODDException("Invalid number of operators");
		
		this.type = type;
		
		switch(type){
			case SEQUENTIAL:
				return createLongTopology(metricsProvider, numOperators);
				
			case FAT: 
				return createFatTopology(metricsProvider, numOperators);
	
			case MULTILEVEL:
				return createMultilevelTopology(metricsProvider, numOperators);
			
			default:
				return createLongTopology(metricsProvider, numOperators);
		}

	}

	public TYPE getType() {
		return type;
	}
	
	public DspGraphBuilder restrictPlacement(ResourceGraph res, double percentage){
		
		Collection<DspVertex> operators = dspGraph.getVertices().values();
		Collection<ResourceVertex> resources = res.getVertices().values();
		
		if (percentage > 1.0)
			percentage = 1;

		long randomSeed = 201512011037L;
		Random rnd = new Random(randomSeed);
		
		for (DspVertex i : operators){
			for (ResourceVertex u : resources){
				if(rnd.nextDouble() <= percentage){
					i.addCandidates(u.getIndex());
				}
			}
		}
		
		return this;
		
	}

	public DspGraphBuilder restrictPlacement(Set<Integer> resNodes){
		
		Collection<DspVertex> operators = dspGraph.getVertices().values();

		for (DspVertex i : operators){
			for (Integer u : resNodes){
				i.addCandidates(u);
			}
		}
	
		return this;
		
	}

	public static void restrictPlacement(DspGraph dspGraph, Set<Integer> resNodes){

		Collection<DspVertex> operators = dspGraph.getVertices().values();

		for (DspVertex i : operators){
			for (Integer u : resNodes){
				i.addCandidates(u);
			}
		}

	}
	


	
	/**
	 * This topology has: 
	 *  - 1 source (first level)
	 *  - (2n/3) operators in the second level
	 *  - n/3 operators in the third level 
	 *  - 1 sink operator (forth level)
	 *  
	 *  where n is equal to numOperators - 2
	 *  
	 * @param metricsProvider
	 * @param numOperators
	 * @return
	 */
	private DspGraphBuilder createMultilevelTopology(
			ApplicationMetricsProvider metricsProvider,
			int numOperators) {
		
		Map<Integer, DspVertex> vDsp = new HashMap<Integer, DspVertex>();
		Map<Pair, DspEdge> eDsp = new HashMap<Pair, DspEdge>();
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();

		/* Source and sink are not replicated */

		/* 1. Create DspVertex */
		for (int i = 0; i < numOperators; i++) {
			int requiredResource = metricsProvider.getRequiredResources(i);
			double executionTime = metricsProvider.getExecutionTime(i);
			double cost = metricsProvider.getCost(i);
			double serviceRate = metricsProvider.getServiceRate(i);
			double codeImageSize = metricsProvider.getCodeImageSize(i);
			double internalStateImageSize = metricsProvider.getInternalStateImageSize(i);
			
			DspVertex v = new DspVertex(i, "op_" + i, requiredResource, executionTime, cost, internalStateImageSize, codeImageSize);
			v.setServiceRate(serviceRate);
			vDsp.put(new Integer(i), v);
		}

		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));

		/* 3. Create DspEdges to represent link between executors */
		int numIntOperators = numOperators - 2;
		int numOperSecondLevel = (int) Math.floor(2.0 * ((double) numIntOperators / 3.0));
		int numOperThirdLevel = numIntOperators - numOperSecondLevel;
		if (numOperThirdLevel < 0){
			numOperThirdLevel = 1;
			numOperSecondLevel = numIntOperators - numOperThirdLevel;
		}
		int[] prevLevel = new int[numOperSecondLevel];
		int sinkId = numOperators - 1;
		
		/* edges for second level operators */
		for (int j = 1; j < numOperSecondLevel + 1; j++){	
			int i = 0;
			DspEdge ij = new DspEdge(i, j, metricsProvider.getDataRate(i, j));
			eDsp.put(new Pair(i, j), ij);
			prevLevel[(j - 1)] = j;			
		}

//		for (int i = 0; i < prevLevel.length; i++)
//			System.out.println("->>> prevlevel["+i + "]" + prevLevel[i]);

		/* edges for third level operators */
		for (int j = numOperSecondLevel + 1; j < numOperSecondLevel + numOperThirdLevel + 1; j++){	
			for(int i : prevLevel){
				DspEdge ij = new DspEdge(i, j, metricsProvider.getDataRate(i, j));
				eDsp.put(new Pair(i, j), ij);
			}
		}

		prevLevel = new int[numOperThirdLevel];
		for (int j = numOperSecondLevel + 1; j < numOperSecondLevel + numOperThirdLevel + 1; j++){	
			prevLevel[(j - numOperSecondLevel - 1)] = j;			
		}
//		for (int i = 0; i < prevLevel.length; i++)
//			System.out.println("->>> prevlevel["+i + "]" + prevLevel[i]);

		
		
		/* edges for non-replicated operators */
		for(int prevId : prevLevel){
			DspEdge ij = new DspEdge(prevId, sinkId, metricsProvider.getDataRate(prevId, sinkId));
			eDsp.put(new Pair(prevId, sinkId), ij);
		}
		
			
		
		/* 4. Compute Paths */
		ArrayList<DspPath> paths = computePaths(vDsp, eDsp, sourcesIndex);

		/* 5. Identify sinks */
		for (DspPath path : paths) {
			sinksIndex.add(path.getSink());
		}

		/* 5. Creating object DspGraph */
		String graphId = "multilevel-topology";
		dspGraph = new DspGraph(graphId, vDsp, eDsp);
		dspGraph.setReconfigurationSyncTime(metricsProvider.getReconfigurationSyncTime());
		dspGraph.setPaths(paths);

		for (Integer so : sourcesIndex)
			dspGraph.addSource(so);

		for (Integer si : sinksIndex)
			dspGraph.addSink(si);

		return this;
	}
	
	private DspGraphBuilder createLongTopology(
			ApplicationMetricsProvider metricsProvider,
			int numOperators) throws ODDException {
		/* SEQUENTIAL layered topology */
		Map<Integer, DspVertex> vDsp = new HashMap<Integer, DspVertex>();
		Map<Pair, DspEdge> eDsp = new HashMap<Pair, DspEdge>();
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();

		/* Source and sink are not replicated */

		/* 1. Create DspVertex */
		for (int i = 0; i < numOperators; i++) {
			int requiredResource = metricsProvider.getRequiredResources(i);
			double executionTime = metricsProvider.getExecutionTime(i);
			double cost = metricsProvider.getCost(i);
			double serviceRate = metricsProvider.getServiceRate(i);
			double codeImageSize = metricsProvider.getCodeImageSize(i);
			double internalStateImageSize = metricsProvider.getInternalStateImageSize(i);
			
			DspVertex v = new DspVertex(i, "op_" + i, requiredResource, executionTime, cost, internalStateImageSize, codeImageSize);
			v.setServiceRate(serviceRate);
			vDsp.put(new Integer(i), v);
		}

		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));

		/* 3. Create DspEdges to represent link between executors */
		int[] prevLevel = new int[] { 0 };
		for (int j = 1; j < numOperators; j++) {
			for (int i : prevLevel) {
				DspEdge ij = new DspEdge(i, j, metricsProvider.getDataRate(i,
						j), metricsProvider.getAvgNumBytesPerTuple(i, j));
				eDsp.put(new Pair(i, j), ij);
			}
			prevLevel = new int[] { j };
		}

		/* 4. Compute Paths */
		ArrayList<DspPath> paths = computePaths(vDsp, eDsp, sourcesIndex);

		/* 5. Identify sinks */
		for (DspPath path : paths) {
			sinksIndex.add(path.getSink());
		}

		/* 5. Creating object DspGraph */
		String graphId = "long-topology";
		dspGraph = new DspGraph(graphId, vDsp, eDsp);
		dspGraph.setPaths(paths);
		dspGraph.setReconfigurationSyncTime(metricsProvider.getReconfigurationSyncTime());

		for (Integer so : sourcesIndex)
			dspGraph.addSource(so);

		for (Integer si : sinksIndex)
			dspGraph.addSink(si);

		return this;
	}

	private DspGraphBuilder createFatTopology(
			ApplicationMetricsProvider metricsProvider,
			int numOperators) throws ODDException {
		
		
		/* FAT topology */
		Map<Integer, DspVertex> vDsp = new HashMap<Integer, DspVertex>();
		Map<Pair, DspEdge> eDsp = new HashMap<Pair, DspEdge>();
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();

		/* Source and sink are not replicated */

		/* 1. Create DspVertex */
		for (int i = 0; i < numOperators; i++) {
			int requiredResource = metricsProvider.getRequiredResources(i);
			double executionTime = metricsProvider.getExecutionTime(i);
			double cost = metricsProvider.getCost(i);
			double serviceRate = metricsProvider.getServiceRate(i);
			double codeImageSize = metricsProvider.getCodeImageSize(i);
			double internalStateImageSize = metricsProvider.getInternalStateImageSize(i);
			
			DspVertex v = new DspVertex(i, "op_" + i, requiredResource,
					executionTime, cost, internalStateImageSize, codeImageSize);
			v.setServiceRate(serviceRate);
			vDsp.put(new Integer(i), v);
		}

		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));

		/* 3. Create DspEdges to represent link between executors */
		int numIntOperators = numOperators - 1;
		int[] prevLevel = new int[numIntOperators - 1];
		int sinkId = numOperators - 1;
		
		/* edges for replicated operators */
		for (int j = 1; j < numIntOperators; j++){	
			int i = 0;
			DspEdge ij = new DspEdge(i, j, metricsProvider.getDataRate(i, j));
			eDsp.put(new Pair(i, j), ij);
			prevLevel[(j - 1)] = j;
			
		}

		/* edges for non-replicated operators */
		for(int prevId : prevLevel){
			DspEdge ij = new DspEdge(prevId, sinkId, metricsProvider.getDataRate(prevId, sinkId));
			eDsp.put(new Pair(prevId, sinkId), ij);
		}
		
			
		/* 4. Compute Paths */
		ArrayList<DspPath> paths = computePaths(vDsp, eDsp, sourcesIndex);

		/* 5. Identify sinks */
		for (DspPath path : paths) {
			sinksIndex.add(path.getSink());
		}

		/* 5. Creating object DspGraph */
		String graphId = "long-topology";
		dspGraph = new DspGraph(graphId, vDsp, eDsp);
		dspGraph.setPaths(paths);
		dspGraph.setReconfigurationSyncTime(metricsProvider.getReconfigurationSyncTime());

		for (Integer so : sourcesIndex)
			dspGraph.addSource(so);

		for (Integer si : sinksIndex)
			dspGraph.addSink(si);

		return this;
	}
	
	static protected ArrayList<DspPath> computePaths(Map<Integer, DspVertex> vDsp,
			Map<Pair, DspEdge> eDsp, List<Integer> sourcesIndex) {
		/* 3. Compute paths between source and sinks */
		/* Creating paths... */
		List<Integer> frontier = new ArrayList<Integer>();

		/* Create dumb paths from source to source itself */
		ArrayList<DspPath> paths = new ArrayList<DspPath>();
		for (Integer so : sourcesIndex) {
			DspPath path = new DspPath(so);
			paths.add(path);
		}

		if (sourcesIndex.size() > 0) {
			/* Initialize graph exploration variables */
			/* Current node must be a source of the DSP Graph */
			Integer currentNode = sourcesIndex.get(0);
			boolean modified = true;

			do {

				modified = false;
				List<Pair> oEdges = getOutgoingEdges(currentNode, eDsp.keySet());

				/* Update nodes nodeTBV frontier */
				for (Pair p : oEdges) {
					frontier.add(new Integer(p.getB()));
				}

				ArrayList<DspPath> pathsToUpdate = getPathsEndingInNode(paths,
						currentNode);

				for (DspPath pathToUpdate : pathsToUpdate) {

					if (oEdges.isEmpty()) {
						continue;
					}

					paths.remove(pathToUpdate);

					for (Pair oe : oEdges) {
						DspPath newPath = pathToUpdate.clone();
						newPath.add(oe.getB());
						paths.add(newPath);
						modified = true;
					}
				}

				if (frontier.isEmpty()) {
					modified = false;
				} else {
					currentNode = frontier.remove(0);
				}
			} while (modified || !frontier.isEmpty());
		}

		return paths;
	}

	static private List<Pair> getOutgoingEdges(int currentNodeId, Set<Pair> edges) {
		List<Pair> outgoingEdges = new ArrayList<Pair>();
		for (Pair e : edges)
			if (e.getA() == currentNodeId)
				outgoingEdges.add(e);
		return outgoingEdges;
	}

	static private ArrayList<DspPath> getPathsEndingInNode(ArrayList<DspPath> paths,
			int nodeId) {

		ArrayList<DspPath> output = new ArrayList<DspPath>();
		for (DspPath path : paths) {
			if (path.isSink(nodeId))
				output.add(path);
		}
		return output;
	}

	public void printGraph() {

		System.out.println("DSP Graph");
		System.out.println("* Vertices ");
		for (DspVertex v : dspGraph.getVertices().values()) {
			System.out.println("   " + v.toString());
		}

		System.out.println();
		System.out.println("* Edges");
		for (DspEdge e : dspGraph.getEdges().values()) {
			System.out.println("   " + e.toString());
		}

		System.out.println();
		System.out.println(" --- ");
		System.out.print("* Sources\n   ");
		for (Integer so : dspGraph.getSources()) {
			System.out.print(so + ", ");
		}
		System.out.println();
		System.out.print("* Sinks\n   ");
		for (Integer si : dspGraph.getSinks()) {
			System.out.print(si + ", ");
		}
		System.out.println();
		System.out.println(" --- ");

		System.out.println();
		System.out.println("* Paths");
		for (DspPath path : dspGraph.getPaths()) {
			System.out.println("> " + path.toString());
		}

		return;

	}

}