package codd;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;

import codd.model.OptimalMultisetSolution;
import codd.model.ResourceVertexMultiset;
import codd.placement.ODDBandwidthModel;
import codd.placement.ODDModel;

/**
 * Parameters for EDRP model.
 * 
 * In addition to basic ODDParamters, this class includes information about
 * current placement, and measured QoS and cost metrics. This also keeps an
 * additional weighting parameter for the objective function, related to the
 * "downtime" metric.
 */
public class EDRPParameters extends ODDParameters {
	
	/*
	 * Bounds for metrics defined in a SLA.
	 */
	protected double TDmax = 1000;

	/* Additional Resource metrics. */
	
	/* NOTE: rate to/from DS are expressed in KiB/s = B/ms */
	protected double uploadRateToDSMin = 100 * 1000 ; /* 100 MiB/s */
	protected double uploadRateToDSMean = 0.0;
	protected double uploadRateToDSStdDev = 0.0;

	protected double downloadRateFromDSMin = uploadRateToDSMin;
	protected double downloadRateFromDSMean = uploadRateToDSMean;
	protected double downloadRateFromDSStdDev = uploadRateToDSStdDev;

	protected double uploadRateToLocalDSMin = 10 * 1000 * 1000; /* 10 GB/s */
	protected double uploadRateToLocalDSMean = 0.0;
	protected double uploadRateToLocalDSStdDev = 0.0;

	protected double downloadRateFromLocalDSMin = uploadRateToLocalDSMin;
	protected double downloadRateFromLocalDSMean = 0.0;
	protected double downloadRateFromLocalDSStdDev = 0.0;
	
	protected double dataStoreRTTMin = 10.0; /* ms */
	protected double dataStoreRTTMean = 0.0;
	protected double dataStoreRTTStdDev = 0.0; /* ms */

	protected double instanceLaunchTimeMin = 500.0;
	protected double instanceLaunchTimeMean = 0.0;
	protected double instanceLaunchTimeStdDev = 0.0;

	protected double reconfigurationSyncTimeMin = 250.0;
	protected double reconfigurationSyncTimeMean = 0.0;
	protected double reconfigurationSyncTimeStdDev = 0.0;

	/* Additional Application metrics. */
	protected double internalStateImageSizeMin = 1024 * 1024; /* 1 MB */
	protected double internalStateImageSizeMean = 0.0;
	protected double internalStateImageSizeStdDev = 0.0;

	protected double codeImageSizeMin = 1.4 * 1024 * 1024; /* 1.4 MB */
	protected double codeImageSizeMean = 0.0;
	protected double codeImageSizeStdDev = 0.0;

	public static final double MAX_AVAILABILITY = 0.9999999999;

	/** Additional weights for objective function. */
	protected double weightDowntime;

	/** Current placement for each DSP operator. */
	protected Map<Integer, ResourceVertexMultiset> currentPlacement;

	/**
	 * Constructs a new EDRPParamters instance cloning the basic ODDParameters.
	 * 
	 * @param currentParams
	 *            Basic ODDParameters.
	 */
	public EDRPParameters(ODDParameters currentParams) {
		super(currentParams.mode, currentParams.bwMode);

		try {
			cloneBasicParameters(currentParams);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Constructs a new EDRPParamters instance.
	 */
	public EDRPParameters(ODDModel.MODE mode, ODDBandwidthModel.MODE bwMode) {
		super(mode, bwMode);
	}

	/**
	 * Constructs a new EDRPParamters instance.
	 */
	public EDRPParameters(ODDBandwidthModel.MODE bwMode) {
		super(ODDModel.MODE.BANDWIDTH, bwMode);
	}

	/**
	 * Constructs a new EDRPParamters instance given the optimal initial
	 * placement.
	 * 
	 * @param currentParams
	 *            Basic ODDParameters.
	 * @param currentSolution
	 *            Optimal solution for initial placement.
	 */
	public EDRPParameters(ODDParameters currentParams, OptimalMultisetSolution currentSolution) {
		this(currentParams);
		updateFromSolution(currentSolution);
	}

	public void updateFromSolution(OptimalMultisetSolution currentSolution) {
		currentPlacement = currentSolution.getMultisetPlacements();
	}

	/**
	 * Clones ODDParameters using Reflection.
	 * 
	 * @param currentParams
	 *            Basic ODDParameters to clone.
	 */
	private void cloneBasicParameters(ODDParameters currentParams)
			throws IllegalArgumentException, IllegalAccessException {
		Field[] fromFields = currentParams.getClass().getDeclaredFields();
		for (Field field : fromFields) {
			if (Modifier.isFinal(field.getModifiers()))
				continue;
			Object value = field.get(currentParams);
			field.set(this, value);
		}
	}

	public void setCurrentPlacement(Map<Integer, ResourceVertexMultiset> placement) {
		this.currentPlacement = placement;
	}

	@Override
	public void setWeights(double availability, double responseTime, double cost, double netMetric) {
		super.setWeights(availability, responseTime, cost, netMetric);
		this.weightDowntime = 1.0 - (weightAvailability + weightCost + weightNetMetric + weightRespTime);
	}

	public void setWeights(double availability, double responseTime, double cost, double netMetric, double downtime) {
		if (downtime + availability + responseTime + cost + netMetric > 1.0)
			System.err.println("Sum of weights exceeds 1!");

		this.weightAvailability = availability;
		this.weightRespTime = responseTime;
		this.weightCost = cost;
		this.weightNetMetric = netMetric;
		this.weightDowntime = downtime;
	}


	public double getWeightDowntime() {
		return weightDowntime;
	}

	public void setWeightDowntime(double weightDowntime) {
		this.weightDowntime = weightDowntime;
	}

	public double getUploadRateToDSMin() {
		return uploadRateToDSMin;
	}

	public void setUploadRateToDSMin(double uploadRateToDSMin) {
		this.uploadRateToDSMin = uploadRateToDSMin;
	}

	public double getUploadRateToDSMean() {
		return uploadRateToDSMean;
	}

	public void setUploadRateToDSMean(double uploadRateToDSMean) {
		this.uploadRateToDSMean = uploadRateToDSMean;
	}

	public double getUploadRateToDSStdDev() {
		return uploadRateToDSStdDev;
	}

	public void setUploadRateToDSStdDev(double uploadRateToDSStdDev) {
		this.uploadRateToDSStdDev = uploadRateToDSStdDev;
	}

	public double getDownloadRateFromDSMin() {
		return downloadRateFromDSMin;
	}

	public void setDownloadRateFromDSMin(double downloadRateFromDSMin) {
		this.downloadRateFromDSMin = downloadRateFromDSMin;
	}

	public double getDownloadRateFromDSMean() {
		return downloadRateFromDSMean;
	}

	public void setDownloadRateFromDSMean(double downloadRateFromDSMean) {
		this.downloadRateFromDSMean = downloadRateFromDSMean;
	}

	public double getDownloadRateFromDSStdDev() {
		return downloadRateFromDSStdDev;
	}

	public void setDownloadRateFromDSStdDev(double downloadRateFromDSStdDev) {
		this.downloadRateFromDSStdDev = downloadRateFromDSStdDev;
	}

	public double getDataStoreRTTMin() {
		return dataStoreRTTMin;
	}

	public void setDataStoreRTTMin(double dataStoreRTTMin) {
		this.dataStoreRTTMin = dataStoreRTTMin;
	}

	public double getDataStoreRTTMean() {
		return dataStoreRTTMean;
	}

	public void setDataStoreRTTMean(double dataStoreRTTMean) {
		this.dataStoreRTTMean = dataStoreRTTMean;
	}

	public double getDataStoreRTTStdDev() {
		return dataStoreRTTStdDev;
	}

	public void setDataStoreRTTStdDev(double dataStoreRTTStdDev) {
		this.dataStoreRTTStdDev = dataStoreRTTStdDev;
	}

	public double getInstanceLaunchTimeMin() {
		return instanceLaunchTimeMin;
	}

	public void setInstanceLaunchTimeMin(double instanceLaunchTimeMin) {
		this.instanceLaunchTimeMin = instanceLaunchTimeMin;
	}

	public double getInstanceLaunchTimeMean() {
		return instanceLaunchTimeMean;
	}

	public void setInstanceLaunchTimeMean(double instanceLaunchTimeMean) {
		this.instanceLaunchTimeMean = instanceLaunchTimeMean;
	}

	public double getInstanceLaunchTimeStdDev() {
		return instanceLaunchTimeStdDev;
	}

	public void setInstanceLaunchTimeStdDev(double instanceLaunchTimeStdDev) {
		this.instanceLaunchTimeStdDev = instanceLaunchTimeStdDev;
	}

	public double getReconfigurationSyncTimeMin() {
		return reconfigurationSyncTimeMin;
	}

	public void setReconfigurationSyncTimeMin(double reconfigurationSyncTimeMin) {
		this.reconfigurationSyncTimeMin = reconfigurationSyncTimeMin;
	}

	public double getReconfigurationSyncTimeMean() {
		return reconfigurationSyncTimeMean;
	}

	public void setReconfigurationSyncTimeMean(double reconfigurationSyncTimeMean) {
		this.reconfigurationSyncTimeMean = reconfigurationSyncTimeMean;
	}

	public double getReconfigurationSyncTimeStdDev() {
		return reconfigurationSyncTimeStdDev;
	}

	public void setReconfigurationSyncTimeStdDev(double reconfigurationSyncTimeStdDev) {
		this.reconfigurationSyncTimeStdDev = reconfigurationSyncTimeStdDev;
	}

	public double getInternalStateImageSizeMin() {
		return internalStateImageSizeMin;
	}

	public void setInternalStateImageSizeMin(double internalStateImageSizeMin) {
		this.internalStateImageSizeMin = internalStateImageSizeMin;
	}

	public double getInternalStateImageSizeMean() {
		return internalStateImageSizeMean;
	}

	public void setInternalStateImageSizeMean(double internalStateImageSizeMean) {
		this.internalStateImageSizeMean = internalStateImageSizeMean;
	}

	public double getInternalStateImageSizeStdDev() {
		return internalStateImageSizeStdDev;
	}

	public void setInternalStateImageSizeStdDev(double internalStateImageSizeStdDev) {
		this.internalStateImageSizeStdDev = internalStateImageSizeStdDev;
	}

	public double getCodeImageSizeMin() {
		return codeImageSizeMin;
	}

	public void setCodeImageSizeMin(double codeImageSizeMin) {
		this.codeImageSizeMin = codeImageSizeMin;
	}

	public double getCodeImageSizeMean() {
		return codeImageSizeMean;
	}

	public void setCodeImageSizeMean(double codeImageSizeMean) {
		this.codeImageSizeMean = codeImageSizeMean;
	}

	public double getCodeImageSizeStdDev() {
		return codeImageSizeStdDev;
	}

	public void setCodeImageSizeStdDev(double codeImageSizeStdDev) {
		this.codeImageSizeStdDev = codeImageSizeStdDev;
	}

	public Map<Integer, ResourceVertexMultiset> getCurrentPlacement() {
		return currentPlacement;
	}

	public double getTDmax() {
		return TDmax;
	}

	public void setTDmax(double tDmax) {
		TDmax = tDmax;
	}

	public double getLogAmin() {
		return Math.log(Amin);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		for (Field field : this.getClass().getSuperclass().getDeclaredFields()) {
			field.setAccessible(true);
			String name = field.getName();
			Object value;
			try {
				value = field.get(this);
				sb.append(String.format("%s=%s%n", name, value));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				continue;
			}
		}
		for (Field field : this.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			String name = field.getName();
			Object value;
			try {
				value = field.get(this);
				sb.append(String.format("%s=%s%n", name, value));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				continue;
			}
		}
		
		return sb.toString();
	}

	public double getUploadRateToLocalDSMin() {
		return uploadRateToLocalDSMin;
	}

	public void setUploadRateToLocalDSMin(double uploadRateToLocalDSMin) {
		this.uploadRateToLocalDSMin = uploadRateToLocalDSMin;
	}

	public double getUploadRateToLocalDSMean() {
		return uploadRateToLocalDSMean;
	}

	public void setUploadRateToLocalDSMean(double uploadRateToLocalDSMean) {
		this.uploadRateToLocalDSMean = uploadRateToLocalDSMean;
	}

	public double getUploadRateToLocalDSStdDev() {
		return uploadRateToLocalDSStdDev;
	}

	public void setUploadRateToLocalDSStdDev(double uploadRateToLocalDSStdDev) {
		this.uploadRateToLocalDSStdDev = uploadRateToLocalDSStdDev;
	}

	public double getDownloadRateFromLocalDSMin() {
		return downloadRateFromLocalDSMin;
	}

	public void setDownloadRateFromLocalDSMin(double downloadRateFromLocalDSMin) {
		this.downloadRateFromLocalDSMin = downloadRateFromLocalDSMin;
	}

	public double getDownloadRateFromLocalDSMean() {
		return downloadRateFromLocalDSMean;
	}

	public void setDownloadRateFromLocalDSMean(double downloadRateFromLocalDSMean) {
		this.downloadRateFromLocalDSMean = downloadRateFromLocalDSMean;
	}

	public double getDownloadRateFromLocalDSStdDev() {
		return downloadRateFromLocalDSStdDev;
	}

	public void setDownloadRateFromLocalDSStdDev(double downloadRateFromLocalDSStdDev) {
		this.downloadRateFromLocalDSStdDev = downloadRateFromLocalDSStdDev;
	}
}