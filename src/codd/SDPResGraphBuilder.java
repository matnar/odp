package codd;

import codd.model.*;
import codd.security.ConfigurationGenerator;
import codd.security.NodeLinkSpecs;
import codd.utils.FloydWarshall;

import java.util.HashMap;
import java.util.Map;

import static codd.security.RequirementsBase.*;

public class SDPResGraphBuilder extends ResGraphBuilder {

	private int index;

	private SDPResGraphBuilder() {
		this.index = 0;
	}

	private ResourceVertex newVertex (int regionId, int res, double speedup, double costPerRes) {
		ResourceVertex v = new ResourceVertex(index, res, speedup, 1.0);
		v.setRegionId(regionId);
		v.setPerResourceCost(costPerRes);

		this.index++;
		return v;
	}


	static public SDPResGraphBuilder create()
	{
		SDPResGraphBuilder rg = new SDPResGraphBuilder();

		Map<Integer, ResourceVertex> vRes = new HashMap<Integer, ResourceVertex>();
		Map<Pair, ResourceEdge> eRes = new HashMap<Pair, ResourceEdge>();

		final int edgeNodes = 3;
		final int fogRegions = 2;
		final int fogNodesPerRegion = 2;
		final int cloudRegions = 2;
		final int cloudNodesForRegion = 4;

		//final double EDGE_COST = 0.0;
		//final double FOG_COST = 0.0;
		//final double CLOUD_COST = 0.0;
		final double EDGE_COST = 1.5;
		final double FOG_COST = 0.7;
		final double CLOUD_COST = 0.1;

		final int EDGE_RES = 2;
		final int FOG_RES = 2;
		final int CLOUD_RES = 4;

		final double EDGE_SPEEDUP = 0.8;
		final double FOG_SPEEDUP = 0.9;
		final double CLOUD_SPEEDUP = 1.00;

		/* 1. Create ResourceVertex to represent nodes (supervisors, set of worker slots) */
		for(int i = 0; i < edgeNodes; i++){
			ResourceVertex v = rg.newVertex(i, EDGE_RES, EDGE_SPEEDUP, EDGE_COST);
			vRes.put(v.getIndex(), v);
		}
		for(int i = 0; i < fogRegions; i++){
			for (int j = 0; j<fogNodesPerRegion; j++) {
				ResourceVertex v = rg.newVertex(edgeNodes + i, FOG_RES, FOG_SPEEDUP, FOG_COST);
				vRes.put(v.getIndex(), v);
			}
		}
		for(int i = 0; i < cloudRegions; i++){
			for (int j = 0; j<cloudNodesForRegion; j++) {
				ResourceVertex v = rg.newVertex(edgeNodes + fogRegions + i, CLOUD_RES, CLOUD_SPEEDUP, CLOUD_COST);
				vRes.put(v.getIndex(), v);
			}
		}

		/*
		 * Compute latencies.
		 * Edge regions: 0,1,2
		 * Fog regions: 3,4
		 * Cloud regions: 5,6
		 */
		FloydWarshall fw = new FloydWarshall(edgeNodes + fogRegions + cloudRegions);
		for (int e1 = 0; e1 < edgeNodes; ++e1)	 {
			for (int e2 = 0; e2 < edgeNodes; ++e2) {
				if (e1 != e2) {
					fw.addEdge(e1, e2, 0.1);
				}
			}
		}
		fw.addEdge(3,5,5);
		fw.addEdge(4,5,7);
		fw.addEdge(5,6,5);
		fw.addEdge(3,6,7);
		fw.addEdge(4,6,5);
		fw.addEdge(0,3,3);
		fw.addEdge(0,4,2);
		fw.addEdge(1,3,1);
		fw.addEdge(1,4,2);
		fw.addEdge(2,3,2);
		fw.addEdge(2,4,2);
		fw.addEdge(3,4, 4);

		double regLatency[][] = fw.floydWarshall();

		for (ResourceVertex u : vRes.values()){
			for (ResourceVertex v : vRes.values()){
				int uReg = u.getRegionId();
				int vReg = v.getRegionId();

				double bw, dataCost;
				int latency;
				if (u.equals(v)) {
					bw = Math.pow(10, 10);
					latency = 0;
					dataCost = 0.0;
				} else if (uReg == vReg) {
					bw = Math.pow(10, 9);
					latency = 0;
					dataCost = 0.0;
				} else {
					latency = (int) (regLatency[uReg][vReg]);
					bw = Math.pow(10, 7);
					dataCost = 0.00000000002;
				}

				ResourceEdge rEdge = new ResourceEdge(u.getIndex(), v.getIndex(), bw, latency, 1.0, dataCost);
				eRes.put(new Pair(u.getIndex(), v.getIndex()), rEdge);
			}
		}


		/* 3. Creating object DspGraph */
		String graphName = "resources";
		rg.resGraph = new ResourceGraph(graphName, vRes, eRes);

		rg.defineNodeAndLinksSecuritySpecs();
		return rg;
	}


	protected void defineNodeAndLinksSecuritySpecs()
	{
		NodeLinkSpecs baseNode = new NodeLinkSpecs();
		baseNode.addForNode(RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "None", 1.0, 1.0, 1.0);
		baseNode.addForNode(RC_PROCESS_ISOLATION, RO_MULTITENANCY, Boolean.TRUE, 1.0, 1.0, 1.0);

		for (ResourceVertex v : resGraph.getVertices().values()) {
			NodeLinkSpecs specs = new NodeLinkSpecs(baseNode);

			int regId = v.getRegionId();
			if (regId < 3) {
				/* edge */
				specs.addForNode(RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "Container", 1.0, 0.8, 1.0);
				//specs.addForNode(RC_PROCESS_ISOLATION, RO_MULTITENANCY, Boolean.TRUE, 1.0, 1.0, 1.0);
			} else if (regId < 5) {
				/* fog */
				specs.addForNode(RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "Container", 1.0, 0.95, 1.0);
				specs.addForNode(RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "VM", 1.0, 0.93, 1.0);
				specs.addForNode(RC_PROCESS_ISOLATION, RO_MULTITENANCY, Boolean.FALSE, 1.0, 1.0, 1.0);
			} else {
				/* cloud */
				specs.addForNode(RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "Container", 1.0, 0.98, 1.0);
				specs.addForNode(RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "VM", 1.0, 0.95, 1.0);
				specs.addForNode(RC_PROCESS_ISOLATION, RO_MULTITENANCY, Boolean.FALSE, 1.0, 1.0, 1.0);
			}

			v.setSecuritySpecs(specs);
		}

		NodeLinkSpecs baseLink = new NodeLinkSpecs();
		baseLink.addForEdge(RC_NETWORK, RO_ENCRYPT_TRAFFIC, Boolean.FALSE, 1.0, 1.0, 1.0 );
		baseLink.addForEdge(RC_NETWORK, RO_ENCRYPT_TRAFFIC, Boolean.TRUE, 1.1, 1.0, 1.0 );

		for (ResourceEdge e : getGraph().getEdges().values()) {
			ResourceVertex v1 = getGraph().getVertices().get(e.getFrom());
			ResourceVertex v2 = getGraph().getVertices().get(e.getTo());

			NodeLinkSpecs specs = new NodeLinkSpecs(baseLink);

			if (v1.getRegionId() < 3 && v2.getRegionId() < 3) {
				/* edge */
				specs.addForEdge(RC_NETWORK, RO_WIRED_CONNECTIVITY, Boolean.FALSE, 1.0, 1.0, 1.0);
			} else {
				specs.addForEdge(RC_NETWORK, RO_WIRED_CONNECTIVITY, Boolean.TRUE, 1.0, 1.0, 1.0);
			}

			e.setSecuritySpecs(specs);
		}
	}

	public void printGraph (boolean alsoEdges, boolean withConfs){

		super.printGraph(alsoEdges);

		if (withConfs) {
			for (ResourceVertex v : getGraph().getVertices().values()) {
				System.out.print(v.getIndex() + ": ");
				System.out.println(ConfigurationGenerator.getAllConfigurations(v.getSecuritySpecs()));
			}

			if (alsoEdges) {
				for (ResourceEdge e : getGraph().getEdges().values()) {
					System.out.print(e.getFrom() + "," + e.getTo() + ": ");
					System.out.println(ConfigurationGenerator.getAllConfigurations(e.getSecuritySpecs()));
				}
			}
		}

	}
}
