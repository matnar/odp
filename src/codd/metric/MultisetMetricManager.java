package codd.metric;

import codd.model.DspEdge;
import codd.model.DspGraph;
import codd.model.DspVertex;
import codd.model.Pair;
import codd.model.ResourceEdge;
import codd.model.ResourceGraph;
import codd.model.ResourceVertex;
import codd.model.ResourceVertexMultiset;

public class MultisetMetricManager {

	protected DspGraph dspGraph;
	protected ResourceGraph resGraph;

	protected double INVALID = -100000;
	protected boolean useMM1;
	
	public MultisetMetricManager(DspGraph dspGraph, ResourceGraph resGraph, boolean useMM1) {
		super();
		
		this.dspGraph = dspGraph;
		this.resGraph = resGraph;
		this.useMM1 = useMM1;
		
	}

	public double getNetworkDelay(int iDsp, int jDsp, ResourceVertexMultiset uResMultiset, ResourceVertexMultiset vResMultiset){
		
		double maxDuv = 0;

		for (ResourceVertex uRes : uResMultiset.getVertices()){
			
			for (ResourceVertex vRes : vResMultiset.getVertices()){

				Pair uvPair = new Pair(uRes.getIndex(), vRes.getIndex());
				ResourceEdge uv = resGraph.getEdges().get(uvPair);
				double Duv = uv.getDelay();
				
				if (Duv > maxDuv)
					maxDuv = Duv;
				
			}
			
		}
		
		return maxDuv;
	}
	
	public double getCost(int iDsp, int jDsp, ResourceVertexMultiset uResMultiset, ResourceVertexMultiset vResMultiset){

		double totalCUV = 0;
		
		for (ResourceVertex uRes : uResMultiset.getVertices()){
			for (ResourceVertex vRes : vResMultiset.getVertices()){

				// do not consider self-loops
				if (uRes.getIndex() == vRes.getIndex())
					continue;
				
				Pair uvPair = new Pair(uRes.getIndex(), vRes.getIndex());
				ResourceEdge uv = resGraph.getEdges().get(uvPair);
		
				totalCUV += getLambda(iDsp, jDsp, uResMultiset, vResMultiset) * uv.getCostPerUnitData();
		
			}
			
		}
		
		return totalCUV;
		
	}
	
	public double getLambda(int iDsp, int jDsp, ResourceVertexMultiset uResMultiset, ResourceVertexMultiset vResMultiset){
		
		Pair ijPair = new Pair(iDsp, jDsp);
		DspEdge ij = dspGraph.getEdges().get(ijPair);
		
		return (ij.getLambda() / ((double) (uResMultiset.getCardinality() * vResMultiset.getCardinality())));
	}
	
	
	
	public double getExecutionTime(int iDsp, ResourceVertexMultiset uResMultiset){
		
		double maxRiu = INVALID;
		
		double Ri_repl = getExecutionTimeRi(iDsp, uResMultiset);
		
		for (ResourceVertex uRes : uResMultiset.getVertices()){
			
			double Riu = Ri_repl / (double) uRes.getSpeedup();
			
			if (Riu > maxRiu)
				maxRiu = Riu;
			
		}
		
		if (maxRiu == INVALID)
			throw new RuntimeException("Invalid execution time for op #" + iDsp + " on ms #" + uResMultiset.getIndex());
		
		return maxRiu;
	}
	
	public double getCost(int iDsp, ResourceVertexMultiset uResMultiset){

		int cardinality = uResMultiset.getCardinality();
		DspVertex i = dspGraph.getVertices().get(iDsp);
		return ((double) i.getRequiredResources() * i.getCostPerResource() * (double) cardinality);
		
	}
	
	/*
	 * We assume that a serving node that hosts an operator 
	 * behaves like an M/M/1 queue.
	 */
	private double getExecutionTimeRi(int iDsp, ResourceVertexMultiset uResMultiset){
		
		double lambda = dspGraph.getIncomingDataRate(iDsp);
		
		if (!useMM1){
			return dspGraph.getVertices().get(iDsp).getExecutionTime();
		}
		
		double uSize = (double) uResMultiset.getCardinality();
		
		DspVertex i = dspGraph.getVertices().get(iDsp);

		/*
		 * TODO
		 * I intercept here response time computation and, if possible,
		 * I use a user-defined function to compute the response time
		 * instead of MM1 model.
		 * 
		 * Maybe we could move the logic in the DspVertex itself.
		 * Or, make the MM1 a ResponseTimeProvider as well.
		 * 
		 * This code should probably be refactored anyway.
		 */
		if (i.getResponseTimeProvider() != null) {
			double r = i.getResponseTimeProvider().getResponseTime(lambda, uResMultiset.getCardinality());
			//System.out.println(String.format("f(%f,  %d)=%f",lambda, uResMultiset.getCardinality(), r));
			return r;
		}

		
		if (i.getServiceRate() <= lambda/uSize) {
			//System.out.println("[WARN] lambda >= service rate");
			return Double.MAX_VALUE;
		}
		
		double mm1RespTime = 1.0 / (i.getServiceRate() - (lambda / uSize));
	
		/*
		if (mm1RespTime <= 0 || Double.isInfinite(mm1RespTime) || Double.isNaN(mm1RespTime))
			mm1RespTime = INVALID;
		*/

		return mm1RespTime;
	}

}
