package codd.metric;

import java.util.Map;

import codd.model.DspGraph;
import codd.model.DspVertex;
import codd.model.ResourceGraph;
import codd.model.ResourceVertex;
import codd.model.ResourceVertexMultiset;

/**
 * Manager for reconfiguration-related and multiset metrics.
 */
public class ReconfigurationMetricsManager extends MultisetMetricManager {

	protected Map<Integer, ResourceVertexMultiset> currentPlacement;

	static private final boolean DEBUG = false;

	public ReconfigurationMetricsManager(DspGraph dspGraph, ResourceGraph resGraph, boolean useMM1,
			Map<Integer, ResourceVertexMultiset> currentPlacement) {
		super(dspGraph, resGraph, useMM1);
		this.currentPlacement = currentPlacement;
	}

	/**
	 * Computes downtime due to reconfiguration for @vDsp being deployed
	 * on @ums.
	 * 
	 * @param vDsp
	 *            DSP operator
	 * @param vms
	 *            Deployment multiset
	 * @return Downtime (in ms) for given configuration
	 */
	public double getOperatorDowntime(DspVertex vDsp, ResourceVertexMultiset vms) {
		ResourceVertexMultiset ums = currentMultiset(vDsp);
		double downtime = 0.0;

		if (!ums.equals(vms)) {
			/* A reconfiguration is needed. */
			downtime += dspGraph.getReconfigurationSyncTime();

			if (ums.getCardinality() == vms.getCardinality())
				downtime += getDowntimeDueToRelocations(vDsp, ums, vms);
			else if (ums.getCardinality() > vms.getCardinality())
				downtime += getDowntimeDueToScalingIn(vDsp, ums, vms);
			else
				downtime += getDowntimeDueToScalingOut(vDsp, ums, vms);
		}
		
		if (DEBUG)
			System.out.println(String.format("Down #%d %d->%d = %f", vDsp.getIndex(), ums.getIndex(), vms.getIndex(), downtime));

		return downtime;
	}

	/**
	 * Computes downtime due to relocations.
	 * 
	 * @param vDsp
	 *            DSP operator
	 * @param ums
	 *            Current deployment multiset
	 * @param vms
	 *            Future deployment multiset
	 * @return Downtime (in ms)
	 */
	private double getDowntimeDueToRelocations(DspVertex vDsp, ResourceVertexMultiset ums, ResourceVertexMultiset vms) {
		double downtime = 0.0;

		for (ResourceVertex u : ums.getVertices()) {
			for (ResourceVertex v : vms.getVertices()) {
				if (u.equals(v))
					continue;
				downtime = Math.max(downtime, getDowntimeDueToRelocations(vDsp, ums, vms, u, v));
			}
		}

		return downtime;
	}

	/**
	 * Computes downtime due to relocations from @u to @v.
	 * 
	 * @param vDsp
	 *            DSP operator
	 * @param ums
	 *            Current deployment multiset
	 * @param vms
	 *            Future deployment multiset
	 * @param u
	 *            Node in @ums
	 * @param v
	 *            Node in @vms
	 * @return Downtime (in ms)
	 */
	private double getDowntimeDueToRelocations(DspVertex vDsp, ResourceVertexMultiset ums, ResourceVertexMultiset vms,
			ResourceVertex u, ResourceVertex v) {
		double downtime = 0.0;

		/* Adds time for downloading code from DS. */
		if (!ums.getVertices().contains(v)) {
			downtime = vDsp.getCodeImageSize() / v.getDownloadRateFromDS() + v.getDataStoreRTT();
		}

		/* Computes max of time for downloading code and uploading state. */
		double stateUpload = (double) deltaIn(u, ums, vms) / (double) ums.getCardinality()
				* vDsp.getInternalStateImageSize();
		stateUpload /= u.getUploadRateToDS();
		stateUpload += u.getDataStoreRTT();
		downtime = Math.max(downtime, stateUpload);

		/* Adds time for downloading state images. */
		downtime += deltaOut(v, ums, vms) * vDsp.getInternalStateImageSize() / (double) ums.getCardinality()
				/ v.getDownloadRateFromDS() + v.getDataStoreRTT();

		/* Adds overhead for launching new instances. */
		if (deltaOut(v, ums, vms) > 0) {
			downtime += v.getInstanceLaunchTime();
		}

		return downtime;
	}

	/**
	 * Computes downtime due to scaling in.
	 * 
	 * @param vDsp
	 *            DSP operator
	 * @param ums
	 *            Current deployment multiset
	 * @param vms
	 *            Future deployment multiset
	 * @return Downtime (in ms)
	 */
	private double getDowntimeDueToScalingIn(DspVertex vDsp, ResourceVertexMultiset ums, ResourceVertexMultiset vms) {
		double downtime = 0.0;

		for (ResourceVertex u : ums.getVertices()) {
			for (ResourceVertex v : vms.getVertices()) {
				if (u.equals(v))
					continue;
				downtime = Math.max(downtime, getDowntimeDueToScalingIn(vDsp, ums, vms, u, v));
			}
		}

		return downtime;
	}

	/**
	 * Computes downtime due to scaling in on the reconfiguration path from @u
	 * to @v.
	 * 
	 * @param vDsp
	 *            DSP operator
	 * @param ums
	 *            Current deployment multiset
	 * @param vms
	 *            Future deployment multiset
	 * @param u
	 *            Node in @ums
	 * @param v
	 *            Node in @vms
	 * @return Downtime (in ms)
	 */
	private double getDowntimeDueToScalingIn(DspVertex vDsp, ResourceVertexMultiset ums, ResourceVertexMultiset vms,
			ResourceVertex u, ResourceVertex v) {
		double downtime = 0.0;

		/* Adds time for downloading code from DS. */
		if (!ums.getVertices().contains(v)) {
			downtime = vDsp.getCodeImageSize() / v.getDownloadRateFromDS() + v.getDataStoreRTT();
		}

		/* Computes max of time for downloading code and uploading state. */
		double stateUpload = (double) deltaIn(u, ums, vms) / (double) ums.getCardinality()
				* vDsp.getInternalStateImageSize();
		stateUpload /= u.getUploadRateToDS();
		stateUpload += u.getDataStoreRTT();
		downtime = Math.max(downtime, stateUpload);

		/* Adds time for downloading state images. */
		double cV = (double) vms.getCardinality();
		double cU = (double) ums.getCardinality();
		double stateDownload = (double) deltaOut(v, ums, vms) / cV;
		stateDownload += Math.min(ums.getMultiplicity(v), vms.getMultiplicity(v)) * (cU - cV) / (cU * cV);
		downtime += stateDownload * vDsp.getInternalStateImageSize() / v.getDownloadRateFromDS() + v.getDataStoreRTT();

		/* Adds overhead for launching new instances. */
		if (deltaOut(v, ums, vms) > 0) {
			downtime += v.getInstanceLaunchTime();
		}

		return downtime;
	}

	/**
	 * Computes downtime due to scaling out.
	 * 
	 * @param vDsp
	 *            DSP operator
	 * @param ums
	 *            Current deployment multiset
	 * @param vms
	 *            Future deployment multiset
	 * @return Downtime (in ms)
	 */
	private double getDowntimeDueToScalingOut(DspVertex vDsp, ResourceVertexMultiset ums, ResourceVertexMultiset vms) {
		double downtime = 0.0;

		for (ResourceVertex u : ums.getVertices()) {
			for (ResourceVertex v : vms.getVertices()) {
				if (u.equals(v))
					continue;
				downtime = Math.max(downtime, getDowntimeDueToScalingOut(vDsp, ums, vms, u, v));
			}
		}

		if (DEBUG)
			System.out.println(String.format("DownSO #%d %d->%d = %f", vDsp.getIndex(), ums.getIndex(), vms.getIndex(), downtime));


		return downtime;
	}

	/**
	 * Computes downtime due to scaling out on the reconfiguration path from @u
	 * to @v.
	 * 
	 * @param vDsp
	 *            DSP operator
	 * @param ums
	 *            Current deployment multiset
	 * @param vms
	 *            Future deployment multiset
	 * @param u
	 *            Node in @ums
	 * @param v
	 *            Node in @vms
	 * @return Downtime (in ms)
	 */
	private double getDowntimeDueToScalingOut(DspVertex vDsp, ResourceVertexMultiset ums, ResourceVertexMultiset vms,
			ResourceVertex u, ResourceVertex v) {
		double downtime = 0.0;

		/* Adds time for downloading code from DS. */
		if (!ums.getVertices().contains(v)) {
			downtime = vDsp.getCodeImageSize() / v.getDownloadRateFromDS() + v.getDataStoreRTT();
		}

		/* Computes max of time for downloading code and uploading state. */
		double stateUpload = (double) deltaIn(u, ums, vms) / (double) ums.getCardinality();
		int cV = vms.getCardinality();
		int cU = ums.getCardinality();
		stateUpload += ums.getMultiplicity(u) * ((double) (cV - cU) / (double) (cV * cU));
		stateUpload *= vDsp.getInternalStateImageSize() / u.getUploadRateToDS();
		stateUpload += u.getDataStoreRTT();
		downtime = Math.max(downtime, stateUpload);

		/* Adds time for downloading state images. */
		downtime += deltaOut(v, ums, vms) * vDsp.getInternalStateImageSize() / (double) ums.getCardinality()
				/ v.getDownloadRateFromDS() + v.getDataStoreRTT();

		/* Adds overhead for launching new instances. */
		if (deltaOut(v, ums, vms) > 0) {
			downtime += v.getInstanceLaunchTime();
		}

		return downtime;
	}

	/**
	 * Returns V(u)-U(u) if it is not positive, otherwise 0.
	 */
	private int deltaIn(ResourceVertex u, ResourceVertexMultiset ums, ResourceVertexMultiset vms) {
		int delta = vms.getMultiplicity(u) - ums.getMultiplicity(u);
		return (delta <= 0) ? delta : 0;
	}

	/**
	 * Returns V(u)-U(u) if it is not negative, otherwise 0.
	 */
	private int deltaOut(ResourceVertex u, ResourceVertexMultiset ums, ResourceVertexMultiset vms) {
		int delta = vms.getMultiplicity(u) - ums.getMultiplicity(u);
		return (delta >= 0) ? delta : 0;
	}

	/**
	 * Returns multiset which @iDsp is currently deployed on.
	 */
	public ResourceVertexMultiset currentMultiset(DspVertex vDsp) {
		return currentPlacement.get(vDsp.getIndex());
	}

}
