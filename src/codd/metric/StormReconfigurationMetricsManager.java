package codd.metric;

import java.util.Map;

import codd.model.DspGraph;
import codd.model.DspVertex;
import codd.model.ResourceGraph;
import codd.model.ResourceVertex;
import codd.model.ResourceVertexMultiset;

/**
 * Manager for reconfiguration-related and multiset metrics, adjusted for Apache
 * Storm implementation.
 */
public class StormReconfigurationMetricsManager extends ReconfigurationMetricsManager {

	static private final boolean DEBUG = false;

	public StormReconfigurationMetricsManager(DspGraph dspGraph, ResourceGraph resGraph, boolean useMM1,
			Map<Integer, ResourceVertexMultiset> currentPlacement) {
		super(dspGraph, resGraph, useMM1, currentPlacement);
	}

	/**
	 * Computes downtime due to reconfiguration for @vDsp being deployed
	 * on @ums.
	 * 
	 * @param vDsp
	 *            DSP operator
	 * @param vms
	 *            Deployment multiset
	 * @return Downtime (in ms) for given configuration
	 */
	@Override
	public double getOperatorDowntime(DspVertex vDsp, ResourceVertexMultiset vms) {
		ResourceVertexMultiset ums = currentMultiset(vDsp);
		double downtime = 0.0;

		if (!ums.equals(vms)) {
			/* A reconfiguration is needed. */
			/* Max over (u,v) pairs ... */
			for (ResourceVertex u : currentMultiset(vDsp).getVertices()) {
				for (ResourceVertex v : vms.getVertices()) {
					if (u.equals(v))
						continue;

					downtime = Math.max(downtime, getDowntimeDueToReconfiguration(vDsp, ums, vms, u, v));
				}
			}

			downtime += dspGraph.getReconfigurationSyncTime();
		}

			//System.out.println( String.format("Down #%d %d->%d = %f", vDsp.getIndex(), ums.getIndex(), vms.getIndex(), downtime));

		return downtime;
	}

	/**
	 * Computes downtime due to reconfiguration from @u to @v.
	 * 
	 * @param vDsp
	 *            DSP operator
	 * @param ums
	 *            Current deployment multiset
	 * @param vms
	 *            Future deployment multiset
	 * @param u
	 *            Node in @ums
	 * @param v
	 *            Node in @vms
	 * @return Downtime (in ms)
	 */
	private double getDowntimeDueToReconfiguration(DspVertex vDsp, ResourceVertexMultiset ums,
			ResourceVertexMultiset vms, ResourceVertex u, ResourceVertex v) {
		double downtime = 0.0;

		/* Adds time for downloading code from DS, if v was not used in current placement. */
		boolean needCode = true;
		for (ResourceVertexMultiset ms : currentPlacement.values()) {
			if (ms.getVertices().contains(v)) {
				needCode = false;
				break;
			}
		}
		if (needCode) {
			downtime = vDsp.getCodeImageSize() / v.getDownloadRateFromDS() + v.getDataStoreRTT();
		}

		/* Adds time for uploading state. */
		double stateUpload = vDsp.getInternalStateImageSize() / (double) ums.getCardinality() * ums.getMultiplicity(u)
				/ u.getUploadRateToDS() + u.getDataStoreRTT();
		downtime += stateUpload;

		/* Adds time for downloading state images. */
		downtime += vms.getMultiplicity(v) * vDsp.getInternalStateImageSize() / (double) vms.getCardinality()
				/ v.getDownloadRateFromDS() + v.getDataStoreRTT();

		/* Adds overhead for launching new instances. */
		downtime += vms.getMultiplicity(v) * v.getInstanceLaunchTime();

		return downtime;
	}

	public double getPauseAndResumeDowntime(DspVertex vDsp) {
		double downtime = 0.0;

		for (ResourceVertex u : currentMultiset(vDsp).getVertices()) {
			downtime = Math.max(downtime, getPauseAndResumeDowntime(vDsp, u));
		}
		
		downtime += dspGraph.getReconfigurationSyncTime();
		
		return downtime;
	}

	protected double getPauseAndResumeDowntime(DspVertex vDsp, ResourceVertex u) {
		double downtime = 0.0;
		ResourceVertexMultiset ums = currentMultiset(vDsp);

		/* Adds time for uploading state. */
		double stateUpload = vDsp.getInternalStateImageSize() / (double) ums.getCardinality() * ums.getMultiplicity(u)
				/ u.getUploadRateToLocalDS();
		downtime += stateUpload;

		/* Adds time for downloading state images. */
		downtime += ums.getMultiplicity(u) * vDsp.getInternalStateImageSize() / (double) ums.getCardinality()
				/ u.getDownloadRateFromLocalDS();

		/* Adds overhead for launching new instances. */
		downtime += ums.getMultiplicity(u) * u.getInstanceLaunchTime();

		return downtime;
	}

}
