package codd.surrogate;

import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.distance.ObjectiveFunctionDistanceStrategy;
import codd.distance.PairwiseDistanceStrategy;
import codd.model.Pair;
import codd.model.ResourceEdge;
import codd.model.ResourceGraph;
import codd.model.ResourceVertex;
import codd.utils.KMeans;
import codd.utils.kmeans.Cluster;
import codd.utils.kmeans.Point;
import codd.utils.vivaldi.VivaldiAlgorithm;

import java.util.*;

public class SurrogateHierarchicalResourceGraphBuilder {

	private static final double VIVALDI_MAX_ERROR = 0.18;

	protected ODDParameters params;
	private ResourceGraph originalResGraph;

	public SurrogateHierarchicalResourceGraphBuilder(
			ResourceGraph originalResGraph, ODDParameters params) {
		this.params = params;
		this.originalResGraph = originalResGraph;
	}

	public ResourceGraph create(ResourceGraph resGraph, int groupingFactor)
			throws ODDException {

		// determine the number of levels
		if (groupingFactor == 1)
			throw new ODDException("Invalid Grouping Factor");

		int graphSize = resGraph.getVertices().values().size();
		int levels = (int) Math.round(Math.log(graphSize)
				/ Math.log(groupingFactor));
		int numSurGraph = levels - 1;

		// DEBUG:
//		System.out.println("Graph Size: " + graphSize + "; levels: " + levels);

		if (numSurGraph <= 0) {
//			System.out.println("Num of sur graph <= 0");
			return resGraph;
		}

//		//  print surrogate graph
//		System.out.println("RES Graph, grouping factor");
//		System.out.println(resGraph.getHierachyLevel());
//		System.out.println("* Vertices ");
//		for (ResourceVertex v : resGraph.getVertices().values()) {
//			System.out.println("   " + v.toString());
//		}
//		System.out.println();
//		System.out.println("* Edges: " + resGraph.getEdges().size());
//		for (ResourceEdge e : resGraph.getEdges().values()) {
//			System.out.println("   " + e.toString());
//		}

		while (numSurGraph > 0) {

			int groupToCreate = (int) Math.pow(groupingFactor, numSurGraph);
//			System.out.println("Create " + groupToCreate + " group.");

			resGraph = createSurrogates(resGraph, groupToCreate);

//			System.out.println(resGraph.getHierachyLevel());
//			// print surrogate graph
//			System.out.println("Surrogate RES Graph, grouping factor");
//			System.out.println("* Vertices ");
//			for (ResourceVertex v : resGraph.getVertices().values()) {
//				System.out.println("   " + v.toString());
//			}
//			System.out.println();
//			System.out.println("* Edges: " + resGraph.getEdges().size());
//			for (ResourceEdge e : resGraph.getEdges().values()) {
//				System.out.println("   " + e.toString());
//			}

			numSurGraph--;
		}

//		 System.out.println(resGraph.getHierachyLevel());

		return resGraph;

	}

	public ResourceGraph unfold(ResourceGraph resGraph, Set<Integer> nodes) {

//		System.out.println("Unfolding: Hierarchy level: " + resGraph.getHierachyLevel());

		if (nodes.size() == 0)
			return null;

		if (resGraph.getHierachyLevel() == 0)
			return resGraph;

		int lowerHierarchyLevel = resGraph.getHierachyLevel() - 1;
		ResourceGraph lowerLevelGraph = resGraph.getLowLevelGraph();
		ResourceGraph twoLowerLevelsGraph;

		if (lowerHierarchyLevel > 0) {
			twoLowerLevelsGraph = lowerLevelGraph.getLowLevelGraph();
		} else {
			twoLowerLevelsGraph = null;
		}

		Set<ResourceVertex> nodesToExplode = new HashSet<>();
		for (Integer i : nodes) {
			nodesToExplode.add(resGraph.getVertices().get(i));
		}

		if (nodesToExplode.size() == 1) {
			ResourceVertex ures = nodesToExplode.iterator().next();
			ResourceGraph irg = ures.getInnerResourceGraph();
			irg.setHierachyLevel(lowerHierarchyLevel);
			irg.setLowLevelGraph(twoLowerLevelsGraph);
			return irg;
		}

		// More than a single node to explode;
		// we should create the edges between surrogate nodes

		// 1. Create the data struct for the lowLevelGraph nodes
		Iterator<ResourceVertex> it = nodesToExplode.iterator();
		Map<Integer, ResourceVertex> lowerLevelNodes = new HashMap<>();
		while (it.hasNext()) {
			ResourceVertex nodeToExplode = it.next();
			for (ResourceVertex v : nodeToExplode.getInnerResourceGraph()
					.getVertices().values()) {
				lowerLevelNodes.put(v.getIndex(), v);
			}
		}

		// edges
		Map<Pair, ResourceEdge> hEdges = new HashMap<>();

		if (lowerHierarchyLevel > 0) {
			hEdges = createSurrogateEdges(twoLowerLevelsGraph, lowerLevelNodes);
		} else {

			// we ended in the real graph, no surrogate edges to be created.

			for (ResourceVertex uRes : lowerLevelNodes.values()) {
				for (ResourceVertex vRes : lowerLevelNodes.values()) {
					Pair p = new Pair(uRes.getIndex(), vRes.getIndex());

					ResourceEdge e = lowerLevelGraph.getEdges().get(p);

					if (e == null)
						continue;

					hEdges.put(p, e);
				}
			}
		}

		// XXX: riscrivere gli ID dei nodi

		ResourceGraph surrogateGraph = new ResourceGraph("surrogate",
				lowerLevelNodes, hEdges);
		surrogateGraph.setHierachyLevel(lowerHierarchyLevel);
		surrogateGraph.setLowLevelGraph(twoLowerLevelsGraph);
		return surrogateGraph;
	}

	public int locateResNode(ResourceGraph resGraph, int uRes) {

		if (resGraph.getHierachyLevel() == 0)
			return uRes;

		for (ResourceVertex surRes : resGraph.getVertices().values()) {
			Set<Integer> nodes = retrieveAllSubsumedNodes(surRes.getInnerResourceGraph());
			
			if (nodes.contains(uRes)) {
				return surRes.getIndex();
			}
		}

		return -1;
	}

	private Set<Integer> retrieveAllSubsumedNodes(ResourceGraph resGraph) {

		Set<Integer> nodes = new HashSet<>();
		for (ResourceVertex surRes : resGraph.getVertices().values()) {

			if (surRes.isSurrogateNode()){
				nodes.addAll(retrieveAllSubsumedNodes(surRes.getInnerResourceGraph()));
			} else {
				nodes.add(surRes.getIndex());
			}
		}

		return nodes;

	}

	private ResourceGraph createSurrogates(ResourceGraph resGraph,
			int groupingFactor) {

		/* 1. Determine groups of nodes */

		/*
		 * Vivaldi computes coordinates for each node; We run Vivaldi using the
		 * ObjectiveFunctionDistanceStrategy
		 */
//		System.out.println("Creating surrogate graph: vivaldi...");
		PairwiseDistanceStrategy distanceFunction = new ObjectiveFunctionDistanceStrategy(
				resGraph, params);
		VivaldiAlgorithm vivaldi = new VivaldiAlgorithm(resGraph, params,
				distanceFunction);
		List<Point> vivaldiNodes = vivaldi
				.computeCoordinates(VIVALDI_MAX_ERROR);
//		System.out.println("Creating surrogate graph: coordinate computed with vivaldi!");

		/* Create groups of nodes with the K-Means clustering algorithm */
//		System.out.println("Creating surrogate graph: computing clusters...");
		KMeans km = new KMeans(vivaldiNodes);
		List<Cluster> clusters = km.calculate(groupingFactor);
//		System.out.println("Creating surrogate graph: clusters computed: " + clusters.size());

		/* 2. Determine the hierarchical surrogate graph */
//		System.out.println("Creating surrogate nodes ...");
		Map<Integer, ResourceVertex> hVertexs = new HashMap<>();
		Map<Pair, ResourceEdge> hEdges;

		/* Create the surrogate nodes */
		int i = getMaxVertexId(resGraph) + 1; // 0;
		for (Cluster cluster : clusters) {
//			System.out.println("Creating surrogate nodes: cluster "
//					+ cluster.getId() + ": " + cluster.getCentroid().toString());
			List<Integer> nodesId = getResourceNodesId(cluster);
			if (nodesId == null || nodesId.isEmpty()) {
				System.out.println(" - node id null or empty");
				continue;
			}

//			System.out.println("Creating surrogate nodes: extracting sub-graph");
			ResourceGraph resExcerpt = ResGraphBuilder.extractSubGraph(resGraph, nodesId);

//			System.out.println("Creating surrogate nodes: sub-graph extracted: " + resExcerpt.getVertices().values().size());
			ResourceVertex sRes = createHierarchicalVertex(i, resExcerpt);
//			System.out.println("Hierarchical vertex created!");
			hVertexs.put(i, sRes);
			i++;
		}

		/* Create the surrogate edges */
//		System.out.println("Creating surrogate edges:");
		hEdges = createSurrogateEdges(resGraph, hVertexs);
//		System.out.println("Creating surrogate edges: created!");

		ResourceGraph surrogateGraph = new ResourceGraph("surrogate", hVertexs,
				hEdges);

		surrogateGraph.setHierachyLevel(resGraph.getHierachyLevel() + 1);
		surrogateGraph.setLowLevelGraph(resGraph);
		return surrogateGraph;

	}

	private int getMaxVertexId(ResourceGraph resGraph) {
		int max = 0;
		for (Integer u : resGraph.getVertices().keySet()) {
			if (u > max)
				max = u;
		}
		return max;
	}

	private List<Integer> getResourceNodesId(Cluster cluster) {

		if (cluster == null || cluster.getPoints() == null)
			return null;

		List<Integer> nodes = new ArrayList<>();

		for (Point p : cluster.getPoints()) {
			if (p.getNode() != null) {
				nodes.add(p.getNode().getIndex());
			}
		}

		return nodes;
	}

	/**
	 * Create a surrogate resource vertex which represents a sub-graph of the
	 * resource graph.
	 * 
	 * We use the following aggregate functions: - availableResources = SUM
	 * availableResources for each node - speedup = AVG speedup for each node -
	 * availability = AVG availability for each node
	 */
	private ResourceVertex createHierarchicalVertex(int surrogateNodeId,
			ResourceGraph resExcerpt) {

		int sAvailableResources = 0;
		double sSpeedup = 0;
		double sAvailability = 0;
		int sNodes = 0;

		for (ResourceVertex uRes : resExcerpt.getVertices().values()) {
			sAvailableResources += uRes.getAvailableResources();
			sSpeedup += uRes.getSpeedup();
			sAvailability += uRes.getAvailability();

			sNodes++;
		}

		sSpeedup = sSpeedup / (double) sNodes;
		sAvailability = sAvailability / (double) sNodes;

		ResourceVertex sRes = new ResourceVertex(surrogateNodeId,
				sAvailableResources, sSpeedup, sAvailability);
		sRes.setInnerResourceGraph(resExcerpt);

		return sRes;
	}

	/**
	 * Create the surrogate resource edges for the hierarchical graph. An edge
	 * is created between every pair of surrogate resource vertexes. Each
	 * resource vertex has also a self-loop surrogate resource edge.
	 * 
	 * We use the following aggregate functions: - bandwidth = SUM bandwidth for
	 * each edge that connect two surrogate vertexes - delay = AVG delay for
	 * each edge - costPerUnitData = AVG costPerUnitData for each edge -
	 * availability = AVG availability for each edge
	 */
	private Map<Pair, ResourceEdge> createSurrogateEdges(
			ResourceGraph resGraph, Map<Integer, ResourceVertex> resNodes) {

		if (resNodes == null)
			return null;

		Map<Pair, ResourceEdge> hEdges = new HashMap<>();

		for (ResourceVertex uRes : resNodes.values()) {
			for (ResourceVertex vRes : resNodes.values()) {
				Pair p = new Pair(uRes.getIndex(), vRes.getIndex());
				ResourceEdge e = createSurrogateEdges(resGraph, uRes, vRes);

				if (e == null)
					continue;

				hEdges.put(p, e);
			}
		}

		return hEdges;
	}

	private ResourceEdge createSurrogateEdges(ResourceGraph resGraph,
			ResourceVertex surrogateVertexFrom, ResourceVertex surrogateVertexTo) {

		Set<Integer> from = new HashSet<>();
		Set<Integer> to = new HashSet<>();
		ResourceEdge surrogateEdge;

		if (surrogateVertexFrom.getInnerResourceGraph() == null
				|| surrogateVertexTo.getInnerResourceGraph() == null)
			return null;

		/* Define self-loop as perfect links */
		if(surrogateVertexFrom.getIndex() == surrogateVertexTo.getIndex()){
			int eid = surrogateVertexFrom.getIndex(); 
			double sBandwidth = Double.MAX_VALUE;
			int sDelay = 0;
			double sAvailability = 1.0;
			double sCostPerUnitData = 0;
			surrogateEdge = new ResourceEdge(eid, eid, sBandwidth, sDelay,
					sAvailability, sCostPerUnitData);
			return surrogateEdge;			
		}

		
		/* Retrieve the ids of resource vertexes on each surrogate nodes */
		for (ResourceVertex r : surrogateVertexFrom.getInnerResourceGraph()
				.getVertices().values()) {
			from.add(r.getIndex());
		}
		for (ResourceVertex r : surrogateVertexTo.getInnerResourceGraph()
				.getVertices().values()) {
			to.add(r.getIndex());
		}

		/*
		 * Retrieve all real edges that starts in the 'from' surrogate vertex
		 * and ends in the 'to' surrogate vertex
		 */
		Collection<ResourceEdge> realEdges = resGraph.getEdges().values();
		Set<ResourceEdge> edgeToBlend = new HashSet<>();
		for (ResourceEdge real : realEdges) {
			if (from.contains(real.getFrom()) && to.contains(real.getTo())) {
				edgeToBlend.add(real);
			}
		}

		/* Blend all edges in a surrogate edge */
		int eFrom = surrogateVertexFrom.getIndex();
		int eTo = surrogateVertexTo.getIndex();
		double sBandwidth = 0;
		int sDelay = 0;
		double sAvailability = 0;
		double sCostPerUnitData = 0;
		int sCount = 0;
		for (ResourceEdge e : edgeToBlend) {
			sAvailability += e.getAvailability();
			sBandwidth += e.getBandwidth();
			sCostPerUnitData += e.getCostPerUnitData();
			sDelay += e.getDelay();
			sCount++;
		}
		sAvailability = sAvailability / (double) sCount;
		sCostPerUnitData = sCostPerUnitData / (double) sCount;
		sDelay = (int) Math.floor(sDelay / (double) sCount);
		if (Double.isInfinite(sBandwidth))
			sBandwidth = Double.MAX_VALUE;
		surrogateEdge = new ResourceEdge(eFrom, eTo, sBandwidth, sDelay,
				sAvailability, sCostPerUnitData);

		return surrogateEdge;
	}

}
