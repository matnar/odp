package codd.utils;

import java.util.ArrayList;
import java.util.List;


/**
 * A multiset (or bag) is a generalization of the concept of a set that,
 * unlike a set, allows multiple instances of the multiset's elements.
 * <p>
 * For example, {a, a, b} and {a, b} are different multisets although
 * they are the same set. However, order does not matter, so {a, a, b}
 * and {a, b, a} are the same multiset.
 * <p>
 * The number of times an element belongs to the multiset is the
 * multiplicity of that member.
 * The total number of elements in a multiset, including repeated
 * memberships, is the cardinality of the multiset.
 * <p>
 * For example, in the multiset {a, a, b, b, b, c} the multiplicities of
 * the members a, b, and c are respectively 2, 3, and 1, and the
 * cardinality of the multiset is 6.
 * <p>
 * Source: https://en.wikipedia.org/wiki/Multiset
 * <p>
 * Given the number of items and the cardinality of the desired multisets,
 * this class allows to generate all possible multisets.
 *
 * @author Matteo Nardelli
 */
public class MultisetGenerator {

	/**
	 * Computes all possible multisets of <b>numItems</b> items
	 * with cardinality <b>cardinality</b>.
	 *
	 * @param numItems
	 * @param cardinality
	 * @return all possible multisets
	 */
	public static List<Multiset> generate(int numItems, int cardinality) {
		return generate(numItems, cardinality, null);
	}

	public static List<Multiset> generate (int numItems, int cardinality, ResVertexMultisetFilter filter) {
		List<Multiset> multisets = new ArrayList<Multiset>();

		/* Initialize indexes and boundaries */
		int j = cardinality;
		int j_1 = cardinality;
		int q = cardinality;
		int maxItem = numItems - 1;
		
		/* Initialize the first multiset as [0 ... 0] */
		int[] a = new int[cardinality];
		for (int i = 0; i < cardinality; i++) {
			a[i] = 0;
		}

		/* Generate multisets */
		while (true) {
						
			/* Emit the computed multiset */
			Multiset ms = new Multiset(a);
			if (filter == null || filter.acceptMultiset(ms))
				multisets.add(ms);
			
			/* Compute next multiset */
			j = cardinality - 1;
			

			/* Find element to update (increment) */
			while (j > -1 && a[j] == maxItem) {
				j--;
			}
			if (j < 0)
				break;
			
			/* Update the multiset elements */
			j_1 = j;

			while (j_1 <= cardinality - 1) {

				a[j_1] = a[j_1] + 1;
				q = j_1;

				while (q < cardinality - 1) {
					a[q + 1] = a[q];
					q += 1;
				}

				q += 1;
				j_1 = q;

			}
		}

		return multisets;
	}

}