package codd.utils;

import codd.utils.hierarchicalclustering.HierarchicalCluster;

public class ClusterUtils {

	/**
	 * Show on the stdout the hierarchicalCluster
	 * @param hierarchicalCluster
	 */
	public static void printCluster(HierarchicalCluster hierarchicalCluster) {
		printCluster(hierarchicalCluster, 0);
	}

	private static void printCluster(HierarchicalCluster hierarchicalCluster, int i) {
		for (int j = 0; j < i; j++)
			System.out.print("| ");
		System.out.print("+ ");

		System.out.println(hierarchicalCluster.getName() + ": covered leafs: "
				+ hierarchicalCluster.countLeafs() + "; direct children: "
				+ hierarchicalCluster.getChildren().size());

		for (String n : hierarchicalCluster.getLeafNames()) {
			for (int j = 0; j < i; j++)
				System.out.print("| ");
			System.out.println(" >> leafname:" + n);
		}

		for (HierarchicalCluster child : hierarchicalCluster.getChildren()) {
			printCluster(child, i + 1);
		}
	}

	
	/**
	 * Get the smallest hierarchicalCluster in <b>hierarchicalCluster</b> containing
	 * <b>innerHierarchicalCluster</b>.
	 * 
	 * @param hierarchicalCluster
	 * @param innerHierarchicalCluster
	 * @return
	 */
	public static HierarchicalCluster getSmallestClusterContaining(HierarchicalCluster hierarchicalCluster,
                                                                   HierarchicalCluster innerHierarchicalCluster) {

		if (hierarchicalCluster == null || innerHierarchicalCluster == null
				|| hierarchicalCluster.equals(innerHierarchicalCluster))
			return null;

		return innerHierarchicalCluster.getParent();

	}

	/**
	 * Get the smallest hierarchicalCluster in <b>hierarchicalCluster</b> containing the elements
	 * <b>srcRes</b> and <b>snkRes</b>.
	 * 
	 * @param hierarchicalCluster
	 * @param srcRes
	 * @param snkRes
	 * @return
	 */
	public static HierarchicalCluster getSmallestClusterContaining(HierarchicalCluster hierarchicalCluster,
                                                                   int srcRes, int snkRes) {
		HierarchicalCluster src = new HierarchicalCluster(Integer.toString(srcRes));
		HierarchicalCluster snk = new HierarchicalCluster(Integer.toString(snkRes));

		if (!(clusterContainsRecursive(hierarchicalCluster, src) && clusterContainsRecursive(
                hierarchicalCluster, snk))) {
			return null;
		}

		if (hierarchicalCluster.getChildren() == null || hierarchicalCluster.getChildren().isEmpty())
			return hierarchicalCluster;

		for (HierarchicalCluster child : hierarchicalCluster.getChildren()) {

			if (clusterContainsRecursive(child, src)
					&& clusterContainsRecursive(child, snk)) {
				return getSmallestClusterContaining(child, srcRes, snkRes);
			}

		}
		return hierarchicalCluster;
	}

	/**
	 * Check if an item is contained or is a sub-hierarchicalCluster in a hierarchicalCluster.
	 * 
	 * @param hierarchicalCluster
	 * @param item
	 * @return
	 */
	public static boolean clusterContainsRecursive(HierarchicalCluster hierarchicalCluster, HierarchicalCluster item) {

		if (hierarchicalCluster.getName().equals(item.getName()))
			return true;

		for (HierarchicalCluster child : hierarchicalCluster.getChildren()) {
			if (clusterContainsRecursive(child, item))
				return true;
		}
		return false;
	}

}
