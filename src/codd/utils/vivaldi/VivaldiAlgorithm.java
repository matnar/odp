package codd.utils.vivaldi;

import codd.ODDParameters;
import codd.distance.PairwiseDistanceStrategy;
import codd.model.Pair;
import codd.model.ResourceEdge;
import codd.model.ResourceGraph;
import codd.model.ResourceVertex;
import codd.utils.kmeans.Point;

import java.util.ArrayList;
import java.util.List;

public class VivaldiAlgorithm {

	private double ALPHA 	= 1;
	private double BETA 	= 0.01;
	private PairwiseDistanceStrategy distFx;
	private ResourceGraph graph;
	
	private static final boolean DEBUG = false;
	
	public VivaldiAlgorithm (ResourceGraph resourceGraph,
							 ODDParameters params,
							 PairwiseDistanceStrategy distanceFunction){
		this.distFx = distanceFunction;
		this.graph = resourceGraph;
	}

	public List<Point> computeCoordinates(double maxPredictionError){
		
		/* Create points */
		List<Point> points = new ArrayList<>();
		for(ResourceVertex vRes : graph.getVertices().values()){
			Point p = new Point(vRes);
			points.add(p);
		}

		boolean anotherRound = true;
		int ctr = 0;
		
		while (anotherRound){

//			for (Point p : points){
//				System.out.println(" v.init: " + p.toString());
//			}

			anotherRound = false;
			double localMaxPredError = 0; 
				
			for(Point uPoint : points){
				int u = uPoint.getNode().getIndex();
				
				for(Point vPoint : points){
					int v = vPoint.getNode().getIndex();
					
					ResourceEdge uvRes = graph.getEdges().get(new Pair(u, v));
					double computedDistance = distFx.getDistance(uvRes);

					/* *** Phase2: Analyze *** */ 
					VivaldiForce f = new VivaldiForce(computedDistance, uPoint, vPoint);
				
					double es = sampleError(computedDistance, uPoint, vPoint);
					double w = getW(uPoint, vPoint);
					double delta = ALPHA * w;

//					System.out.println("V: delta = " + delta + "; sampleError=" + es);
					uPoint.setPredictionError(es * BETA * w + uPoint.getPredictionError() * (1 - BETA * w));
					vPoint.setPredictionError(es * BETA * w + vPoint.getPredictionError() * (1 - BETA * w));
					
					uPoint = f.movePoint(uPoint, delta);
					vPoint = f.movePoint(vPoint, -1.0 * delta);

//					System.out.println("V: moved point = " + vPoint.toString());

					if (vPoint.getPredictionError() > maxPredictionError)
						anotherRound = true;

					if (vPoint.getPredictionError() > localMaxPredError)
						localMaxPredError = vPoint.getPredictionError();

//					if (Double.isNaN(vPoint.get(0))){
//						System.out.println("!! Vivaldi: point NaN!");
//					}

				}
			}

//			for (Point p : points){
//				System.out.println(" v.partial: " + p.toString());
//			}
//
			if (DEBUG)
				System.out.println("Round " + ctr + " - max pred err = " + localMaxPredError);
			
			ctr++;
			if (ctr >= 1000)
				break;
			
		}
		
		if (DEBUG)
			System.out.println("Vivaldi: round executed " + ctr); 

//		for (Point p : points){
//			System.out.println("- Vivaldi point: " + p.toString());
//		}
		return points;
	}

	private double getW(Point a, Point b){
		if (a.getPredictionError() == 0 &&  b.getPredictionError() == 0)
			return 1.0;
		else 
			return (a.getPredictionError() / (a.getPredictionError() + b.getPredictionError()));
	}

	private double sampleError(double computedDistance, Point a, Point b){
		
		double estimatedDistance = EuclidianDistance.distance(a, b);

//		System.out.println("estimated distance =  " + estimatedDistance + "; computed dist = " + computedDistance);
		if (computedDistance == 0)
			return 0;
		else
			return Math.abs(estimatedDistance - computedDistance) / Math.abs(computedDistance); 
		
	}

}
