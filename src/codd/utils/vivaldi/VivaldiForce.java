package codd.utils.vivaldi;

import java.util.Random;

import codd.utils.kmeans.Point;

public class VivaldiForce {
	private double[] versor;
	private double magnitude;
	
	public VivaldiForce(double realDistance, Point a, Point b) {

		this.versor = computeVersor(a, b);
		this.magnitude = computeMagnitude(realDistance, a, b);
		
	}

	private double[] computeVersor(Point a, Point b) {

		double[] versor = new double[Point.CARDINALITY];
		double sum = 0;
		
		for (int i = 0; i < Point.CARDINALITY; i++) {
			versor[i] = a.get(i) - b.get(i);
			sum += Math.abs(versor[i]);
		}

		if (sum == 0) {
			/* two points are in the same location */
			Random rnd = new Random();

			for (int i = 0; i < Point.CARDINALITY; i++) {
				versor[i] = rnd.nextDouble();
				sum += Math.abs(versor[i]);
			}
		}

		/* Set value only for latency dimensions */
		for (int i = 0; i < Point.CARDINALITY; i++) {
			versor[i] = versor[i] / sum;
		}

		return versor;

	}

	private double computeMagnitude(double realDistance, Point a, Point b) {
		return realDistance - EuclidianDistance.distance(a, b);
	}

	public void multiply(double factor) {
		magnitude = magnitude * factor;
	}

	public Point movePoint(Point point, double time) {

		if (point == null || Point.CARDINALITY != versor.length)
			return null;

		for (int i = 0; i < Point.CARDINALITY; i++) {
			point.set(i, point.get(i) + time * magnitude * versor[i]);
		}

		return point;

	}

	@Override
	public String toString() {
		String str = "|" + magnitude + "|*u(";
		for (int i = 0; i < versor.length; i++) {
			str += versor[i] + ", ";
		}
		str += ")";
		return str;
	}

}