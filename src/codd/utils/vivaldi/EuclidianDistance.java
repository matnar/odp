package codd.utils.vivaldi;

import codd.utils.kmeans.Point;

public class EuclidianDistance {

	public static double norm(Point a) {
		
		if (a == null)
			return Double.NaN;
		
		double squareSum = 0;
		
		for (int i = 0; i < Point.CARDINALITY; i++)
			squareSum += Math.pow(a.get(i), 2.0);
		
		squareSum = Math.sqrt(squareSum);
				
		return squareSum;
	}

	public static Point multiply(double a, Point p) {
		
		if(p == null)
			return null;
		
		int dim = Point.CARDINALITY;
		Point newPoint = new Point(p.getNode());
		
		for (int i = 0; i < dim; i++){
			newPoint.set(i, (a * p.get(i)));
		}

		return newPoint;
		
	}

	public static double distance(Point a, Point b) {

		if (a == null || b == null)
			return Double.NaN;
		
		double squareSum = 0;
		
		for (int i = 0; i < Point.CARDINALITY; i++)
			squareSum += Math.pow((a.get(i) - b.get(i)), 2.0);
		
		squareSum = Math.sqrt(squareSum);
				
		return squareSum;

	}

}
