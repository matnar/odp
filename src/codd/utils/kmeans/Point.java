package codd.utils.kmeans;

import java.util.Random;

import codd.model.ResourceVertex;

public class Point {

	public static final int CARDINALITY = 4;
	private double[] coordinates;
	private double predictionError;
	
	private int cluster_number = 0;

	private ResourceVertex node;

	public Point(ResourceVertex node){
		this.node = node;
		this.predictionError = 1.0;
		this.coordinates = new double[CARDINALITY];
		for (int i = 0; i < CARDINALITY; i++){
			coordinates[i] = 0;
		}
	}
	
	public static Point createRandomPoint(double minCoord, double maxCoord){
		Random rnd = new Random();
		Point p = new Point(null);
		for(int i = 0; i < Point.CARDINALITY; i++){
			p.set(i, minCoord + rnd.nextFloat() * (maxCoord - minCoord));
		}
		return p;
	}
	
	public void set(int index, double x) {
		if (index < CARDINALITY)
			this.coordinates[index] = x;
	}

	public double get(int index) {
		if (index < CARDINALITY)
			return coordinates[index];
		return 0;
	}

	public void setCluster(int n) {
		this.cluster_number = n;
	}

	public int getCluster() {
		return this.cluster_number;
	}

	public ResourceVertex getNode() {
		return node;
	}

	public void setNode(ResourceVertex node) {
		this.node = node;
	}
	
	public double getPredictionError() {
		return predictionError;
	}

	public void setPredictionError(double predictionError) {
		this.predictionError = predictionError;
	}

	public String toString() {
		String str = "(";
		for (int i = 0; i < coordinates.length; i++){
			str += coordinates[i];
			if (i != coordinates.length - 1){
				str += ", ";				
			}
		}
		str += ")";
		return str;
	}
}