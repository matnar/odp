package codd.utils.hierarchicalclustering;

import java.util.ArrayList;
import java.util.List;

public class HierarchicalCluster {

    private String name;

    private HierarchicalCluster parent;

    private List<HierarchicalCluster> children;

    private List<String> leafNames;

    private Distance distance = new Distance();

    public HierarchicalCluster(String name) {
        this.name = name;
        leafNames = new ArrayList<String>();
    }

    public Distance getDistance() {
        return distance;
    }

    public Double getWeightValue() {
        return distance.getWeight();
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public List<HierarchicalCluster> getChildren() {
        if (children == null) {
            children = new ArrayList<HierarchicalCluster>();
        }

        return children;
    }

    public void appendLeafNames(List<String> lnames) {
        leafNames.addAll(lnames);
    }

    public List<String> getLeafNames() {
        return leafNames;
    }

    public HierarchicalCluster getParent() {
        return parent;
    }

    public void setParent(HierarchicalCluster parent) {
        this.parent = parent;
    }


    public String getName() {
        return name;
    }

    public void addChild(HierarchicalCluster hierarchicalCluster) {
        getChildren().add(hierarchicalCluster);

    }

    @Override
    public String toString() {
        return "HierarchicalCluster " + name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        HierarchicalCluster other = (HierarchicalCluster) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (name == null) ? 0 : name.hashCode();
    }

    public boolean isLeaf() {
        return getChildren().size() == 0;
    }

    public int countLeafs() {
        return countLeafs(this, 0);
    }

    public int countLeafs(HierarchicalCluster node, int count) {
        if (node.isLeaf()) count++;
        for (HierarchicalCluster child : node.getChildren()) {
            count += child.countLeafs();
        }
        return count;
    }

}
