package codd.utils.hierarchicalclustering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class HierarchicalClusteringAlgorithm {

    public HierarchicalCluster performClustering(double[][] distances,
                                                 String[] clusterNames, LinkageStrategy linkageStrategy) {

        checkArguments(distances, clusterNames, linkageStrategy);

        /* Setup model */
        List<HierarchicalCluster> hierarchicalClusters = createClusters(clusterNames);
        DistanceMap linkages = createLinkages(distances, hierarchicalClusters);

        /* Process */
        HierarchicalClusterBuilder builder = new HierarchicalClusterBuilder(hierarchicalClusters, linkages);
        while (!builder.isTreeComplete()) {
            builder.agglomerate(linkageStrategy);
        }

        return builder.getRootCluster();
    }

    private void checkArguments(double[][] distances, String[] clusterNames,
                                LinkageStrategy linkageStrategy) {
        if (distances == null || distances.length == 0
                || distances[0].length != distances.length) {
            throw new IllegalArgumentException("Invalid distance matrix");
        }
        if (distances.length != clusterNames.length) {
            throw new IllegalArgumentException("Invalid cluster name array");
        }
        if (linkageStrategy == null) {
            throw new IllegalArgumentException("Undefined linkage strategy");
        }
        int uniqueCount = new HashSet<String>(Arrays.asList(clusterNames)).size();
        if (uniqueCount != clusterNames.length) {
            throw new IllegalArgumentException("Duplicate names");
        }
    }

    private DistanceMap createLinkages(double[][] distances,
                                       List<HierarchicalCluster> hierarchicalClusters) {
        DistanceMap linkages = new DistanceMap();
        for (int col = 0; col < hierarchicalClusters.size(); col++) {
            for (int row = col + 1; row < hierarchicalClusters.size(); row++) {
                ClusterPair link = new ClusterPair();
                HierarchicalCluster lHierarchicalCluster = hierarchicalClusters.get(col);
                HierarchicalCluster rHierarchicalCluster = hierarchicalClusters.get(row);
                link.setLinkageDistance(distances[col][row]);
                link.setlHierarchicalCluster(lHierarchicalCluster);
                link.setrHierarchicalCluster(rHierarchicalCluster);
                linkages.add(link);
            }
        }
        return linkages;
    }

    private List<HierarchicalCluster> createClusters(String[] clusterNames) {
        List<HierarchicalCluster> hierarchicalClusters = new ArrayList<HierarchicalCluster>();
        for (String clusterName : clusterNames) {
            HierarchicalCluster hierarchicalCluster = new HierarchicalCluster(clusterName);
            hierarchicalClusters.add(hierarchicalCluster);
        }
        return hierarchicalClusters;
    }

}
