package codd.utils.hierarchicalclustering;

public class ClusterPair implements Comparable<ClusterPair> {

    private static long globalIndex = 0;

    private HierarchicalCluster lHierarchicalCluster;
    private HierarchicalCluster rHierarchicalCluster;
    private Double linkageDistance;

    public ClusterPair() {
    }

    public HierarchicalCluster getOtherCluster(HierarchicalCluster c) {
        return lHierarchicalCluster == c ? rHierarchicalCluster : lHierarchicalCluster;
    }

    public HierarchicalCluster getlHierarchicalCluster() {
        return lHierarchicalCluster;
    }

    public void setlHierarchicalCluster(HierarchicalCluster lHierarchicalCluster) {
        this.lHierarchicalCluster = lHierarchicalCluster;
    }

    public HierarchicalCluster getrHierarchicalCluster() {
        return rHierarchicalCluster;
    }

    public void setrHierarchicalCluster(HierarchicalCluster rHierarchicalCluster) {
        this.rHierarchicalCluster = rHierarchicalCluster;
    }

    public Double getLinkageDistance() {
        return linkageDistance;
    }

    public void setLinkageDistance(Double distance) {
        this.linkageDistance = distance;
    }

    @Override
    public int compareTo(ClusterPair o) {
        int result;
        if (o == null || o.getLinkageDistance() == null) {
            result = -1;
        } else if (getLinkageDistance() == null) {
            result = 1;
        } else {
            result = getLinkageDistance().compareTo(o.getLinkageDistance());
        }

        return result;
    }

    public HierarchicalCluster agglomerate(String name) {
        if (name == null) {
            name = "clstr#" + (++globalIndex);
        }
        HierarchicalCluster hierarchicalCluster = new HierarchicalCluster(name);
        hierarchicalCluster.setDistance(new Distance(getLinkageDistance()));
        //New clusters will track their children's leaf names; i.e. each hierarchicalCluster knows what part of the original data it contains
        hierarchicalCluster.appendLeafNames(lHierarchicalCluster.getLeafNames());
        hierarchicalCluster.appendLeafNames(rHierarchicalCluster.getLeafNames());
        hierarchicalCluster.addChild(lHierarchicalCluster);
        hierarchicalCluster.addChild(rHierarchicalCluster);
        lHierarchicalCluster.setParent(hierarchicalCluster);
        rHierarchicalCluster.setParent(hierarchicalCluster);

        Double lWeight = lHierarchicalCluster.getWeightValue();
        Double rWeight = rHierarchicalCluster.getWeightValue();
        double weight = lWeight + rWeight;
        hierarchicalCluster.getDistance().setWeight(weight);

        return hierarchicalCluster;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (lHierarchicalCluster != null) {
            sb.append(lHierarchicalCluster.getName());
        }
        if (rHierarchicalCluster != null) {
            if (sb.length() > 0) {
                sb.append(" + ");
            }
            sb.append(rHierarchicalCluster.getName());
        }
        sb.append(" : ").append(linkageDistance);
        return sb.toString();
    }

}
