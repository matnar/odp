package codd.utils.hierarchicalclustering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class HierarchicalClusterBuilder {

    private DistanceMap distances;
    private List<HierarchicalCluster> hierarchicalClusters;

    public HierarchicalClusterBuilder(List<HierarchicalCluster> hierarchicalClusters, DistanceMap distances) {
        this.hierarchicalClusters = hierarchicalClusters;
        this.distances = distances;
    }

    public void agglomerate(LinkageStrategy linkageStrategy) {
        ClusterPair minDistLink = distances.removeFirst();
        if (minDistLink != null) {
            hierarchicalClusters.remove(minDistLink.getrHierarchicalCluster());
            hierarchicalClusters.remove(minDistLink.getlHierarchicalCluster());

            HierarchicalCluster oldHierarchicalClusterL = minDistLink.getlHierarchicalCluster();
            HierarchicalCluster oldHierarchicalClusterR = minDistLink.getrHierarchicalCluster();
            HierarchicalCluster newHierarchicalCluster = minDistLink.agglomerate(null);

            for (HierarchicalCluster iClust : hierarchicalClusters) {
                ClusterPair link1 = findByClusters(iClust, oldHierarchicalClusterL);
                ClusterPair link2 = findByClusters(iClust, oldHierarchicalClusterR);
                ClusterPair newLinkage = new ClusterPair();
                newLinkage.setlHierarchicalCluster(iClust);
                newLinkage.setrHierarchicalCluster(newHierarchicalCluster);
                Collection<Distance> distanceValues = new ArrayList<Distance>();

                if (link1 != null) {
                    Double distVal = link1.getLinkageDistance();
                    Double weightVal = link1.getOtherCluster(iClust).getWeightValue();
                    distanceValues.add(new Distance(distVal, weightVal));
                    distances.remove(link1);
                }
                if (link2 != null) {
                    Double distVal = link2.getLinkageDistance();
                    Double weightVal = link2.getOtherCluster(iClust).getWeightValue();
                    distanceValues.add(new Distance(distVal, weightVal));
                    distances.remove(link2);
                }

                Distance newDistance = linkageStrategy.calculateDistance(distanceValues);

                newLinkage.setLinkageDistance(newDistance.getDistance());
                distances.add(newLinkage);

            }
            hierarchicalClusters.add(newHierarchicalCluster);
        }
    }

    private ClusterPair findByClusters(HierarchicalCluster c1, HierarchicalCluster c2) {
        return distances.findByCodePair(c1, c2);
    }

    public boolean isTreeComplete() {
        return hierarchicalClusters.size() == 1;
    }

    public HierarchicalCluster getRootCluster() {
        if (!isTreeComplete()) {
            throw new RuntimeException("No root available");
        }
        return hierarchicalClusters.get(0);
    }

}
