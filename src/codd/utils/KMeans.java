package codd.utils;

import codd.utils.kmeans.Cluster;
import codd.utils.kmeans.Point;
import codd.utils.vivaldi.EuclidianDistance;

import java.util.ArrayList;
import java.util.List;

public class KMeans {

	private List<Point> points;
	private List<Cluster> clusters;
	private double minCoord;
	private double maxCoord;

	private static boolean DEBUG = false;
	
	public KMeans(List<Point> points) {
		this.points = new ArrayList<>(points);
		this.clusters = new ArrayList<>();
		minCoord = Double.MAX_VALUE;
		maxCoord = Double.MIN_VALUE;
	}

	public List<Cluster> calculate(int numClusters) {
		boolean finish = false;
		int iteration = 0;

		computeMinMaxCoordinate();

		numClusters = Math.min(numClusters, points.size());
		for (int i = 0; i < numClusters; i++) {
			Cluster cluster = new Cluster(i);
			Point centroid = points.get(i); // Point.createRandomPoint(minCoord, maxCoord);
			cluster.setCentroid(centroid);
			clusters.add(cluster);
		}

//		for (Point p : points){
//			System.out.println("Point: " + p.toString());
//			if (p.get(0) == Double.NaN)
//				System.exit(1);
//		}
//		for (Cluster c : clusters){
//			System.out.println("Cluster/centroid: " + c.getCentroid().toString());
//		}

		// Add in new data, one at a time, recalculating centroids with each new
		// one.
		while (!finish) {
			clearClusters();

			List<Point> lastCentroids = getCentroids();
//			for (Point p : lastCentroids){
//				System.out.println("int: " + iteration + ", last centroid: " + p);
//			}

			assignCluster();
			calculateCentroids();
			iteration++;
			List<Point> currentCentroids = getCentroids();

			// Calculates total distance between new and old Centroids
			double distance = 0;
			for (int i = 0; i < lastCentroids.size(); i++) {
				double localDist = EuclidianDistance.distance(lastCentroids.get(i),
						currentCentroids.get(i));

//				System.out.println("["+ iteration + "] local dist: " + localDist);
				distance += localDist;
			}
			
			if (DEBUG){
				System.out.println("*** K-Means ******************************************** ");
				System.out.println(" Iteration: " + iteration);
				System.out.println(" Centroid distances: " + distance);
				System.out.println(" Average Clusters Distances : " + averageClusterDistance());
				System.out.println("*** /K-Means ******************************************** ");
			}
			
			if (distance == 0) {
				finish = true;
			}

			if (iteration >= 1000)
				finish = true;
		}

		if (DEBUG)
			plotClusters();

		return clusters;

	}

	private void clearClusters() {
		for (Cluster cluster : clusters) {
			cluster.clear();
		}
	}

	private List<Point> getCentroids() {

		List<Point> centroids = new ArrayList<>();
		for (Cluster cluster : clusters) {
			Point aux = cluster.getCentroid();
			Point point = new Point(null);
			for (int i = 0; i < Point.CARDINALITY; i++) {
				point.set(i, aux.get(i));
			}
			centroids.add(point);
		}
		return centroids;
	}

	private double averageClusterDistance(){

		double avgDistance = 0;
		for (Cluster c : clusters){
			Point centroid = c.getCentroid();

			double intraDistance 	= 0;
			double clusterElements 	= 0;
			
			
			for(Point p : c.getPoints()){
				intraDistance = EuclidianDistance.distance(p, centroid);
				clusterElements++;
			}
			
			avgDistance += intraDistance / clusterElements;
		}
		avgDistance = avgDistance / (double) clusters.size();
		
		return avgDistance;

	}
	
	private void assignCluster() {
		double max = Double.MAX_VALUE;
		double min = max;
		int cluster = 0;
		double distance = 0.0;

		for (Point point : points) {
			min = max;
			for (int i = 0; i < clusters.size(); i++) {
				Cluster c = clusters.get(i);
				distance = EuclidianDistance.distance(point, c.getCentroid());

				if (Double.isNaN(distance)){
					System.out.println(" Assign cluster: not a number for point: " + point.toString());
				}

				if (distance < min) {
					min = distance;
					cluster = i;
				}
			}
			point.setCluster(cluster);
			clusters.get(cluster).addPoint(point);
		}
	}

	private void calculateCentroids() {
		for (Cluster cluster : clusters) {
			List<Point> list = cluster.getPoints();
			int n_points = list.size();

			double avgDistances[] = new double[Point.CARDINALITY];
			for (int i = 0; i < Point.CARDINALITY; i++) {
				avgDistances[i] = 0;
			}
			for (Point point : list) {
				for (int i = 0; i < Point.CARDINALITY; i++) {
					avgDistances[i] += point.get(i);
				}
			}

			Point centroid = cluster.getCentroid();
			if (n_points > 0) {
				for (int i = 0; i < Point.CARDINALITY; i++) {
					avgDistances[i] = avgDistances[i] / (double) n_points;
					centroid.set(i, avgDistances[i]);
				}
			}
		}
	}
	private void computeMinMaxCoordinate() {
		minCoord = Double.MAX_VALUE;
		maxCoord = Double.MIN_VALUE;

		for (Point p : points) {
			for (int i = 0; i < Point.CARDINALITY; i++) {
				if (minCoord > p.get(i))
					minCoord = p.get(i);
				if (maxCoord < p.get(i))
					maxCoord = p.get(i);
			}
		}

		if (DEBUG)
			System.out.println("max: " + maxCoord + " min: " + minCoord);
	}

	private void plotClusters() {
		for (Cluster c : clusters) {
			c.plotCluster();
		}
	}


}