package codd.utils;



public interface ResVertexMultisetFilter {

	boolean acceptMultiset(Multiset multiset);
}
