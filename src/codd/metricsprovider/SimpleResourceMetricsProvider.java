package codd.metricsprovider;

import codd.ODDParameters;

import java.util.Random;

public class SimpleResourceMetricsProvider extends ResourceMetricsProvider {

	private double linkDelayMin;
	private double linkDelayMean;
	private double linkDelayStdDev;

	private double linkAvailabilityMin;
	private double linkAvailabilityMean;
	private double linkAvailabilityStdDev;

	private double linkBandwidthMin;
	private double linkBandwidthMean;
	private double linkBandwidthStdDev;

	private double linkCostPerUnitData;
	
	private double nodeAvailResourcesMin;
	private double nodeAvailResourcesMean;
	private double nodeAvailResourcesStdDev;

	private double nodeSpeedupMin;
	private double nodeSpeedupMean;
	private double nodeSpeedupStdDev;

	private double nodeAvailabilityMin;
	private double nodeAvailabilityMean;
	private double nodeAvailabilityStdDev;
	
	protected Random rnd;
	private final long seed = 201512021054l;

	private enum MT {
		LINK_DELAY, LINK_AVAILABILITY, LINK_BANDWIDTH, NODE_AVAILABLE_RESOURCES, NODE_SPEEDUP, NODE_AVAILABILITY
	};

	/**
	 * Get resource metrics. A gaussian distribution is used; if a constant
	 * value is required, use mean = stdDev = 0 and set the desired value to the
	 * 'min' parameter
	 */
	public SimpleResourceMetricsProvider(ODDParameters params) {
		super();
		this.linkDelayMin = params.getLinkDelayMin();
		this.linkDelayMean = params.getLinkDelayMean();
		this.linkDelayStdDev = params.getLinkDelayStdDev();
		this.linkAvailabilityMin = params.getLinkAvailMin();
		this.linkAvailabilityMean = params.getLinkAvailMean();
		this.linkAvailabilityStdDev = params.getLinkAvailStdDev();
		this.linkBandwidthMin = params.getLinkBandwidthMin();
		this.linkBandwidthMean = params.getLinkBandwidthMean();
		this.linkBandwidthStdDev = params.getLinkBandwidthStdDev();
		this.linkCostPerUnitData = params.getLinkCostPerUnitData();
		this.nodeAvailResourcesMin = params.getNodeAvailResourcesMin();
		this.nodeAvailResourcesMean = params.getNodeAvailResourcesMean();
		this.nodeAvailResourcesStdDev = params.getNodeAvailResourcesStdDev();
		this.nodeSpeedupMin = params.getNodeSpeedupMin();
		this.nodeSpeedupMean = params.getNodeSpeedupMean();
		this.nodeSpeedupStdDev = params.getNodeSpeedupStdDev();
		this.nodeAvailabilityMin = params.getNodeAvailabilityMin();
		this.nodeAvailabilityMean = params.getNodeAvailabilityMean();
		this.nodeAvailabilityStdDev = params.getNodeAvailabilityStdDev();
		
		this.rnd = new Random(seed);
	}

	@Override
	public double getLatency(int fromVresId, int toVresId) {

		if (fromVresId == toVresId)
			return 0.0;
		
		return value(rnd.nextGaussian(), MT.LINK_DELAY);

	}

	@Override
	public double getAvailability(int fromVresId, int toVresId) {

		if (fromVresId == toVresId)
			return 1.0;
		
		return value(rnd.nextGaussian(), MT.LINK_AVAILABILITY);

	}

	@Override
	public double getBandwidth(int fromVresId, int toVresId) {

		if (fromVresId == toVresId)
			return Double.MAX_VALUE;

		return value(rnd.nextGaussian(), MT.LINK_BANDWIDTH);

	}

	@Override
	public double getAvailability(int vResId) {

		return value(rnd.nextDouble(), MT.NODE_AVAILABILITY);

//		return value(rnd.nextGaussian(), MT.NODE_AVAILABILITY);
	}

	@Override
	public double getSpeedup(int vResId) {
		return value(rnd.nextGaussian(), MT.NODE_SPEEDUP);
	}

	@Override
	public int getAvailableResources(int vResId) {
		return (int) Math.floor(value(rnd.nextGaussian(),
				MT.NODE_AVAILABLE_RESOURCES));
	}
	
	@Override
	public double getCostPerUnitData(int uDsp, int vDsp) {
		return this.linkCostPerUnitData;
	}

	private double value(double gaussian, MT mt) {

		switch (mt) {
		case LINK_AVAILABILITY:

			if (linkAvailabilityMean == 0.0 && linkAvailabilityStdDev == 0.0)
				return linkAvailabilityMin;
			return Math.min(1.0, Math.max(linkAvailabilityMin, gaussian * linkAvailabilityStdDev + linkAvailabilityMean));

		case LINK_BANDWIDTH:

			if (linkBandwidthMean == 0.0 && linkBandwidthStdDev == 0.0)
				return linkBandwidthMin;
			return Math.max(linkBandwidthMin, gaussian * linkBandwidthStdDev + linkBandwidthMean);

		case LINK_DELAY:

			if (linkDelayMean == 0.0 && linkDelayStdDev == 0.0)
				return linkDelayMin;
			return Math.max(linkDelayMin, gaussian * linkDelayStdDev + linkDelayMean);

		case NODE_AVAILABILITY:
			// XXX: nodeAvailabilityMean contains nodeAvailabilityMax
			if (nodeAvailabilityMean == 0.0 && nodeAvailabilityStdDev == 0.0)
				return nodeAvailabilityMin;
			
			return ((nodeAvailabilityMean - nodeAvailabilityMin) * gaussian + nodeAvailabilityMin);
//			return Math.max(nodeAvailabilityMin, gaussian * nodeAvailabilityStdDev + nodeAvailabilityMean);

		case NODE_AVAILABLE_RESOURCES:

			if (nodeAvailResourcesMean == 0.0 && nodeAvailResourcesStdDev == 0.0)
				return nodeAvailResourcesMin;
			return Math.max(nodeAvailResourcesMin, gaussian * nodeAvailResourcesStdDev + nodeAvailResourcesMean);

		case NODE_SPEEDUP:

			if (nodeSpeedupMean == 0.0 && nodeSpeedupStdDev == 0.0)
				return nodeSpeedupMin;
			return Math.max(nodeSpeedupMin, gaussian * nodeSpeedupStdDev + nodeSpeedupMean);

		default:
			return 0.0;
		}

	}

}
