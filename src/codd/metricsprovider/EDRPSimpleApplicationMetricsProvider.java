package codd.metricsprovider;

import codd.DspGraphBuilder.TYPE;
import codd.EDRPParameters;

public class EDRPSimpleApplicationMetricsProvider extends SimpleApplicationMetricsProvider {
	
	protected double internalStateImageSizeMin;
	protected double internalStateImageSizeMean;
	protected double internalStateImageSizeStdDev;
	
	protected double codeImageSizeMin;
	protected double codeImageSizeMean;
	protected double codeImageSizeStdDev;

	protected double reconfigurationSyncTimeMin;
	protected double reconfigurationSyncTimeMean;
	protected double reconfigurationSyncTimeStdDev;
	
	public EDRPSimpleApplicationMetricsProvider(TYPE type, int nDsp, EDRPParameters params) {
		super(type, nDsp, params);
		this.codeImageSizeMean = params.getCodeImageSizeMean();
		this.codeImageSizeMin = params.getCodeImageSizeMin();
		this.codeImageSizeStdDev = params.getCodeImageSizeStdDev();
		this.internalStateImageSizeMean = params.getInternalStateImageSizeMean();
		this.internalStateImageSizeMin = params.getInternalStateImageSizeMin();
		this.internalStateImageSizeStdDev = params.getInternalStateImageSizeStdDev();
		this.reconfigurationSyncTimeMean = params.getReconfigurationSyncTimeMean();
		this.reconfigurationSyncTimeMin = params.getReconfigurationSyncTimeMin();
		this.reconfigurationSyncTimeStdDev = params.getReconfigurationSyncTimeStdDev();
	}
	
	public double getInternalStateImageSize() {
		if (internalStateImageSizeMean == 0.0 && internalStateImageSizeStdDev == 0.0)
			return internalStateImageSizeMin;
		return Math.max(internalStateImageSizeMin,
				rnd.nextGaussian() * internalStateImageSizeStdDev + internalStateImageSizeMean);
	}

	public double getCodeImageSize() {
		if (codeImageSizeMean == 0.0 && codeImageSizeStdDev == 0.0)
			return codeImageSizeMin;
		return Math.max(codeImageSizeMin, rnd.nextGaussian() * codeImageSizeStdDev + codeImageSizeMean);
	}

	public double getReconfigurationSyncTime() {
		if (reconfigurationSyncTimeMean == 0.0 && reconfigurationSyncTimeStdDev == 0.0)
			return reconfigurationSyncTimeMin;
		return Math.max(reconfigurationSyncTimeMin,
				rnd.nextGaussian() * reconfigurationSyncTimeStdDev + reconfigurationSyncTimeMean);
	}


}
