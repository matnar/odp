package codd.metricsprovider;

import codd.EDRPParameters;

public class EDRPSimpleResourceMetricsProvider extends SimpleResourceMetricsProvider {

	protected double uploadRateToDSMin;
	protected double uploadRateToDSMean;
	protected double uploadRateToDSStdDev;

	protected double downloadRateFromDSMin;
	protected double downloadRateFromDSMean;
	protected double downloadRateFromDSStdDev;

	protected double uploadRateToLocalDSMin;
	protected double uploadRateToLocalDSMean;
	protected double uploadRateToLocalDSStdDev;

	protected double downloadRateFromLocalDSMin;
	protected double downloadRateFromLocalDSMean;
	protected double downloadRateFromLocalDSStdDev;
	
	protected double dataStoreRTTMin;
	protected double dataStoreRTTMean;
	protected double dataStoreRTTStdDev;

	protected double instanceLaunchTimeMin;
	protected double instanceLaunchTimeMean;
	protected double instanceLaunchTimeStdDev;

	public EDRPSimpleResourceMetricsProvider(EDRPParameters params) {
		super(params);
		this.uploadRateToDSMean = params.getUploadRateToDSMean();
		this.uploadRateToDSMin = params.getUploadRateToDSMin();
		this.uploadRateToDSStdDev = params.getUploadRateToDSStdDev();
		this.downloadRateFromDSMean = params.getDownloadRateFromDSMean();
		this.downloadRateFromDSMin = params.getDownloadRateFromDSMin();
		this.downloadRateFromDSStdDev = params.getDownloadRateFromDSStdDev();
		this.uploadRateToLocalDSMean = params.getUploadRateToLocalDSMean();
		this.uploadRateToLocalDSMin = params.getUploadRateToLocalDSMin();
		this.uploadRateToLocalDSStdDev = params.getUploadRateToLocalDSStdDev();
		this.downloadRateFromLocalDSMean = params.getDownloadRateFromLocalDSMean();
		this.downloadRateFromLocalDSMin = params.getDownloadRateFromLocalDSMin();
		this.downloadRateFromLocalDSStdDev = params.getDownloadRateFromLocalDSStdDev();
		this.instanceLaunchTimeMean = params.getInstanceLaunchTimeMean();
		this.instanceLaunchTimeMin = params.getInstanceLaunchTimeMin();
		this.instanceLaunchTimeStdDev = params.getInstanceLaunchTimeStdDev();
		this.dataStoreRTTMean = params.getDataStoreRTTMean();
		this.dataStoreRTTMin = params.getDataStoreRTTMin();
		this.dataStoreRTTStdDev = params.getDataStoreRTTStdDev();
	}

	@Override
	public double getUploadRateToDS(int vRes) {
		if (uploadRateToDSMean == 0.0 && uploadRateToDSStdDev == 0.0)
			return uploadRateToDSMin;

		return Math.max(uploadRateToDSMin, rnd.nextGaussian() * uploadRateToDSStdDev + uploadRateToDSMean);
	}

	@Override
	public double getDownloadRateFromDS(int vRes) {
		if (downloadRateFromDSMean == 0.0 && downloadRateFromDSStdDev == 0.0)
			return downloadRateFromDSMin;
		return Math.max(downloadRateFromDSMin, rnd.nextGaussian() * downloadRateFromDSStdDev + downloadRateFromDSMean);
	}

	@Override
	public double getUploadRateToLocalDS(int vRes) {
		if (uploadRateToLocalDSMean == 0.0 && uploadRateToLocalDSStdDev == 0.0)
			return uploadRateToLocalDSMin;

		return Math.max(uploadRateToLocalDSMin, rnd.nextGaussian() * uploadRateToLocalDSStdDev + uploadRateToLocalDSMean);
	}

	@Override
	public double getDownloadRateFromLocalDS(int vRes) {
		if (downloadRateFromLocalDSMean == 0.0 && downloadRateFromLocalDSStdDev == 0.0)
			return downloadRateFromLocalDSMin;
		return Math.max(downloadRateFromLocalDSMin, rnd.nextGaussian() * downloadRateFromLocalDSStdDev + downloadRateFromLocalDSMean);
	}

	@Override
	public double getInstanceLaunchTime(int vRes) {
		if (instanceLaunchTimeMean == 0.0 && instanceLaunchTimeStdDev == 0.0)
			return instanceLaunchTimeMin;
		return Math.max(instanceLaunchTimeMin, rnd.nextGaussian() * instanceLaunchTimeStdDev + instanceLaunchTimeMean);
	}

	@Override
	public double getDataStoreRTT(int vRes) {
		if (dataStoreRTTMean == 0.0 && dataStoreRTTStdDev == 0.0)
			return dataStoreRTTMin;
		return Math.max(dataStoreRTTMin, rnd.nextGaussian() * dataStoreRTTStdDev + dataStoreRTTMean);
	}

}
