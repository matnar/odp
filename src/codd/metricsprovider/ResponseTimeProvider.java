package codd.metricsprovider;

public interface ResponseTimeProvider {

	double getResponseTime(double lambda, int replicas);
}
