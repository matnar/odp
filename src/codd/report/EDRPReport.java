package codd.report;

import java.io.IOException;

import codd.DspGraphBuilder;
import codd.EDRPParameters;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.model.OptimalEDRPSolution;
import codd.model.OptimalMultisetSolution;
import codd.model.OptimalSolution;
import codd.model.Pair;

public class EDRPReport extends ODRPReport {

	public EDRPReport(String experimentId, String filename) {
		super(experimentId, filename);
	}
	
	

	@Override
	public void write(String idSeries, int run, String modelType, DspGraphBuilder gbuilder,
			ApplicationMetricsProvider amp, ResGraphBuilder rbuilder, ResourceMetricsProvider rmp,
			double restrictionOnVres, int srcRes, int snkRes, ODDParameters params, OptimalSolution solution)
			throws ReportException, IOException {
		if (!(solution instanceof OptimalEDRPSolution))
			throw new RuntimeException("Cannot use this report for a non EDRP solution!");
		if (!(params instanceof EDRPParameters))
			throw new RuntimeException("Cannot use this report for a non EDRP solution!");
		super.write(idSeries, run, modelType, gbuilder, amp, rbuilder, rmp, restrictionOnVres, srcRes, snkRes, params,
				solution);
	}



	@Override
	protected void printFields(String idSeries, int run, String modelType, DspGraphBuilder gbuilder,
			ApplicationMetricsProvider amp, ResGraphBuilder rbuilder, ResourceMetricsProvider rmp,
			double restrictionOnVres, int srcRes, int snkRes, ODDParameters params, OptimalMultisetSolution sol)
			throws IOException {
		super.printFields(idSeries, run, modelType, gbuilder, amp, rbuilder, rmp, restrictionOnVres, srcRes, snkRes, params,
				sol);

		OptimalEDRPSolution edrpSol = (OptimalEDRPSolution)sol;
		printField(edrpSol.getOptTDown());
		printField(termTDown(edrpSol, (EDRPParameters)params));
		printField(edrpSol.getInstancesCount());
		
		/* Computes src lambda. */
		double srcLambda = 0.0;
		for (Pair p : gbuilder.getGraph().getEdges().keySet())
			if (p.getA() == 0)
				srcLambda += gbuilder.getGraph().getEdges().get(p).getLambda();
		printField(srcLambda);
				
	}

	@Override
	protected void printHeadings() throws IOException {
		super.printHeadings();
		printField("TDown");
		printField("termTDown");
		printField("activeInstances");
		printField("srcLambda");
	}



	@Override
	protected double termR(OptimalMultisetSolution sol, ODDParameters params) {
		EDRPParameters eParams = (EDRPParameters)params;
		return params.getWeightRespTime() * sol.getOptR() / eParams.getRmax();
	}

	@Override
	protected double termC(OptimalMultisetSolution sol, ODDParameters params) {
		EDRPParameters eParams = (EDRPParameters)params;
		return params.getWeightCost() * sol.getOptC() / eParams.getCmax();
	}

	@Override
	protected double termA(OptimalMultisetSolution sol, ODDParameters params) {
		EDRPParameters eParams = (EDRPParameters)params;
		return params.getWeightAvailability() * sol.getOptLogA() / eParams.getLogAmin();
	}

	@Override
	protected double termZ(OptimalMultisetSolution sol, ODDParameters params) {
		EDRPParameters eParams = (EDRPParameters)params;
		return params.getWeightNetMetric() * sol.getOptZ() / eParams.getZmax();
	}
	

	protected double termTDown(OptimalEDRPSolution sol, EDRPParameters params) {
		return params.getWeightDowntime() * sol.getOptTDown() / params.getTDmax();
	}
	

}
