package codd.report;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import codd.DspGraphBuilder;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.model.OptimalMultisetSolution;
import codd.model.OptimalSolution;
import codd.placement.ODDModel;

public class ODRPReport extends Report {

	public ODRPReport(String experimentId, String filename) {
		super(experimentId, filename);
	}

	protected BufferedWriter buffer;

	@Override
	public void write(String idSeries, int run, ODDModel model, DspGraphBuilder gbuilder,
			ApplicationMetricsProvider amp, ResGraphBuilder rbuilder, ResourceMetricsProvider rmp,
			double restrictionOnVres, ODDParameters params, OptimalSolution solution)
			throws ReportException, IOException {
		write(idSeries, run, model, gbuilder, amp, rbuilder, rmp, restrictionOnVres, -1, -1, params, solution);
	}

	@Override
	public void write(String idSeries, int run, ODDModel model, DspGraphBuilder gbuilder,
			ApplicationMetricsProvider amp, ResGraphBuilder rbuilder, ResourceMetricsProvider rmp,
			double restrictionOnVres, int srcRes, int snkRes, ODDParameters params, OptimalSolution solution)
			throws ReportException, IOException {
		write(idSeries, run, (model != null) ? model.type() : "heuristics", gbuilder, amp, rbuilder, rmp,
				restrictionOnVres, srcRes, snkRes, params, solution);
	}

	protected void printField(String field) throws IOException {
		buffer.write(field + ",");
	}

	protected void printField(Object field) throws IOException {
		buffer.write(field.toString() + ",");
	}

	protected void printField(String field, boolean lastField) throws IOException {
		if (!lastField)
			printField(field);
		else
			buffer.write(field + "\n");
	}

	public void write(String idSeries, int run, String modelType, DspGraphBuilder gbuilder,
			ApplicationMetricsProvider amp, ResGraphBuilder rbuilder, ResourceMetricsProvider rmp,
			double restrictionOnVres, int srcRes, int snkRes, ODDParameters params, OptimalSolution solution)
			throws ReportException, IOException {

		OptimalMultisetSolution sol;
		if (solution instanceof OptimalMultisetSolution) {
			sol = (OptimalMultisetSolution) solution;
		} else {
			throw new RuntimeException(
					"Cannot use this report for a solution that is not instance of OptimalMultisetSolution");
		}

		File f = new File(filename);

		boolean writeHeadings = false;
		if (!f.canWrite()) {
			f.createNewFile();
			writeHeadings = true;
		}

		try {
			buffer = new BufferedWriter(new FileWriter(f, true));

			if (writeHeadings) {
				printHeadings();
				printField("resolutionTime", true);
			}

			printFields(idSeries, run, modelType, gbuilder, amp, rbuilder, rmp, restrictionOnVres, srcRes, snkRes,
					params, sol);
			printField(new Date().toString(), true);
			buffer.flush();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (buffer != null)
				buffer.close();
		}

	}

	protected void printFields(String idSeries, int run, String modelType, DspGraphBuilder gbuilder,
			ApplicationMetricsProvider amp, ResGraphBuilder rbuilder, ResourceMetricsProvider rmp,
			double restrictionOnVres, int srcRes, int snkRes, ODDParameters params, OptimalMultisetSolution sol)
			throws IOException {
		printField(idSeries);
		printField(this.experimentId);
		printField(run);
		printField(modelType);

		printField(rbuilder.getGraph().getVertices().size());
		printField(rmp.getAvailableResources(0));

		printField(gbuilder.getType());
		printField(gbuilder.getGraph().getVertices().size());
		printField(gbuilder.getGraph().getEdges().size());
		printField(2);

		printField(((double) gbuilder.getGraph().getVertices().size()
				/ (double) rbuilder.getGraph().getVertices().size()));
		printField(restrictionOnVres);
		printField(srcRes);
		printField(snkRes);

		printField(params.getGap());
		printField(params.getTimeLimit());
		printField(sol.getCompilationTime());
		printField(sol.getResolutionTime());

		printField(sol.getStatus());
		printField(sol.getOptObjValue());

		printField(sol.getOptR());
		printField(termR(sol, params));
		printField(sol.getOptC());
		printField(termC(sol, params));
		printField(Math.exp(sol.getOptLogA()));
		printField(termA(sol, params));
		printField(sol.getOptZ());
		printField(termZ(sol, params));

		printField("\"" + sol.toString() + "\"");

	}

	protected void printHeadings() throws IOException {
		printField("#idSerie");
		printField("id");
		printField("#run");
		printField("model");

		printField("#res");
		printField("#Cu");

		printField("type");
		printField("#vdsp");
		printField("#edsp");
		printField("#pinned");

		printField("#%request");
		printField("#%restrictionOnVres");
		printField("srcRes");
		printField("snkRes");

		printField("gap");
		printField("timeout");
		printField("compilationTime");
		printField("resolutionTime");

		printField("solution");
		printField("obj");

		printField("R");
		printField("termR");
		printField("C");
		printField("termC");
		printField("A");
		printField("termA");
		printField("Z");
		printField("termZ");

		printField("x");
	}

	protected double termR(OptimalMultisetSolution sol, ODDParameters params) {
		return (params.getWeightRespTime() * (params.getRmax() - sol.getOptR())
				/ (params.getRmax() - params.getRmin()));
	}

	protected double termC(OptimalMultisetSolution sol, ODDParameters params) {
		return (params.getWeightCost() * (params.getCmax() - (sol.getOptC()) / (params.getCmax() - params.getCmin())));
	}

	protected double termA(OptimalMultisetSolution sol, ODDParameters params) {
		return ((sol.getOptLogA() - Math.log(params.getAmin())) * params.getWeightAvailability()
				/ (Math.log(params.getAmax()) - Math.log(params.getAmin())));
	}

	protected double termZ(OptimalMultisetSolution sol, ODDParameters params) {
		return (params.getWeightNetMetric() * (params.getZmax() - sol.getOptZ())
				/ (params.getZmax() - params.getZmin()));
	}
}
