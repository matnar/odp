package codd.placement;

import codd.ODDException;
import codd.ODDParameters;
import codd.model.*;
import ilog.concert.*;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplexModeler;

import java.util.List;


public class ODDBandwidthModel implements ODDModel {
	
	public enum MODE{
		NO_BANDWIDTH,
		INTERNODE_TRAFFIC, 
		NETWORK_UTILIZATION,
		APPROX_ELASTIC_ENERGY
	};
	
	protected static final boolean DEBUG = false;
	
	protected static final boolean SWAP_ON_DISK = true;
	protected static final int SWAP_AFTER_MEMORY_USAGE 	= 4096;		// in MB
	protected static final int SWAP_MAX_DISK_USAGE 		= 20000;	// in MB 
	protected static final String SWAP_DIR				= "wordir_cplex";

	protected ODDParameters params;
	
	protected DspGraph dspGraph;
	protected ResourceGraph resGraph;
	protected IloCplex cplex;
	protected PlacementX X;
	protected PlacementY Y;
	protected OptimalSolution solution;
	
	protected MODE mode;
	
	protected IloNumExpr R;
	protected IloNumExpr Z;
	protected IloNumExpr logA;

	private long compilationTime;
	
	public ODDBandwidthModel(DspGraph dspGraph, ResourceGraph resGraph, ODDParameters parameters) throws ODDException {
		
		this.dspGraph = dspGraph;
		this.resGraph = resGraph;
	
		this.params = parameters;
		
		this.solution = null;
		this.R = null;
		this.logA = null;
		this.X = null;
		this.Y = null;
		this.compilationTime = Long.MAX_VALUE;
		
		this.mode = parameters.getBwMode();
		
		try {
			this.cplex = new IloCplex();
		} catch (IloException exc) {
			throw new ODDException("Error while creating model: " + exc.getMessage());
		}
	}
	
	public ODDBandwidthModel(DspGraph dspGraph, ResourceGraph resGraph) throws ODDException {
		this(dspGraph, resGraph, new ODDParameters(ODDModel.MODE.BANDWIDTH, MODE.INTERNODE_TRAFFIC));
	}

	public DspGraph getDspGraph() {
		return dspGraph;
	}

	public ResourceGraph getResGraph() {
		return resGraph;
	}


	
	
	public void compile() throws ODDException{
		
		IloModeler modeler = new IloCplexModeler();	
		
		compilationTime = System.currentTimeMillis();
		
		/* *******************************************************************************
		 * Decision Variables		
		 ********************************************************************************/
		try {
			this.X = new PlacementX(dspGraph, resGraph);
			this.Y = new PlacementY(dspGraph, resGraph);
		} catch (IloException exc) {
			throw new ODDException("Error while defining decision variables X and Y: " + exc.getMessage());
		}	
		
				
		/* *******************************************************************************
		 * Response-Time		
		 ********************************************************************************/
		try {
			R = cplex.numVar(0, Double.MAX_VALUE, "R");
			for (DspPath path : dspGraph.getPaths()) {

				/* R_computation */
				IloLinearNumExpr Rpex = modeler.linearNumExpr();	
				for(Integer i : path.getNodesIndexes()){
					DspVertex in = dspGraph.getVertices().get(i);
					for(ResourceVertex vn : resGraph.getVertices().values()){
						
						if (in.deployableOn(vn)){
							int u = vn.getIndex();
							Rpex.addTerm(in.getExecutionTime() / vn.getSpeedup(), X.get(i, u));							
						}
					}
				}
				
				/* R_comunication*/
				IloLinearNumExpr Rptx = modeler.linearNumExpr();
				List<Integer> sequence = path.getNodesIndexes();
				for (int k = 0; k < sequence.size() - 1; k++) {
					Integer ik = sequence.get(k);
					Integer ik1 = sequence.get(k + 1);
					for (ResourceEdge eRes : resGraph.getEdges().values()) {
						int u = eRes.getFrom();
						int v = eRes.getTo();
						DspVertex ikDsp = dspGraph.getVertices().get(ik);
						DspVertex ik1Dsp = dspGraph.getVertices().get(ik1);

						if(ikDsp.deployableOn(u) && ik1Dsp.deployableOn(v)){
							Rptx.addTerm(eRes.getDelay(), Y.get(ik, ik1, u, v));
						}
					}
				}						
				
				/* R, ref.18082015, Eq.12 */
				IloNumExpr Rp = modeler.sum(Rpex, Rptx);
				cplex.addLe(Rp, R, "objBound_R");
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Response-Time: " + exc.getMessage());
		}
		
		/* *******************************************************************************
		 * Availability		
		 ********************************************************************************/
		try {
			logA  = modeler.numVar(-Double.MAX_VALUE, 0, "logA");
			
			/* A nodes */
			IloLinearNumExpr logAex = modeler.linearNumExpr();
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					
					if (iDsp.deployableOn(uRes)){
						int i = iDsp.getIndex();
						int u = uRes.getIndex();
						logAex.addTerm(Math.log(uRes.getAvailability()), X.get(i, u));
					}
				}					
			}				
			
			/* A links */
			IloLinearNumExpr logAtx = modeler.linearNumExpr();
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				for (ResourceEdge uvRes: resGraph.getEdges().values()) {
					int i = ijDsp.getFrom();
					int j = ijDsp.getTo();
					int u = uvRes.getFrom();
					int v = uvRes.getTo();
					
					DspVertex iDsp = dspGraph.getVertices().get(i);
					DspVertex jDsp = dspGraph.getVertices().get(j);
					
					if(iDsp.deployableOn(u) && jDsp.deployableOn(v)){
						logAtx.addTerm(Math.log(uvRes.getAvailability()), Y.get(i, j, u, v));
					}
				}					
			}
			IloNumExpr logAx = modeler.sum(logAex, logAtx);
			cplex.addEq(logAx, logA, "objBound_A");
			
			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Availability: " + exc.getMessage());
		}
		
		/* *******************************************************************************
		 * Bandwidth: internode traffic, network utilization or elastic energy
		 ********************************************************************************/
		try {
			Z = cplex.numVar(0, Double.MAX_VALUE, "Z");
			IloLinearNumExpr exprZ = modeler.linearNumExpr();

			for (DspEdge ij : dspGraph.getEdges().values()) {
				int i = ij.getFrom();
				int j = ij.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);
				
				/* Z = sum_{ij} Z_{ij} */
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int u = uRes.getIndex();
					
					if(iDsp.deployableOn(u)){
						for (ResourceVertex vRes : resGraph.getVertices().values()) {
							int v = vRes.getIndex();
							
							if(jDsp.deployableOn(v)){
								double lambdaij = ij.getLambda();
								if (u == v)
									continue;

								if (mode == MODE.INTERNODE_TRAFFIC){
								
									exprZ.addTerm(lambdaij, Y.get(i, j, u, v));							
								
								} else if (mode == MODE.NETWORK_UTILIZATION){
									
									ResourceEdge uvRes = resGraph.getEdges().get(new Pair(u, v));
									double lambdaTimesDelay = lambdaij * uvRes.getDelay();
									exprZ.addTerm(lambdaTimesDelay, Y.get(i, j, u, v));							
									
								} else if (mode == MODE.APPROX_ELASTIC_ENERGY){
									
									ResourceEdge uvRes = resGraph.getEdges().get(new Pair(u, v));
									double lambdaTimesDelay2 = lambdaij * uvRes.getDelay() * uvRes.getDelay();
									exprZ.addTerm(lambdaTimesDelay2, Y.get(i, j, u, v));														
								}
							}
						}
					}
				}					
			}

			cplex.addEq(exprZ, Z, "objBound_Z");

		} catch (IloException exc) {
			throw new ODDException("Error while defining Bandwidth term in ObjFunct: " + exc.getMessage());
		}
		
		
		/* *******************************************************************************
		 * Objective
		 ********************************************************************************/	
		IloObjective obj;
		IloNumExpr objRExpr, objAExpr, objZExpr, objExpr;
		try {			
			objRExpr = modeler.prod(modeler.sum(params.getRmax(), modeler.negative(R)), params.getWeightRespTime() / (params.getRmax() - params.getRmin()));
			objAExpr = modeler.prod(modeler.sum(logA, (-Math.log(params.getAmin()))), 	params.getWeightAvailability() / (Math.log(params.getAmax()) - Math.log(params.getAmin())));
			objZExpr = modeler.prod(modeler.sum(params.getZmax(), modeler.negative(Z)), params.getWeightNetMetric() / (params.getZmax() - params.getZmin()));
			objExpr  = modeler.sum(objRExpr, objAExpr, objZExpr);
			obj 	 = modeler.maximize(objExpr);
			cplex.addObjective(obj.getSense(), obj.getExpr(), "Fx");		
		} catch (IloException exc) {
			throw new ODDException("Error while defining Objective Function: " + exc.getMessage());
		}
		

		/* *******************************************************************************
		 * Capacity Bound - Eq.15
		 ********************************************************************************/
		try {
			for (ResourceVertex uRes : resGraph.getVertices().values()) {
				IloLinearNumExpr deployedOpCapacity = modeler.linearNumExpr();
				for (DspVertex iDsp : dspGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();
					
					if (iDsp.deployableOn(u)){
						deployedOpCapacity.addTerm(iDsp.getRequiredResources(), X.get(i, u));
					}
				}					
				cplex.addLe(deployedOpCapacity, uRes.getAvailableResources(), 
						"capb_res_" + uRes.getIndex());
			}			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Capacity Bound: " + exc.getMessage());
		}
		
		/* *******************************************************************************
		 * Uniqueness Bound - Eq.16
		 ********************************************************************************/		
		try {
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				IloLinearNumExpr exprUniqueness = modeler.linearNumExpr();
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();
					
					if (iDsp.deployableOn(u)){
						exprUniqueness.addTerm(1.0, X.get(i, u));
					}
				}
				cplex.addEq(exprUniqueness, 1.0, 
						"uniqb_dsp_" + iDsp.getIndex());
			}			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}
		
		/* *******************************************************************************
		 * Connectivity Bound - Eq.28, 29 - review 25.09.2015 
		 ********************************************************************************/
		try {
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);
			
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int u = uRes.getIndex();
					
					if(iDsp.deployableOn(u)){
						IloLinearNumExpr exprConn = modeler.linearNumExpr();
						for (ResourceVertex vRes : resGraph.getVertices().values()) {
							int v = vRes.getIndex();
							
							if(jDsp.deployableOn(v)){
								exprConn.addTerm(1.0, Y.get(i, j, u, v));
							}						
						}
						cplex.addEq(X.get(i, u), exprConn, "conb1_" + i + "," + j);
					}						
				}
			}
		
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);
				
				for (ResourceVertex vRes : resGraph.getVertices().values()) {
					int v = vRes.getIndex();
					
					if(jDsp.deployableOn(v)){
						IloLinearNumExpr exprConn = modeler.linearNumExpr();
						for (ResourceVertex uRes : resGraph.getVertices().values()) {
							int u = uRes.getIndex();
							
							if(iDsp.deployableOn(u)){
								exprConn.addTerm(1.0, Y.get(i, j, u, v));
							}	
						}
						cplex.addEq(X.get(j, v), exprConn, "conb2_" + i + "," + j);	
					}
				}
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Connectivity Bound: " + exc.getMessage());
		}	
		compilationTime = System.currentTimeMillis() - compilationTime;

		/* XXX: DEBUG */ 
		if (DEBUG){
			try {
				cplex.exportModel("lpex1.lp");
			} catch (IloException e) {
				throw new ODDException("Error while exporting ODDModel on file: " + e.getMessage());
			}	
		}
		
		if (SWAP_ON_DISK){
			try {
				
				if (DEBUG)
					System.out.println("Setting NodeFileInd=" + 3 + "; "
							+ " WorkDir='wordir_cplex/'; "
							+ " WorkMem=1024MB;"
							+ " TreLim=20000MB");
				
				/* When the tree memory limit is reached, a group of nodes is removed from the in-memory
				 * set as needed. By default, CPLEX transfers nodes to node files when the in-memory set 
				 * is larger than 128 MBytes, and it keeps the resulting node files in compressed form in 
				 * memory. At settings 3, the node files are transferred to disk, in compressed form, into
				 * a directory named by the working directory parameter (CPX_PARAM_WORKDIR, WorkDir), 
				 * and CPLEX actively manages which nodes remain in memory for processing. 
				 */
				cplex.setParam(IloCplex.IntParam.NodeFileInd, 2);
				cplex.setParam(IloCplex.StringParam.WorkDir, SWAP_DIR);

				/* 
				 * To avoid a failure due to running out of memory, 
				 * the working memory parameter, WorkMem, is set (expressed in MB).
				 * It instructs CPLEX to begin compressing the storage of nodes 
				 * before it consumes all of available memory. 
				 */
				cplex.setParam(IloCplex.DoubleParam.WorkMem, SWAP_AFTER_MEMORY_USAGE); // in MB

				/* 
				 * Because the storage of nodes can require a lot of space, 
				 * it may also be advisable to set a tree limit on the size of the 
				 * entire tree being stored so that not all of your disk will be 
				 * filled up with working storage. 
				 * The call to the MIP optimizer will be stopped after the size of the 
				 * tree exceeds the value of TreLim (in MB).
				 */
				cplex.setParam(IloCplex.DoubleParam.TreLim,  SWAP_MAX_DISK_USAGE); // in MB 
				// set to 20 GB

			} catch (IloException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void clean() {
		if (cplex != null)
			cplex.end();
	}
		
	public OptimalSolution solve() throws ODDException{
		
		if (!DEBUG)
			cplex.setOut(null);
		
		if (R == null)
			compile();
		
		try {
			solution = new OptimalSolution(dspGraph.getVertices().size());
		    
			if (params.isUseTimeLimit()){
				cplex.setParam(IloCplex.IntParam.TimeLimit, params.getTimeLimit()); //expressed in seconds 
				
			}
			if (params.isDefineOptimalGap()){			
				cplex.setParam(IloCplex.DoubleParam.EpGap, params.getGap());
			}
			
			long enlapsedTime = System.currentTimeMillis();
			if(cplex.solve()){
				enlapsedTime = System.currentTimeMillis() - enlapsedTime;
				
			    solution.setOptObjValue(cplex.getObjValue());
			    solution.setOptR(cplex.getValue(R));
			    solution.setOptLogA(cplex.getValue(logA));
			    solution.setOptZ(cplex.getValue(Z));
			    solution.setResolutionTime(enlapsedTime);
			    solution.setCompilationTime(compilationTime);
			    
			    for (DspVertex dspOperator : dspGraph.getVertices().values()) {
					for (ResourceVertex resNode : resGraph.getVertices().values()) {
						int i = dspOperator.getIndex();
						int u = resNode.getIndex();
						double xval = 0;
						if (dspOperator.deployableOn(u))
							xval = cplex.getValue(X.get(i, u));
			        	if (xval > 0)
			        		solution.setPlacement(i, u);
					}				
				}		

				if (DEBUG)
					cplex.writeSolution("solution.sol");

			} else {
				if (DEBUG)
					System.out.println("Unable to find a solution");
			}

			solution.setStatus(cplex.getStatus());

			/* Printing solution */
			if (DEBUG){
				System.out.println(" ---------------------------------------------- ");
				System.out.println(solution.toString());
				System.out.println(" ---------------------------------------------- ");
			}

		} catch (IloException e) {
			throw new ODDException("Error while solving OPP Model: " + e.getMessage());
		}
		
		return solution;
	}

	@Override
	public String type() {
		return "BANDWIDTH";
	}

	
	@Override
	public void pin(int dspIndex, int resIndex) {
		try {
		
			if (X.get(dspIndex, resIndex) != null)
				cplex.addEq(X.get(dspIndex, resIndex), 1, "pinned");

		} catch (IloException e) {
			e.printStackTrace();
		}	
	}
	
}
