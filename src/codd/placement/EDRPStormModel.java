package codd.placement;

import codd.EDRPParameters;
import codd.ODDException;
import codd.metric.StormReconfigurationMetricsManager;
import codd.model.DspGraph;
import codd.model.DspVertex;
import codd.model.OptimalSolution;
import codd.model.ResourceGraph;
import codd.model.ResourceVertexMultiset;
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloModeler;
import ilog.concert.IloNumVar;

/**
 * Elastic DSP Replication and Placement model, adjusted for Apache Storm.
 *
 * If you use this model, please cite:
 * V. Cardellini, F. Lo Presti, M. Nardelli, G. Russo Russo,
 * "Optimal Operator Deployment and Replication for Elastic Distributed Data Stream Processing",
 * Concurrency and Computation: Practice and Experience,
 * Vol. 30, No. 9, May 2018. doi: 10.1002/cpe.4334.
 *
 * @author Gabriele Russo Russo
 */
public class EDRPStormModel extends EDRPModel {

	/** Signals if one or more reconfigurations are planned. */
	private IloNumVar delta;

	public EDRPStormModel(DspGraph dspGraph, ResourceGraph resGraph, EDRPParameters parameters, int maxReplication, boolean prunedMultisetsGeneration) throws ODDException {
		super(dspGraph, resGraph, parameters, maxReplication, prunedMultisetsGeneration);
		metricManager = new StormReconfigurationMetricsManager(dspGraph, resGraph, parameters.isOdrpUseMM1(),
				parameters.getCurrentPlacement());
	}

	public EDRPStormModel(DspGraph dspGraph, ResourceGraph resGraph, EDRPParameters parameters, int maxReplication) throws ODDException {
		super(dspGraph, resGraph, parameters, maxReplication, false);
		metricManager = new StormReconfigurationMetricsManager(dspGraph, resGraph, parameters.isOdrpUseMM1(),
				parameters.getCurrentPlacement());
	}

	@Override
	protected void compileConstraintsAndVars(IloModeler modeler) throws ODDException {
		/* Compiles basic EDRP model. */
		super.compileConstraintsAndVars(modeler);

		StormReconfigurationMetricsManager reconfigurationMetricsManager = (StormReconfigurationMetricsManager) metricManager;

		/*
		 * Delta variable (signals if any reconfigurations are planned).
		 */
		try {
			delta = cplex.boolVar("deltaReconf");

			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				for (ResourceVertexMultiset vms : allMultisets.values()) {
					if (!iDsp.deployableOn(vms))
						continue;
					if (vms.equals(reconfigurationMetricsManager.currentMultiset(iDsp))) {
						continue;
					}

					cplex.addGe(delta, X.get(iDsp.getIndex(), vms.getIndex()), "deltaBound" + iDsp.getIndex() + "_" + vms.getIndex());
				}
			}
		} catch (IloException e) {
			e.printStackTrace();
			return;
		}

		/*
		 * Downtime variable. It is the maximum downtime over the DSP operators.
		 * Here we have to take into account pause-and-resume overhead due to Storm rebalance.
		 */
		try {
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				IloLinearNumExpr expr = modeler.linearNumExpr();
				expr.addTerm(reconfigurationMetricsManager.getPauseAndResumeDowntime(iDsp), delta);
				cplex.addGe(TDown, expr, "DownOpDelta" + iDsp.getIndex());
			}

		} catch (IloException e) {
			throw new ODDException("Error while adding constraints on TDown variable");
		}

	}

	@Override
	public String type() {
		return EDRP_MODEL_TYPE;
	}
	
	




	@Override
	public OptimalSolution solve() throws ODDException {
		OptimalSolution sol = super.solve();
		
		try {
			System.out.println("Delta: " + cplex.getValue(delta));
		} catch (IloException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sol;
	}






	public static final String EDRP_MODEL_TYPE = "edrp-storm";

}
