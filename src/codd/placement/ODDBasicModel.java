package codd.placement;

import codd.ODDException;
import codd.ODDParameters;
import codd.model.*;
import ilog.concert.*;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplexModeler;

import java.util.List;


public class ODDBasicModel implements ODDModel {
	
	protected static final boolean DEBUG = false;
	
	protected ODDParameters params;
	
	protected DspGraph dspGraph;
	protected ResourceGraph resGraph;
	protected IloCplex cplex;
	protected PlacementX X;
	protected PlacementY Y;
	protected OptimalSolution solution;
	
	protected IloNumExpr R;
	protected IloNumExpr logA;
	
	private long compilationTime;
	
	public ODDBasicModel(DspGraph dspGraph, ResourceGraph resGraph,
			ODDParameters parameters) throws ODDException {
		
		this.dspGraph = dspGraph;
		this.resGraph = resGraph;
		
		this.params = parameters;
		
		this.solution = null;
		this.R = null;
		this.logA = null;
		this.X = null;
		this.Y = null;
		this.compilationTime = Long.MAX_VALUE;
		
		try {
			this.cplex = new IloCplex();
		} catch (IloException exc) {
			throw new ODDException("Error while creating model: " + exc.getMessage());
		}
	}
	
	public ODDBasicModel(DspGraph dspGraph, ResourceGraph resGraph) throws ODDException {
		this(dspGraph, resGraph, new ODDParameters(ODDModel.MODE.BASIC, null));
	}

	public DspGraph getDspGraph() {
		return dspGraph;
	}

	public ResourceGraph getResGraph() {
		return resGraph;
	}

	public void compile() throws ODDException{
		
		IloModeler modeler = new IloCplexModeler();	

		compilationTime = System.currentTimeMillis();
		
		/********************************************************************************
		 * Decision Variables		
		 ********************************************************************************/
		try {
			this.X = new PlacementX(dspGraph, resGraph);
			this.Y = new PlacementY(dspGraph, resGraph);
		} catch (IloException exc) {
			throw new ODDException("Error while defining decision variables X and Y: " + exc.getMessage());
		}	
		
				
		/********************************************************************************
		 * Response-Time		
		 ********************************************************************************/
		try {
			R = cplex.numVar(0, Double.MAX_VALUE, "R");
			for (DspPath path : dspGraph.getPaths()) {

				/* R_computation */
				IloLinearNumExpr Rpex = modeler.linearNumExpr();	
				for(Integer i : path.getNodesIndexes()){
					DspVertex in = dspGraph.getVertices().get(i);
					for(ResourceVertex vn : resGraph.getVertices().values()){
						
						if (in.deployableOn(vn)){
							int u = vn.getIndex();
							Rpex.addTerm(in.getExecutionTime() / vn.getSpeedup(), X.get(i, u));							
						}
					}
				}
				
				/* R_comunication*/
				IloLinearNumExpr Rptx = modeler.linearNumExpr();
				List<Integer> sequence = path.getNodesIndexes();
				for (int k = 0; k < sequence.size() - 1; k++) {
					Integer ik = sequence.get(k);
					Integer ik1 = sequence.get(k + 1);
					for (ResourceEdge eRes : resGraph.getEdges().values()) {
						int u = eRes.getFrom();
						int v = eRes.getTo();
						DspVertex ikDsp = dspGraph.getVertices().get(ik);
						DspVertex ik1Dsp = dspGraph.getVertices().get(ik1);

						if(ikDsp.deployableOn(u) && ik1Dsp.deployableOn(v)){
							Rptx.addTerm(eRes.getDelay(), Y.get(ik, ik1, u, v));
						}
					}
				}					
				
				/* XXX: ref.18082015, Eq.12 */
				IloNumExpr Rp = modeler.sum(Rpex, Rptx);
				cplex.addLe(Rp, R, "objBound_R");
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Response-Time: " + exc.getMessage());
		}
		
		/********************************************************************************
		 * Availability		
		 ********************************************************************************/
		try {
			logA  = modeler.numVar(-Double.MAX_VALUE, 0, "logA");
			
			/* A nodes */
			IloLinearNumExpr logAex = modeler.linearNumExpr();
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					
					if (iDsp.deployableOn(uRes)){
						int i = iDsp.getIndex();
						int u = uRes.getIndex();
						logAex.addTerm(Math.log(uRes.getAvailability()), X.get(i, u));
					}
				}					
			}				
			
			/* A links */
			IloLinearNumExpr logAtx = modeler.linearNumExpr();
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				for (ResourceEdge uvRes: resGraph.getEdges().values()) {
					int i = ijDsp.getFrom();
					int j = ijDsp.getTo();
					int u = uvRes.getFrom();
					int v = uvRes.getTo();
					
					DspVertex iDsp = dspGraph.getVertices().get(i);
					DspVertex jDsp = dspGraph.getVertices().get(j);
					
					if(iDsp.deployableOn(u) && jDsp.deployableOn(v)){
						logAtx.addTerm(Math.log(uvRes.getAvailability()), Y.get(i, j, u, v));
					}
				}					
			}
			IloNumExpr logAx = modeler.sum(logAex, logAtx);
			cplex.addEq(logAx, logA, "objBound_A");
			
			
			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Availability: " + exc.getMessage());
		}
		
		/********************************************************************************
		 * Objective
		 ********************************************************************************/	
		IloObjective obj;
		IloNumExpr objRExpr, objAExpr, objExpr;
		try {			
			objRExpr = modeler.prod(modeler.sum(params.getRmax(), modeler.negative(R)), params.getWeightRespTime() / (params.getRmax() - params.getRmin()));
			objAExpr = modeler.prod(modeler.sum(logA, (-Math.log(params.getAmin()))), 	params.getWeightAvailability() / (Math.log(params.getAmax()) - Math.log(params.getAmin())));
			objExpr  = modeler.sum(objRExpr, objAExpr);
			obj 	 = modeler.maximize(objExpr);
			cplex.addObjective(obj.getSense(), obj.getExpr(), "Fx");		
		} catch (IloException exc) {
			throw new ODDException("Error while defining Objective Function: " + exc.getMessage());
		}
		

		/********************************************************************************
		 * Capacity Bound - Eq.15
		 ********************************************************************************/
		try {
			for (ResourceVertex uRes : resGraph.getVertices().values()) {
				IloLinearNumExpr deployedOpCapacity = modeler.linearNumExpr();
				for (DspVertex iDsp : dspGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();
					
					if (iDsp.deployableOn(u)){
						deployedOpCapacity.addTerm(iDsp.getRequiredResources(), X.get(i, u));
					}
				}					
				cplex.addLe(deployedOpCapacity, uRes.getAvailableResources(), 
						"capb_res_" + uRes.getIndex());
			}			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Capacity Bound: " + exc.getMessage());
		}
		
		/********************************************************************************
		 * Uniqueness Bound - Eq.16
		 ********************************************************************************/		
		try {
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				IloLinearNumExpr exprUniqueness = modeler.linearNumExpr();
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();
					
					if (iDsp.deployableOn(u)){
						exprUniqueness.addTerm(1.0, X.get(i, u));
					}
				}
				cplex.addEq(exprUniqueness, 1.0, 
						"uniqb_dsp_" + iDsp.getIndex());
			}			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}
		
		/********************************************************************************
		 * Connectivity Bound - Eq.28, 29 - review 25.09.2015 
		 ********************************************************************************/
		try {
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);
			
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int u = uRes.getIndex();
					
					if(iDsp.deployableOn(u)){
						IloLinearNumExpr exprConn = modeler.linearNumExpr();
						for (ResourceVertex vRes : resGraph.getVertices().values()) {
							int v = vRes.getIndex();
							
							if(jDsp.deployableOn(v)){
								if (Y.get(i, j, u, v) != null)
									exprConn.addTerm(1.0, Y.get(i, j, u, v));
							}						
						}
						cplex.addEq(X.get(i, u), exprConn, "conb1_" + i + "," + j);
					}						
				}
			}
		
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);
				
				for (ResourceVertex vRes : resGraph.getVertices().values()) {
					int v = vRes.getIndex();
					
					if(jDsp.deployableOn(v)){
						IloLinearNumExpr exprConn = modeler.linearNumExpr();
						for (ResourceVertex uRes : resGraph.getVertices().values()) {
							int u = uRes.getIndex();
							
							if(iDsp.deployableOn(u)){
								if (Y.get(i, j, u, v) != null)
									exprConn.addTerm(1.0, Y.get(i, j, u, v));
							}	
						}
						cplex.addEq(X.get(j, v), exprConn, "conb2_" + i + "," + j);	
					}
				}
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Connectivity Bound: " + exc.getMessage());
		}	
		compilationTime = System.currentTimeMillis() - compilationTime;

		
		/* XXX: DEBUG */ 
		try {
			cplex.exportModel("lpex1.lp");
		} catch (IloException e) {
			throw new ODDException("Error while exporting ODDModel on file: " + e.getMessage());
		}
		
	}
	
	public void exportModel() throws ODDException{
		/* XXX: DEBUG */ 
		try {
			cplex.exportModel("lpex1.lp");
		} catch (IloException e) {
			throw new ODDException("Error while exporting ODDModel on file: " + e.getMessage());
		}
	}
	
	public OptimalSolution solve() throws ODDException{
		
		
		if (R == null)
			compile();
		
		try {
			if (params.isUseTimeLimit()){
				cplex.setParam(IloCplex.IntParam.TimeLimit, params.getTimeLimit()); //expressed in seconds 
				
			}
			if (params.isDefineOptimalGap()){			
				cplex.setParam(IloCplex.DoubleParam.EpGap, params.getGap());
			}
			
			solution = new OptimalSolution(dspGraph.getVertices().size());
			
			if (!DEBUG){
				cplex.setOut(null);
				cplex.setWarning(null);
			}
			
			long enlapsedTime = System.currentTimeMillis();
			if(cplex.solve()){
				enlapsedTime = System.currentTimeMillis() - enlapsedTime;
				
			    solution.setOptObjValue(cplex.getObjValue());
			    solution.setOptR(cplex.getValue(R));
			    solution.setOptLogA(cplex.getValue(logA));
			    solution.setResolutionTime(enlapsedTime);
			    solution.setCompilationTime(compilationTime);

			    for (DspVertex dspOperator : dspGraph.getVertices().values()) {
					for (ResourceVertex resNode : resGraph.getVertices().values()) {
						int i = dspOperator.getIndex();
						int u = resNode.getIndex();
						double xval = 0;
						if (dspOperator.deployableOn(u))
							xval = cplex.getValue(X.get(i, u));
			        	if (xval > 0)
			        		solution.setPlacement(i, u);
					}				
				}		

				// cplex.writeSolution("solution.sol");

			} else {
				System.out.println("Unable to find a solution");
			}

			solution.setStatus(cplex.getStatus());

		} catch (IloException e) {
			throw new ODDException("Error while solving OPP Model: " + e.getMessage());
		}
		
		return solution;
	}
	
	public void clean() {
		if (cplex != null)
			cplex.end();
	}
	
	@Override
	public String type() {
		return "BASIC";
	}

	@Override
	public void pin(int iDsp, int uRes){

		try {
		
			if (X.get(iDsp, uRes) != null)
				cplex.addEq(X.get(iDsp, uRes), 1, "pinned_" + iDsp + "," + uRes);
			else
				System.out.println("Invalid pinning");
			
		} catch (IloException e) {
			e.printStackTrace();
		}	
		
	}
}
