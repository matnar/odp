package codd.placement;

import codd.ODDException;
import codd.ODDParameters;
import codd.model.*;
import codd.security.NodeLinkConfiguration;
import codd.security.RequirementsForest;
import codd.security.ScoredValue;
import codd.security.Tree;
import ilog.concert.*;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplexModeler;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Security-aware DSP Placement (SDP).
 *
 * If you use this model, please cite:
 * G. Russo Russo, V. Cardellini, F. Lo Presti, M. Nardelli,
 * "Towards a Security-aware Deployment of Data Streaming Applications in Fog Computing",
 * Fog/Edge Computing For Security, Privacy, and Applications,
 * Advances in Information Security, W. Chang and J. Wu (eds.), Springer, vol. 83, pp. 355–385, 2021.
 * doi: 10.1007/978-3-030-57328-7_14.
 *
 * @author Gabriele Russo Russo
 */
public class SDPModel implements ODDModel {

	protected static final boolean DEBUG = false;

	protected ODDParameters params;
	protected RequirementsForest requirementsForest;

	protected DspGraph dspGraph;
	protected ResourceGraph resGraph;
	protected IloCplex cplex;
	protected PlacementXConfs X;
	protected PlacementYConfs Y;
	protected OptimalSDPSolution solution;

	protected ODDBandwidthModel.MODE mode;

	protected IloNumExpr R;
	protected IloNumExpr C;
	protected IloNumExpr logA;
	protected IloNumExpr logS;

	protected IloNumVar exclusiveUseVar[];
	protected IloNumVar pathRespTimeVar[];

	private long compilationTime;

	private Objective objective;

	public enum Objective
	{
		RESPONSE_TIME, SECURITY, COST
	}

	public SDPModel(DspGraph dspGraph, ResourceGraph resGraph, ODDParameters parameters,
					RequirementsForest requirementsForest, Objective objective) throws ODDException {

		this.dspGraph = dspGraph;
		this.resGraph = resGraph;

		this.params = parameters;
		this.requirementsForest = requirementsForest;
		this.objective = objective;

		this.solution = null;
		this.R = null;
		this.logA = null;
		this.logS = null;
		this.C = null;
		this.X = null;
		this.Y = null;
		this.compilationTime = Long.MAX_VALUE;

		this.mode = parameters.getBwMode();

		try {
			this.cplex = new IloCplex();
		} catch (IloException exc) {
			throw new ODDException("Error while creating model: " + exc.getMessage());
		}

		this.exclusiveUseVar = new IloNumVar[resGraph.getVertices().size()];
		this.pathRespTimeVar = new IloNumVar[dspGraph.getPaths().size()];
	}

	public SDPModel(DspGraph dspGraph, ResourceGraph resGraph, RequirementsForest requirementsForest, Objective objective) throws ODDException {
		this(dspGraph, resGraph, new ODDParameters(ODDModel.MODE.BANDWIDTH, ODDBandwidthModel.MODE.INTERNODE_TRAFFIC),
				requirementsForest, objective);
	}

	public DspGraph getDspGraph() {
		return dspGraph;
	}

	public ResourceGraph getResGraph() {
		return resGraph;
	}


	
	
	public void compile() throws ODDException{
		
		IloModeler modeler = new IloCplexModeler();	
		
		compilationTime = System.currentTimeMillis();
		
		/********************************************************************************
		 * Decision Variables		
		 ********************************************************************************/
		try {
			this.X = new PlacementXConfs(dspGraph, resGraph);
			this.Y = new PlacementYConfs(dspGraph, resGraph);

		} catch (IloException exc) {
			throw new ODDException("Error while defining decision variables X and Y: " + exc.getMessage());
		}

		try {
			for (int v = 0; v < exclusiveUseVar.length; ++v) {
				exclusiveUseVar[v] = modeler.boolVar("excl_" + v);
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining decision variables X and Y: " + exc.getMessage());
		}


	/********************************************************************************
		 * Response-Time		
		 ********************************************************************************/
		try {
			R = cplex.numVar(0, Double.MAX_VALUE, "R");
			int pi = 0;
			for (DspPath path : dspGraph.getPaths()) {

				pathRespTimeVar[pi] = cplex.numVar(0, Double.MAX_VALUE, "Rp" + path.getSource() + "-" + path.getSink());

				/* R_computation */
				IloLinearNumExpr Rpex = modeler.linearNumExpr();	
				for(Integer i : path.getNodesIndexes()){
					DspVertex in = dspGraph.getVertices().get(i);
					for(ResourceVertex vn : resGraph.getVertices().values()){
						if (in.deployableOn(vn)){
							int u = vn.getIndex();
							for (NodeLinkConfiguration conf : vn.getConfigurations()) {
								double speedup = conf.getSpeedupCoeff() * vn.getSpeedup();
								double exTerm = in.getExecutionTime() / speedup;
								Rpex.addTerm(exTerm, X.get(i, u, conf.getIndex()));
							}
						}
					}
				}
				
				/* R_comunication*/
				IloLinearNumExpr Rptx = modeler.linearNumExpr();
				List<Integer> sequence = path.getNodesIndexes();
				for (int k = 0; k < sequence.size() - 1; k++) {
					Integer ik = sequence.get(k);
					Integer ik1 = sequence.get(k + 1);
					for (ResourceEdge eRes : resGraph.getEdges().values()) {
						int u = eRes.getFrom();
						int v = eRes.getTo();
						DspVertex ikDsp = dspGraph.getVertices().get(ik);
						DspVertex ik1Dsp = dspGraph.getVertices().get(ik1);

						if(ikDsp.deployableOn(u) && ik1Dsp.deployableOn(v)){
							for (NodeLinkConfiguration conf : eRes.getConfigurations()) {
								double delay = conf.getDelayCoeff() * eRes.getDelay();
								Rptx.addTerm(delay, Y.get(ik, ik1, u, v, conf.getIndex()));
							}
						}
					}
				}						
				
				/* R, ref.18082015, Eq.12 */
				IloNumExpr Rp = modeler.sum(Rpex, Rptx);
				cplex.addLe(Rp, R, "objBound_R");
				cplex.addEq(Rp, pathRespTimeVar[pi], "objBound_Rpath" + pi);


				/* Response time constraints */
				if (dspGraph.getVertices().get(path.getSink()).getId().equals("alarmSink")) {
					cplex.addLe(Rp, params.getCriticalRmax());
	 			} else {
					cplex.addLe(Rp, params.getRmax());
				}

				pi++;
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Response-Time: " + exc.getMessage());
		}
		
		/********************************************************************************
		 * Availability		
		 ********************************************************************************/
		// TODO: availability coeff from conf?
		try {
			logA  = modeler.numVar(-Double.MAX_VALUE, 0, "logA");
			
			/* A nodes */
			IloLinearNumExpr logAex = modeler.linearNumExpr();
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					
					if (iDsp.deployableOn(uRes)){
						int i = iDsp.getIndex();
						int u = uRes.getIndex();
						for (NodeLinkConfiguration conf : uRes.getConfigurations()) {
							logAex.addTerm(Math.log(uRes.getAvailability()), X.get(i, u, conf.getIndex()));
						}
					}
				}					
			}				
			
			/* A links */
			IloLinearNumExpr logAtx = modeler.linearNumExpr();
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				for (ResourceEdge uvRes: resGraph.getEdges().values()) {
					int i = ijDsp.getFrom();
					int j = ijDsp.getTo();
					int u = uvRes.getFrom();
					int v = uvRes.getTo();
					
					DspVertex iDsp = dspGraph.getVertices().get(i);
					DspVertex jDsp = dspGraph.getVertices().get(j);
					
					if(iDsp.deployableOn(u) && jDsp.deployableOn(v)){
						for (NodeLinkConfiguration conf : uvRes.getConfigurations()) {
							logAtx.addTerm(Math.log(uvRes.getAvailability()), Y.get(i, j, u, v, conf.getIndex()));
						}
					}
				}					
			}
			IloNumExpr logAx = modeler.sum(logAex, logAtx);
			cplex.addEq(logAx, logA, "objBound_A");
			
			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Availability: " + exc.getMessage());
		}

		final double MIN_S = 0.0001;

		/********************************************************************************
		 * Security-related constraints
		 ********************************************************************************/

		try {
			logS  = modeler.numVar(-Double.MAX_VALUE, 0, "logS");
		} catch (IloException e) {
			e.printStackTrace();
		}

		try {
			ArrayList<String> reqCategories = new ArrayList<>(requirementsForest.getReqCategories());
			for (String rc : reqCategories) {
				Tree rcTree = requirementsForest.getTree(rc);
				for (Tree vertexOrEdgeTree : rcTree.getChildren()) {
					for (Tree roTree : vertexOrEdgeTree.getChildren()) {
						String ro = (String)roTree.getRootElem();

						if (vertexOrEdgeTree.getRootElem() instanceof DspVertex) {
							DspVertex iDsp = (DspVertex)vertexOrEdgeTree.getRootElem();
							for (ResourceVertex uRes : resGraph.getVertices().values()) {
								if (iDsp.deployableOn(uRes)) {
									int i = iDsp.getIndex();
									int u = uRes.getIndex();
									for (NodeLinkConfiguration conf : uRes.getConfigurations()) {
										Object confValue = conf.getSpec(rc, ro);
										double score = 0;
										if (confValue != null ) {
											for (Tree valueTree : roTree.getChildren())	 {
												ScoredValue sv = (ScoredValue)valueTree.getRootElem();
												if (sv.value.equals(confValue)) {
													score = sv.score;
												}
											}
										}
//										String msg = String.format("%s on %s (%s) -> %f\n", iDsp.getId(),
//												uRes.getIndex(), conf.toString(), score);
//										System.out.print(msg);
										if (score < MIN_S) {
											/* Invalid conf. */
											cplex.addEq(0.0, X.get(i,u,conf.getIndex()));
										}
									}
								}
							}
						} else {
							DspEdge eDsp = (DspEdge)vertexOrEdgeTree.getRootElem();
							for (ResourceEdge eRes : resGraph.getEdges().values()) {
								int i = eDsp.getFrom();
								int j = eDsp.getTo();
								int u = eRes.getFrom();
								int v = eRes.getTo();

								DspVertex iDsp = dspGraph.getVertices().get(i);
								DspVertex jDsp = dspGraph.getVertices().get(j);

								if(iDsp.deployableOn(u) && jDsp.deployableOn(v)){
									for (NodeLinkConfiguration conf : eRes.getConfigurations()) {
										Object confValue = conf.getSpec(rc, ro);
										double score = 0;
										if (confValue != null ) {
											for (Tree valueTree : roTree.getChildren())	 {
												ScoredValue sv = (ScoredValue)valueTree.getRootElem();
												if (sv.value.equals(confValue)) {
													score = sv.score;
												}
											}
										}
										if (score < MIN_S) {
											/* Invalid conf. */
											cplex.addLe(0.0, X.get(i,u,conf.getIndex()));
										}
									}
								}
							}
						}
					}

				}
			}

			/*
			 * Global S constraint.
			 */
			if (params.getSecurityMinDelta() > 0.0) {
				cplex.addGe(logS, Math.log(params.getSecurityMinDelta()));
			}

		} catch (IloException exc) {
			throw new ODDException("Error while defining Security constraints: " + exc.getMessage());
		}

		/********************************************************************************
		 * Security-related metrics
		 ********************************************************************************/
		try {
			/* S nodes */
			ArrayList<String> reqCategories = new ArrayList<>(requirementsForest.getReqCategories());
			IloLinearNumExpr logSex = modeler.linearNumExpr();
			for (String rc : reqCategories) {
				Tree rcTree = requirementsForest.getTree(rc);
				for (Tree vertexOrEdgeTree : rcTree.getChildren()) {
					for (Tree roTree : vertexOrEdgeTree.getChildren()) {
						String ro = (String)roTree.getRootElem();

							if (vertexOrEdgeTree.getRootElem() instanceof DspVertex) {
								DspVertex iDsp = (DspVertex)vertexOrEdgeTree.getRootElem();
								for (ResourceVertex uRes : resGraph.getVertices().values()) {
									if (iDsp.deployableOn(uRes)) {
										int i = iDsp.getIndex();
										int u = uRes.getIndex();
										for (NodeLinkConfiguration conf : uRes.getConfigurations()) {
											Object confValue = conf.getSpec(rc, ro);
											double score = 0;
											if (confValue != null ) {
												for (Tree valueTree : roTree.getChildren())	 {
													ScoredValue sv = (ScoredValue)valueTree.getRootElem();
													if (sv.value.equals(confValue)) {
														score = sv.score;
													}
												}
											}
											if (score >= MIN_S) {
												double logScore = Math.log(score);
												logSex.addTerm(logScore, X.get(i, u, conf.getIndex()));
											}
										}
									}
								}
							} else {
								DspEdge eDsp = (DspEdge)vertexOrEdgeTree.getRootElem();
								for (ResourceEdge eRes : resGraph.getEdges().values()) {
									int i = eDsp.getFrom();
									int j = eDsp.getTo();
									int u = eRes.getFrom();
									int v = eRes.getTo();

									DspVertex iDsp = dspGraph.getVertices().get(i);
									DspVertex jDsp = dspGraph.getVertices().get(j);

									if(iDsp.deployableOn(u) && jDsp.deployableOn(v)){
										for (NodeLinkConfiguration conf : eRes.getConfigurations()) {
											Object confValue = conf.getSpec(rc, ro);
											double score = 0;
											if (confValue != null ) {
												for (Tree valueTree : roTree.getChildren())	 {
													ScoredValue sv = (ScoredValue)valueTree.getRootElem();
													if (sv.value.equals(confValue)) {
														score = sv.score;
													}
												}
											}
											if (score >= MIN_S) {
												double logScore = Math.log(score);
												logSex.addTerm(logScore, Y.get(i, j, u, v, conf.getIndex()));
											}
										}
									}
								}
							}
						}

				}
			}
			cplex.addEq(logSex, logS, "objBound_S");


		} catch (IloException exc) {
			throw new ODDException("Error while defining Security metric: " + exc.getMessage());
		}


		/********************************************************************************
		 * Exclusive use of the nodes.
		 ********************************************************************************/
		final double BIG_M = 99999.0;

		try {
			for (ResourceVertex vRes : resGraph.getVertices().values()) {
				int v = vRes.getIndex();
				IloLinearNumExpr exprExcl = modeler.linearNumExpr(0.0);
				IloLinearNumExpr exprNotExcl = modeler.linearNumExpr(0.0);

				for (Integer i : dspGraph.getVertices().keySet()) {
					for (NodeLinkConfiguration conf : vRes.getConfigurations()) {
						if (!conf.doesRequireExclusiveUse())
							exprNotExcl.addTerm(1.0, X.get(i, v, conf.getIndex()));
						else
							exprExcl.addTerm(1.0, X.get(i, v, conf.getIndex()));
					}
				}

				cplex.addGe(exclusiveUseVar[v], exprExcl);

				IloNumExpr lhs = modeler.negative(modeler.sum(exclusiveUseVar[v],-1.0));
				IloNumExpr lhs2 = modeler.prod(BIG_M, lhs);
				cplex.addGe(lhs2, exprNotExcl);
			}
		} catch (IloException e) {
			e.printStackTrace();
		}

		/********************************************************************************
		 * Cost
		 ********************************************************************************/
		try {
			C = cplex.numVar(0, Double.MAX_VALUE, "C");
			IloLinearNumExpr costExpr = modeler.linearNumExpr();

			for (ResourceVertex vRes : resGraph.getVertices().values()) {
				int v = vRes.getIndex();

				for (NodeLinkConfiguration conf : vRes.getConfigurations()) {
					if (conf.doesRequireExclusiveUse())
						continue;
					for (Integer i : dspGraph.getVertices().keySet()) {
						DspVertex iDsp = dspGraph.getVertices().get(i);
						if (iDsp.deployableOn(v)) {
							final double cost = iDsp.getRequiredResources() * conf.getUsedResCoeff() * conf.getCostCoeff() * vRes.getPerResourceCost();
							costExpr.addTerm(cost, X.get(i, v, conf.getIndex()));
						}
					}
				}

				// TODO: maybe we should consider specific configuration cost coeff as well
				costExpr.addTerm(vRes.getExlusiveUseCost(), exclusiveUseVar[v]);
			}

			for (ResourceEdge eRes : resGraph.getEdges().values()) {
				int u = eRes.getFrom();
				int v = eRes.getTo();
				for (DspEdge eDsp : dspGraph.getEdges().values()) {
					int i = eDsp.getFrom();
					int j = eDsp.getTo();

					DspVertex iDsp = dspGraph.getVertices().get(i);
					DspVertex jDsp = dspGraph.getVertices().get(j);

					if (iDsp.deployableOn(u) && jDsp.deployableOn(v)){
						for (NodeLinkConfiguration conf : eRes.getConfigurations()) {
							final double cost = eDsp.getLambda()*eDsp.getBytesPerTuple()*conf.getUsedBwCoeff()*eRes.getCostPerUnitData();
							costExpr.addTerm(cost, Y.get(i,j,u,v,conf.getIndex()));
						}
					}
				}
			}

			cplex.addEq(costExpr, C);

			/* Cost constraint */
			cplex.addLe(C, params.getCmax());
		} catch (IloException e) {
			e.printStackTrace();
		}




		/********************************************************************************
		 * Objective
		 ********************************************************************************/
		IloObjective obj;
		IloNumExpr objRExpr, objAExpr, objCExpr, objSExpr, objExpr;
		try {			
			//objRExpr = modeler.prod(modeler.sum(params.getRmax(), modeler.negative(R)), params.getWeightRespTime() / (params.getRmax() - params.getRmin()));
			objCExpr = modeler.prod(modeler.sum(params.getCmax(), modeler.negative(C)), params.getWeightCost() / (params.getCmax() - params.getCmin()));
			objAExpr = modeler.prod(modeler.sum(logA, (-Math.log(params.getAmin()))), 	params.getWeightAvailability() / (Math.log(params.getAmax()) - Math.log(params.getAmin())));

			final double logSmin = Math.log(MIN_S);
			final double logSmax = 0.0;
			//objSExpr = modeler.prod(modeler.sum(logS, (-logSmin)), 	params.getWeightSecurity() / (logSmax - logSmin));

			//objExpr  = modeler.sum(objRExpr, objAExpr, objSExpr, objCExpr);
			//obj 	 = modeler.maximize(objExpr);

			if (objective.equals(Objective.SECURITY)) {
				obj = modeler.maximize(logS);
			} else if (objective.equals(Objective.RESPONSE_TIME)) {
				obj = modeler.minimize(R);
			} else if (objective.equals(Objective.COST)) {
				obj = modeler.minimize(C);
			} else {
				throw new RuntimeException("Invalid objective!");
			}

			cplex.addObjective(obj.getSense(), obj.getExpr(), "Fx");
		} catch (IloException exc) {
			throw new ODDException("Error while defining Objective Function: " + exc.getMessage());
		}
		

		/********************************************************************************
		 * Capacity Bound - Eq.15
		 ********************************************************************************/
		try {
			for (ResourceVertex uRes : resGraph.getVertices().values()) {
				IloLinearNumExpr deployedOpCapacity = modeler.linearNumExpr();
				for (DspVertex iDsp : dspGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();
					
					if (iDsp.deployableOn(u)){
						for (NodeLinkConfiguration conf : uRes.getConfigurations()) {
							double requiredRes = iDsp.getRequiredResources() * conf.getUsedResCoeff();
							deployedOpCapacity.addTerm(requiredRes, X.get(i, u, conf.getIndex()));
						}
					}
				}					
				cplex.addLe(deployedOpCapacity, uRes.getAvailableResources(), 
						"capb_res_" + uRes.getIndex());
			}			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Capacity Bound: " + exc.getMessage());
		}

		try {
			for (ResourceEdge eRes : resGraph.getEdges().values()) {
				IloLinearNumExpr deployedOpCapacity = modeler.linearNumExpr();
				int u = eRes.getFrom();
				int v = eRes.getTo();
				for (DspEdge eDsp : dspGraph.getEdges().values()) {
					int i = eDsp.getFrom();
					int j = eDsp.getTo();

					DspVertex iDsp = dspGraph.getVertices().get(i);
					DspVertex jDsp = dspGraph.getVertices().get(j);

					if (iDsp.deployableOn(u) && jDsp.deployableOn(v)){
						for (NodeLinkConfiguration conf : eRes.getConfigurations()) {
							double requiredRes = eDsp.getLambda()*eDsp.getBytesPerTuple()*conf.getUsedBwCoeff();
							deployedOpCapacity.addTerm(requiredRes, Y.get(i, j, u, v, conf.getIndex()));
						}
					}
				}
				cplex.addLe(deployedOpCapacity, eRes.getBandwidth(), "capb_res_" + u + "," + v);
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Capacity Bound for links: " + exc.getMessage());
		}

		/********************************************************************************
		 * Uniqueness Bound - Eq.16
		 ********************************************************************************/		
		try {
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				IloLinearNumExpr exprUniqueness = modeler.linearNumExpr();
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();
					
					if (iDsp.deployableOn(u)){
						for (NodeLinkConfiguration conf : uRes.getConfigurations()) {
							exprUniqueness.addTerm(1.0, X.get(i, u, conf.getIndex()));
						}
					}
				}
				cplex.addEq(exprUniqueness, 1.0, 
						"uniqb_dsp_" + iDsp.getIndex());
			}			
		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}
		
		/********************************************************************************
		 * Connectivity Bound - Eq.28, 29 - review 25.09.2015 
		 ********************************************************************************/
		try {
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);
			
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int u = uRes.getIndex();
					
					if(iDsp.deployableOn(u)){
						IloLinearNumExpr exprConn = modeler.linearNumExpr();
						for (ResourceVertex vRes : resGraph.getVertices().values()) {
							int v = vRes.getIndex();
							
							if(jDsp.deployableOn(v)){
								ResourceEdge uvRes = resGraph.getEdges().get(new Pair(u, v));
								for (NodeLinkConfiguration conf : uvRes.getConfigurations()) {
									exprConn.addTerm(1.0, Y.get(i, j, u, v, conf.getIndex()));
								}
							}						
						}

						IloLinearNumExpr lhsExpr = modeler.linearNumExpr();
						for (NodeLinkConfiguration conf : uRes.getConfigurations()) {
							lhsExpr.addTerm(1.0, X.get(i, u, conf.getIndex()));
						}
						cplex.addEq(lhsExpr, exprConn, "conb1_" + i + "," + j);
					}
				}
			}
		
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);
				
				for (ResourceVertex vRes : resGraph.getVertices().values()) {
					int v = vRes.getIndex();
					
					if(jDsp.deployableOn(v)){
						IloLinearNumExpr exprConn = modeler.linearNumExpr();
						for (ResourceVertex uRes : resGraph.getVertices().values()) {
							int u = uRes.getIndex();
							
							if(iDsp.deployableOn(u)){
								ResourceEdge uvRes = resGraph.getEdges().get(new Pair(u,v));
								for (NodeLinkConfiguration conf : uvRes.getConfigurations()) {
									exprConn.addTerm(1.0, Y.get(i, j, u, v, conf.getIndex()));
								}
							}	
						}

						IloLinearNumExpr lhsExpr = modeler.linearNumExpr();
						for (NodeLinkConfiguration conf : vRes.getConfigurations()) {
							lhsExpr.addTerm(1.0, X.get(j, v, conf.getIndex()));
						}
						cplex.addEq(lhsExpr, exprConn, "conb2_" + i + "," + j);
					}
				}
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Connectivity Bound: " + exc.getMessage());
		}	
		compilationTime = System.currentTimeMillis() - compilationTime;

		/* XXX: DEBUG */ 
		if (DEBUG){
			try {
				cplex.exportModel("lpex1.lp");
			} catch (IloException e) {
				throw new ODDException("Error while exporting ODDModel on file: " + e.getMessage());
			}	
		}
		

		
	}
	
	public void clean() {
		if (cplex != null)
			cplex.end();
	}
		
	public OptimalSolution solve() throws ODDException{
		
		if (!DEBUG)
			cplex.setOut(null);
		
		if (R == null)
			compile();
		
		try {
			solution = new OptimalSDPSolution(dspGraph.getVertices().size(), resGraph, dspGraph);
		    
			if (params.isUseTimeLimit()){
				cplex.setParam(IloCplex.IntParam.TimeLimit, params.getTimeLimit()); //expressed in seconds 
				
			}
			if (params.isDefineOptimalGap()){			
				cplex.setParam(IloCplex.DoubleParam.EpGap, params.getGap());
			}
			
			long enlapsedTime = System.currentTimeMillis();
			if(cplex.solve()){
				enlapsedTime = System.currentTimeMillis() - enlapsedTime;
				
			    solution.setOptObjValue(cplex.getObjValue());
			    solution.setOptR(cplex.getValue(R));
			    solution.setOptLogA(cplex.getValue(logA));
			    solution.setOptLogS(cplex.getValue(logS));
			    solution.setOptC(cplex.getValue(C));
			    //solution.setOptZ(cplex.getValue(Z));
			    solution.setResolutionTime(enlapsedTime);
			    solution.setCompilationTime(compilationTime);
			    
			    for (DspVertex dspOperator : dspGraph.getVertices().values()) {
					int i = dspOperator.getIndex();
					for (ResourceVertex resNode : resGraph.getVertices().values()) {
						int u = resNode.getIndex();
						if (!dspOperator.deployableOn(u))
							continue;
						for (NodeLinkConfiguration conf : resNode.getConfigurations()) {
							double xval = cplex.getValue(X.get(i, u, conf.getIndex()));
							if (xval > 0) {
								solution.setPlacement(i, u);
								solution.setPlacementWithConf(i, u, conf.getIndex());
							}
						}
					}				
				}

				for (DspEdge dspEdge : dspGraph.getEdges().values()) {
					int i = dspEdge.getFrom();
					int j = dspEdge.getTo();
					DspVertex iop = dspGraph.getVertices().get(i);
					DspVertex jop = dspGraph.getVertices().get(j);

					for (ResourceEdge resEdge : resGraph.getEdges().values()) {
						int u = resEdge.getFrom();
						int v = resEdge.getTo();

						if (!iop.deployableOn(u) || !jop.deployableOn(v))
							continue;

						for (NodeLinkConfiguration conf : resEdge.getConfigurations()) {
							double yval = cplex.getValue(Y.get(i, j, u, v, conf.getIndex()));
							if (yval > 0) {
								solution.setLinkConfiguration(i,j,conf.getIndex());
							}
						}
					}
				}

				for (ResourceVertex resNode : resGraph.getVertices().values()) {
					int u = resNode.getIndex();
					double val = cplex.getValue(exclusiveUseVar[u]);
					solution.setExclusivelyUsedNode(u, (val > 0));
				}

				double pathRespTime[] = new double[dspGraph.getPaths().size()];
				for (int i = 0; i<dspGraph.getPaths().size(); i++) {
					pathRespTime[i] = cplex.getValue(pathRespTimeVar[i]);
				}
				solution.setPathRespTime(pathRespTime);

				if (DEBUG)
					cplex.writeSolution("solution.sol");

			} else {
				System.out.println("Unable to find a solution");
			}

			solution.setStatus(cplex.getStatus());

			/* Printing solution */
		    System.out.println(" ---------------------------------------------- ");
			System.out.println(solution.toString());
		    System.out.println(" ---------------------------------------------- ");

		} catch (IloException e) {
			e.printStackTrace();
			throw new ODDException("Error while solving OPP Model: " + e.getMessage());
		}
		
		return solution;
	}

	@Override
	public String type() {
		return "SDP";
	}

	
	@Override
	public void pin(int dspIndex, int resIndex) {

		try {

			ResourceVertex uRes = resGraph.getVertices().get(resIndex);
			IloLinearNumExpr expr = cplex.linearNumExpr();
			for (NodeLinkConfiguration conf : uRes.getConfigurations()) {
				IloNumVar var = X.get(dspIndex, resIndex, conf.getIndex());
				if (var != null)	 {
					expr.addTerm(1.0, var);
				}
			}
			cplex.addEq(expr, 1, "pinned");

		} catch (IloException e) {
			e.printStackTrace();
		}	
		
	}
	
}
