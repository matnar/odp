package codd.placement;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloModeler;
import ilog.concert.IloNumExpr;
import ilog.concert.IloObjective;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplexModeler;

import java.util.List;
import java.util.Map;

import codd.ODDException;
import codd.ODDParameters;
import codd.ResVertexMultisetGenerator;
import codd.metric.MultisetMetricManager;
import codd.model.ActiveLinkZ;
import codd.model.ActiveNodeZ;
import codd.model.DspEdge;
import codd.model.DspGraph;
import codd.model.DspPath;
import codd.model.DspVertex;
import codd.model.OptimalMultisetSolution;
import codd.model.OptimalSolution;
import codd.model.Pair;
import codd.model.PlacementXMultiset;
import codd.model.PlacementYMultiset;
import codd.model.ResourceEdge;
import codd.model.ResourceGraph;
import codd.model.ResourceVertex;
import codd.model.ResourceVertexMultiset;

import static codd.placement.ODDBandwidthModel.SWAP_MAX_DISK_USAGE;

/**
 * Optimal DSP Replication and Placement model (ODRP).
 *
 * If you use this model, please cite:
 * V. Cardellini, V. Grassi, F. Lo Presti, M. Nardelli,
 * "Optimal Operator Replication and Placement for Distributed Stream Processing Systems",
 * ACM SIGMETRICS Performance Evaluation Review,
 * Vol. 44, No. 4, pp. 11-22, May 2017.
 * doi: 10.1145/3092819.3092823.
 *
 * @author Matteo Nardelli
 */
public class ODDReplicatedOperatorsModel extends ODDBandwidthModel implements ODDModel {

	protected final boolean DEBUG = true;
	protected static boolean CPLEX_DEBUG = false;

	protected MultisetMetricManager metricManager;
	protected Map<Integer, ResourceVertexMultiset> allMultisets;

	protected ActiveNodeZ Zv;
	protected ActiveLinkZ Ze;
	protected IloNumExpr C;
	protected OptimalMultisetSolution solution;

	protected long compilationTime;
	protected int maxMultisetCardinality = ResVertexMultisetGenerator.MULTISET_CARDINALITY;

	public Map<Integer, ResourceVertexMultiset> getMultisets()
	{
		return allMultisets;
	}

	public ODDReplicatedOperatorsModel(DspGraph dspGraph, ResourceGraph resGraph, ODDParameters parameters) throws ODDException {

		super(dspGraph, resGraph, parameters);

		this.C = null;
		this.solution = null;

		this.metricManager = new MultisetMetricManager(dspGraph, resGraph, parameters.isOdrpUseMM1());

	}

	protected void initializeVariables() throws IloException {
		this.X = new PlacementXMultiset(dspGraph, resGraph, allMultisets.values());
		this.Y = new PlacementYMultiset(dspGraph, resGraph, allMultisets.values());

		this.Zv = new ActiveNodeZ(resGraph);
		this.Ze = new ActiveLinkZ(resGraph);
	}

	protected boolean canDeploy(DspVertex v, ResourceVertexMultiset multiset) {
		boolean canDeploy = v.deployableOn(multiset) && (X.get(v.getIndex(), multiset.getIndex()) != null);
		return canDeploy;
	}

	protected void compileConstraintsAndVars(IloModeler modeler) throws ODDException {
		/********************************************************************************
		 * Decision Variables		
		 ********************************************************************************/
		try {
			initializeVariables();
		} catch (IloException exc) {
			throw new ODDException("Error while defining decision variables X and Y: " + exc.getMessage());
		}

		/********************************************************************************
		 * Response-Time		
		 ********************************************************************************/
		try {
			R = cplex.numVar(0, Double.MAX_VALUE, "R");
			for (DspPath path : dspGraph.getPaths()) {

				/* R_computation */
				IloLinearNumExpr Rpex = modeler.linearNumExpr();
				for (Integer i : path.getNodesIndexes()) {
					DspVertex in = dspGraph.getVertices().get(i);
					boolean ok = false;

					for (ResourceVertexMultiset ums : allMultisets.values()) {

						if (canDeploy(in, ums)) {
							int u = ums.getIndex();
							double execTime = metricManager.getExecutionTime(i, ums);
							if (execTime < Double.MAX_VALUE)
								ok = true;

							Rpex.addTerm(execTime, X.get(i, u));
						}

					}

					if (!ok)
						throw new RuntimeException("No feasible deployment for operator " + i);

				}
				
				/* R_comunication*/
				IloLinearNumExpr Rptx = modeler.linearNumExpr();
				List<Integer> sequence = path.getNodesIndexes();
				for (int k = 0; k < sequence.size() - 1; k++) {
					Integer ik = sequence.get(k);
					Integer ik1 = sequence.get(k + 1);
					DspVertex ikDsp = dspGraph.getVertices().get(ik);
					DspVertex ik1Dsp = dspGraph.getVertices().get(ik1);

					for (ResourceVertexMultiset ums : allMultisets.values()) {
						for (ResourceVertexMultiset vms : allMultisets.values()) {
							if (canDeploy(ikDsp, ums) && canDeploy(ik1Dsp, vms)) {
								int u = ums.getIndex();
								int v = vms.getIndex();

								Rptx.addTerm(
										metricManager.getNetworkDelay(ik.intValue(), ik1.intValue(), ums, vms),
										Y.get(ik, ik1, u, v));

							}
						}
					}
				}

				IloNumExpr Rp = modeler.sum(Rpex, Rptx);
				cplex.addLe(Rp, R, "objBound_R");
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Response-Time: " + exc.getMessage());
		}

		/********************************************************************************
		 * Response-Time: constraint on R values (which have to be greater than 0)
		 ********************************************************************************/
		try {
			/* R_computation */
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				int i = iDsp.getIndex();
				for (ResourceVertexMultiset ums : allMultisets.values()) {
					if (canDeploy(iDsp, ums)) {
						int u = ums.getIndex();
						IloLinearNumExpr rPositivity = modeler.linearNumExpr();
						rPositivity.addTerm(metricManager.getExecutionTime(i, ums), X.get(i, u));
						cplex.addGe(rPositivity, 0, "positivity_R_" + i + "_" + u);
					}
				}
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Response-Time: " + exc.getMessage());
		}

		/********************************************************************************
		 * Cost		
		 ********************************************************************************/
		try {
			C = modeler.numVar(0, Double.MAX_VALUE, "C");
			
			/* C nodes */
			IloLinearNumExpr Cex = modeler.linearNumExpr();
			for (DspVertex iDsp : dspGraph.getVertices().values()) {

				// Ci
				for (ResourceVertexMultiset ums : allMultisets.values()) {
					if (canDeploy(iDsp, ums)) {
						int i = iDsp.getIndex();
						int u = ums.getIndex();
						double ciU = metricManager.getCost(i, ums);

						Cex.addTerm(ciU, X.get(i, u));
					}
				}
			}				
			
			/* C links */
			IloLinearNumExpr Ctx = modeler.linearNumExpr();
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {

				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);

				// Cij
				for (ResourceVertexMultiset ums : allMultisets.values()) {
					if (canDeploy(iDsp, ums)) {
						for (ResourceVertexMultiset vms : allMultisets.values()) {
							if (canDeploy(jDsp, vms)) {
								int u = ums.getIndex();
								int v = vms.getIndex();
								double cijUV = metricManager.getCost(i, j, ums, vms);

								Ctx.addTerm(cijUV, Y.get(i, j, u, v));
							}
						}
					}
				}
			}
			IloNumExpr Cexp = modeler.sum(Cex, Ctx);
			cplex.addEq(Cexp, C, "objBound_C");

		} catch (IloException exc) {
			throw new ODDException("Error while defining Availability: " + exc.getMessage());
		}


		/********************************************************************************
		 * Availability		
		 ********************************************************************************/
		try {
			logA = modeler.numVar(-Double.MAX_VALUE, 0, "logA");
			
			/* A nodes */
			IloLinearNumExpr logAex = modeler.linearNumExpr();
			for (ResourceVertex uRes : resGraph.getVertices().values()) {
				int u = uRes.getIndex();
				logAex.addTerm(Math.log(uRes.getAvailability()), Zv.get(u));
			}					
			
			/* A links */
			IloLinearNumExpr logAtx = modeler.linearNumExpr();
			for (ResourceEdge uvRes : resGraph.getEdges().values()) {
				int u = uvRes.getFrom();
				int v = uvRes.getTo();

				logAtx.addTerm(Math.log(uvRes.getAvailability()), Ze.get(u, v));
			}

			IloNumExpr logAx = modeler.sum(logAex, logAtx);
			cplex.addEq(logAx, logA, "objBound_A");

		} catch (IloException exc) {
			throw new ODDException("Error while defining Availability: " + exc.getMessage());
		}


		/********************************************************************************
		 * Bandwidth: internode traffic, network utilization or elastic energy
		 ********************************************************************************/
		if (!params.getMode().equals(ODDModel.MODE.BASIC)) {

			try {
				Z = cplex.numVar(0, Double.MAX_VALUE, "Z");
				IloLinearNumExpr exprZ = modeler.linearNumExpr();

				for (DspEdge ij : dspGraph.getEdges().values()) {
					int i = ij.getFrom();
					int j = ij.getTo();
					DspVertex iDsp = dspGraph.getVertices().get(i);
					DspVertex jDsp = dspGraph.getVertices().get(j);
					
					/* Z = sum_{ij} Z_{ij} */
					for (ResourceVertexMultiset ums : allMultisets.values()) {
						if (canDeploy(iDsp,ums)) {
							int umsIndex = ums.getIndex();

							for (ResourceVertexMultiset vms : allMultisets.values()) {
								if (canDeploy(jDsp,vms)) {
									int vmsIndex = vms.getIndex();

									double lambda = metricManager.getLambda(i, j, ums, vms) * ij.getBytesPerTuple();

									for (ResourceVertex uRes : ums.getVertices()) {
										for (ResourceVertex vRes : vms.getVertices()) {

											// do not consider self-loops (i.e., traffic exchanged within the same node)
											if (uRes.getIndex() == vRes.getIndex())
												continue;

											if (mode == ODDBandwidthModel.MODE.INTERNODE_TRAFFIC) {

												exprZ.addTerm(lambda, Y.get(i, j, umsIndex, vmsIndex));

											} else if (mode == ODDBandwidthModel.MODE.NETWORK_UTILIZATION) {

												ResourceEdge uvRes = resGraph.getEdges().get(new Pair(uRes.getIndex(), vRes.getIndex()));
												double lambdaTimesDelay = lambda * uvRes.getDelay();
												exprZ.addTerm(lambdaTimesDelay, Y.get(i, j, umsIndex, vmsIndex));

											} else if (mode == ODDBandwidthModel.MODE.APPROX_ELASTIC_ENERGY) {

												ResourceEdge uvRes = resGraph.getEdges().get(new Pair(uRes.getIndex(), vRes.getIndex()));
												double lambdaTimesDelay2 = lambda * uvRes.getDelay() * uvRes.getDelay();
												exprZ.addTerm(lambdaTimesDelay2, Y.get(i, j, umsIndex, vmsIndex));

											}
										}
									}
								}
							}
						}
					}
				}

				cplex.addEq(exprZ, Z, "objBound_Z");

			} catch (IloException exc) {
				throw new ODDException("Error while defining Bandwidth term in ObjFunct: " + exc.getMessage());
			}
		}


		/********************************************************************************
		 * Capacity Bound - Eq.15
		 ********************************************************************************/
		try {
			for (ResourceVertex uRes : resGraph.getVertices().values()) {
				IloLinearNumExpr uResCapacity = modeler.linearNumExpr();
				for (DspVertex iDsp : dspGraph.getVertices().values()) {
					int i = iDsp.getIndex();

					for (ResourceVertexMultiset ums : allMultisets.values()) {
						if (canDeploy(iDsp,ums)) {
							int u = ums.getIndex();
							int uMult = ums.getMultiplicity(uRes);
							int resi = iDsp.getRequiredResources();

							uResCapacity.addTerm(uMult * resi, X.get(i, u));
						}
					}

				}
				cplex.addLe(uResCapacity, uRes.getAvailableResources(),
						"capb_res_" + uRes.getIndex());
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Capacity Bound: " + exc.getMessage());
		}

		/********************************************************************************
		 * Zv def 
		 ********************************************************************************/
		try {
			double M1 = dspGraph.getVertices().size() * allMultisets.size();
			if (M1 != 0)
				M1 = (double) 1.0 / M1;

			for (ResourceVertex uRes : resGraph.getVertices().values()) {
				IloLinearNumExpr exprActivateNodeZ = modeler.linearNumExpr();
				IloLinearNumExpr exprActivateNodeZLB = modeler.linearNumExpr();

				for (DspVertex iDsp : dspGraph.getVertices().values()) {
					int i = iDsp.getIndex();

					for (ResourceVertexMultiset ums : allMultisets.values()) {

						if (!ums.getVertices().contains(uRes))
							continue;

						if (canDeploy(iDsp,ums)) {
							int umsIndex = ums.getIndex();

							exprActivateNodeZ.addTerm(M1, X.get(i, umsIndex));
							exprActivateNodeZLB.addTerm(1.0, X.get(i, umsIndex));
						}
					}
				}

				int u = uRes.getIndex();
				cplex.addGe(Zv.get(u), exprActivateNodeZ, "activation_node_z_" + u);
				cplex.addLe(Zv.get(u), exprActivateNodeZLB, "activation_node_lb_z_" + u);

			}

		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}


		/********************************************************************************
		 * Ze def 
		 ********************************************************************************/
		try {
			double N1 = dspGraph.getVertices().size() * allMultisets.size() * allMultisets.size();
			if (N1 != 0)
				N1 = (double) 1.0 / N1;

			for (ResourceEdge uvRes : resGraph.getEdges().values()) {
				IloLinearNumExpr exprActivateLinkZ = modeler.linearNumExpr();
				IloLinearNumExpr exprActivateLinkZLB = modeler.linearNumExpr();

				int u = uvRes.getFrom();
				int v = uvRes.getTo();
				ResourceVertex uRes = resGraph.getVertices().get(u);
				ResourceVertex vRes = resGraph.getVertices().get(v);

				for (DspEdge ijDsp : dspGraph.getEdges().values()) {
					int i = ijDsp.getFrom();
					int j = ijDsp.getTo();
					DspVertex iDsp = dspGraph.getVertices().get(i);
					DspVertex jDsp = dspGraph.getVertices().get(j);

					for (ResourceVertexMultiset ums : allMultisets.values()) {
						if (ums.getVertices().contains(uRes) && canDeploy(iDsp,ums)) {
							int umsIndex = ums.getIndex();

							for (ResourceVertexMultiset vms : allMultisets.values()) {
								if (vms.getVertices().contains(vRes) && canDeploy(jDsp, vms)) {
									int vmsIndex = vms.getIndex();

									exprActivateLinkZ.addTerm(N1, Y.get(i, j, umsIndex, vmsIndex));
									exprActivateLinkZLB.addTerm(1.0, Y.get(i, j, umsIndex, vmsIndex));
								}
							}
						}
					}
				}

				cplex.addGe(Ze.get(u, v), exprActivateLinkZ, "activation_link_z_" + u);
				cplex.addLe(Ze.get(u, v), exprActivateLinkZLB, "activation_link_lb_z_" + u);

			}

		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}


		/********************************************************************************
		 * Uniqueness Bound - Eq.16
		 ********************************************************************************/
		try {
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				int i = iDsp.getIndex();
				IloLinearNumExpr exprUniqueness = modeler.linearNumExpr();
				for (ResourceVertexMultiset ums : allMultisets.values()) {
					if (canDeploy(iDsp,ums)) {
						int u = ums.getIndex();

						exprUniqueness.addTerm(1.0, X.get(i, u));
					}
				}
				cplex.addEq(exprUniqueness, 1.0,
						"uniqb_dsp_" + iDsp.getIndex());
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}


		/********************************************************************************
		 * Connectivity Bound - Eq.28, 29 - review 25.09.2015 
		 ********************************************************************************/
		try {
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);

				for (ResourceVertexMultiset ums : allMultisets.values()) {
					if (canDeploy(iDsp,ums)) {
						int u = ums.getIndex();

						IloLinearNumExpr exprConn = modeler.linearNumExpr();

						for (ResourceVertexMultiset vms : allMultisets.values()) {
							if (canDeploy(jDsp,vms)) {
								int v = vms.getIndex();

								exprConn.addTerm(1.0, Y.get(i, j, u, v));
							}
						}
						cplex.addEq(X.get(i, u), exprConn, "conb1_" + i + "," + j);
					}
				}
			}

			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);

				for (ResourceVertexMultiset vms : allMultisets.values()) {
					if (canDeploy(jDsp,vms)) {
						int v = vms.getIndex();

						IloLinearNumExpr exprConn = modeler.linearNumExpr();

						for (ResourceVertexMultiset ums : allMultisets.values()) {
							if (canDeploy(iDsp,ums)) {
								int u = ums.getIndex();

								exprConn.addTerm(1.0, Y.get(i, j, u, v));
							}
						}
						cplex.addEq(X.get(j, v), exprConn, "conb2_" + i + "," + j);
					}
				}

			}

		} catch (IloException exc) {
			throw new ODDException("Error while defining Connectivity Bound: " + exc.getMessage());
		}
	}


	protected void compileObjective(IloModeler modeler) throws ODDException {

		/********************************************************************************
		 * Objective
		 ********************************************************************************/
		IloObjective obj;
		IloNumExpr objRExpr, objCExpr, objAExpr, objZExpr, objExpr;
		try {
			objRExpr = modeler.prod(modeler.sum(params.getRmax(), modeler.negative(R)), params.getWeightRespTime() / (params.getRmax() - params.getRmin()));
			objCExpr = modeler.prod(modeler.sum(params.getCmax(), modeler.negative(C)), params.getWeightCost() / (params.getCmax() - params.getCmin()));
			objAExpr = modeler.prod(modeler.sum(logA, (-Math.log(params.getAmin()))), params.getWeightAvailability() / (Math.log(params.getAmax()) - Math.log(params.getAmin())));

			if (params.getMode().equals(ODDModel.MODE.BASIC)) {
				objExpr = modeler.sum(objRExpr, objCExpr, objAExpr);
			} else {
				objZExpr = modeler.prod(modeler.sum(params.getZmax(), modeler.negative(Z)), params.getWeightNetMetric() / (params.getZmax() - params.getZmin()));
				objExpr = modeler.sum(objRExpr, objCExpr, objAExpr, objZExpr);
			}
			obj = modeler.maximize(objExpr);
			cplex.addObjective(obj.getSense(), obj.getExpr(), "Fx");
		} catch (IloException exc) {
			throw new ODDException("Error while defining Objective Function: " + exc.getMessage());
		}


	}

	public void compile() throws ODDException {

		allMultisets = generateResourceMultisets(maxMultisetCardinality);
		IloModeler modeler = new IloCplexModeler();

		compilationTime = System.currentTimeMillis();
		compileConstraintsAndVars(modeler);
		compileObjective(modeler);
		compilationTime = System.currentTimeMillis() - compilationTime;

		if (DEBUG) {
			try {
				cplex.exportModel("lpex1.lp");
			} catch (IloException e) {
				throw new ODDException("Error while exporting ODDModel on file: " + e.getMessage());
			}
		}

		if (SWAP_ON_DISK) {
			try {

				if (DEBUG)
					System.out.println("Setting NodeFileInd=" + 3 + "; "
							+ " WorkDir= " + ODDBandwidthModel.SWAP_DIR
							+ " WorkMemMB=" + ODDBandwidthModel.SWAP_AFTER_MEMORY_USAGE
							+ " TreLim=20000MB");
				
				/* When the tree memory limit is reached, a group of nodes is removed from the in-memory
				 * set as needed. By default, CPLEX transfers nodes to node files when the in-memory set 
				 * is larger than 128 MBytes, and it keeps the resulting node files in compressed form in 
				 * memory. At settings 3, the node files are transferred to disk, in compressed form, into
				 * a directory named by the working directory parameter (CPX_PARAM_WORKDIR, WorkDir), 
				 * and CPLEX actively manages which nodes remain in memory for processing. 
				 */
				cplex.setParam(IloCplex.IntParam.NodeFileInd, 3);
				cplex.setParam(IloCplex.StringParam.WorkDir, ODDBandwidthModel.SWAP_DIR);

				/* 
				 * To avoid a failure due to running out of memory, 
				 * the working memory parameter, WorkMem, is set (expressed in MB).
				 * It instructs CPLEX to begin compressing the storage of nodes 
				 * before it consumes all of available memory. 
				 */
				cplex.setParam(IloCplex.DoubleParam.WorkMem, ODDBandwidthModel.SWAP_AFTER_MEMORY_USAGE); // in MB

				/* 
				 * Because the storage of nodes can require a lot of space, 
				 * it may also be advisable to set a tree limit on the size of the 
				 * entire tree being stored so that not all of your disk will be 
				 * filled up with working storage. 
				 * The call to the MIP optimizer will be stopped after the size of the 
				 * tree exceeds the value of TreLim (in MB).
				 */
				cplex.setParam(IloCplex.DoubleParam.TreLim, ODDBandwidthModel.SWAP_MAX_DISK_USAGE); // in MB
				// set to 20 GB
				
				/*
				 * Limits number of used threads.
				 */
				cplex.setParam(IloCplex.IntParam.Threads, 12);


			} catch (IloException e) {
				e.printStackTrace();
			}

		}

		System.out.println("Compilation done.");

	}

	@Override
	public void pin(int iDsp, int uRes) {

		// we are assuming the first items within our multiset are singleton. 
		// we want to pin data source and data destination on single operatore, exploring all possible pair
		// where source -> u, sink -> v, with u != v

		try {
			System.out.println("Pinning " + iDsp + " on " + uRes);
			cplex.addEq(X.get(iDsp, uRes), 1.0, "pinned_" + iDsp + "," + uRes);
		} catch (IloException e) {
			e.printStackTrace();
		}

	}

	public OptimalSolution solve() throws ODDException {

		if (!CPLEX_DEBUG)
			cplex.setOut(null);

		if (R == null)
			compile();

		int vars = cplex.getNbinVars() + cplex.getNsemiIntVars() + cplex.getNsemiContVars() + cplex.getNintVars();
		System.out.println("Variables: " + vars);

		try {
			solution = new OptimalMultisetSolution(dspGraph.getVertices().size(), allMultisets);

			if (params.isUseTimeLimit()) {
				cplex.setParam(IloCplex.IntParam.TimeLimit, params.getTimeLimit()); //expressed in seconds 

			}
			if (params.isDefineOptimalGap()) {
				cplex.setParam(IloCplex.DoubleParam.EpGap, params.getGap());
			}

			long enlapsedTime = System.currentTimeMillis();
			if (cplex.solve()) {
				enlapsedTime = System.currentTimeMillis() - enlapsedTime;

				solution.setOptObjValue(cplex.getObjValue());
				solution.setOptR(cplex.getValue(R));
				solution.setOptC(cplex.getValue(C));
				solution.setOptLogA(cplex.getValue(logA));
				if (params.getMode().equals(ODDModel.MODE.BASIC)) {
					solution.setOptZ(0);
				} else {
					solution.setOptZ(cplex.getValue(Z));
				}
				solution.setResolutionTime(enlapsedTime);
				solution.setCompilationTime(compilationTime);

				for (DspVertex iDsp : dspGraph.getVertices().values()) {
					for (ResourceVertexMultiset ums : allMultisets.values()) {
						int i = iDsp.getIndex();

						if (canDeploy(iDsp,ums)) {
							int u = ums.getIndex();
							double xval = cplex.getValue(X.get(i, u));

							if (xval > 0)
								solution.setPlacement(i, u);
						}
					}
				}

				if (DEBUG)
					cplex.writeSolution("solution.sol");

			} else {
				System.out.println("Unable to find a solution");
			}

			solution.setStatus(cplex.getStatus());

			/* Printing solution */
			if (DEBUG) {
				System.out.println(" ---------------------------------------------- ");
				System.out.println(solution.toString().replace(';', '\n'));
				System.out.println(" ---------------------------------------------- ");

				System.out.println(" . R=" + solution.getOptR());
				System.out.println(" . term_R=" + (params.getWeightRespTime() * (params.getRmax() - solution.getOptR()) / (params.getRmax() - params.getRmin())));
				System.out.println(" . C=" + solution.getOptC());
				System.out.println(" . term_C=" + (params.getWeightCost() * (params.getCmax() - solution.getOptC()) / (params.getCmax() - params.getCmin())));
				System.out.println(" . A=" + Math.exp(solution.getOptLogA()));
				System.out.println(" . term_A=" + (solution.getOptLogA() - Math.log(params.getAmin())) * params.getWeightAvailability() / (Math.log(params.getAmax()) - Math.log(params.getAmin())));
				System.out.println(" . Z=" + solution.getOptZ());
				System.out.println(" . term_Z=" + (params.getWeightNetMetric() * (params.getZmax() - solution.getOptZ()) / (params.getZmax() - params.getZmin())));
			}

		} catch (IloException e) {
			throw new ODDException("Error while solving OPP Model: " + e.getMessage());
		}

		return solution;
	}

	protected Map<Integer, ResourceVertexMultiset> generateResourceMultisets(int maxCardinality) {

		ResVertexMultisetGenerator resMsGenerator = new ResVertexMultisetGenerator(resGraph, maxCardinality);
		return resMsGenerator.generate();

	}


	@Override
	public String type() {
		return MODEL_TYPE;
	}

	public final static String MODEL_TYPE = "REPLICATION";

}
