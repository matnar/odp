package codd.placement;

import codd.ODDException;
import codd.model.OptimalSolution;

/**
 * ODP Model interface.
 *
 * @author Matteo Nardelli
 */
public interface ODDModel {
	
	enum MODE{
		BANDWIDTH,
		BASIC
	}
	
	void compile() throws ODDException;
	
	OptimalSolution solve() throws ODDException;

	void clean();
	
	String type();
	
	void pin(int dspIndex, int resIndex);

}
