package codd.placement;

import java.util.List;
import java.util.Map;

import codd.EDRPParameters;
import codd.ODDException;
import codd.ResVertexMultisetGenerator;
import codd.metric.ReconfigurationMetricsManager;
import codd.model.*;
import codd.utils.Multiset;
import codd.utils.ResVertexMultisetFilter;
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloModeler;
import ilog.concert.IloNumExpr;
import ilog.concert.IloObjective;
import ilog.cplex.IloCplex;

/**
 * Elastic DSP Replication and Placement model.
 *
 * If you use this model, please cite:
 * V. Cardellini, F. Lo Presti, M. Nardelli, G. Russo Russo,
 * "Optimal Operator Deployment and Replication for Elastic Distributed Data Stream Processing",
 * Concurrency and Computation: Practice and Experience,
 * Vol. 30, No. 9, May 2018. doi: 10.1002/cpe.4334.
 *
 * @author Gabriele Russo Russo
 */
public class EDRPModel extends ODDReplicatedOperatorsModel implements ResVertexMultisetFilter {

	/** Downtime due to reconfiguration. */
	protected IloNumExpr TDown = null;

	/** Flag to ignore bound on downtime. */
	private boolean skipDowntimeBound = false;

	/** Current placement. */
	private Map<Integer, ResourceVertexMultiset> currentPlacement;

	/** Prune multisets generation. */
	private boolean prunedMultisetsGeneration = false;


	public EDRPModel(DspGraph dspGraph, ResourceGraph resGraph, EDRPParameters parameters, int maxReplication, boolean prunedMultisetsGeneration) throws ODDException {
		super(dspGraph, resGraph, parameters);
		this.maxMultisetCardinality = maxReplication;
		this.currentPlacement = parameters.getCurrentPlacement();
		this.prunedMultisetsGeneration = prunedMultisetsGeneration;
		metricManager = new ReconfigurationMetricsManager(dspGraph, resGraph, parameters.isOdrpUseMM1(),
				parameters.getCurrentPlacement());
	}

	public EDRPModel(DspGraph dspGraph, ResourceGraph resGraph, EDRPParameters parameters, int maxReplication, boolean prunedMultisetsGeneration, boolean skipDowntimeBound)
			throws ODDException {
		this(dspGraph, resGraph, parameters, maxReplication, prunedMultisetsGeneration);
		this.skipDowntimeBound = skipDowntimeBound;
	}

	@Override
	protected void initializeVariables() throws IloException {
		if (!prunedMultisetsGeneration) {
			super.initializeVariables();
		} else {
			PrunedPlacementXMultiset prunedX = new PrunedPlacementXMultiset(dspGraph, resGraph, allMultisets.values(), currentPlacement);
			this.X = prunedX;
			this.Y = new PrunedPlacementYMultiset(dspGraph, resGraph, allMultisets.values(), prunedX);

			this.Zv = new ActiveNodeZ(resGraph);
			this.Ze = new ActiveLinkZ(resGraph);
		}
	}

	@Override
	protected void compileConstraintsAndVars(IloModeler modeler) throws ODDException {
		/* Compiles basic ODRP model. */
		super.compileConstraintsAndVars(modeler);

		EDRPParameters eParams = (EDRPParameters) params;
		ReconfigurationMetricsManager reconfigurationMetrics = (ReconfigurationMetricsManager) metricManager;

		/*
		 * Downtime variable. It is the maximum downtime over the DSP operators.
		 */
		try {
			TDown = modeler.numVar(0, Double.MAX_VALUE, "TDown");

			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				IloLinearNumExpr expr = modeler.linearNumExpr();
				for (ResourceVertexMultiset ums : allMultisets.values()) {
					if (canDeploy(iDsp,ums)) {
						expr.addTerm(reconfigurationMetrics.getOperatorDowntime(iDsp, ums),
								X.get(iDsp.getIndex(), ums.getIndex()));
					}
				}
				cplex.addGe(TDown, expr, "DownOp" + iDsp.getIndex());
			}

		} catch (IloException e) {
			throw new ODDException("Error while defining TDown variable");
		}

		/*
		 * Enforces SLA bounds.
		 */
		try {
			cplex.addLe(R, eParams.getRmax(), "SLA-R");
			cplex.addLe(C, eParams.getCmax(), "SLA-C");
			cplex.addLe(Z, eParams.getZmax(), "SLA-Z");
			cplex.addGe(logA, eParams.getLogAmin(), "SLA-logA");
			if (!skipDowntimeBound)
				cplex.addLe(TDown, eParams.getTDmax(), "SLA-TD");
		} catch (IloException e) {
			throw new ODDException("Error while defining SLA bounds: " + e.getMessage());
		}

	}

	@Override
	protected void compileObjective(IloModeler modeler) throws ODDException {
		EDRPParameters edrpParams = (EDRPParameters) params;

		IloObjective obj;
		IloNumExpr objRExpr, objCExpr, objAExpr, objZExpr, objTDownExpr, objExpr;
		try {
			objRExpr = modeler.prod(R, edrpParams.getWeightRespTime() / edrpParams.getRmax());
			objCExpr = modeler.prod(C, edrpParams.getWeightCost() / edrpParams.getCmax());
			objTDownExpr = modeler.prod(TDown, edrpParams.getWeightDowntime() / edrpParams.getTDmax());
			objAExpr = modeler.prod(logA, edrpParams.getWeightAvailability() / edrpParams.getLogAmin());

			if (params.getMode().equals(ODDModel.MODE.BASIC)) {
				objExpr = modeler.sum(objRExpr, objCExpr, objAExpr, objTDownExpr);
			} else {
				objZExpr = modeler.prod(Z, edrpParams.getWeightNetMetric() / edrpParams.getZmax());
				objExpr = modeler.sum(objRExpr, objCExpr, objAExpr, objZExpr, objTDownExpr);
			}
			obj = modeler.minimize(objExpr);
			cplex.addObjective(obj.getSense(), obj.getExpr(), "Fx");
		} catch (IloException exc) {
			throw new ODDException("Error while defining Objective Function: " + exc.getMessage());
		}

	}

	@Override
	public String type() {
		return EDRP_MODEL_TYPE;
	}

	public static final String EDRP_MODEL_TYPE = "edrp";

	@Override
	public OptimalSolution solve() throws ODDException {
		if (!CPLEX_DEBUG)
			cplex.setOut(null);

		if (R == null)
			compile();

		int vars = cplex.getNbinVars() + cplex.getNsemiIntVars() + cplex.getNsemiContVars() + cplex.getNintVars();
		System.out.println("Variables: " + vars);

		try {
			solution = new OptimalEDRPSolution(dspGraph.getVertices().size(), allMultisets);

			if (params.isUseTimeLimit())
				cplex.setParam(IloCplex.IntParam.TimeLimit, params.getTimeLimit()); // expressed
																					// in
																					// seconds
			if (params.isDefineOptimalGap())
				cplex.setParam(IloCplex.DoubleParam.EpGap, params.getGap());

			long elapsedTime = System.currentTimeMillis();
			if (cplex.solve()) {
				elapsedTime = System.currentTimeMillis() - elapsedTime;

				/* Sets placement */
				for (DspVertex iDsp : dspGraph.getVertices().values()) {
					for (ResourceVertexMultiset ums : allMultisets.values()) {
						int i = iDsp.getIndex();

						if (canDeploy(iDsp,ums)) {
							int u = ums.getIndex();
							double xval = cplex.getValue(X.get(i, u));

							if (xval > 0)
								solution.setPlacement(i, u);
						}
					}
				}

				solution.setOptObjValue(cplex.getObjValue());
				solution.setOptLogA(cplex.getValue(logA));

				if (params.getWeightRespTime() > 0.0) 
					solution.setOptR(cplex.getValue(R));
				else
					solution.setOptR(computeAchievedResponseTime(solution));

				if (params.getWeightCost() > 0.0)
					solution.setOptC(cplex.getValue(C));
				else
					solution.setOptC(computeAchievedCost(solution));
				
				if (((EDRPParameters)params).getWeightDowntime()>0.0)
					((OptimalEDRPSolution) solution).setOptTDown(cplex.getValue(TDown));
				else
					((OptimalEDRPSolution) solution).setOptTDown(computeAchievedDowntime(solution));

				if (params.getMode().equals(ODDModel.MODE.BASIC)) {
					solution.setOptZ(0);
				} else {
					solution.setOptZ(cplex.getValue(Z));
				}

				solution.setResolutionTime(elapsedTime);
				solution.setCompilationTime(compilationTime);


			} else {

				System.out.println("Unable to find a solution: " + cplex.getStatus());
			}

			solution.setStatus(cplex.getStatus());

			/* Printing solution */
			printSolution((OptimalEDRPSolution) solution, (EDRPParameters) params);

		} catch (IloException e) {
			e.printStackTrace(); // TODO
			throw new ODDException("Error while solving OPP Model: " + e.getMessage());
		}

		return solution;
	}

	protected void printSolution(OptimalEDRPSolution solution, EDRPParameters params) {
		System.out.println(" ---------------------------------------------- ");
		System.out.println(solution.toString().replace(';', '\n'));
		System.out.println(" ---------------------------------------------- ");

		System.out.println(" . R=" + solution.getOptR());
		System.out.println(" . term_R=" + (params.getWeightRespTime() / params.getRmax() * solution.getOptR()));
		System.out.println(" . C=" + solution.getOptC());
		System.out.println(" . term_C=" + (params.getWeightCost() / params.getCmax() * solution.getOptC()));
		System.out.println(" . A=" + Math.exp(solution.getOptLogA()));
		System.out
				.println(" . term_A=" + (params.getWeightAvailability() / params.getLogAmin() * solution.getOptLogA()));
		System.out.println(" . Z=" + solution.getOptZ());
		System.out.println(" . term_Z=" + (params.getWeightNetMetric() / params.getZmax() * solution.getOptZ()));
		System.out.println(" . TDown=" + solution.getOptTDown());
		System.out
				.println(" . term_TDown=" + (params.getWeightDowntime() / params.getTDmax() * solution.getOptTDown()));

		System.out.println("Response: " + solution.getOptR());
		System.out.println("Cost: " + solution.getOptC());
		System.out.println("Downtime: " + solution.getOptTDown());
	}
	
	protected double computeAchievedCost (OptimalMultisetSolution solution)
	{
		double cost = 0.0;
		for (DspVertex iDsp : dspGraph.getVertices().values()) {
			cost += metricManager.getCost(iDsp.getIndex(), solution.getMultisetPlacement(iDsp.getIndex()));
		}
		for (DspEdge eDsp : dspGraph.getEdges().values()) {
			int iDsp = eDsp.getFrom();
			int jDsp = eDsp.getTo();
			cost += metricManager.getCost(iDsp, jDsp, solution.getMultisetPlacement(iDsp),
					solution.getMultisetPlacement(jDsp));
		}
		return cost;
	}

	protected double computeAchievedDowntime (OptimalMultisetSolution solution)
	{
		double downtime = 0.0;
		ReconfigurationMetricsManager rmm = (ReconfigurationMetricsManager)metricManager;
		for (DspVertex iDsp : dspGraph.getVertices().values()) {
			downtime = Math.max(downtime, rmm.getOperatorDowntime(iDsp, solution.getMultisetPlacement(iDsp.getIndex())));
		}
		return downtime;
	}

	protected double computeAchievedResponseTime (OptimalMultisetSolution solution)
	{
		double maxR = 0.0;
		for (DspPath path : dspGraph.getPaths()) {
			double R = 0.0;
			/* R_computation */
			for (Integer i : path.getNodesIndexes()) {
				double execTime = metricManager.getExecutionTime(i, solution.getMultisetPlacement(i));
				R += execTime;
			}

			/* R_comunication */
			List<Integer> sequence = path.getNodesIndexes();
			for (int k = 0; k < sequence.size() - 1; k++) {
				Integer ik = sequence.get(k);
				Integer ik1 = sequence.get(k + 1);
				R += metricManager.getNetworkDelay(ik.intValue(), ik1.intValue(), solution.getMultisetPlacement(ik),
						solution.getMultisetPlacement(ik1));
			}
			maxR = Math.max(R, maxR);
		}
		return maxR;
	}

	@Override
	protected Map<Integer, ResourceVertexMultiset> generateResourceMultisets(int maxCardinality) {
		ResVertexMultisetGenerator resMsGenerator;
		if (prunedMultisetsGeneration == false) {
			resMsGenerator = new ResVertexMultisetGenerator(resGraph, maxCardinality);
		} else {
			resMsGenerator = new ResVertexMultisetGenerator(resGraph, maxCardinality, this);
		}

		Map<Integer, ResourceVertexMultiset> multisets = resMsGenerator.generate();
		System.out.println("All multisets : " + multisets.size());
		return multisets;
	}

	@Override
	public boolean acceptMultiset(Multiset multiset) {
		/* Accept if at least one vertex can be deployed on this multiset. */
		ResourceVertexMultiset resourceVertexMultiset = new ResourceVertexMultiset(0, multiset, resGraph);
		if (resourceVertexMultiset.getCardinality() == 1)
			return true;
		for (Integer iDsp : dspGraph.getVertices().keySet()) {
			ResourceVertexMultiset current = currentPlacement.get(iDsp);
			if (current.isAtOneStepFrom(resourceVertexMultiset)) {
				return true;
			}
		}

		return false;
	}
}
