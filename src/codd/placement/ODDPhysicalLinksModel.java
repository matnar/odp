package codd.placement;

import codd.ODDException;
import codd.ODDParameters;
import codd.model.*;
import ilog.concert.*;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplexModeler;

import java.util.List;


public class ODDPhysicalLinksModel implements ODDModel {

	protected static final boolean DEBUG = true;

	public static boolean INCLUDE_SELF_LOOPS = true;

	protected ODDParameters params;
	
	protected DspGraph dspGraph;
	protected ResourceGraph resGraph;
	protected IloCplex cplex;
	protected PlacementX X;
	protected PlacementY Y;
	protected ActiveLinkZ Z;
	protected PlacementXColocation L;
	protected OptimalSolution solution;
	
	protected IloNumExpr R;
	protected IloNumExpr logA;
	protected IloNumExpr C;

	private long compilationTime;
	
	public ODDPhysicalLinksModel(DspGraph dspGraph, ResourceGraph resGraph,
			ODDParameters parameters) throws ODDException {
		
		this.dspGraph = dspGraph;
		this.resGraph = resGraph;
		
		this.params = parameters;
		
		this.solution = null;
		this.R = null;
		this.logA = null;
		this.X = null;
		this.Y = null;
		this.L = null;
		this.compilationTime = Long.MAX_VALUE;
		
		try {
			this.cplex = new IloCplex();
		} catch (IloException exc) {
			throw new ODDException("Error while creating model: " + exc.getMessage());
		}
	}
	
//	public ODDPhysicalLinksModel(DspGraph dspGraph, ResourceGraph resGraph) throws ODDException {
//		this(dspGraph, resGraph, new ODDParameters(ODDModel.MODE.BASIC, null));
//	}

	public DspGraph getDspGraph() {
		return dspGraph;
	}

	public ResourceGraph getResGraph() {
		return resGraph;
	}

	public void compile() throws ODDException{

		if (INCLUDE_SELF_LOOPS){
			compileIncludingSelfLoops();
		} else {
			compileWithoutSelfLoops();
		}

	}

	private void compileWithoutSelfLoops() throws ODDException{

		IloModeler modeler = new IloCplexModeler();

		compilationTime = System.currentTimeMillis();

		/* *******************************************************************************
		 * Decision Variables
		 ********************************************************************************/
		try {
			this.X = new PlacementX(dspGraph, resGraph);
			this.Y = new PlacementY(dspGraph, resGraph);
			this.Z = new ActiveLinkZ(resGraph);
		} catch (IloException exc) {
			throw new ODDException("Error while defining decision variables X, Y, and Z: " + exc.getMessage());
		}


		/* *******************************************************************************
		 * Response-Time
		 ********************************************************************************/
		try {
			R = cplex.numVar(0, Double.MAX_VALUE, "R");
			for (DspPath path : dspGraph.getPaths()) {

				/* R_computation */
				IloLinearNumExpr Rpex = modeler.linearNumExpr();
				for(Integer i : path.getNodesIndexes()){
					DspVertex in = dspGraph.getVertices().get(i);
					for(ResourceVertex vn : resGraph.getVertices().values()){

						if (in.deployableOn(vn)){
							int u = vn.getIndex();
							Rpex.addTerm(in.getExecutionTime() / vn.getSpeedup(), X.get(i, u));
						}
					}
				}

				/* R_communication*/
				IloLinearNumExpr Rptx = modeler.linearNumExpr();
				List<Integer> sequence = path.getNodesIndexes();
				for (int k = 0; k < sequence.size() - 1; k++) {
					Integer ik = sequence.get(k);
					Integer ik1 = sequence.get(k + 1);
					for (ResourceEdge eRes : resGraph.getEdges().values()) {
						int u = eRes.getFrom();
						int v = eRes.getTo();
						DspVertex ikDsp = dspGraph.getVertices().get(ik);
						DspVertex ik1Dsp = dspGraph.getVertices().get(ik1);

						if(ikDsp.deployableOn(u) && u != v && ik1Dsp.deployableOn(v)){
							Rptx.addTerm(eRes.getDelay(), Y.get(ik, ik1, u, v));
						}
					}
				}

				IloNumExpr Rp = modeler.sum(Rpex, Rptx);
				cplex.addLe(Rp, R, "objBound_R");
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Response-Time: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Availability
		 ********************************************************************************/
		try {
			logA  = modeler.numVar(-Double.MAX_VALUE, 0, "logA");

			/* A nodes */
			IloLinearNumExpr logAex = modeler.linearNumExpr();
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				for (ResourceVertex uRes : resGraph.getVertices().values()) {

					if (iDsp.deployableOn(uRes)){
						int i = iDsp.getIndex();
						int u = uRes.getIndex();
						logAex.addTerm(Math.log(uRes.getAvailability()), X.get(i, u));
					}
				}
			}

			/* A links */
			IloLinearNumExpr logAtx = modeler.linearNumExpr();
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				for (ResourceEdge uvRes: resGraph.getEdges().values()) {
					int i = ijDsp.getFrom();
					int j = ijDsp.getTo();
					int u = uvRes.getFrom();
					int v = uvRes.getTo();

					DspVertex iDsp = dspGraph.getVertices().get(i);
					DspVertex jDsp = dspGraph.getVertices().get(j);

					if(iDsp.deployableOn(u) && jDsp.deployableOn(v)){
//						logAtx.addTerm(Math.log(uvRes.getAvailability()), Y.get(i, j, u, v));
						logAtx.addTerm(Math.log(uvRes.getAvailability()), Z.get(u, v));
					}
				}
			}
			IloNumExpr logAx = modeler.sum(logAex, logAtx);
			cplex.addEq(logAx, logA, "objBound_A");



		} catch (IloException exc) {
			throw new ODDException("Error while defining Availability: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Cost
		 ********************************************************************************/
		try {
			C  = modeler.numVar(0, Double.MAX_VALUE, "C");

			/* C links */
			IloLinearNumExpr Ctx = modeler.linearNumExpr();
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {

				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(i);

				// Cij
				for (ResourceEdge uvRes: resGraph.getEdges().values()) {
					int u = uvRes.getFrom();
					int v = uvRes.getTo();

					if(iDsp.deployableOn(u) && jDsp.deployableOn(v)){
						Ctx.addTerm(uvRes.getCostPerUnitData(), Y.get(i, j, u, v));
					}
				}

			}
			cplex.addEq(Ctx, C, "objBound_C");

		} catch (IloException exc) {
			throw new ODDException("Error while defining Availability: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Objective
		 ********************************************************************************/
		IloObjective obj;
		IloNumExpr objRExpr, objAExpr, objCExpr, objExpr;
		try {
			objRExpr = modeler.prod(modeler.sum(params.getRmax(), modeler.negative(R)), params.getWeightRespTime() / (params.getRmax() - params.getRmin()));
			objAExpr = modeler.prod(modeler.sum(logA, (-Math.log(params.getAmin()))), 	params.getWeightAvailability() / (Math.log(params.getAmax()) - Math.log(params.getAmin())));
			objCExpr = modeler.prod(modeler.sum(params.getCmax(), modeler.negative(C)), params.getWeightCost() / (params.getCmax() - params.getCmin()));
			objExpr  = modeler.sum(objRExpr, objAExpr, objCExpr);
			obj 	 = modeler.maximize(objExpr);
			cplex.addObjective(obj.getSense(), obj.getExpr(), "Fx");
		} catch (IloException exc) {
			throw new ODDException("Error while defining Objective Function: " + exc.getMessage());
		}


		/* *******************************************************************************
		 * Capacity Bound
		 ********************************************************************************/
		try {
			for (ResourceVertex uRes : resGraph.getVertices().values()) {
				IloLinearNumExpr deployedOpCapacity = modeler.linearNumExpr();
				for (DspVertex iDsp : dspGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();

					if (iDsp.deployableOn(u)){
						deployedOpCapacity.addTerm(iDsp.getRequiredResources(), X.get(i, u));
					}
				}
				cplex.addLe(deployedOpCapacity, uRes.getAvailableResources(),
						"capb_res_" + uRes.getIndex());
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Capacity Bound: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Uniqueness Bound
		 ********************************************************************************/
		try {
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				IloLinearNumExpr exprUniqueness = modeler.linearNumExpr();
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();

					if (iDsp.deployableOn(u)){
						exprUniqueness.addTerm(1.0, X.get(i, u));
					}
				}
				cplex.addEq(exprUniqueness, 1.0,
						"uniqb_dsp_" + iDsp.getIndex());
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Z def
		 ********************************************************************************/
		try {
			double M1 = dspGraph.getEdges().size();
			if (M1 != 0)
				M1 = 1.0 / M1;

			for (ResourceEdge uvRes : resGraph.getEdges().values()) {
				int u = uvRes.getFrom();
				int v = uvRes.getTo();

				if (u == v)
					continue;

				IloLinearNumExpr exprActivation = modeler.linearNumExpr();
				IloLinearNumExpr exprActivationLB = modeler.linearNumExpr();
				for (DspEdge ijDsp : dspGraph.getEdges().values()) {
					int i = ijDsp.getFrom();
					int j = ijDsp.getTo();
					exprActivation.addTerm(M1, Y.get(i, j, u, v));
					exprActivationLB.addTerm(1.0, Y.get(i, j, u, v));
				}

				cplex.addGe(Z.get(u, v), exprActivation, "activation_link_z_"+u+","+v);
				cplex.addLe(Z.get(u, v), exprActivationLB, "activation_link_lb_z_"+u+","+v);
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Connectivity Bound - review 26.03.2017
		 ********************************************************************************/
		try {
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);

				/* Equation 19 */
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int u = uRes.getIndex();

					if(!iDsp.deployableOn(u)){
						continue;
					}

					IloLinearNumExpr exprXiuPlac = modeler.linearNumExpr();
					for (ResourceVertex vRes : resGraph.getVertices().values()) {
						int v = vRes.getIndex();

						if(jDsp.deployableOn(u) && u != v && iDsp.deployableOn(v) && Y.get(i, j, v, u) != null){
							exprXiuPlac.addTerm(1.0, Y.get(i, j, v, u));
						}
						if(jDsp.deployableOn(v) && u != v && Y.get(i, j, u, v) != null){
							exprXiuPlac.addTerm(-1.0, Y.get(i, j, u, v));
						}

					}

					cplex.addEq(cplex.sum(X.get(j, u), cplex.prod(-1.0, X.get(i, u))), exprXiuPlac, "plac1_X_" + i + "," + u);

				}

				/* Equation 20 */
				for (ResourceVertex vRes : resGraph.getVertices().values()) {
					int v = vRes.getIndex();

					if(!jDsp.deployableOn(v)){
						continue;
					}

					IloLinearNumExpr exprXjvPlac = modeler.linearNumExpr();
					for (ResourceVertex uRes : resGraph.getVertices().values()) {
						int u = uRes.getIndex();

						if(jDsp.deployableOn(u) && u != v && iDsp.deployableOn(v) && Y.get(i, j, v, u) != null){
							exprXjvPlac.addTerm(1.0, Y.get(i, j, v, u));
						}
						if(jDsp.deployableOn(v) && u != v && Y.get(i, j, u, v) != null){
							exprXjvPlac.addTerm(-1.0, Y.get(i, j, u, v));
						}
					}

					cplex.addEq(cplex.sum(X.get(i, v), cplex.prod(-1.0, X.get(j, v))), exprXjvPlac, "plac2_X_" + j + "," + v);

				}

			}

		} catch (IloException exc) {
			throw new ODDException("Error while defining Connectivity Bound: " + exc.getMessage());
		}

		compilationTime = System.currentTimeMillis() - compilationTime;

		if (DEBUG){
			try {
				cplex.exportModel("lpex1.lp");
			} catch (IloException e) {
				throw new ODDException("Error while exporting ODDModel on file: " + e.getMessage());
			}
		}

	}

	private void compileIncludingSelfLoops() throws ODDException{

		IloModeler modeler = new IloCplexModeler();

		compilationTime = System.currentTimeMillis();

		/* *******************************************************************************
		 * Decision Variables
		 ********************************************************************************/
		try {
			this.X = new PlacementX(dspGraph, resGraph);
			this.Y = new PlacementY(dspGraph, resGraph);
			this.Z = new ActiveLinkZ(resGraph);
			this.L = new PlacementXColocation(dspGraph, resGraph);
		} catch (IloException exc) {
			throw new ODDException("Error while defining decision variables X, Y, and Z: " + exc.getMessage());
		}


		/* *******************************************************************************
		 * Response-Time
		 ********************************************************************************/
		try {
			R = cplex.numVar(0, Double.MAX_VALUE, "R");
			for (DspPath path : dspGraph.getPaths()) {

				/* R_computation */
				IloLinearNumExpr Rpex = modeler.linearNumExpr();
				for(Integer i : path.getNodesIndexes()){
					DspVertex in = dspGraph.getVertices().get(i);
					for(ResourceVertex vn : resGraph.getVertices().values()){

						if (in.deployableOn(vn)){
							int u = vn.getIndex();
							Rpex.addTerm(in.getExecutionTime() / vn.getSpeedup(), X.get(i, u));
						}
					}
				}

				/* R_communication*/
				IloLinearNumExpr Rptx = modeler.linearNumExpr();
				List<Integer> sequence = path.getNodesIndexes();
				for (int k = 0; k < sequence.size() - 1; k++) {
					Integer ik = sequence.get(k);
					Integer ik1 = sequence.get(k + 1);
					for (ResourceEdge eRes : resGraph.getEdges().values()) {
						int u = eRes.getFrom();
						int v = eRes.getTo();
						DspVertex ikDsp = dspGraph.getVertices().get(ik);
						DspVertex ik1Dsp = dspGraph.getVertices().get(ik1);

						if(ikDsp.deployableOn(u) && ik1Dsp.deployableOn(v)){
							Rptx.addTerm(eRes.getDelay(), Y.get(ik, ik1, u, v));
						}
					}
				}

				IloNumExpr Rp = modeler.sum(Rpex, Rptx);
				cplex.addLe(Rp, R, "objBound_R");
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Response-Time: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Availability
		 ********************************************************************************/
		try {
			logA  = modeler.numVar(-Double.MAX_VALUE, 0, "logA");

			/* A nodes */
			IloLinearNumExpr logAex = modeler.linearNumExpr();
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				for (ResourceVertex uRes : resGraph.getVertices().values()) {

					if (iDsp.deployableOn(uRes)){
						int i = iDsp.getIndex();
						int u = uRes.getIndex();
						logAex.addTerm(Math.log(uRes.getAvailability()), X.get(i, u));
					}
				}
			}

			/* A links */
			IloLinearNumExpr logAtx = modeler.linearNumExpr();
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				for (ResourceEdge uvRes: resGraph.getEdges().values()) {
					int i = ijDsp.getFrom();
					int j = ijDsp.getTo();
					int u = uvRes.getFrom();
					int v = uvRes.getTo();

					DspVertex iDsp = dspGraph.getVertices().get(i);
					DspVertex jDsp = dspGraph.getVertices().get(j);

					if(iDsp.deployableOn(u) && jDsp.deployableOn(v)){
//						logAtx.addTerm(Math.log(uvRes.getAvailability()), Y.get(i, j, u, v));
						logAtx.addTerm(Math.log(uvRes.getAvailability()), Z.get(u, v));
					}
				}
			}
			IloNumExpr logAx = modeler.sum(logAex, logAtx);
			cplex.addEq(logAx, logA, "objBound_A");



		} catch (IloException exc) {
			throw new ODDException("Error while defining Availability: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Cost
		 ********************************************************************************/
		try {
			C  = modeler.numVar(0, Double.MAX_VALUE, "C");

			/* C links */
			IloLinearNumExpr Ctx = modeler.linearNumExpr();
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {

				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(i);

				// Cij
				for (ResourceEdge uvRes: resGraph.getEdges().values()) {
					int u = uvRes.getFrom();
					int v = uvRes.getTo();

					if(iDsp.deployableOn(u) && jDsp.deployableOn(v)){
						Ctx.addTerm(uvRes.getCostPerUnitData(), Y.get(i, j, u, v));
					}
				}

			}
			cplex.addEq(Ctx, C, "objBound_C");

		} catch (IloException exc) {
			throw new ODDException("Error while defining Availability: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Objective
		 ********************************************************************************/
		IloObjective obj;
		IloNumExpr objRExpr, objAExpr, objCExpr, objExpr;
		try {
			objRExpr = modeler.prod(modeler.sum(params.getRmax(), modeler.negative(R)), params.getWeightRespTime() / (params.getRmax() - params.getRmin()));
			objAExpr = modeler.prod(modeler.sum(logA, (-Math.log(params.getAmin()))), 	params.getWeightAvailability() / (Math.log(params.getAmax()) - Math.log(params.getAmin())));
			objCExpr = modeler.prod(modeler.sum(params.getCmax(), modeler.negative(C)), params.getWeightCost() / (params.getCmax() - params.getCmin()));
			objExpr  = modeler.sum(objRExpr, objAExpr, objCExpr);
			obj 	 = modeler.maximize(objExpr);
			cplex.addObjective(obj.getSense(), obj.getExpr(), "Fx");
		} catch (IloException exc) {
			throw new ODDException("Error while defining Objective Function: " + exc.getMessage());
		}


		/* *******************************************************************************
		 * Capacity Bound
		 ********************************************************************************/
		try {
			for (ResourceVertex uRes : resGraph.getVertices().values()) {
				IloLinearNumExpr deployedOpCapacity = modeler.linearNumExpr();
				for (DspVertex iDsp : dspGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();

					if (iDsp.deployableOn(u)){
						deployedOpCapacity.addTerm(iDsp.getRequiredResources(), X.get(i, u));
					}
				}
				cplex.addLe(deployedOpCapacity, uRes.getAvailableResources(),
						"capb_res_" + uRes.getIndex());
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Capacity Bound: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Uniqueness Bound
		 ********************************************************************************/
		try {
			for (DspVertex iDsp : dspGraph.getVertices().values()) {
				IloLinearNumExpr exprUniqueness = modeler.linearNumExpr();
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int i = iDsp.getIndex();
					int u = uRes.getIndex();

					if (iDsp.deployableOn(u)){
						exprUniqueness.addTerm(1.0, X.get(i, u));
					}
				}
				cplex.addEq(exprUniqueness, 1.0,
						"uniqb_dsp_" + iDsp.getIndex());
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Z def - review 16.03.2016
		 ********************************************************************************/
		try {
			double M1 = dspGraph.getEdges().size();
			if (M1 != 0)
				M1 = 1.0 / M1;

			for (ResourceEdge uvRes : resGraph.getEdges().values()) {
				int u = uvRes.getFrom();
				int v = uvRes.getTo();

				IloLinearNumExpr exprActivation = modeler.linearNumExpr();
				IloLinearNumExpr exprActivationLB = modeler.linearNumExpr();
				for (DspEdge ijDsp : dspGraph.getEdges().values()) {
					int i = ijDsp.getFrom();
					int j = ijDsp.getTo();
					exprActivation.addTerm(M1, Y.get(i, j, u, v));
					exprActivationLB.addTerm(1.0, Y.get(i, j, u, v));
				}

				cplex.addGe(Z.get(u, v), exprActivation, "activation_link_z_"+u+","+v);
				cplex.addLe(Z.get(u, v), exprActivationLB, "activation_link_lb_z_"+u+","+v);
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * L def
		 * ********************************************************************************/
		try {
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int u = uRes.getIndex();

					IloNumExpr exprActivationUB = cplex.prod(0.5, cplex.sum(X.get(i, u), X.get(j, u)));
					IloNumExpr exprActivationLB = cplex.sum(cplex.sum(X.get(i, u), X.get(j, u)), -1.0);

					cplex.addLe(L.get(i,j,u), exprActivationUB, "colocation_ub_"+i+","+j+","+u);
					cplex.addGe(L.get(i,j,u), exprActivationLB, "colocation_lb_"+i+","+j+","+u);
				}
			}
		} catch (IloException exc) {
			throw new ODDException("Error while defining Uniqueness Bound: " + exc.getMessage());
		}


		/* *******************************************************************************
		 * Link Mapping
		 * *******************************************************************************/
		try {
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);

				IloLinearNumExpr exprYExistence = modeler.linearNumExpr();

				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int u = uRes.getIndex();
					if(!iDsp.deployableOn(u)){
						continue;
					}

					for (ResourceVertex vRes : resGraph.getVertices().values()) {
						int v = vRes.getIndex();

						if(!jDsp.deployableOn(v)){
							continue;
						}

						if (Y.get(i, j, u, v) != null)
							exprYExistence.addTerm(1.0, Y.get(i, j, u, v));
					}

				}

				cplex.addGe(exprYExistence, 1.0, "link_connect_" + i + "," + j);
			}

		} catch (IloException exc) {
			throw new ODDException("Error while defining Connectivity Bound: " + exc.getMessage());
		}

		/* *******************************************************************************
		 * Connectivity Bound - review 26.03.2017
		 ********************************************************************************/
		try {
			for (DspEdge ijDsp : dspGraph.getEdges().values()) {
				int i = ijDsp.getFrom();
				int j = ijDsp.getTo();
				DspVertex iDsp = dspGraph.getVertices().get(i);
				DspVertex jDsp = dspGraph.getVertices().get(j);

				/* Equation 19 */
				for (ResourceVertex uRes : resGraph.getVertices().values()) {
					int u = uRes.getIndex();

					if(!iDsp.deployableOn(u)){
						continue;
					}

					IloLinearNumExpr exprXiuPlac = modeler.linearNumExpr();
					for (ResourceVertex vRes : resGraph.getVertices().values()) {
						int v = vRes.getIndex();

						if(jDsp.deployableOn(u) && iDsp.deployableOn(v) && Y.get(i, j, v, u) != null){
							exprXiuPlac.addTerm(1.0, Y.get(i, j, v, u));
						}
						if(jDsp.deployableOn(v) && u != v && Y.get(i, j, u, v) != null){
							exprXiuPlac.addTerm(-1.0, Y.get(i, j, u, v));
						}

					}

					cplex.addEq(cplex.sum(L.get(i,j,u), cplex.sum(X.get(j, u), cplex.prod(-1.0, X.get(i, u)))), exprXiuPlac, "plac1_X_" + i + "," + u);

				}

				/* Equation 20 */
				for (ResourceVertex vRes : resGraph.getVertices().values()) {
					int v = vRes.getIndex();

					if(!jDsp.deployableOn(v)){
						continue;
					}

					IloLinearNumExpr exprXjvPlac = modeler.linearNumExpr();
					for (ResourceVertex uRes : resGraph.getVertices().values()) {
						int u = uRes.getIndex();

						if(jDsp.deployableOn(u) && iDsp.deployableOn(v) && Y.get(i, j, v, u) != null){
							exprXjvPlac.addTerm(1.0, Y.get(i, j, v, u));
						}
						if(jDsp.deployableOn(v) && u != v && Y.get(i, j, u, v) != null){
							exprXjvPlac.addTerm(-1.0, Y.get(i, j, u, v));
						}
					}

					cplex.addEq(cplex.sum(L.get(i,j,v), cplex.sum(X.get(i, v), cplex.prod(-1.0, X.get(j, v)))), exprXjvPlac, "plac2_X_" + j + "," + v);

				}

			}

		} catch (IloException exc) {
			throw new ODDException("Error while defining Connectivity Bound: " + exc.getMessage());
		}

		compilationTime = System.currentTimeMillis() - compilationTime;

		if (DEBUG){
			try {
				cplex.exportModel("lpex1.lp");
			} catch (IloException e) {
				throw new ODDException("Error while exporting ODDModel on file: " + e.getMessage());
			}
		}

	}

	public OptimalSolution solve() throws ODDException{
		
		
		if (R == null)
			compile();
		
		try {
			if (params.isUseTimeLimit()){
				cplex.setParam(IloCplex.IntParam.TimeLimit, params.getTimeLimit()); //expressed in seconds 
				
			}
			if (params.isDefineOptimalGap()){			
				cplex.setParam(IloCplex.DoubleParam.EpGap, params.getGap());
			}
			
			solution = new OptimalSolution(dspGraph.getVertices().size());
			
			if (!DEBUG){
				cplex.setOut(null);
				cplex.setWarning(null);
			}
			
			long enlapsedTime = System.currentTimeMillis();
			if(cplex.solve()){
				enlapsedTime = System.currentTimeMillis() - enlapsedTime;
				
			    solution.setOptObjValue(cplex.getObjValue());
			    solution.setOptR(cplex.getValue(R));
			    solution.setOptLogA(cplex.getValue(logA));
			    solution.setOptC(cplex.getValue(C));
			    solution.setResolutionTime(enlapsedTime);
			    solution.setCompilationTime(compilationTime);

				for (DspEdge ij : dspGraph.getEdges().values()) {
					for (ResourceEdge uv  : resGraph.getEdges().values()) {
						int i = ij.getFrom();
						int j = ij.getTo();
						int u = uv.getFrom();
						int v = uv.getTo();

						if (!INCLUDE_SELF_LOOPS && u == v)
							continue;

						DspVertex iDsp = dspGraph.getVertices().get(i);
						DspVertex jDsp = dspGraph.getVertices().get(j);
						ResourceVertex uRes = resGraph.getVertices().get(u);
						ResourceVertex vRes = resGraph.getVertices().get(v);

						if (iDsp.deployableOn(uRes) && jDsp.deployableOn(vRes)) {

							if (Y.get(i,j,u,v) == null)
								continue;;

							double xyval = cplex.getValue(Y.get(i, j, u, v));

							if (xyval > 0) {
								solution.addLink(u,v);
							}

						}
					}
				}

//				/* DEBUG */
//				for (ResourceEdge uvRes : resGraph.getEdges().values()) {
//					int u = uvRes.getFrom();
//					int v = uvRes.getTo();
//					double zval = cplex.getValue(Z.get(u, v));
//					if (zval > 0) {
//						System.out.println("Z("+u+","+v+")=1");
//					}
//				}
//				/* DEBUG */

				for (DspVertex dspOperator : dspGraph.getVertices().values()) {
					for (ResourceVertex resNode : resGraph.getVertices().values()) {
						int i = dspOperator.getIndex();
						int u = resNode.getIndex();
						double xval = 0;
						if (dspOperator.deployableOn(u))
							xval = cplex.getValue(X.get(i, u));
			        	if (xval > 0) {
							solution.setPlacement(i, u);
							solution.addNode(u);
						}
					}				
				}		

				if (DEBUG)
					cplex.writeSolution("solution.sol");

			} else {
				System.out.println("Unable to find a solution");
			}

			solution.setStatus(cplex.getStatus());

		} catch (IloException e) {
			throw new ODDException("Error while solving OPP Model: " + e.getMessage());
		}
		
		return solution;
	}

	public void clean() {
		if (cplex != null)
			cplex.end();
	}

	@Override
	public void pin(int dspIndex, int resIndex) {

		try {
		
			if (X.get(dspIndex, resIndex) != null)
				cplex.addEq(X.get(dspIndex, resIndex), 1, "pinned");
			else
				System.out.println("Invalid pinning");
			
		} catch (IloException e) {
			e.printStackTrace();
		}	
		
	}
	
	@Override
	public String type() {
		return "PHYSICAL";
	}

}
