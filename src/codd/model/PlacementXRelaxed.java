package codd.model;

import ilog.concert.IloException;


public class PlacementXRelaxed extends PlacementX {
	
	protected static final double LOWER_BOUND = 0.0; 
	protected static final double UPPER_BOUND = 1.0; 
	
	public PlacementXRelaxed(DspGraph dsp, ResourceGraph res) throws IloException {
		super(dsp, res);
		
		/* XXX: you can restrict x_iu here... */
		for (DspVertex dspOperator : dsp.getVertices().values()) {
			for (ResourceVertex resNode : res.getVertices().values()) {
				int i = dspOperator.getIndex();
				int u = resNode.getIndex();
				this.X[i][u] = this.modeler.numVar(LOWER_BOUND, 
						UPPER_BOUND, "x[" + i + "][" + u + "]");
			}				
		}				
	}
}
