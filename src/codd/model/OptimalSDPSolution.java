package codd.model;


import codd.security.NodeLinkConfiguration;

import java.util.Arrays;

public class OptimalSDPSolution extends OptimalSolution {

	protected Pair dspToResConf[];
	protected int dspLinkConf[][];
	protected boolean exclusivelyUsedNode[];
	protected ResourceGraph resourceGraph;
	protected DspGraph dspGraph;

	protected double pathRespTime[];

	protected double optLogS;

	public OptimalSDPSolution(int numberOfDspVertices, ResourceGraph resGraph, DspGraph dspGraph) {
		super(numberOfDspVertices);

		this.resourceGraph = resGraph;
		this.dspGraph = dspGraph;

		this.optLogS = -1;

		dspToResConf = new Pair[numberOfDspVertices];
		exclusivelyUsedNode = new boolean[resGraph.getVertices().values().size()];
		dspLinkConf = new int[numberOfDspVertices][numberOfDspVertices];

		for (int i = 0; i < dspToRes.length; i++)
			dspToResConf[i] = new Pair(NOT_DEPLOYED, NOT_DEPLOYED);
	}

	public void setPlacementWithConf (int vDspIndex, int vResIndex, int confIndex){

		if (!(vDspIndex < dspToRes.length))
			throw new IndexOutOfBoundsException();

		dspToResConf[vDspIndex] = new Pair(vResIndex, confIndex);

	}

	public void setLinkConfiguration (int from, int to, int confIndex) {
		dspLinkConf[from][to] = confIndex;
	}

	public Pair getPlacementWithConf(int vDspIndex){
		return dspToResConf[vDspIndex];
	}

	public void setExclusivelyUsedNode (int vResIndex, boolean exclUse) {
		exclusivelyUsedNode[vResIndex] = exclUse;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(status.toString());
		sb.append('\n');
		if (!status.equals(Status.OPTIMAL))	 {
			return sb.toString();
		}

		sb.append(super.toString());
		sb.append('\n');

		sb.append("{");
		for (int i = 0; i < dspToRes.length; i++){
			sb.append(dspGraph.getVertices().get(i).id);
			sb.append("->");
			Pair p = getPlacementWithConf(i);
			ResourceVertex v = resourceGraph.vertices.get(p.getA());
			NodeLinkConfiguration conf = v.getConfigurations().get(p.getB());
			sb.append(String.format("vr%d (%s)", v.index, conf.toString()));

			if (i < (dspToRes.length - 1)){
				sb.append("; ");
			}
		}
		sb.append("}");
		sb.append('\n');

		for (DspEdge eDsp : dspGraph.edges.values()) {
			int i = eDsp.from;
			int j = eDsp.to;

			sb.append(String.format("(%d,%d)", i,j));
			sb.append("->");

			int u = dspToRes[i];
			int v = dspToRes[j];
			ResourceEdge resEdge = resourceGraph.edges.get(new Pair(u,v));

			int confIndex = dspLinkConf[i][j];
			NodeLinkConfiguration conf = resEdge.getConfigurations().get(confIndex);
			sb.append(conf.toString());

			sb.append("; ");
		}
		sb.append('\n');

		sb.append("Exclusively used: ");
		for (Integer v : resourceGraph.getVertices().keySet()) {
			if (exclusivelyUsedNode[v]) {
				sb.append(v);
				sb.append(';');
			}
		}
		sb.append('\n');

		sb.append(String.format("R = %f\n", optR));
		sb.append(String.format("Rpath = %s\n", Arrays.toString(pathRespTime)));


		sb.append(String.format("C = %f\n", optC));
		sb.append(String.format("S = %f (logS = %f)\n", Math.exp(optLogS), optLogS));



		return sb.toString();
	}

	public String report()
	{
		String reportStr = toString().replace('\n', ';');
		return reportStr;
	}

	public double getOptLogS() {
		return optLogS;
	}

	public double[] getPathRespTime() {
		return pathRespTime;
	}

	public void setPathRespTime(double[] pathRespTime) {
		this.pathRespTime = pathRespTime;
	}

	public void setOptLogS(double optLogS) {
		this.optLogS = optLogS;
	}
}
