package codd.model;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DspGraph {

	protected  String id;
	protected  Map<Pair, DspEdge>  edges;
	protected  Map<Integer, DspVertex>  vertices;
	
	protected Set<Integer>  sources;
	protected Set<Integer>  sinks;
	
	protected List<DspPath> paths;
	
	/** Synchronization time required by each reconfiguration (includes sync overhead before and after the reconfiguration actions). */
	protected double reconfigurationSyncTime = 0.0;

	public DspGraph(String id, 
			Map<Integer, DspVertex> vertices,
			Map<Pair, DspEdge> edges) {
		super();
		this.id = id;
		this.edges = edges;
		this.vertices = vertices;

		this.sources = new HashSet<Integer>();
		this.sinks = new HashSet<Integer>();
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<Pair, DspEdge> getEdges() {
		return edges;
	}

	public void setEdges(Map<Pair, DspEdge> edges) {
		this.edges = edges;
	}

	public Map<Integer, DspVertex> getVertices() {
		return vertices;
	}

	public void setVertices(Map<Integer, DspVertex> vertices) {
		this.vertices = vertices;
	}

	public List<DspPath>  getPaths() {
		return paths;
	}

	public void setPaths(List<DspPath> paths) {
		this.paths = paths;
	}

	public boolean addSource(int index){
		return this.sources.add(new Integer(index));
	}
	public boolean removeSource(int index){
		return this.sources.remove(new Integer(index));
	}
	public boolean isSource(int sourceIndex){
		return this.sources.contains(new Integer(sourceIndex));
	}
	
	public boolean addSink(int index){
		return this.sinks.add(new Integer(index));
	}
	public boolean removeSink(int index){
		return this.sinks.remove(new Integer(index));
	}
	public boolean isSink(int sinkIndex){
		return this.sinks.contains(new Integer(sinkIndex));
	}

	public Set<Integer> getSources() {
		return sources;
	}

	public void setSources(Set<Integer> sources) {
		this.sources = sources;
	}

	public Set<Integer> getSinks() {
		return sinks;
	}

	public void setSinks(Set<Integer> sinks) {
		this.sinks = sinks;
	}

	public double getIncomingDataRate(int vertexIndex){
		
		double incomingDR = 0;
		
		for(DspEdge e : this.edges.values()){
			
			if (e.getTo() == vertexIndex){
				incomingDR += e.getLambda();
			}
		
		}
		
//		System.out.println("Incoming Rate ["+ vertexIndex + "]: " + incomingDR);
		return incomingDR;
		
	}

	public double getOutgoingDataRate(int vertexIndex){
		
		double outgoingDR = 0;
		
		for(DspEdge e : this.edges.values()){
			
			if (e.getFrom() == vertexIndex){
				outgoingDR += e.getLambda();
			}
		
		}
		
		return outgoingDR;
		
	}
	
	@Override
	public String toString() {
		return "DspGraph(" + id + ")";		
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		DspGraph other = (DspGraph) obj;
		
		if (id != null && vertices != null && edges != null){

			return (id.equals(other.id) &&
					vertices.equals(other.vertices) &&
					edges.equals(other.edges));
			
		}
			
		return false;
			
	}

	public double getReconfigurationSyncTime() {
		return reconfigurationSyncTime;
	}

	public void setReconfigurationSyncTime(double reconfigurationSyncTime) {
		this.reconfigurationSyncTime = reconfigurationSyncTime;
	}
	
}
