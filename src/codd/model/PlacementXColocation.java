package codd.model;

import ilog.concert.IloException;
import ilog.concert.IloModeler;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;

public class PlacementXColocation {

	protected IloModeler modeler;
	protected IloNumVar L[][][];

	public PlacementXColocation(DspGraph dsp, ResourceGraph res) throws IloException {

		/* XXX: to be fixed */
		int resSize = res.getVertices().size();
		for (ResourceVertex uRes : res.getVertices().values()){
			if (uRes.getIndex() >= resSize)
				resSize = uRes.getIndex() + 1;
		}

//		this.L = new IloNumVar[res.getVertices().size()][res.getVertices().size()];
		this.L = new IloNumVar[dsp.getVertices().size()][dsp.getVertices().size()][resSize];
		this.modeler = new IloCplexModeler();

		for (DspVertex iDsp : dsp.getVertices().values()) {
			int i = iDsp.getIndex();
			for (DspVertex jDsp : dsp.getVertices().values()) {
				int j = jDsp.getIndex();
				for (ResourceVertex uRes : res.getVertices().values()) {
					int u = uRes.getIndex();
					this.L[i][j][u] = this.modeler.boolVar("L[" + i + "][" + j + "][" + u + "]");
				}
			}
		}

	}
	
	public IloNumVar get(final int i, final int j, final int u) {
		return this.L[i][j][u];
	}
	@Override
	public String toString() {
		return "y(" + super.toString() + ")";
	}

}
