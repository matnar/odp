package codd.model;

import java.util.HashSet;
import java.util.Set;

import codd.metricsprovider.ResponseTimeProvider;

public class DspVertex {
	
	protected int index;
	protected String id;
	protected int requiredResources;
	protected double executionTime;
	private Set<Integer> candidates;
	private boolean restrictedPlacement;
	private double costPerResource;
	private double serviceRate;
	private int maxReplication;
	protected double internalStateImageSize;
	protected double codeImageSize;
	
	private ResponseTimeProvider responseTimeProvider;

	public ResponseTimeProvider getResponseTimeProvider() {
		return responseTimeProvider;
	}

	public void setResponseTimeProvider(ResponseTimeProvider responseTimeProvider) {
		this.responseTimeProvider = responseTimeProvider;
	}

	public DspVertex(int index, String id, 
			int requiredResources, double executionTime, 
			double costPerResource) {
		this(index, id, requiredResources, executionTime, costPerResource, 0.0, 0.0);
	}

	public DspVertex(int index, String id) {
		this(index, id, 1, 1.0, 1.0, 0.0, 0.0);
	}

	public DspVertex(int index, String id, 
			int requiredResources, double executionTime, 
			double costPerResource, double internalStateImageSize, double codeImageSize) {
		this.index = index;
		this.id = id;
		this.requiredResources = requiredResources;
		this.executionTime = executionTime;
		this.restrictedPlacement = false;
		this.candidates = new HashSet<Integer>();
		this.costPerResource = costPerResource;
		this.maxReplication = Integer.MAX_VALUE;
		this.internalStateImageSize = internalStateImageSize;
		this.codeImageSize = codeImageSize;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getRequiredResources() {
		return requiredResources;
	}

	public void setRequiredResources(int requiredResources) {
		this.requiredResources = requiredResources;
	}

	public double getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(double executionTime) {
		this.executionTime = executionTime;
	}

	public void setMM1ExecutionTime (double inputRate) {
		if (inputRate >= serviceRate)
			throw new RuntimeException("utilization >= 1");

		this.executionTime = 1.0/(serviceRate - inputRate);
	}
	
	public Set<Integer> getCandidates() {
		return candidates;
	}
	public void setCandidates(Set<Integer> candidates) {
		restrictedPlacement = (candidates != null);
		
		this.candidates = candidates;
	}
	public void addCandidates(Integer vResIndex) {
		restrictedPlacement = true;
		this.candidates.add(vResIndex);
	}

	public boolean hasRestrictedPlacement() {
		return restrictedPlacement;
	}

	public void setRestrictedPlacement(boolean restrictedPlacement) {
		this.restrictedPlacement = restrictedPlacement;
	}
	
	public double getCostPerResource() {
		return costPerResource;
	}

	public void setCostPerResource(double costPerResource) {
		this.costPerResource = costPerResource;
	}
	
	public double getServiceRate() {
		return serviceRate;
	}

	public void setServiceRate(double serviceRate) {
		this.serviceRate = serviceRate;
	}

	public int getMaxReplication() {
		return maxReplication;
	}

	public void setMaxReplication(int maxReplication) {
		this.maxReplication = maxReplication;
	}

	public double getInternalStateImageSize() {
		return internalStateImageSize;
	}

	public void setInternalStateImageSize(double internalStateImageSize) {
		this.internalStateImageSize = internalStateImageSize;
	}

	public double getCodeImageSize() {
		return codeImageSize;
	}

	public void setCodeImageSize(double codeImageSize) {
		this.codeImageSize = codeImageSize;
	}

	public boolean deployableOn(ResourceVertexMultiset resMultiset){
		
		boolean deployableOnMultiset = true;		

		if (resMultiset == null)
			return false;
		
		if (resMultiset.getCardinality() > maxReplication)
			return false;
		
		for (ResourceVertex u : resMultiset.getVertices()){
			if (!deployableOn(u.getIndex())){
				deployableOnMultiset = false;
				break;
			}
		}
		return deployableOnMultiset;
		
	}
	public boolean deployableOn(ResourceVertex vRes) {
		if (vRes == null)
			return false;

		return deployableOn(vRes.getIndex());
	}
	public boolean deployableOn(Integer vResIndex) {
		if (!this.hasRestrictedPlacement())
			return true;
		else
			return this.candidates.contains(vResIndex);
	}

	@Override
	public String toString() {
		return "[vd" + index + ",'" + id + "' r"+ requiredResources + ", d" + executionTime + ", sr" + serviceRate + "]";
	}
	
	@Override
	public int hashCode() {
		return index;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		DspVertex other = (DspVertex) obj;
		
		return index == other.index;
	}
	
}
