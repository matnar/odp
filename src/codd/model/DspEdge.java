package codd.model;

public class DspEdge {

	protected int from;
	protected int to;
	protected double lambda;
	protected double bytesPerTuple;
	
	public DspEdge(int from, int to, double lambda) {
		super();
		this.from = from;
		this.to = to;
		this.lambda = lambda;
		this.bytesPerTuple = 1500;
	}

	public DspEdge(int from, int to, double lambda, int bytesPerTuple) {
		this(from, to, lambda);
		this.bytesPerTuple = bytesPerTuple;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public double getLambda() {
		return lambda;
	}

	public void setLambda(double lambda) {
		this.lambda = lambda;
	}
	
	public double getBytesPerTuple() {
		return bytesPerTuple;
	}

	public void setBytesPerTuple(double bytesPerTuple) {
		this.bytesPerTuple = bytesPerTuple;
	}

	@Override
	public String toString() {
		return "[ed(" + from + "," + to + "), dr:"+ lambda +"]";
	}
		
	@Override
	public int hashCode() {
		return from * 1000 + to;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		DspEdge other = (DspEdge) obj;
		
		return (from == other.from && to == other.to);
			
	}

}
