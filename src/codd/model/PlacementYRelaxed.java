package codd.model;

import ilog.concert.IloException;

public class PlacementYRelaxed extends PlacementY {
		
	protected static final double LOWER_BOUND = 0.0; 
	protected static final double UPPER_BOUND = 1.0; 
	
	public PlacementYRelaxed(DspGraph dsp, ResourceGraph res) throws IloException {
		super(dsp, res); 
		
		/* 
		 * Define Y variables as (double) numbers; 
		 * therefore, y[][][][] represents the probability of 
		 * mapping the logical stream ij over the logical link uv 
		 */
		for (DspEdge eDsp : dsp.getEdges().values()) {
			for (ResourceEdge eRes : res.getEdges().values()) {
				int i = eDsp.getFrom();
				int j = eDsp.getTo();
				int u = eRes.getFrom();
				int v = eRes.getTo();
				this.Y[i][j][u][v] = this.modeler.numVar(LOWER_BOUND, 
						UPPER_BOUND, "y[" + i + "][" + j + "][" + u + "][" + v + "]");
			}
		}		
		
	}
	
}
