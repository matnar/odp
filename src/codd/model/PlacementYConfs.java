package codd.model;

import codd.security.ConfigurationGenerator;
import ilog.concert.IloException;
import ilog.concert.IloModeler;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;

public class PlacementYConfs {

	protected IloModeler modeler;
	protected IloNumVar Y[][][][][];

	public PlacementYConfs(DspGraph dsp, ResourceGraph res) throws IloException {
		
		/* XXX: to be fixed */
		int resSize = res.getVertices().size();
		for (ResourceVertex uRes : res.getVertices().values()){
			if (uRes.getIndex() >= resSize)
				resSize = uRes.getIndex() + 1;
		}
	
		this.Y = new IloNumVar[dsp.getVertices().size()][dsp.getVertices().size()][resSize][resSize][];
		this.modeler = new IloCplexModeler();
		
		/* XXX: you can restrict y_ijuv here... */
		for (DspEdge eDsp : dsp.getEdges().values()) {
			int i = eDsp.getFrom();
			int j = eDsp.getTo();
			
			for (ResourceEdge eRes : res.getEdges().values()) {
				int u = eRes.getFrom();
				int v = eRes.getTo();

				int confs = ConfigurationGenerator.getAllConfigurations(eRes.getSecuritySpecs()).size();
				if (confs < 1)
					throw new RuntimeException("Less than 1 configuration found.");

				this.Y[i][j][u][v] = new IloNumVar[confs];
				for (int c = 0; c<confs; c++) {
					this.Y[i][j][u][v][c] = this.modeler.boolVar("y[" + i + "][" + j + "][" + u + "][" + v + "]["+ c + "]");
				}
			}
			
//			/* Add self-loop */
//			for (ResourceVertex vRes : res.getVertices().values()) {
//				int u = vRes.getIndex();
//				this.Y[i][j][u][u] = this.modeler.boolVar("y[" + i + "][" + j + "][" + u + "][" + u + "]");
//			}
			
		}		
		
	}
	
	public IloNumVar get(final int i, final int j, final int u, final int v, final int conf) {
		return this.Y[i][j][u][v][conf];
	}
	@Override
	public String toString() {
		return "y(" + super.toString() + ")";
	}

}
