package codd.model;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;

import java.util.Collection;
import java.util.Random;


public class PlacementXMultiset extends PlacementX {
	
	public PlacementXMultiset(DspGraph dsp, ResourceGraph res, Collection<ResourceVertexMultiset> resMultisets) throws IloException {
		super(dsp, res);

		this.X = new IloNumVar[dsp.getVertices().size()][resMultisets.size()];
		for (DspVertex v : dsp.getVertices().values()) {
			for (ResourceVertexMultiset ms : resMultisets)
				X[v.getIndex()][ms.getIndex()] = null;
		}
		this.modeler = new IloCplexModeler();

		for (DspVertex iDsp : dsp.getVertices().values()) {
			int i = iDsp.getIndex();
			for (ResourceVertexMultiset resMultiset : resMultisets) {
				int ums = resMultiset.getIndex();
				this.X[i][ums] = this.modeler.boolVar("x[" + i + "][" + ums + "]");
			}
		}
	}

}
