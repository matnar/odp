package codd.model;

import codd.security.ConfigurationGenerator;
import ilog.concert.IloException;
import ilog.concert.IloModeler;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;


public class PlacementXConfs {
	
	protected IloModeler modeler;
	protected IloNumVar X[][][];
	
	public PlacementXConfs (DspGraph dsp, ResourceGraph res) throws IloException {
		
		/* XXX: to be fixed */
		int resSize = res.getVertices().size();
		for (ResourceVertex uRes : res.getVertices().values()){
			if (uRes.getIndex() >= resSize)
				resSize = uRes.getIndex() + 1;
		}

		this.X = new IloNumVar[dsp.getVertices().size()][resSize][];
		this.modeler = new IloCplexModeler();
		
		for (DspVertex dspOperator : dsp.getVertices().values()) {
			for (ResourceVertex resNode : res.getVertices().values()) {
				int confs = ConfigurationGenerator.getAllConfigurations(resNode.getSecuritySpecs()).size();
				if (confs < 1)
					throw new RuntimeException("Less than 1 configuration found.");
				int i = dspOperator.getIndex();
				int u = resNode.getIndex();
				this.X[i][u] = new IloNumVar[confs];
				for (int c = 0; c<confs; c++) {
					this.X[i][u][c] = this.modeler.boolVar("x[" + i + "][" + u + "][" + c + "]");
				}
			}
		}		
		
	}
	
	public IloNumVar get(final int i, final int u, final int conf) {
		if (i > X.length - 1 || u > X[i].length - 1)
			return null;
		
		return this.X[i][u][conf];
	}

	@Override
	public String toString() {
		return "x(" + super.toString() + ")";
	}

}
