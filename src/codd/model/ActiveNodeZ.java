package codd.model;

import ilog.concert.IloException;
import ilog.concert.IloModeler;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;

public class ActiveNodeZ {
	
	protected IloModeler modeler;
	protected IloNumVar Z[];
	
	public ActiveNodeZ(ResourceGraph res) throws IloException {
		this.Z = new IloNumVar[res.getVertices().size()];
		this.modeler = new IloCplexModeler();
		
		/* XXX: you can restrict y_ijuv here... */
		for (ResourceVertex uRes : res.getVertices().values()) {
			int u = uRes.getIndex();
			this.Z[u] = this.modeler.boolVar("z[" + u + "]");
		}
	
	}
	
	public IloNumVar get(final int u) {
		return this.Z[u];
	}
	@Override
	public String toString() {
		return "y(" + super.toString() + ")";
	}

}
