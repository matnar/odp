package codd.model;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;

import java.util.Collection;
import java.util.Map;

public class PrunedPlacementXMultiset extends PlacementX {

	public PrunedPlacementXMultiset(DspGraph dsp, ResourceGraph res, Collection<ResourceVertexMultiset> resMultisets,
									Map<Integer, ResourceVertexMultiset> currentPlacement) throws IloException {
		super(dsp, res);

		this.X = new IloNumVar[dsp.getVertices().size()][resMultisets.size()];
		this.modeler = new IloCplexModeler();

		for (DspVertex iDsp : dsp.getVertices().values()) {
			int i = iDsp.getIndex();
			ResourceVertexMultiset current = currentPlacement.get(i);

			for (ResourceVertexMultiset resMultiset : resMultisets) {
				int ums = resMultiset.getIndex();
				if (current.isAtOneStepFrom(resMultiset)) {
					this.X[i][ums] = this.modeler.boolVar("x[" + i + "][" + ums + "]");
					//System.out.println("OK: " + i + "-" + resMultiset.getIndex() + "->" + X[i][ums]);
				} else {
					//System.out.println("Skip: " + i + "-" + resMultiset.getIndex());
					this.X[i][ums] = null;
				}
			}
		}
	}


}
