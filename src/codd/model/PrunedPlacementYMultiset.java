package codd.model;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;

import java.util.Collection;

public class PrunedPlacementYMultiset extends PlacementY {


	public PrunedPlacementYMultiset(DspGraph dsp, ResourceGraph res, Collection<ResourceVertexMultiset> resMultisets, PrunedPlacementXMultiset X) throws IloException {
		super(dsp, res); 
		
		this.Y = new IloNumVar[dsp.getVertices().size()][dsp.getVertices().size()][resMultisets.size()][resMultisets.size()];
		this.modeler = new IloCplexModeler();

		for (DspEdge eDsp : dsp.getEdges().values()) {
			int i = eDsp.getFrom();
			int j = eDsp.getTo();
			/*  Retrieve multisets for operator i */
			for (ResourceVertexMultiset uResMultiset : resMultisets) {
				int ums = uResMultiset.getIndex();
				/*  Retrieve multisets for operator j */
				for (ResourceVertexMultiset vResMultiset : resMultisets) {
					int vms = vResMultiset.getIndex();
					this.Y[i][j][ums][vms] = null;
				}
			}
		}

		for (DspEdge eDsp : dsp.getEdges().values()) {
			int i = eDsp.getFrom();
			int j = eDsp.getTo();
			
			/*  Retrieve multisets for operator i */
			for (ResourceVertexMultiset uResMultiset : resMultisets) {
			
				int ums = uResMultiset.getIndex();
				if (X.get(i, ums) == null)
					continue;
	
				/*  Retrieve multisets for operator j */
				for (ResourceVertexMultiset vResMultiset : resMultisets) {
					int vms = vResMultiset.getIndex();
					if (X.get(j, vms) != null)
						Y[i][j][ums][vms] = this.modeler.boolVar("y[" + i + "][" + j + "][" + ums + "][" + vms + "]");
				}
			}
		}	
				
	}
		
}
