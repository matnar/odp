package codd.model;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;

import java.util.Collection;

public class PlacementYMultiset extends PlacementY {
		
	protected static final double LOWER_BOUND = 0.0; 
	protected static final double UPPER_BOUND = 1.0;


	
	public PlacementYMultiset(DspGraph dsp, ResourceGraph res, Collection<ResourceVertexMultiset> resMultisets) throws IloException {
		super(dsp, res); 
		
		this.Y = new IloNumVar[dsp.getVertices().size()][dsp.getVertices().size()][resMultisets.size()][resMultisets.size()];
		this.modeler = new IloCplexModeler();
		
		/* XXX: you can restrict y_ijuv here... */
		for (DspEdge eDsp : dsp.getEdges().values()) {
			int i = eDsp.getFrom();
			int j = eDsp.getTo();
			
			/*  Retrieve multisets for operator i */
			for (ResourceVertexMultiset uResMultiset : resMultisets) {
			
				int ums = uResMultiset.getIndex();
	
				/*  Retrieve multisets for operator j */
				for (ResourceVertexMultiset vResMultiset : resMultisets) {
				
					int vms = vResMultiset.getIndex();
					this.Y[i][j][ums][vms] = this.modeler.boolVar("y[" + i + "][" + j + "][" + ums + "][" + vms + "]");
					
				}
			}
		}	
				
	}
		
}
