package codd.model;

import ilog.concert.IloException;
import ilog.concert.IloModeler;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplexModeler;

public class ActiveLinkZ {
	
	protected IloModeler modeler;
	protected IloNumVar Z[][];
	
	public ActiveLinkZ(ResourceGraph res) throws IloException {

		/* XXX: to be fixed */
		int resSize = res.getVertices().size();
		for (ResourceVertex uRes : res.getVertices().values()){
			if (uRes.getIndex() >= resSize)
				resSize = uRes.getIndex() + 1;
		}

//		this.Z = new IloNumVar[res.getVertices().size()][res.getVertices().size()];
		this.Z = new IloNumVar[resSize][resSize];
		this.modeler = new IloCplexModeler();
		
		/* XXX: you can restrict y_ijuv here... */
		for (ResourceEdge eRes : res.getEdges().values()) {
			int u = eRes.getFrom();
			int v = eRes.getTo();
			this.Z[u][v] = this.modeler.boolVar("z[" + u + "][" + v + "]");
		}
	
	}
	
	public IloNumVar get(final int u, final int v) {
		return this.Z[u][v];
	}
	@Override
	public String toString() {
		return "y(" + super.toString() + ")";
	}

}
