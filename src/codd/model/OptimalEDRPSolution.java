package codd.model;

import java.util.Map;

/**
 * Solution of EDRP problem.
 */
public class OptimalEDRPSolution extends OptimalMultisetSolution {

	/** Optimal value for downtime. */
	protected double optTDown;
	

	public OptimalEDRPSolution(int numberOfDspVertices, Map<Integer, ResourceVertexMultiset> allMultisets) {
		super(numberOfDspVertices, allMultisets);
	}

	public double getOptTDown() {
		return optTDown;
	}

	public void setOptTDown(double optTDown) {
		this.optTDown = optTDown;
	}

	/**
	 * Returns count of deployed instances.
	 * @return Count of deployed instances.
	 */
	public int getInstancesCount()
	{
		int count = 0;

		for (Integer i : dspToRes.keySet()) {
			count += dspToRes.get(i).getCardinality();
		}
		
		return count;
	}
	
}
