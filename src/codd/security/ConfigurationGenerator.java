package codd.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ConfigurationGenerator {

	static public List<NodeLinkConfiguration> getAllConfigurations (NodeLinkSpecs specs) {
		List<NodeLinkConfiguration> confs = new ArrayList<>();

		List<String> keys = new ArrayList<>(specs.getKeys());
		if (keys.isEmpty()) {
			confs.add(new NodeLinkConfiguration(new HashMap<String, SpecWithCoeffs>(), 0));
			return confs;
		}

		int optionCnt[] = new int[keys.size()];
		int numOfConfigurations = 1;
		for (int i = 0; i<keys.size(); i++) {
			optionCnt[i] = specs.get(keys.get(i)).size();
			numOfConfigurations *= optionCnt[i];
		}

		HashMap<String, SpecWithCoeffs> confSpecs = new HashMap<>();
		int confCount = 0;
		int index[] = null;
		while (confCount < numOfConfigurations) {
			if (confCount == 0) {
				index = new int[keys.size()];
				for (int i = 0; i < keys.size(); i++) {
					index[i] = 0;
				}
			} else {
				/* next conf */
				boolean incremented = false;
				int j = 0;
				while (j < keys.size() && !incremented) {
					if (index[j] < optionCnt[j] - 1) {
						index[j]++;
						incremented = true;
					} else {
						index[j] = 0;
					}
					j++;
				}
			}

			/* Build configuration based on indices */
			for (int i = 0; i<keys.size(); i++) {
				String key = keys.get(i);
				SpecWithCoeffs s = specs.get(key).get(index[i]);
				confSpecs.put(key, s);
			}

			NodeLinkConfiguration conf = new NodeLinkConfiguration(confSpecs, confCount);
			confs.add(conf);

			++confCount;
		}

		return confs;
	}
}
