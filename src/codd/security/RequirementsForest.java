package codd.security;

import codd.model.DspEdge;
import codd.model.DspVertex;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RequirementsForest {

	private Map<String, Tree> trees = new HashMap<>();


	public void add (DspVertex op, String reqCategory, String reqObj, Object value, double score) {
		Tree tree = getTree(reqCategory);
		Tree opTree = tree.getOrAddChildren(op);
		Tree objTree = opTree.getOrAddChildren(reqObj);
		objTree.getOrAddChildren(new ScoredValue(value, score));
	}

	public void add (DspEdge edge, String reqCategory, String reqObj, Object value, double score) {
		Tree tree = getTree(reqCategory);
		Tree opTree = tree.getOrAddChildren(edge);
		Tree objTree = opTree.getOrAddChildren(reqObj);
		objTree.getOrAddChildren(new ScoredValue(value, score));
	}


	public Tree getTree (String reqCategory) {
		if (!trees.containsKey(reqCategory))
			trees.put(reqCategory, new Tree(reqCategory));

		return trees.get(reqCategory);
	}

	public Set<String> getReqCategories()
	{
		return trees.keySet();
	}

	public void print ()
	{
		for (Tree tree : trees.values()) {
			print(tree);
			System.out.print('\n');
		}
	}

	public void print (Tree tree)
	{
		System.out.println("Category: " + tree.getRootElem());
		for (Tree dspObj : tree.getChildren()) {
			for (Tree ro : dspObj.getChildren()) {
				System.out.println(String.format("%s - %s wants: ", dspObj.getRootElem(), ro.getRootElem()));
				for (Tree val : ro.getChildren()) {
					ScoredValue sval = (ScoredValue)val.getRootElem();
					System.out.println(String.format("%s (%.2f)", sval.value.toString(), sval.score));
				}
			}
		}
	}
}
