package codd.security;

import java.util.Objects;

public class ScoredValue {

	public double score = 1.0;
	public Object value;

	public ScoredValue (Object value, double score) {
		this.value = value;
		this.score = score;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ScoredValue that = (ScoredValue) o;
		return Objects.equals(value, that.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}
}
