package codd.security;

import java.util.*;

public class NodeLinkSpecs {

	private Map<String, List<SpecWithCoeffs>> specs = new HashMap<>();

	public NodeLinkSpecs() {}

	public NodeLinkSpecs (NodeLinkSpecs other) {
		for (String k : other.specs.keySet()) {
			specs.put(k, new ArrayList<>(other.specs.get(k)));
		}
	}

	public void addForNode (String rc, String ro, Object value, double requiredResCoeff, double speedupCoeff, double costCoeff) {
		String key = rcro2key(rc,ro);
		SpecWithCoeffs s = SpecWithCoeffs.makeForNode(value, requiredResCoeff, speedupCoeff, costCoeff);

		add(key, s);
	}

	public void addForEdge (String rc, String ro, Object value, double usedBandwidthCoeff, double delayCoeff, double costCoeff) {
		String key = rcro2key(rc,ro);
		SpecWithCoeffs s = SpecWithCoeffs.makeForEdge(value, usedBandwidthCoeff, delayCoeff, costCoeff);
		add(key, s);
	}

	private void add (String key, SpecWithCoeffs s) {
		if (!specs.containsKey(key)) {
			specs.put(key, new ArrayList<SpecWithCoeffs>());
		}

		specs.get(key).add(s);
	}

	public Set<String> getKeys() {
		return specs.keySet();
	}

	public List<SpecWithCoeffs> get (String rc, String ro) {
		return specs.get(rcro2key(rc,ro));
	}

	public List<SpecWithCoeffs> get (String key) {
		return specs.get(key);
	}

	static public String rcro2key (String rc, String ro) {
		return rc + "__" + ro;
	}

	public void print()
	{
		for (String key : specs.keySet()) {
			System.out.println(key);
			for (SpecWithCoeffs s : specs.get(key)) {
				System.out.print(String.format("[%s (%f,%f,%f,%f,%f)] ", s.value.toString(),
						s.usedResCoeff, s.speedupCoeff, s.costCoeff, s.usedBwCoeff, s.delayCoeff));
			}
			System.out.print('\n');
		}
	}
}
