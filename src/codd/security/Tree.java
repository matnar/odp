package codd.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Tree {

	private Object rootElem;
	private List<Tree> children;

	public Tree (Object root) {
		this.rootElem = root;
		children = new ArrayList<>();
	}

	public Object getRootElem()
	{
		return rootElem;
	}

	public List<Tree> getChildren()
	{
		return children;
	}

	public Tree getOrAddChildren (Object obj) {
		for (Tree c : children) {
			if (c.rootElem.equals(obj))
				return c;
		}

		Tree newT = new Tree(obj);
		children.add(newT);
		return newT;
	}

	@Override
	public int hashCode()
	{
		return Objects.hashCode(rootElem);
	}
}
