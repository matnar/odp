package codd.security;

public class SpecWithCoeffs {

	public Object value;
	public double costCoeff = 1.0;
	public double usedResCoeff = 1.0;
	public double speedupCoeff = 1.0;
	public double usedBwCoeff = 1.0;
	public double delayCoeff = 1.0;

	private SpecWithCoeffs()
	{

	}

	static public SpecWithCoeffs makeForNode (Object value, double requiredResCoeff, double speedupCoeff, double costCoeff) {
		SpecWithCoeffs s = new SpecWithCoeffs();
		s.value = value;
		s.costCoeff = costCoeff;
		s.usedResCoeff = requiredResCoeff;
		s.speedupCoeff = speedupCoeff;
		return s;
	}

	static public SpecWithCoeffs makeForEdge (Object value, double usedBwCoeff, double delayCoeff, double costCoeff) {
		SpecWithCoeffs s = new SpecWithCoeffs();
		s.value = value;
		s.costCoeff = costCoeff;
		s.usedBwCoeff = usedBwCoeff;
		s.delayCoeff = delayCoeff;
		return s;
	}
}
