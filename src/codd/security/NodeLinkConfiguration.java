package codd.security;

import java.util.HashMap;
import java.util.Map;

public class NodeLinkConfiguration {
	private Map<String, SpecWithCoeffs> specs = new HashMap<>();
	private double costCoeff = 1.0;
	private double usedResCoeff = 1.0;
	private double speedupCoeff = 1.0;
	private double usedBwCoeff = 1.0;
	private double delayCoeff = 1.0;

	private int index;

	public NodeLinkConfiguration (Map<String, SpecWithCoeffs> specs, int index)
	{
		this.specs.putAll(specs);
		this.index = index;
	}

	public Object getSpec (String rc, String ro)  {
		final String key = NodeLinkSpecs.rcro2key(rc,ro);
		if (!specs.containsKey(key))
			return null;

		return specs.get(key).value;
	}

	public int getIndex() {
		return index;
	}

	private void computeCoeffs()
	{
		costCoeff = 1.0;
		for (SpecWithCoeffs s : specs.values()) {
			costCoeff *= s.costCoeff;
		}

		speedupCoeff = 1.0;
		for (SpecWithCoeffs s : specs.values()) {
			speedupCoeff *= s.speedupCoeff;
		}

		usedResCoeff = 1.0;
		for (SpecWithCoeffs s : specs.values()) {
			usedResCoeff *= s.usedResCoeff;
		}

		usedBwCoeff = 1.0;
		for (SpecWithCoeffs s : specs.values()) {
			usedBwCoeff *= s.usedBwCoeff;
		}

		delayCoeff = 1.0;
		for (SpecWithCoeffs s : specs.values()) {
			delayCoeff *= s.delayCoeff;
		}

	}

	public boolean doesRequireExclusiveUse() {
		final String key = NodeLinkSpecs.rcro2key(RequirementsBase.RC_PROCESS_ISOLATION, RequirementsBase.RO_MULTITENANCY);
		if (!specs.containsKey(key))
			return false;

		Boolean value = (Boolean)specs.get(key).value;
		return !value;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (String key : specs.keySet()) {
			sb.append(String.format("%s->%s ", key, specs.get(key).value.toString()));
		}
		sb.append(']');
		return sb.toString();
	}

	public double getCostCoeff() {
		return costCoeff;
	}

	public double getUsedResCoeff() {
		return usedResCoeff;
	}

	public double getSpeedupCoeff() {
		return speedupCoeff;
	}

	public double getUsedBwCoeff() {
		return usedBwCoeff;
	}

	public double getDelayCoeff() {
		return delayCoeff;
	}
}
