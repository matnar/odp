package codd.security;

public class RequirementsBase {

	private  RequirementsBase() {}


	final public static String RC_NETWORK = "Network";
	final public static String RO_ENCRYPT_TRAFFIC = "Encrypted Traffic";
	final public static String RO_WIRED_CONNECTIVITY = "WiredConn";


	// ---------------------------------------

	final public static String RC_PROCESS_ISOLATION = "ProcessIsolation";

	final public static String RO_RUNTIME_ENV = "Runtime Environment";
	final public static String RO_MULTITENANCY = "Multi-tenancy";
}
