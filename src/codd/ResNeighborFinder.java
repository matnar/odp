package codd;

import codd.distance.ObjectiveFunctionDistanceStrategy;
import codd.distance.PairwiseDistanceStrategy;
import codd.model.Pair;
import codd.model.ResourceEdge;
import codd.model.ResourceGraph;

import java.util.*;

public class ResNeighborFinder {
	
	ResourceGraph graph; 
	ODDParameters params;
	PairwiseDistanceStrategy distStrategy;
	private boolean DEBUG = false;

	public enum MODE {
		BESTFIT, 
		PROBABILISTIC
	};
	
	public ResNeighborFinder(ResourceGraph resourceGraph, ODDParameters params) {
		
		this.graph = resourceGraph;
		this.params = params;
		this.distStrategy = new ObjectiveFunctionDistanceStrategy(this.graph, this.params);
		
	}

	public Set<Integer> findNeighbors(int k, Set<Integer> nodes, MODE mode){
		
		Set<Integer> neighbors = new HashSet<Integer>();

		if (DEBUG)
			System.out.println("ResNeighborFinder - MODE: " + mode);
		
		for (Integer node : nodes){
			List<Integer> singleNodeNeighbors = findNeighbors(k, node.intValue(), mode);
			
			neighbors.addAll(singleNodeNeighbors);
		}
		
		return neighbors;
		
	}

	/**
	 * Find neighbors considering the distance from nodeA 
	 * plus the distance from nodeB
	 * 
	 * @param k
	 * @param nodeA
	 * @param nodeB
	 * @param mode
	 * @return
	 */
	public List<Integer> findNeighbors(int k, int nodeA, int nodeB, MODE mode){
		
		Map<Pair, ResourceEdge>  edges = graph.getEdges();
		
		List<KeyValue> neighborDistance = new ArrayList<ResNeighborFinder.KeyValue>();
	
		for (Integer vertex : graph.getVertices().keySet()){
			
			Pair aToVertex = new Pair(nodeA, vertex);
			Pair vertexToB = new Pair(vertex, nodeB);
			
			ResourceEdge e1 = edges.get(aToVertex);
			ResourceEdge e2 = edges.get(vertexToB);
			
			double distance = distStrategy.getDistance(e1) + distStrategy.getDistance(e2);

			KeyValue kv = new KeyValue(vertex, distance);
			neighborDistance.add(kv);
			
		}

		return sortAndGetK(k, neighborDistance, mode);
		
	}

	public List<Integer> findNeighbors(int k, int node, MODE mode){
		
		Collection<ResourceEdge> edges = graph.getEdges().values();
		Iterator<ResourceEdge> it = edges.iterator();
		
		List<KeyValue> neighborDistance = new ArrayList<ResNeighborFinder.KeyValue>();
		
		while(it.hasNext()){
			ResourceEdge e = it.next();
			
			if (node == e.getFrom() && node != e.getTo()){
				
				double distance = distStrategy.getDistance(e);
				
				KeyValue kv = new KeyValue(e.getTo(), distance);
				
				neighborDistance.add(kv);
			}
		}
		
		return sortAndGetK(k, neighborDistance, mode);
		
	}
	
	private List<Integer> sortAndGetK(int k, List<KeyValue> neighborDistance, MODE mode){
		
		Collections.sort(neighborDistance);	

		List<Integer> neighbors = new ArrayList<Integer>();

		if (k > neighborDistance.size())
			k = neighborDistance.size();
		
		switch(mode){
		case PROBABILISTIC:
			double maxDistance = 0; 
			Random rnd = new Random();
			
			for (KeyValue kv : neighborDistance){
				if (maxDistance < kv.getValue())
					maxDistance = kv.getValue();
			}
			
			int fallback = 10000;
			
//			System.out.println("Max Distance: " + maxDistance);
			
			while ((k - neighbors.size()) > 0){
				
				for (KeyValue kv : neighborDistance){
					double normDist = kv.getValue() / maxDistance;
					double neighborProbability = 1 - normDist;
//					System.out.println(". Node " + kv.getKey() + " - normDistance: " + normDist + "; prob:" + neighborProbability);
					
					if (rnd.nextFloat() < neighborProbability){
						neighbors.add(kv.getKey());			
						
//						System.out.println(". Node " + kv.getKey() + " chosen. New k="+(k - neighbors.size()));
						
						if ((k - neighbors.size()) == 0)
							break;
//					} else {
//						System.out.println(". Node " + kv.getKey() + " NOT chosen. New k="+(k - neighbors.size()));
					}
				}
				
				fallback--;
				
				if (fallback <= 0){
					
					System.out.println("-- Find Neighbors: Fallback activated: switching from probabilistic to best-fit --");
					
					neighbors.clear();
					
					for (int i = 0; i < k; i++){

						neighbors.add(neighborDistance.get(i).getKey());
//			 			System.out.println(neighborDistance.get(i).getKey() + " -- " + neighborDistance.get(i).getValue());
						
					}
				}
					
			}
			
		break;
		case BESTFIT:
		default:
//			System.out.println(" ### " + node + " ### ### ### ### ###");
			 
			for (int i = 0; i < k; i++){

				neighbors.add(neighborDistance.get(i).getKey());
//	 			System.out.println(neighborDistance.get(i).getKey() + " -- " + neighborDistance.get(i).getValue());
				
			}
			break;
		}
		
		return neighbors;
		
	}
	
	
	private class KeyValue implements Comparable<KeyValue>{

		int key;
		double value;
		
		public KeyValue(int key, double value) {
			this.key = key;
			this.value = value;
		}
		
		public int getKey() {
			return key;
		}
		public double getValue() {
			return value;
		}

		@Override
		public int compareTo(KeyValue o) {
			// ascending order
			return (int) (100000.0 * (this.value - o.value));
		}
		
	}
	
}
