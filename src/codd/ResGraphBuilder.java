package codd;

import codd.metricsprovider.ResourceMetricsProvider;
import codd.model.Pair;
import codd.model.ResourceEdge;
import codd.model.ResourceGraph;
import codd.model.ResourceVertex;
import codd.utils.FloydWarshall;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResGraphBuilder {

	protected TYPE type;
	protected ResourceGraph resGraph; 
	private int availableResourceMultiplier;
	
	public enum TYPE {
		FULL_MESH, ANSNET, MULTIHOP, BRITE, UNKNOWN
	}

	
	public ResGraphBuilder() {
		this.type = TYPE.UNKNOWN;
		resGraph = null;
	}
	public ResGraphBuilder(ResourceGraph resourceGraph) {
		this.resGraph = resourceGraph;
		this.type = TYPE.UNKNOWN;
	}
	public ResGraphBuilder(int availableResourceMultiplier) {
		resGraph = null;
		this.availableResourceMultiplier = availableResourceMultiplier;
	}
		
	public ResourceGraph getGraph(){
		return resGraph;
	}

	public ResGraphBuilder 	create(
			ResourceMetricsProvider metricsProvider,
			TYPE type,
			int numResources
			){

		this.type = type;
				
		switch (type) {
		case FULL_MESH:
			return createFullMesh(metricsProvider, numResources);
		
		case ANSNET:
			return createAnsnet(metricsProvider, numResources);
		
		case MULTIHOP: 
			return testMultihop();
		
		case BRITE:
			return createBrite(metricsProvider, numResources);
		default:
			return createAnsnet(metricsProvider, numResources);		
		}
	}
	
//	@Deprecated
//	public ResourceGraph extractSubGraph(List<Integer> resIds){
//
//		if (resGraph == null)
//			return null;
//
//		Map<Integer, ResourceVertex> sNodes = new HashMap<>();
//		Map<Pair, ResourceEdge> sEdges = new HashMap<>();
//
//		/* Extract all nodes */
//		for (Integer id : resIds){
//			sNodes.put(id, resGraph.getVertices().get(id));
//		}
//
//		/* Extract all vertices */
//		for (ResourceVertex uRes : sNodes.values()){
//			int u = uRes.getIndex();
//			for (ResourceVertex vRes : sNodes.values()){
//				int v = vRes.getIndex();
//
//				Pair uv = new Pair(u, v);
//				sEdges.put(uv, resGraph.getEdges().get(uv));
//			}
//		}
//
//		return new ResourceGraph("excerpt", sNodes, sEdges);
//
//	}
	
	public static ResourceGraph extractSubGraph(ResourceGraph resourceGraph, List<Integer> resIds){
		
		if (resourceGraph == null)
			return null;
		
		Map<Integer, ResourceVertex> sNodes = new HashMap<>();
		Map<Pair, ResourceEdge> sEdges = new HashMap<>();

		/* Extract all nodes */
		for (Integer id : resIds){
			sNodes.put(id, resourceGraph.getVertices().get(id));
		}
		
		/* Extract all vertices */
		for (ResourceVertex uRes : sNodes.values()){
			int u = uRes.getIndex();
			for (ResourceVertex vRes : sNodes.values()){
				int v = vRes.getIndex();
				
				Pair uv = new Pair(u, v);
				sEdges.put(uv, resourceGraph.getEdges().get(uv));
			}	
		}

		return new ResourceGraph("excerpt", sNodes, sEdges);
		
	}
	
	public TYPE getType() {
		return type;
	}
	public void setType(TYPE type) {
		this.type = type;
	}
	
	private ResGraphBuilder createFullMesh(
			ResourceMetricsProvider metricsProvider,
			int numResources
			){

		Map<Integer, ResourceVertex> vRes = new HashMap<>();
		Map<Pair, ResourceEdge> eRes = new HashMap<>();


		/* 1. Create ResourceVertex to represent nodes (supervisors, set of worker slots) */
		for(int index = 0; index < numResources; index++){
			int cu 			= metricsProvider.getAvailableResources(index);
			double su 		= metricsProvider.getSpeedup(index);
			double bigAu	= metricsProvider.getAvailability(index);
			double instanceLaunchTime = metricsProvider.getInstanceLaunchTime(index);
			double uploadRateToDS = metricsProvider.getUploadRateToDS(index);
			double downloadRateFromDS = metricsProvider.getDownloadRateFromDS(index);
			double uploadRateToLocalDS = metricsProvider.getUploadRateToLocalDS(index);
			double downloadRateFromLocalDS = metricsProvider.getDownloadRateFromLocalDS(index);
			double dataStoreRTT = metricsProvider.getDataStoreRTT(index);
			ResourceVertex v = new ResourceVertex(index, cu, su, bigAu, instanceLaunchTime, uploadRateToDS, downloadRateFromDS, uploadRateToLocalDS, downloadRateFromLocalDS, dataStoreRTT);
			vRes.put(index, v);
		}


		/* 2. Create DspEdges to represent link between executors */
		for (ResourceVertex u : vRes.values()){
			for (ResourceVertex v : vRes.values()){
				int uIndex = u.getIndex();
				int vIndex = v.getIndex();

				int latUV = (int) metricsProvider.getLatency(uIndex, vIndex);
				double bigAUV = metricsProvider.getAvailability(uIndex, vIndex);
				double bwUV = metricsProvider.getBandwidth(uIndex, vIndex);
				double costUV = metricsProvider.getCostPerUnitData(uIndex, vIndex);

				ResourceEdge rEdge = new ResourceEdge(uIndex, vIndex, bwUV, latUV, bigAUV, costUV);
				eRes.put(new Pair(uIndex, vIndex), rEdge);
			}
		}


		/* 3. Creating object DspGraph */
		String graphName = "resources";
		resGraph = new ResourceGraph(graphName, vRes, eRes);
		return this;

	}

	private ResGraphBuilder createAnsnet(
			ResourceMetricsProvider metricsProvider,
			int numResources){

		Map<Integer, ResourceVertex> vRes = new HashMap<>();
		Map<Pair, ResourceEdge> eRes = new HashMap<>();

		int nodes = 32;

		if (numResources < nodes)
			nodes = numResources;

		/* 1. Create ResourceVertex to represent nodes (supervisors, set of worker slots) */
		for(int index = 0; index < nodes; index++){
			int cu 			= metricsProvider.getAvailableResources(index);
			double su 		= metricsProvider.getSpeedup(index);
			double bigAu	= metricsProvider.getAvailability(index);
			double instanceLaunchTime = metricsProvider.getInstanceLaunchTime(index);
			double uploadRateToDS = metricsProvider.getUploadRateToDS(index);
			double downloadRateFromDS = metricsProvider.getDownloadRateFromDS(index);
			double uploadRateToLocalDS = metricsProvider.getUploadRateToLocalDS(index);
			double downloadRateFromLocalDS = metricsProvider.getDownloadRateFromLocalDS(index);
			double dataStoreRTT = metricsProvider.getDataStoreRTT(index);
			ResourceVertex v = new ResourceVertex(index, cu, su, bigAu, instanceLaunchTime, uploadRateToDS, downloadRateFromDS, uploadRateToLocalDS, downloadRateFromLocalDS, dataStoreRTT);
			
			vRes.put(index, v);
		}




		/* 2. Create DspEdges to represent link between executors */
//		double bw = Double.MAX_VALUE;
//		double av = 1.0;

		//		eRes.put(new Pair(0, 1), new ResourceEdge(0, 1, bw, 12, av));
//		eRes.put(new Pair(0, 2), new ResourceEdge(0, 2, bw, 28, av));
//		eRes.put(new Pair(0, 3), new ResourceEdge(0, 3, bw, 43, av));
//
//		eRes.put(new Pair(1, 0), new ResourceEdge(1, 0, bw, 12, av));
//		eRes.put(new Pair(1, 2), new ResourceEdge(1, 2, bw, 23, av));
//		eRes.put(new Pair(1, 8), new ResourceEdge(1, 8, bw, 34, av));
//		eRes.put(new Pair(1, 9), new ResourceEdge(1, 9, bw, 43, av));
//
//		eRes.put(new Pair(2, 0),  new ResourceEdge(2, 0, bw, 28, av));
//		eRes.put(new Pair(2, 1),  new ResourceEdge(2, 1, bw, 23, av));
//		eRes.put(new Pair(2, 3),  new ResourceEdge(2, 3, bw, 12, av));
//		eRes.put(new Pair(2, 10), new ResourceEdge(2,10, bw, 62, av));
//
//		eRes.put(new Pair(3, 0),  new ResourceEdge(3, 0, bw, 43, av));
//		eRes.put(new Pair(3, 2),  new ResourceEdge(3, 2, bw, 12, av));
//		eRes.put(new Pair(3, 4),  new ResourceEdge(3, 4, bw, 15, av));
//
//		eRes.put(new Pair(4, 2),  new ResourceEdge(4, 2, bw, 20, av));
//		eRes.put(new Pair(4, 3),  new ResourceEdge(4, 3, bw, 14, av));
//		eRes.put(new Pair(4, 6),  new ResourceEdge(4, 6, bw, 23, av));
//		eRes.put(new Pair(4,15),  new ResourceEdge(4,15, bw, 49, av));
//		eRes.put(new Pair(4, 5),  new ResourceEdge(4, 5, bw, 7, av));
//
//		eRes.put(new Pair(5, 4),  new ResourceEdge(5,  4, bw, 7, av));
//		eRes.put(new Pair(5, 12), new ResourceEdge(5, 12, bw, 39, av));
//		eRes.put(new Pair(5, 16), new ResourceEdge(5, 16, bw, 43, av));
//
//		eRes.put(new Pair(6, 4),  new ResourceEdge(6, 4,  bw, 23, av));
//		eRes.put(new Pair(6, 8),  new ResourceEdge(6, 8,  bw, 16, av));
//		eRes.put(new Pair(6, 12), new ResourceEdge(6, 12, bw, 22, av));
//
//		eRes.put(new Pair(7, 8),  new ResourceEdge(7, 8,  bw, 12, av));
//		eRes.put(new Pair(7, 10), new ResourceEdge(7, 10, bw, 10, av));
//
//		eRes.put(new Pair(8, 1),  new ResourceEdge(8, 1, bw, 34, av));
//		eRes.put(new Pair(8, 6),  new ResourceEdge(8, 6, bw, 16, av));
//		eRes.put(new Pair(8, 7),  new ResourceEdge(8, 7, bw, 12, av));
//
//		eRes.put(new Pair(9, 1),   new ResourceEdge(9, 1,  bw, 43, av));
//		eRes.put(new Pair(9, 10),  new ResourceEdge(9, 10, bw, 9, av));
//		eRes.put(new Pair(9, 17),  new ResourceEdge(9, 17, bw, 11, av));
//
//		eRes.put(new Pair(10, 2),   new ResourceEdge(10, 2,  bw, 62, av));
//		eRes.put(new Pair(10, 7),   new ResourceEdge(10, 7,  bw, 10, av));
//		eRes.put(new Pair(10, 9),   new ResourceEdge(10, 9,  bw, 9, av));
//		eRes.put(new Pair(10, 11),  new ResourceEdge(10, 11, bw, 12, av));
//		eRes.put(new Pair(10, 13),  new ResourceEdge(10, 13, bw, 24, av));
//		eRes.put(new Pair(10, 18),  new ResourceEdge(10, 18, bw, 14, av));
//
//		eRes.put(new Pair(11, 10),  new ResourceEdge(11, 10, bw, 12, av));
//		eRes.put(new Pair(11, 14),  new ResourceEdge(11, 14, bw, 11, av));
//
//		eRes.put(new Pair(12, 5),   new ResourceEdge(12, 5,  bw, 39, av));
//		eRes.put(new Pair(12, 6),   new ResourceEdge(12, 6 , bw, 22, av));
//		eRes.put(new Pair(12, 13),  new ResourceEdge(12, 13, bw,  9, av));
//		eRes.put(new Pair(12, 16),  new ResourceEdge(12, 16, bw, 26, av));
//
//		eRes.put(new Pair(13, 8),   new ResourceEdge(13, 8,  bw, 16, av));
//		eRes.put(new Pair(13, 10),  new ResourceEdge(13, 10, bw, 24, av));
//		eRes.put(new Pair(13, 12),  new ResourceEdge(13, 12, bw,  9, av));
//		eRes.put(new Pair(13, 14),  new ResourceEdge(13, 14, bw,  8, av));
//		eRes.put(new Pair(13, 15),  new ResourceEdge(13, 15, bw, 17, av));
//		eRes.put(new Pair(13, 25),  new ResourceEdge(13, 25, bw, 32, av));
//
//		eRes.put(new Pair(14, 13),  new ResourceEdge(14, 13, bw,  8, av));
//		eRes.put(new Pair(14, 11),  new ResourceEdge(14, 11, bw, 11, av));
//		eRes.put(new Pair(14, 19),  new ResourceEdge(14, 19, bw, 22, av));
//
//		eRes.put(new Pair(15, 13),  new ResourceEdge(15, 13, bw, 17, av));
//		eRes.put(new Pair(15, 4),   new ResourceEdge(15,  4, bw, 49, av));
//		eRes.put(new Pair(15, 16),  new ResourceEdge(15, 16, bw,  7, av));
//		eRes.put(new Pair(15, 30),  new ResourceEdge(15, 30, bw, 26, av));
//
//		eRes.put(new Pair(16, 5),   new ResourceEdge(16, 5,  bw, 43, av));
//		eRes.put(new Pair(16, 12),  new ResourceEdge(16, 12, bw, 26, av));
//		eRes.put(new Pair(16, 31),  new ResourceEdge(16, 31, bw, 34, av));
//		eRes.put(new Pair(16, 15),  new ResourceEdge(16, 15, bw,  7, av));
//
//		eRes.put(new Pair(17,  9),  new ResourceEdge(17,  9, bw, 11, av));
//		eRes.put(new Pair(17, 18),  new ResourceEdge(17, 18, bw, 12, av));
//		eRes.put(new Pair(17, 20),  new ResourceEdge(17, 20, bw, 23, av));
//
//		eRes.put(new Pair(18, 10),  new ResourceEdge(18, 10, bw, 14, av));
//		eRes.put(new Pair(18, 17),  new ResourceEdge(18, 17, bw, 12, av));
//		eRes.put(new Pair(18, 23),  new ResourceEdge(18, 23, bw, 19, av));
//		eRes.put(new Pair(18, 19),  new ResourceEdge(18, 19, bw, 13, av));
//
//		eRes.put(new Pair(19, 18),  new ResourceEdge(19, 18, bw, 13, av));
//		eRes.put(new Pair(19, 14),  new ResourceEdge(19, 14, bw, 22, av));
//		eRes.put(new Pair(19, 25),  new ResourceEdge(19, 25, bw, 18, av));
//
//		eRes.put(new Pair(20, 17),  new ResourceEdge(20, 17, bw, 23, av));
//		eRes.put(new Pair(20, 21),  new ResourceEdge(20, 21, bw, 11, av));
//		eRes.put(new Pair(20, 22),  new ResourceEdge(20, 22, bw, 23, av));
//
//		eRes.put(new Pair(21, 23),  new ResourceEdge(21, 23, bw, 11, av));
//		eRes.put(new Pair(21, 20),  new ResourceEdge(21, 20, bw, 11, av));
//		eRes.put(new Pair(21, 22),  new ResourceEdge(21, 22, bw,  9, av));
//
//		eRes.put(new Pair(22, 24),  new ResourceEdge(22, 24, bw, 19, av));
//		eRes.put(new Pair(22, 20),  new ResourceEdge(22, 20, bw, 23, av));
//		eRes.put(new Pair(22, 21),  new ResourceEdge(22, 21, bw,  9, av));
//
//		eRes.put(new Pair(23, 18),  new ResourceEdge(23, 18, bw, 19, av));
//		eRes.put(new Pair(23, 21),  new ResourceEdge(23, 21, bw, 11, av));
//		eRes.put(new Pair(23, 24),  new ResourceEdge(23, 24, bw,  9, av));
//		eRes.put(new Pair(23, 26),  new ResourceEdge(23, 26, bw, 17, av));
//
//		eRes.put(new Pair(24, 22),  new ResourceEdge(24, 22, bw, 19, av));
//		eRes.put(new Pair(24, 23),  new ResourceEdge(24, 23, bw,  9, av));
//		eRes.put(new Pair(24, 26),  new ResourceEdge(24, 26, bw, 15, av));
//		eRes.put(new Pair(24, 28),  new ResourceEdge(24, 28, bw, 31, av));
//
//		eRes.put(new Pair(25, 13),  new ResourceEdge(25, 13, bw, 32, av));
//		eRes.put(new Pair(25, 19),  new ResourceEdge(25, 19, bw, 18, av));
//		eRes.put(new Pair(25, 26),  new ResourceEdge(25, 26, bw, 10, av));
//		eRes.put(new Pair(25, 30),  new ResourceEdge(25, 30, bw, 16, av));
//
//		eRes.put(new Pair(26, 23),  new ResourceEdge(26, 23, bw, 17, av));
//		eRes.put(new Pair(26, 24),  new ResourceEdge(26, 24, bw, 15, av));
//		eRes.put(new Pair(26, 25),  new ResourceEdge(26, 25, bw, 10, av));
//		eRes.put(new Pair(26, 27),  new ResourceEdge(26, 27, bw, 24, av));
//
//		eRes.put(new Pair(27, 26),  new ResourceEdge(27, 26, bw, 24, av));
//		eRes.put(new Pair(27, 28),  new ResourceEdge(27, 28, bw, 11, av));
//		eRes.put(new Pair(27, 29),  new ResourceEdge(27, 29, bw, 10, av));
//		eRes.put(new Pair(27, 30),  new ResourceEdge(27, 30, bw, 10, av));
//
//		eRes.put(new Pair(28, 24),  new ResourceEdge(28, 24, bw, 31, av));
//		eRes.put(new Pair(28, 27),  new ResourceEdge(28, 27, bw, 11, av));
//		eRes.put(new Pair(28, 29),  new ResourceEdge(28, 29, bw, 12, av));
//
//		eRes.put(new Pair(29, 27),  new ResourceEdge(29, 27, bw, 10, av));
//		eRes.put(new Pair(29, 28),  new ResourceEdge(29, 28, bw, 12, av));
//		eRes.put(new Pair(29, 31),  new ResourceEdge(29, 31, bw, 14, av));
//
//		eRes.put(new Pair(30, 15),  new ResourceEdge(30, 15, bw, 26, av));
//		eRes.put(new Pair(30, 25),  new ResourceEdge(30, 25, bw, 16, av));
//		eRes.put(new Pair(30, 27),  new ResourceEdge(30, 27, bw, 10, av));
//		eRes.put(new Pair(30, 31),  new ResourceEdge(30, 31, bw,  7, av));
//
//		eRes.put(new Pair(31, 30),  new ResourceEdge(31, 30, bw,  7, av));
//		eRes.put(new Pair(31, 29),  new ResourceEdge(31, 29, bw, 14, av));
//		eRes.put(new Pair(31, 16),  new ResourceEdge(31, 16, bw, 34, av));

		double[][] latency = computeAnsnetDistances();

		for (ResourceVertex u : vRes.values()){
			for (ResourceVertex v : vRes.values()){
				int uIndex = u.getIndex();
				int vIndex = v.getIndex();

				int latUV = (int) latency[uIndex][vIndex];
				double bigAUV = metricsProvider.getAvailability(uIndex, vIndex);
				double bwUV = metricsProvider.getBandwidth(uIndex, vIndex);
				double cost = metricsProvider.getCostPerUnitData(uIndex, vIndex);

				ResourceEdge rEdge = new ResourceEdge(uIndex, vIndex, bwUV, latUV, bigAUV, cost);
				eRes.put(new Pair(uIndex, vIndex), rEdge);
			}
		}


		/* 3. Creating object DspGraph */

		String graphName = "resources";
		resGraph = new ResourceGraph(graphName, vRes, eRes);

		return this;

	}


	private double[][] computeAnsnetDistances(){

		int resNodes = 32;
		FloydWarshall fw = new FloydWarshall(resNodes);

		/* Define direct links */
		fw.addEdge(0, 1, 12);
		fw.addEdge(0, 2, 28);
		fw.addEdge(0, 3, 43);

		fw.addEdge(1, 0, 12);
		fw.addEdge(1, 2, 23);
		fw.addEdge(1, 8, 34);
		fw.addEdge(1, 9, 43);

		fw.addEdge(2, 0, 28);
		fw.addEdge(2, 1, 23);
		fw.addEdge(2, 3, 12);
		fw.addEdge(2,10, 62);

		fw.addEdge(3, 0, 43);
		fw.addEdge(3, 2, 12);
		fw.addEdge(3, 4, 15);

		fw.addEdge(4, 2, 20);
		fw.addEdge(4, 3, 14);
		fw.addEdge(4, 6, 23);
		fw.addEdge(4,15, 49);
		fw.addEdge(4, 5, 7);

		fw.addEdge(5,  4, 7);
		fw.addEdge(5, 12, 39);
		fw.addEdge(5, 16, 43);

		fw.addEdge(6, 4, 23);
		fw.addEdge(6, 8, 16);
		fw.addEdge(6, 12, 22);

		fw.addEdge(7, 8, 12);
		fw.addEdge(7, 10, 10);

		fw.addEdge(8, 1, 34);
		fw.addEdge(8, 6, 16);
		fw.addEdge(8, 7, 12);

		fw.addEdge(9, 1, 43);
		fw.addEdge(9, 10, 9);
		fw.addEdge(9, 17, 11);

		fw.addEdge(10, 2, 62);
		fw.addEdge(10, 7, 10);
		fw.addEdge(10, 9, 9);
		fw.addEdge(10, 11, 12);
		fw.addEdge(10, 13, 24);
		fw.addEdge(10, 18, 14);

		fw.addEdge(11, 10, 12);
		fw.addEdge(11, 14, 11);

		fw.addEdge(12, 5, 39);
		fw.addEdge(12, 6 , 22);
		fw.addEdge(12, 13,  9);
		fw.addEdge(12, 16, 26);

		fw.addEdge(13, 8, 16);
		fw.addEdge(13, 10, 24);
		fw.addEdge(13, 12,  9);
		fw.addEdge(13, 14,  8);
		fw.addEdge(13, 15, 17);
		fw.addEdge(13, 25, 32);

		fw.addEdge(14, 13,  8);
		fw.addEdge(14, 11, 11);
		fw.addEdge(14, 19, 22);

		fw.addEdge(15, 13, 17);
		fw.addEdge(15,  4, 49);
		fw.addEdge(15, 16,  7);
		fw.addEdge(15, 30, 26);

		fw.addEdge(16, 5, 43);
		fw.addEdge(16, 12, 26);
		fw.addEdge(16, 31, 34);
		fw.addEdge(16, 15,  7);

		fw.addEdge(17,  9, 11);
		fw.addEdge(17, 18, 12);
		fw.addEdge(17, 20, 23);

		fw.addEdge(18, 10, 14);
		fw.addEdge(18, 17, 12);
		fw.addEdge(18, 23, 19);
		fw.addEdge(18, 19, 13);

		fw.addEdge(19, 18, 13);
		fw.addEdge(19, 14, 22);
		fw.addEdge(19, 25, 18);

		fw.addEdge(20, 17, 23);
		fw.addEdge(20, 21, 11);
		fw.addEdge(20, 22, 23);

		fw.addEdge(21, 23, 11);
		fw.addEdge(21, 20, 11);
		fw.addEdge(21, 22,  9);

		fw.addEdge(22, 24, 19);
		fw.addEdge(22, 20, 23);
		fw.addEdge(22, 21,  9);

		fw.addEdge(23, 18, 19);
		fw.addEdge(23, 21, 11);
		fw.addEdge(23, 24,  9);
		fw.addEdge(23, 26, 17);

		fw.addEdge(24, 22, 19);
		fw.addEdge(24, 23,  9);
		fw.addEdge(24, 26, 15);
		fw.addEdge(24, 28, 31);

		fw.addEdge(25, 13, 32);
		fw.addEdge(25, 19, 18);
		fw.addEdge(25, 26, 10);
		fw.addEdge(25, 30, 16);

		fw.addEdge(26, 23, 17);
		fw.addEdge(26, 24, 15);
		fw.addEdge(26, 25, 10);
		fw.addEdge(26, 27, 24);

		fw.addEdge(27, 26, 24);
		fw.addEdge(27, 28, 11);
		fw.addEdge(27, 29, 10);
		fw.addEdge(27, 30, 10);

		fw.addEdge(28, 24, 31);
		fw.addEdge(28, 27, 11);
		fw.addEdge(28, 29, 12);

		fw.addEdge(29, 27, 10);
		fw.addEdge(29, 28, 12);
		fw.addEdge(29, 31, 14);

		fw.addEdge(30, 15, 26);
		fw.addEdge(30, 25, 16);
		fw.addEdge(30, 27, 10);
		fw.addEdge(30, 31,  7);

		fw.addEdge(31, 30,  7);
		fw.addEdge(31, 29, 14);
		fw.addEdge(31, 16, 34);

		/* Compute shortest path matrix */

		return fw.floydWarshall();

	}




	private ResGraphBuilder createBrite(
			ResourceMetricsProvider metricsProvider,
			int numResources){

		String filename;
		int size;

		switch(numResources){
		case 16:
			filename = "brite_networks/brite2_16.brite";
			size = 16;
			break;
		case 36:
			filename = "brite_networks/brite2_36.brite";
			size = 36;
			break;
		case 49:
			filename = "brite_networks/brite2_49.brite";
			size = 49;
			break;
		case 64:
			filename = "brite_networks/brite2_64.brite";
			size = 64;
			break;
		case 81:
			filename = "brite_networks/brite2_81.brite";
			size = 81;
			break;
		case 100:
			filename = "brite_networks/brite2_100.brite";
			size = 100;
			break;
		case 121:
			filename = "brite_networks/brite2_121.brite";
			size = 121;
			break;
		case 144:
			filename = "brite_networks/brite2_144.brite";
			size = 144;
			break;
		case 169:
			filename = "brite_networks/brite2_169.brite";
			size = 169;
			break;
		case 196:
			filename = "brite_networks/brite2_196.brite";
			size = 196;
			break;
		case 410:
			filename = "brite_networks/brite3_4_10_40.brite";
			size = 40;
			break;
		case 420:
			filename = "brite_networks/brite3_4_20_80.brite";
			size = 80;
			break;
		case 430:
			filename = "brite_networks/brite3_4_30_120.brite";
			size = 120;
			break;
		case 440:
			filename = "brite_networks/brite3_4_40_160.brite";
			size = 160;
			break;
		case 450:
			filename = "brite_networks/brite3_4_50_200.brite";
			size = 200;
			break;
		default:
			filename = "brite_networks/brite2_36.brite";
			size = 36;
			break;
		}
//			case 36:
//				filename = "brite_networks/network36.brite";
//				size = 36;
//				break;
//			case 49:
//				filename = "brite_networks/network49.brite";
//				size = 49;
//				break;
//			case 64:
//				filename = "brite_networks/network64.brite";
//				size = 64;
//				break;
//			case 81:
//				filename = "brite_networks/network81.brite";
//				size = 81;
//				break;
//			case 100:
//				filename = "brite_networks/network100.brite";
//				size = 100;
//				break;
//			case 196:
//				filename = "brite_networks/network196.brite";
//				size = 196;
//				break;
//			default:
//				filename = "brite_networks/network36.brite";
//				size = 36;
//				break;
//			}

		System.out.println("Network with: " + size);


		Map<Integer, ResourceVertex> vRes = new HashMap<>();
		Map<Pair, ResourceEdge> eRes = new HashMap<>();


		BufferedReader br = null;

		int offset = -1;

		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(filename));

			skipUntil(br, "N");

			/* 1. Create ResourceVertex to represent nodes (supervisors, set of worker slots) */
			while ((sCurrentLine = br.readLine()) != null) {

				if (sCurrentLine.startsWith("E"))
					break;

				if (sCurrentLine.equals(""))
					continue;

				/*
					Field		Meaning
				  --------------------------------------------------
					NodeId		Unique id for each node
					xpos		x-axis coordinate in the plane
					ypos		y-axis coordinate in the plane
					indegree	Indegree of the node
					outdegree	Outdegree of the node
					ASid		id of the AS this node belongs to (if hierarchical)
					type		Type assigned to the node (e.g. router, AS)
				 */

				String[] elements = sCurrentLine.split("\t");

				int index = Integer.parseInt(elements[0]);

				if (offset == -1)
					offset = index;
				index = index - offset;

//				String name = elements[1];
//				int x = Integer.parseInt(elements[1]);
//				int y = Integer.parseInt(elements[2]);
				int cu 			= metricsProvider.getAvailableResources(index);
				double su 		= metricsProvider.getSpeedup(index);
				double bigAu	= metricsProvider.getAvailability(index);
				double instanceLaunchTime = metricsProvider.getInstanceLaunchTime(index);
				double uploadRateToDS = metricsProvider.getUploadRateToDS(index);
				double downloadRateFromDS = metricsProvider.getDownloadRateFromDS(index);
				double uploadRateToLocalDS = metricsProvider.getUploadRateToLocalDS(index);
				double downloadRateFromLocalDS = metricsProvider.getDownloadRateFromLocalDS(index);
				double dataStoreRTT = metricsProvider.getDataStoreRTT(index);
				
				ResourceVertex v = new ResourceVertex(index, cu, su, bigAu, instanceLaunchTime, uploadRateToDS, downloadRateFromDS, uploadRateToLocalDS, downloadRateFromLocalDS, dataStoreRTT);
				
				vRes.put(index, v);


			}

// 			skipUntil(br, "E");
			FloydWarshall fw = new FloydWarshall(size);

			/* Define direct links */
			while ((sCurrentLine = br.readLine()) != null) {

				if (sCurrentLine.equals(""))
					continue;

				String[] elements = sCurrentLine.split("\t");

				/*  Field		Meaning
				 	----------------------------------------
					EdgeId		Unique id for each edge
					from		node id of source
					to			node id of destination
					length		Euclidean length
					delay		propagation delay
					bandwidth	bandwidth (assigned by AssignBW method)
					ASfrom		if hierarchical topology, AS id of source node
					ASto		if hierarchical topology, AS id of destination node
					type		Type assigned to the edge by classification routine
				 */

				int from 	= 	Integer.parseInt(elements[1]) - offset;
				int to 		= 	Integer.parseInt(elements[2]) - offset;
				int length 	= 	(int) (1.0 * Double.parseDouble(elements[4]));

				fw.addEdge(from, to, length);

			}

			/* Compute shortest path matrix */
			double[][] latency = fw.floydWarshall();

			for (ResourceVertex u : vRes.values()){
				for (ResourceVertex v : vRes.values()){
					int uIndex = u.getIndex();
					int vIndex = v.getIndex();

					int latUV = (int) latency[uIndex][vIndex];
					double bigAUV = metricsProvider.getAvailability(uIndex, vIndex);
					double bwUV = metricsProvider.getBandwidth(uIndex, vIndex);
					double costUV = metricsProvider.getCostPerUnitData(uIndex, vIndex);

					ResourceEdge rEdge = new ResourceEdge(uIndex, vIndex, bwUV, latUV, bigAUV, costUV);
					eRes.put(new Pair(uIndex, vIndex), rEdge);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}


		/* 3. Creating object DspGraph */

		String graphName = "resources";
		resGraph = new ResourceGraph(graphName, vRes, eRes);

		return this;

	}


	private static void skipUntil(BufferedReader br, String letter) throws IOException{

		String current;
		while ((current = br.readLine()) != null) {
			if (current.startsWith(letter)){
				return;
			}
		}

	}


	private ResGraphBuilder testMultihop(){

		Map<Integer, ResourceVertex> vRes = new HashMap<>();
		Map<Pair, ResourceEdge> eRes = new HashMap<>();
//		/* A node with 0 resources is a node that cannot host a dsp operator
//		 * e.g., a network switch */
//		vRes.put(new Integer(0), new ResourceVertex(0, 1, 1, 1));
//		vRes.put(new Integer(1), new ResourceVertex(1, 0, 1, 1));
//		vRes.put(new Integer(2), new ResourceVertex(2, 0, 1, 1));
//		vRes.put(new Integer(3), new ResourceVertex(3, 1, 1, 1));
//		vRes.put(new Integer(4), new ResourceVertex(4, 1, 1, 1));
//		eRes.put(new Pair(0, 1), new ResourceEdge(0, 1, Double.MAX_VALUE, 10, 1, 1));
//		eRes.put(new Pair(1, 2), new ResourceEdge(1, 2, Double.MAX_VALUE, 10, 1, 1));
//		eRes.put(new Pair(2, 3), new ResourceEdge(2, 3, Double.MAX_VALUE, 10, 1, 1));
//		eRes.put(new Pair(3, 4), new ResourceEdge(3, 4, Double.MAX_VALUE, 10, 1, 1));
//		eRes.put(new Pair(0, 0), new ResourceEdge(0, 0, Double.MAX_VALUE, 0, 1, 1));
//		eRes.put(new Pair(1, 1), new ResourceEdge(1, 1, Double.MAX_VALUE, 0, 1, 1));
//		eRes.put(new Pair(2, 2), new ResourceEdge(2, 2, Double.MAX_VALUE, 0, 1, 1));
//		eRes.put(new Pair(3, 3), new ResourceEdge(3, 3, Double.MAX_VALUE, 0, 1, 1));
//		eRes.put(new Pair(4, 4), new ResourceEdge(4, 4, Double.MAX_VALUE, 0, 1, 1));


		// Second network
		/* A node with 0 resources is a node that cannot host a dsp operator
		 * e.g., a network switch */
		vRes.put(0, new ResourceVertex(0, 0, 1, 1));
		vRes.put(1, new ResourceVertex(1, 0, 1, 1));
		vRes.put(2, new ResourceVertex(2, 0, 1, 1));
		vRes.put(4, new ResourceVertex(4, 2, 1, 1));
		vRes.put(6, new ResourceVertex(6, 2, 1, 1));
		vRes.put(7, new ResourceVertex(7, 2, 1, 1));


//		vRes.put(new Integer(3), new ResourceVertex(3, 0, 1, 1));

		/* Link between switches */
		eRes.put(new Pair(0, 1), new ResourceEdge(0, 1, Double.MAX_VALUE, 6815, 1, 1));
		eRes.put(new Pair(1, 0), new ResourceEdge(1, 0, Double.MAX_VALUE, 6815, 1, 1));
		eRes.put(new Pair(2, 0), new ResourceEdge(2, 0, Double.MAX_VALUE, 23138, 1, 1));
		eRes.put(new Pair(0, 2), new ResourceEdge(0, 2, Double.MAX_VALUE, 23138, 1, 1));

		// Test multi-path between switches
//		eRes.put(new Pair(0, 3), new ResourceEdge(0, 3, Double.MAX_VALUE, 6815, 1, 1));
//		eRes.put(new Pair(3, 0), new ResourceEdge(3, 0, Double.MAX_VALUE, 6815, 1, 1));
//		eRes.put(new Pair(0, 1), new ResourceEdge(0, 1, Double.MAX_VALUE, 68, 1, 1));
//		eRes.put(new Pair(1, 0), new ResourceEdge(1, 0, Double.MAX_VALUE, 68000, 1, 1));
//		eRes.put(new Pair(3, 1), new ResourceEdge(3, 1, Double.MAX_VALUE, 6815, 1, 1));
//		eRes.put(new Pair(1, 3), new ResourceEdge(1, 3, Double.MAX_VALUE, 6815, 1, 1));
//		eRes.put(new Pair(2, 0), new ResourceEdge(2, 0, Double.MAX_VALUE, 23138, 1, 1));
//		eRes.put(new Pair(0, 2), new ResourceEdge(0, 2, Double.MAX_VALUE, 23138, 1, 1));

		/* Edge-links */
		eRes.put(new Pair(0, 4), new ResourceEdge(0, 4, Double.MAX_VALUE, 1000, 1, 1));
		eRes.put(new Pair(4, 0), new ResourceEdge(4, 0, Double.MAX_VALUE, 1000, 1, 1));
		eRes.put(new Pair(6, 2), new ResourceEdge(6, 2, Double.MAX_VALUE, 1000, 1, 1));
		eRes.put(new Pair(2, 6), new ResourceEdge(2, 6, Double.MAX_VALUE, 1000, 1, 1));
		eRes.put(new Pair(7, 1), new ResourceEdge(7, 1, Double.MAX_VALUE, 1000, 1, 1));
		eRes.put(new Pair(1, 7), new ResourceEdge(1, 7, Double.MAX_VALUE, 1000, 1, 1));

		// Add a second link between 4 and 7
		eRes.put(new Pair(4, 7), new ResourceEdge(4, 7, Double.MAX_VALUE, 100000, 1, 1));
		// Add a third link between 4 and 7
		vRes.put(9, new ResourceVertex(9, 0, 1, 1));
		eRes.put(new Pair(4, 8), new ResourceEdge(4, 8, Double.MAX_VALUE, 10000, 1, 1));
		eRes.put(new Pair(8, 7), new ResourceEdge(8, 7, Double.MAX_VALUE, 10000, 1, 1));

		/* Self-links */
		eRes.put(new Pair(4, 4), new ResourceEdge(4, 4, Double.MAX_VALUE, 0, 1, 1));
		eRes.put(new Pair(6, 6), new ResourceEdge(6, 6, Double.MAX_VALUE, 0, 1, 1));
		eRes.put(new Pair(7, 7), new ResourceEdge(7, 7, Double.MAX_VALUE, 0, 1, 1));

//		eRes.put(new Pair(0, 0), new ResourceEdge(0, 0, Double.MAX_VALUE, 0, 1, 1));
//		eRes.put(new Pair(1, 1), new ResourceEdge(1, 1, Double.MAX_VALUE, 0, 1, 1));
//		eRes.put(new Pair(2, 2), new ResourceEdge(2, 2, Double.MAX_VALUE, 0, 1, 1));

//		// /second network


//		/* A third network */
//		vRes.put(new Integer(0), new ResourceVertex(0, 1, 1, 1));
//		vRes.put(new Integer(1), new ResourceVertex(1, 1, 1, 1));
//		vRes.put(new Integer(2), new ResourceVertex(2, 1, 1, 1));
//		vRes.put(new Integer(3), new ResourceVertex(3, 1, 1, 1));
//		vRes.put(new Integer(4), new ResourceVertex(4, 0, 1, 1));
//		vRes.put(new Integer(5), new ResourceVertex(5, 0, 1, 1));
//		vRes.put(new Integer(6), new ResourceVertex(6, 0, 1, 1));
//		vRes.put(new Integer(7), new ResourceVertex(7, 0, 1, 1));
////		vRes.put(new Integer(8), new ResourceVertex(8, 1, 1, 1));
//
//		/* Link between switches */
//		eRes.put(new Pair(4, 5), new ResourceEdge(4, 5, Double.MAX_VALUE, 6815, 1, 1));
//		eRes.put(new Pair(5, 6), new ResourceEdge(5, 6, Double.MAX_VALUE, 6815, 1, 1));
//		eRes.put(new Pair(6, 7), new ResourceEdge(6, 7, Double.MAX_VALUE, 6815, 1, 1));
//		eRes.put(new Pair(7, 4), new ResourceEdge(7, 4, Double.MAX_VALUE, 6815, 1, 1));
//
//		/* Edge-links */
//		eRes.put(new Pair(0, 4), new ResourceEdge(0, 4, Double.MAX_VALUE, 1000, 1, 1));
//		eRes.put(new Pair(4, 0), new ResourceEdge(4, 0, Double.MAX_VALUE, 1000, 1, 1));
//		eRes.put(new Pair(1, 4), new ResourceEdge(1, 4, Double.MAX_VALUE, 1000, 1, 1));
//		eRes.put(new Pair(4, 1), new ResourceEdge(4, 1, Double.MAX_VALUE, 1000, 1, 1));
//		eRes.put(new Pair(2, 6), new ResourceEdge(2, 6, Double.MAX_VALUE, 1000, 1, 1));
//		eRes.put(new Pair(6, 2), new ResourceEdge(6, 2, Double.MAX_VALUE, 1000, 1, 1));
//		eRes.put(new Pair(3, 6), new ResourceEdge(3, 6, Double.MAX_VALUE, 1000, 1, 1));
//		eRes.put(new Pair(6, 3), new ResourceEdge(6, 3, Double.MAX_VALUE, 1000, 1, 1));
////		eRes.put(new Pair(8, 6), new ResourceEdge(8, 6, Double.MAX_VALUE, 1000, 1, 1));
////		eRes.put(new Pair(6, 8), new ResourceEdge(6, 8, Double.MAX_VALUE, 1000, 1, 1));


		String graphName = "resources";
		resGraph = new ResourceGraph(graphName, vRes, eRes);

		return this;

	}



	public void printGraph(boolean alsoEdges){

		System.out.println("RES Graph");
		System.out.println("* Vertices ");
		for(ResourceVertex v : resGraph.getVertices().values()){
			System.out.println("   " + v.toString());
		}

		System.out.println();
		System.out.println("* Edges: " + resGraph.getEdges().size());
		if (alsoEdges){
			for(ResourceEdge e : resGraph.getEdges().values()){
				System.out.println("   " + e.toString());
			}
		}

	}


	public int getMaxExecutorPerSlot() {
		return availableResourceMultiplier;
	}

}
