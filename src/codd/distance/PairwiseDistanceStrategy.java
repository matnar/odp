package codd.distance;

import codd.model.ResourceEdge;

public interface PairwiseDistanceStrategy {
	
	public double getDistance(ResourceEdge uvRes);
		
}
