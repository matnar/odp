package codd.distance;

import codd.ODDParameters;
import codd.model.ResourceEdge;
import codd.model.ResourceGraph;
import codd.model.ResourceVertex;


/**
 * This class computes distances between computing nodes relying by
 * combining and weighting the QoS metric as in the objective function
 * maximized by ODP. 
 * 
 */
public class ObjectiveFunctionDistanceStrategy implements PairwiseDistanceStrategy {
	
	private ResourceGraph graph;
	private ODDParameters params;
	private PointwiseNormalizationFactors normFact;
	

	public ObjectiveFunctionDistanceStrategy(ResourceGraph resourceGraph, ODDParameters params) {
	
		this.graph = resourceGraph;
		this.params = params;
		this.normFact = new PointwiseNormalizationFactors(this.graph);
		
	}
	

	/**
	 * Get distance along the edge uv
	 * 
	 * @param 	uvRes	Edge representing the network link among nodes u and v
	 * @return 	distance between node u and v
	 */
	public double getDistance(ResourceEdge uvRes){
		
		
		/* *******************************************************************************
		 * Objective:
		 * here we solve a minimization problem (instead of a maximization one).
		 * We rewrite the objective expression so that the resulting "distance" function
		 * is minimized when all the related terms are minimized/maximized w.r.t. their 
		 * weights.
		 * *******************************************************************************/	
		int v = uvRes.getTo();
		ResourceVertex vRes = graph.getVertices().get(v);
		// minimize response time
		double weightRespTime = params.getWeightRespTime() / (normFact.getRmax() - normFact.getRmin());
		double speedup1 = 1 / vRes.getSpeedup();
		double respTimeExpr = weightRespTime * (uvRes.getDelay() + speedup1 - normFact.getRmin());

		// maximize availability
		double weightAvailability = params.getWeightAvailability() / (Math.log(normFact.getAmax()) - Math.log(normFact.getAmin()));
		double availabilityExpr = weightAvailability * (Math.log(normFact.getAmax()) - Math.log(uvRes.getAvailability()) - Math.log(vRes.getAvailability()));

		// minimize network usage (assuming unitary the exchanged data rate)
		double weightNetMetric = params.getWeightNetMetric() / (normFact.getZmax() - normFact.getZmin());
		double netMetricExpr = weightNetMetric * (uvRes.getDelay() - normFact.getZmin());

		// minimize monetary cost (assuming unitary the exchanged data rate)
		// note that nodes do not add cost, because this model assume cost for resources not nodes
		double weightCost = params.getWeightCost() / (normFact.getCmax() - normFact.getCmin());
		double monetaryCostExpr = weightCost * (uvRes.getCostPerUnitData() - normFact.getCmin());

		return respTimeExpr + availabilityExpr + netMetricExpr + monetaryCostExpr;
		
		// Simple way: node distance
//		 return uvRes.getDelay();
	}


	// XXX: in questa versione, uso i pesi Rmin, Rmax etc della fx obj che sono 
	// 		pensati per tener conto del tempo di risposta di tutta l'applicazione
	// 		invece la distaza deve valutare solo il link ed il nodo destinazione
//	private double computeDistance(ResourceEdge uvRes){
//		
//		
//		/* *******************************************************************************
//		 * Objective:
//		 * here we solve a minimization problem (instead of a maximization one).
//		 * We rewrite the objective expression so that the resulting "distance" function
//		 * is minimized when all the related terms are minimized/maximized w.r.t. their 
//		 * weights.
//		 * *******************************************************************************/	
//		
//		int v = uvRes.getTo();
//		ResourceVertex vRes = graph.getVertices().get(v);
//		// minimize response time
//		double weightRespTime = params.getwR() / (params.getRmax() - params.getRmin());
//		double speedup1 = 1 / vRes.getSpeedup();
//		double respTimeExpr = weightRespTime * (uvRes.getDelay() + speedup1 - params.getRmin());
//
//		System.out.println("-resptime: "  + respTimeExpr);
//		
//		// maximize availability
//		double weightAvailability = params.getwA() / (Math.log(params.getAmax()) - Math.log(params.getAmin()));
//		double availabilityExpr = weightAvailability * (Math.log(params.getAmax()) - Math.log(uvRes.getAvailability()) - Math.log(vRes.getAvailability()));
//		
//		System.out.println("-avail: "  + availabilityExpr);
//		
//		// minimize network usage (assuming unitary the exchanged data rate)
//		double weightNetMetric = params.getwZ() / (params.getZmax() - params.getZmin());
//		double netMetricExpr = weightNetMetric * (uvRes.getDelay() - params.getZmin());
//
//		// minimize monetary cost (assuming unitary the exchanged data rate)
//		// note that nodes do not add cost, because this model assume cost for resources not nodes
//		double weightCost = params.getwC() / (params.getCmax() - params.getCmin());
//		double monetaryCostExpr = weightCost * (uvRes.getCostPerUnitData() - params.getCmin());
//
//		return respTimeExpr + availabilityExpr + netMetricExpr + monetaryCostExpr;
//		
//		// Simple way: node distance
////		 return uvRes.getDelay();
//	}

	

	private class PointwiseNormalizationFactors {
	
		double Rmax = Double.MIN_VALUE;
		double Rmin = Double.MAX_VALUE;
		double Cmax = Double.MIN_VALUE;
		double Cmin = Double.MAX_VALUE;
		double Amax = Double.MIN_VALUE;
		double Amin = Double.MAX_VALUE;
		double Zmax = Double.MIN_VALUE;
		double Zmin = Double.MAX_VALUE;
		private static final double EPSILON = 0.00001;
		
		PointwiseNormalizationFactors(ResourceGraph graph) {
		
			for(ResourceEdge uvRes : graph.getEdges().values()){
				ResourceVertex vRes = graph.getVertices().get(uvRes.getTo());
				
				double curRespTime 		= uvRes.getDelay() +  (1.0 / vRes.getSpeedup());
				double curAvailability 	= uvRes.getAvailability() * vRes.getAvailability();
				double curNetMetric 	= uvRes.getDelay();
				double curCost			= uvRes.getCostPerUnitData();
				
				if (curRespTime > Rmax)
					Rmax = curRespTime;
				if (curRespTime < Rmin)
					Rmin = curRespTime;
				if (Rmax - Rmin < EPSILON){
					Rmin = 0;
					Rmax = 1;
				}

				if (curAvailability > Amax)
					Amax = curAvailability;
				if (curAvailability < Amin)
					Amin = curAvailability;
				if (Amax - Amin < EPSILON){
					Amin = 0;
					Amax = 1;
				}

				if (curNetMetric > Zmax)
					Zmax = curNetMetric;
				if (curNetMetric < Zmin)
					Zmin = curNetMetric;
				if (Zmax - Zmin < EPSILON){
					Zmin = 0;
					Zmax = 1;
				}

				if (curCost > Cmax)
					Cmax = curCost;
				if (curCost < Cmin)
					Cmin = curCost;
				if (Cmax - Cmin < EPSILON){
					Cmax = 1;
					Cmin = 0;
				}

			}
			
		}

		public double getRmax() {
			return Rmax;
		}
		public double getRmin() {
			return Rmin;
		}
		public double getCmax() {
			return Cmax;
		}
		public double getCmin() {
			return Cmin;
		}
		public double getAmax() {
			return Amax;
		}
		public double getAmin() {
			return Amin;
		}
		public double getZmax() {
			return Zmax;
		}
		public double getZmin() {
			return Zmin;
		}
		
	}
}
