package codd;

import codd.model.*;
import codd.security.RequirementsForest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static codd.security.RequirementsBase.*;

public class SDPDspGraphBuilder extends DspGraphBuilder {

	private SDPDspGraphBuilder (DspGraph graph) {
		super(graph);
	}

	public static SDPDspGraphBuilder create() {
		Map<Integer, DspVertex> vDsp = new HashMap<Integer, DspVertex>();
		Map<Pair, DspEdge> eDsp = new HashMap<Pair, DspEdge>();
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();

		/* Source and sink are not replicated */

		final double mu0 = 1000;

		/* 1. Create DspVertex */
		DspVertex v = new DspVertex(0, "datasource");
		v.setServiceRate(mu0 * 10.0);
		vDsp.put(v.getIndex(), v);

		v = new DspVertex(1, "parser");
		v.setServiceRate(mu0 * 2.0);
		vDsp.put(v.getIndex(), v);

		v = new DspVertex(2, "filter");
		v.setServiceRate(mu0 * 3.0);
		vDsp.put(v.getIndex(), v);

		v = new DspVertex(3, "statsOp");
		v.setServiceRate(mu0 * 0.9);
		vDsp.put(v.getIndex(), v);

		v = new DspVertex(4, "alarmSender");
		v.setServiceRate(mu0 * 0.85);
		vDsp.put(v.getIndex(), v);

		v = new DspVertex(5, "alarmSink");
		v.setServiceRate(mu0 * 10.0);
		vDsp.put(v.getIndex(), v);

		v = new DspVertex(6, "statsSink");
		v.setServiceRate(mu0 * 10.0);
		vDsp.put(v.getIndex(), v);

		/* 2. Set node 0 as source */
		final double lambda0 = 850;
		sourcesIndex.add(new Integer(0));
		vDsp.get(0).setMM1ExecutionTime(lambda0);


		/* 3. Create DspEdges to represent link between executors */
		/* Compute execution time for each operator */
		DspEdge ij = new DspEdge(0, 1, lambda0, 3500);
		eDsp.put(new Pair(0, 1), ij);
		vDsp.get(1).setMM1ExecutionTime(ij.getLambda());

		ij = new DspEdge(1, 2, 0.99 * lambda0, 1500);
		eDsp.put(new Pair(1, 2), ij);
		vDsp.get(2).setMM1ExecutionTime(ij.getLambda());

		ij = new DspEdge(2, 3, 0.7 *  lambda0, 1500);
		eDsp.put(new Pair(2, 3), ij);
		vDsp.get(3).setMM1ExecutionTime(ij.getLambda());

		ij = new DspEdge(1, 4, 0.99 * lambda0, 1500);
		eDsp.put(new Pair(1, 4), ij);
		vDsp.get(4).setMM1ExecutionTime(ij.getLambda());

		ij = new DspEdge(4, 5, 0.05 * lambda0, 1500);
		eDsp.put(new Pair(4, 5), ij);
		vDsp.get(5).setMM1ExecutionTime(ij.getLambda());

		ij = new DspEdge(3, 6, 0.7 * lambda0, 1500);
		eDsp.put(new Pair(3, 6), ij);
		vDsp.get(6).setMM1ExecutionTime(ij.getLambda());


		/* 4. Compute Paths */
		ArrayList<DspPath> paths = computePaths(vDsp, eDsp, sourcesIndex);

		/* 5. Identify sinks */
		for (DspPath path : paths) {
			sinksIndex.add(path.getSink());
		}


		/* 7. Creating object DspGraph */
		String graphId = "fog-topology";
		DspGraph dspGraph = new DspGraph(graphId, vDsp, eDsp);
		dspGraph.setPaths(paths);

		for (Integer so : sourcesIndex)
			dspGraph.addSource(so);

		for (Integer si : sinksIndex)
			dspGraph.addSink(si);


		return new SDPDspGraphBuilder(dspGraph);

	}

	public RequirementsForest createRequirements()
	{
		RequirementsForest rf = new RequirementsForest();

		DspVertex opParser = getGraph().getVertices().get(1);
		DspVertex opAlarmSender = getGraph().getVertices().get(4);
		DspVertex sinkAlarm = getGraph().getVertices().get(5);

		rf.add(opParser, RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "VM", 1.0);
		rf.add(opParser, RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "Container", 1.0);
		rf.add(opAlarmSender, RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "VM", 1.0);
		rf.add(opAlarmSender, RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "Container", 1.0);
		//rf.add(sinkAlarm, RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "VM", 1.0);
		//rf.add(sinkAlarm, RC_PROCESS_ISOLATION, RO_RUNTIME_ENV, "Container", 1.0);

		rf.add(opParser, RC_PROCESS_ISOLATION, RO_MULTITENANCY, Boolean.FALSE, 1.0);
		rf.add(opParser, RC_PROCESS_ISOLATION, RO_MULTITENANCY, Boolean.TRUE, 0.9);
		rf.add(opAlarmSender, RC_PROCESS_ISOLATION, RO_MULTITENANCY, Boolean.FALSE, 1.0);

		DspEdge sourceParser = getGraph().getEdges().get(new Pair(0,1));
		DspEdge parserAlarm = getGraph().getEdges().get(new Pair(1,4));
		DspEdge alarmSink = getGraph().getEdges().get(new Pair(4,5));

		DspEdge criticalEdges[] = {sourceParser, parserAlarm, alarmSink};

		for (DspEdge e : criticalEdges) {
			rf.add(e, RC_NETWORK, RO_ENCRYPT_TRAFFIC, Boolean.TRUE, 1.0);
			rf.add(e, RC_NETWORK, RO_ENCRYPT_TRAFFIC, Boolean.FALSE, 0.0);
		}
		for (DspEdge e : getGraph().getEdges().values()) {
			boolean isCritical = false;
			for (DspEdge critical : criticalEdges) {
				if (critical.equals(e)) {
					isCritical = true;
					break;
				}
			}

			if (isCritical)
				continue;

			rf.add(e, RC_NETWORK, RO_ENCRYPT_TRAFFIC, Boolean.TRUE, 1.0);
			rf.add(e, RC_NETWORK, RO_ENCRYPT_TRAFFIC, Boolean.FALSE, 0.5);
		}

		return rf;
	}
}
