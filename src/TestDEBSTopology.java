import codd.ODDException;
import codd.ODDParameters;
import codd.heuristics.*;
import codd.model.*;
import codd.placement.ODDBandwidthModel;
import codd.placement.ODDBasicModel;
import codd.placement.ODDModel;
import codd.placement.ODDModel.MODE;

import java.util.*;


public class TestDEBSTopology {

	private enum ALGORITHM {OPTIMAL, RELAXATION, CLUSTER_BASED, HIERARCHICAL,
		GREEDY_FIRSTFIT, GREEDY_FIRSTFIT_NODISTFX, GREEDY_LOCALSEARCH, TABUSEARCH };

	private static int K_NEIGHBORS = 5;
	private static int GROUPING_FACTOR = 5;
	private static boolean useAllCandidate = false;

	
	public static void main(String[] args) {

		System.out.println("Optimal DSP Placement library");

		int NUM_RUNS = 1;

		/* ******* PARAMETERS ********** */
		srcRes = 0;
		snkRes = 0;

		K_NEIGHBORS = 5;
		GROUPING_FACTOR = 2;

		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(0);
		params.setWeightRespTime(1);
		params.setWeightCost(0);
		params.setWeightNetMetric(0);
		setParameters(params);
		params.setBwMode(ODDBandwidthModel.MODE.NETWORK_UTILIZATION);
		params.setTimeLimit(86400.0); // 24h

		/* ********* Sequential Application *********  */
		params.setRmax(3098.0);			params.setRmin(144.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(304000.0); 		params.setZmin(8000.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		DspGraph dspGraph = createDspGraph();
		ResourceGraph resGraph = createResGraph(60);

		for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
			try {

				run("debs2015gc-opt", dspGraph, resGraph, params, ALGORITHM.OPTIMAL);
				run("debs2015gc-hierodp" + GROUPING_FACTOR, dspGraph, resGraph, params, ALGORITHM.HIERARCHICAL);
				run("debs2015gc-odpps", dspGraph, resGraph, params, ALGORITHM.CLUSTER_BASED);
				run("debs2015gc-resodp" + K_NEIGHBORS, dspGraph, resGraph, params, ALGORITHM.RELAXATION);

				run("debs2015gc-greedyff-nod", dspGraph, resGraph, params, ALGORITHM.GREEDY_FIRSTFIT_NODISTFX);
				run("debs2015gc-greedyff", dspGraph, resGraph, params, ALGORITHM.GREEDY_FIRSTFIT);
				run("debs2015gc-localsearch", dspGraph, resGraph, params, ALGORITHM.GREEDY_LOCALSEARCH);
				run("debs2015gc-tabusearch", dspGraph, resGraph, params, ALGORITHM.TABUSEARCH);

			} catch (ODDException e) {
				e.printStackTrace();
			}

		}

	}

	private static void run(
			String idSeries,
			DspGraph dspGraph,
			ResourceGraph resGraph,
			ODDParameters params,
			ALGORITHM algorithm)
			throws ODDException {
		
//		System.out.println("Running " + idSeries + ": experiment: "
//				+ experiment + ", run: " + runNumber);
		
		OptimalSolution solution = null;

		if(ALGORITHM.CLUSTER_BASED.equals(algorithm)){

			ODPPrunedSpace placementAlgorithm = new ODPPrunedSpace(dspGraph, resGraph,
					params,
					srcRes, snkRes);

			solution = placementAlgorithm.solve();

		} else if (ALGORITHM.HIERARCHICAL.equals(algorithm)){
			
			HierarchicalODP placementAlgorithm = new HierarchicalODP(dspGraph, resGraph,
					params,
					srcRes, snkRes, GROUPING_FACTOR);

			solution = placementAlgorithm.solve();

		} else if (ALGORITHM.RELAXATION.equals(algorithm)){
			
			RESODP placementAlgorithm = new RESODP(dspGraph, resGraph,
					params,
					srcRes, snkRes, useAllCandidate, K_NEIGHBORS);

			solution = placementAlgorithm.solve();

		} else if (ALGORITHM.GREEDY_FIRSTFIT.equals(algorithm)){

			GreedyFirstfit placementAlgorithm = new GreedyFirstfit(dspGraph, resGraph,
					params,
					srcRes, snkRes);

			solution = placementAlgorithm.solve();

		} else if (ALGORITHM.GREEDY_FIRSTFIT_NODISTFX.equals(algorithm)){

			GreedyFirstfit placementAlgorithm = new GreedyFirstfit(dspGraph, resGraph,
					params,
					srcRes, snkRes);

			solution = placementAlgorithm.solveWithoutDistanceFunction();

		} else if (ALGORITHM.GREEDY_LOCALSEARCH.equals(algorithm)){

			GreedyLocalSearch placementAlgorithm = new GreedyLocalSearch(dspGraph, resGraph,
					params,
					srcRes, snkRes);

			solution = placementAlgorithm.solve();


		} else if (ALGORITHM.TABUSEARCH.equals(algorithm)){
			
			TabuSearch placementAlgorithm = new TabuSearch(dspGraph, resGraph,
					params,
					srcRes, snkRes);

			solution = placementAlgorithm.solve();

			
		} else if (ALGORITHM.OPTIMAL.equals(algorithm)) {
			
			ODDModel model = new ODDBasicModel(dspGraph, resGraph, params);

			model.compile();

			for (Integer src : dspGraph.getSources()){
				model.pin(src, srcRes);
			}
			for (Integer snk : dspGraph.getSinks()){
				model.pin(snk, snkRes);
			}

			solution = model.solve();

		} 

		if (solution == null)
			return;

		System.out.println(idSeries + ", " +
				"Res time: compilation, " + solution.getCompilationTime() + ", resolution, " + solution.getResolutionTime() + ", " +
				"Solution, " + solution.getOptR() + "");

	}

	private static ResourceGraph createResGraph(int num) {

		Map<Integer, ResourceVertex> vRes = new HashMap<>();
		Map<Pair, ResourceEdge> eRes = new HashMap<>();

		/* 1. Create ResourceVertex to represent nodes (supervisors, set of worker slots) */
		for(int index = 0; index < num; index++){
			int cu 			= 2;
			double su 		= 1.0;
			double bigAu	= 1.0;
			double instanceLaunchTime = 1;
			double uploadRateToDS = 1;
			double downloadRateFromDS = 1;
			double uploadRateToLocalDS = 1;
			double downloadRateFromLocalDS = 1;
			double dataStoreRTT = 1;
			ResourceVertex v =
					new ResourceVertex(index, cu, su, bigAu,
							instanceLaunchTime,
							uploadRateToDS, downloadRateFromDS, uploadRateToLocalDS, downloadRateFromLocalDS,
							dataStoreRTT);
			vRes.put(index, v);
		}


		/* 2. Create DspEdges to represent link between executors */
		Random r = new Random();
		for (ResourceVertex u : vRes.values()){
			for (ResourceVertex v : vRes.values()){
				int uIndex = u.getIndex();
				int vIndex = v.getIndex();

				double p = r.nextDouble();
				int d = 0;
				if (p < 0.31)
					d = 1;
				else if (p < 0.46)
					d = 33;
				else if (p < 0.49)
					d = 45;
				else if (p < 0.52)
					d = 65;
				else if (p < 0.74)
					d = 93;
				else if (p < 0.96)
					d = 125;
				else
					d = 150;

				int latUV = d;
				double bigAUV = 1.0;
				double bwUV = Double.MAX_VALUE;
				double costUV = 1.0;

				ResourceEdge rEdge = new ResourceEdge(uIndex, vIndex, bwUV, latUV, bigAUV, costUV);
				eRes.put(new Pair(uIndex, vIndex), rEdge);
			}
		}


		/* 3. Creating object DspGraph */
		String graphName = "resources";
		ResourceGraph resGraph = new ResourceGraph(graphName, vRes, eRes);

		return resGraph;
	}

	private static DspGraph createDspGraph() {

		Map<Integer, DspVertex> vDsp = new HashMap<Integer, DspVertex>();
		Map<Pair, DspEdge> eDsp = new HashMap<Pair, DspEdge>();
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();

		/* Source and sink are not replicated */

		/* 1. Create DspVertex */
		for (int i = 0; i < 8; i++) {
			int requiredResource = 1;
			double executionTime = 1.0;
			double cost = 1.0;
			double serviceRate = 1.0;
			double codeImageSize = 1.0;
			double internalStateImageSize = 1.0;

			DspVertex v = new DspVertex(i, "op_" + i, requiredResource, executionTime, cost, internalStateImageSize, codeImageSize);
			v.setServiceRate(serviceRate);
			vDsp.put(i, v);
		}

		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));

		eDsp.put(new Pair(0, 1), new DspEdge(0, 1, 5.0));
		eDsp.put(new Pair(1, 2), new DspEdge(1, 2, 5.0));
		eDsp.put(new Pair(2, 3), new DspEdge(2, 3, 5.0));
		eDsp.put(new Pair(2, 4), new DspEdge(2, 4, 5.0));
		eDsp.put(new Pair(3, 5), new DspEdge(3, 5, 5.0));
		eDsp.put(new Pair(4, 5), new DspEdge(4, 5, 5.0));
		eDsp.put(new Pair(5, 6), new DspEdge(5, 6, 5.0));
		eDsp.put(new Pair(6, 7), new DspEdge(6, 7, 5.0));

		ArrayList<Integer> p1d = new ArrayList<>(Arrays.asList(0,1,2,3,5,6,7));
		ArrayList<Integer> p2d = new ArrayList<>(Arrays.asList(0,1,2,4,5,6,7));
		DspPath p1 = new DspPath(p1d);
		DspPath p2 = new DspPath(p2d);
		ArrayList<DspPath> paths = new ArrayList<>();
		paths.add(p1);
		paths.add(p2);


		/* 5. Creating object DspGraph */
		String graphId = "multilevel-topology";
		DspGraph dspGraph = new DspGraph(graphId, vDsp, eDsp);
		dspGraph.setReconfigurationSyncTime(1.0);
		dspGraph.setPaths(paths);

		dspGraph.addSource(new Integer(0));
		dspGraph.addSink(new Integer(7));

		return dspGraph;

	}


	private static void setParameters(ODDParameters params){

		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		

		params.setAvgBytePerTupleMin(1500.0);
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);

//		params.setLambdaMin(0.014 / 5.0); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMin(100.0); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0); // 0.05; // 0.00005; // 0.15; // 0.0005;
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(2.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		// XXX: node availability is generated with a uniform distribution
		// between NodeAvailabilityMin and NodeAvailabilityMean
		params.setNodeAvailabilityMin(.97);
		params.setNodeAvailabilityMean(.99999);
		params.setNodeAvailabilityStdDev(0.0003);
		
		params.setServiceRate(1.0);
		
	}


	/* ******* PARAMETERS ********** */
	/* ODD Weights */
	private static double WEIGHT_AVAILABILITY 	= 0;
	private static double WEIGHT_RESP_TIME 		= 0;
	private static double WEIGHT_COST			= 0;
	private static double WEIGHT_NET_METRIC 	= 0;
	/* ******* .PARAMETERS ********** */
	
	private static int srcRes = 0;
	private static int snkRes = 0;
	
}
