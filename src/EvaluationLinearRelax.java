import java.io.IOException;

import codd.DspGraphBuilder;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.OptimalSolution;
import codd.placement.ODDBasicModel;
import codd.placement.ODDBasicRelaxedModel;
import codd.placement.ODDModel;
import codd.placement.ODDModel.MODE;
import codd.report.Report;
import codd.report.ReportException;


public class EvaluationLinearRelax {
	

	public static void main(String[] args) {


		System.out.println("Optimal DSP Placement library");
		
		/* Experiments on the linear relaxation of the ODP model */
//		experiment7();

		/* Experiments with the ANSNET resource model */
//		experiment8();
		experiment9();
			
	}
	
	

	/**
	 * Linear Relaxation on Y Variables. 
	 * Resolution time vs percent of occupied resources
	 */
	@SuppressWarnings("unused")
	private static void experiment7(){
		
		/**
		 * Relax Y Variables
		 */
		/* ******* PARAMETERS ********** */
		int NRES = 20;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.FULL_MESH;
		//double PERCENTAGE_OCCUPATION 	= 0.5;
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 10; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
//		int NUM_RUNS = 10;
		int NUM_EXPERIMENTS = 5;
		int NUM_RUNS = 1;
//		int NUM_EXPERIMENTS = 1;
		int STEP = 10;
		boolean relaxX = false;
		boolean relaxY = true;
		/* ******* .PARAMETERS ********** */
			
//		dspType 				= DspGraphBuilder.TYPE.SEQUENTIAL;
//		for (int experiment = 0; experiment < NUM_EXPERIMENTS; experiment++){
//			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
//				try {
//					compileRelaxedSolveAndReport("nDSPSeqYRelaxed", experiment, runNumber, NRES, resType, NDSP + (STEP * experiment), dspType, RESTRICTION_ON_VRES, relaxX, relaxY);
//				} catch (ODDException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		
//		dspType 				= DspGraphBuilder.TYPE.FAT;
//		for (int experiment = 0; experiment < NUM_EXPERIMENTS; experiment++){
//			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
//				try {
//					compileRelaxedSolveAndReport("nDSPFatYRelaxed", experiment, runNumber, NRES, resType, NDSP + (STEP * experiment), dspType, RESTRICTION_ON_VRES, relaxX, relaxY);
//				} catch (ODDException e) {
//					e.printStackTrace();
//				}
//			}
//		}
		
		
		NRES = 20; // 3
		NDSP = 20; // 5
		dspType 				= DspGraphBuilder.TYPE.SEQUENTIAL;
		resType 				= ResGraphBuilder.TYPE.ANSNET;
		try {
			compileRelaxedSolveAndReport("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, true, true);
		} catch (ODDException e) {
			e.printStackTrace();
		}

//		dspType 				= DspGraphBuilder.TYPE.FAT;
//		try {
//			compileRelaxedSolveAndReport("nDSPFatYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, true, true);
//		} catch (ODDException e) {
//			e.printStackTrace();
//		}

	}
	
	
	/**
	 * Basic Model on the Ansnet Network (32 nodes).
	 *  
	 * Preliminary Test: 20 DSP operators, 32 Res nodes.
	 * Comparison between ANSNET and a Fully Connected (MESH) network
	 */
	@SuppressWarnings("unused")
	private static void experiment8(){
		/* ******* PARAMETERS ********** */
		int NRES = 32;
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 20; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		
		System.out.println("\n\n\n************************************************************************************");
		System.out.println(" Full mesh vs Ansnet ");
		System.out.println("************************************************************************************");
		System.out.println("Sequential Topology \n\n\n");

		dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		try {
			compileSolveAndReport("FullyConnectedSeq", 1, 1, NRES, ResGraphBuilder.TYPE.FULL_MESH, NDSP, dspType, RESTRICTION_ON_VRES);
		} catch (ODDException e) {
			e.printStackTrace();
		}
		try {
			compileSolveAndReport("AnsnetSeq", 1, 1, NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, RESTRICTION_ON_VRES);
		} catch (ODDException e) {
			e.printStackTrace();
		}
		System.out.println("\n************************************************************************************");
		System.out.println("Fat Topology \n\n\n");

		dspType = DspGraphBuilder.TYPE.FAT;
		try {
			compileSolveAndReport("FullyConnectedFat", 1, 1, NRES, ResGraphBuilder.TYPE.FULL_MESH, NDSP, dspType, RESTRICTION_ON_VRES);
		} catch (ODDException e) {
			e.printStackTrace();
		}
		try {
			compileSolveAndReport("AnsnetFat", 1, 1, NRES, ResGraphBuilder.TYPE.FULL_MESH, NDSP, dspType, RESTRICTION_ON_VRES);
		} catch (ODDException e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Basic Model on the Ansnet Network (32 nodes).
	 *  
	 * Resolution time vs DSP application graph size (Sequential, Fat)
	 */
	private static void experiment9(){
	
		/* ******* PARAMETERS ********** */
		int NRES = 32;
		//double PERCENTAGE_OCCUPATION 	= 0.5;
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 10; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		int NUM_RUNS = 10;
		int NUM_EXPERIMENTS = 5;
		int STEP = 10;
		/* ******* .PARAMETERS ********** */
			
		dspType 				= DspGraphBuilder.TYPE.SEQUENTIAL;
		for (int experiment = 0; experiment < NUM_EXPERIMENTS; experiment++){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					compileSolveAndReport("nDSPAnsnetSeq", experiment, runNumber, NRES, ResGraphBuilder.TYPE.ANSNET, NDSP + (STEP * experiment), dspType, RESTRICTION_ON_VRES);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		
		dspType 				= DspGraphBuilder.TYPE.FAT;
		for (int experiment = 0; experiment < NUM_EXPERIMENTS; experiment++){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					compileSolveAndReport("nDSPAnsnetFat", experiment, runNumber, NRES, ResGraphBuilder.TYPE.ANSNET, NDSP + (STEP * experiment), dspType, RESTRICTION_ON_VRES);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		
		dspType 				= DspGraphBuilder.TYPE.SEQUENTIAL;
		for (int experiment = 0; experiment < NUM_EXPERIMENTS; experiment++){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					compileSolveAndReport("nDSPFullConnectSeq", experiment, runNumber, NRES, ResGraphBuilder.TYPE.FULL_MESH, NDSP + (STEP * experiment), dspType, RESTRICTION_ON_VRES);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		
		dspType 				= DspGraphBuilder.TYPE.FAT;
		for (int experiment = 0; experiment < NUM_EXPERIMENTS; experiment++){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					compileSolveAndReport("nDSPFullConnectFat", experiment, runNumber, NRES, ResGraphBuilder.TYPE.FULL_MESH, NDSP + (STEP * experiment), dspType, RESTRICTION_ON_VRES);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		
	}


	

	
	
	private static void compileSolveAndReport(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType, 
			int NDSP, DspGraphBuilder.TYPE dspType, 
			double RESTRICTION_ON_VRES
			) throws ODDException {
		
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);
		
		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);

		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, resType, NRES);
//		rbuilder.printGraph(false);

		
		DspGraphBuilder gbuilder = new DspGraphBuilder();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		gbuilder.create(amp, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			gbuilder.restrictPlacement(rbuilder.getGraph(), RESTRICTION_ON_VRES);
//		gbuilder.printGraph();
		
		
		ODDModel model = new ODDBasicModel(gbuilder.getGraph(), rbuilder.getGraph(), params);

		
//		ODDParameters params = new ODDParameters(gbuilder.getGraph(), rbuilder.getGraph(), MODE.BANDWIDTH, ODDBandwidthModel.MODE.INTERNODE_TRAFFIC);
//		params.setwA(0);
//		params.setwR(0);
//		params.setwZ(1.0);
//		ODDModel model = new ODDBandwidthModel(gbuilder.getGraph(), rbuilder.getGraph(), params);

// 		params.setGap(0.1);
//		model.compile();	
		OptimalSolution solution = model.solve();
		
		Report report = new Report(""+experiment, "output");
		try {
			report.write(idSeries, runNumber, model, gbuilder, amp, rbuilder, rmp, RESTRICTION_ON_VRES, params, solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}

	
	private static void compileRelaxedSolveAndReport(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType,
			int NDSP, DspGraphBuilder.TYPE dspType, 
			double RESTRICTION_ON_VRES,
			boolean relaxX, boolean relaxY
			) throws ODDException{
		
		System.out.println("Running " + idSeries + ": experiment: " + experiment + ", run: " + runNumber);

		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);

		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, resType, NRES);
//		rbuilder.printGraph(false);

		
		DspGraphBuilder gbuilder = new DspGraphBuilder();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		gbuilder.create(amp, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			gbuilder.restrictPlacement(rbuilder.getGraph(), RESTRICTION_ON_VRES);
//		gbuilder.printGraph();
		
		ODDModel model = new ODDBasicRelaxedModel(gbuilder.getGraph(), rbuilder.getGraph(), params, relaxX, relaxY);

		
//		ODDParameters params = new ODDParameters(gbuilder.getGraph(), rbuilder.getGraph(), MODE.BANDWIDTH, ODDBandwidthModel.MODE.INTERNODE_TRAFFIC);
//		params.setwA(0);
//		params.setwR(0);
//		params.setwZ(1.0);
//		ODDModel model = new ODDBandwidthModel(gbuilder.getGraph(), rbuilder.getGraph(), params);

// 		params.setGap(0.1);
//		model.compile();	
		OptimalSolution solution = model.solve();
		
		
		Report report = new Report(""+experiment, "output");
		try {
			report.write(idSeries, runNumber, model, gbuilder, amp, rbuilder, rmp, RESTRICTION_ON_VRES, params, solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}

	private static void setParameters(ODDParameters params){
		
		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		

		params.setAvgBytePerTupleMin(1500.0);
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);

		params.setLambdaMin(0.014 / 5.0); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0); // 0.05; // 0.00005; // 0.15; // 0.0005;
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(2.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		params.setNodeAvailabilityMin(.85);
		params.setNodeAvailabilityMean(.95);
		params.setNodeAvailabilityStdDev(0.03);
		
		params.setServiceRate(1.0); // NOT_USED
		
	}


	/* ******* PARAMETERS ********** */
	/* ODD Weights */
	private static final double WEIGHT_AVAILABILITY = 0;
	private static final double WEIGHT_RESP_TIME = 1;
	private static final double WEIGHT_COST = 0;
	private static final double WEIGHT_NET_METRIC = 0;
	/* ******* .PARAMETERS ********** */
	
}
