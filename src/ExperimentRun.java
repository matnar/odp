import codd.DspGraphBuilder;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.ResGraphBuilder.TYPE;

public class ExperimentRun 
{
	public String idSeries;
	public int experiment;
	public int runNumber;
	public int nRes;
	public ResGraphBuilder.TYPE resType;
	public int nDsp;
	public DspGraphBuilder.TYPE dspType;
	public int srcRes;
	public int snkRes;
	
	/* ODD Parameters */
	public ODDParameters params;

	
	
	public ExperimentRun(String idSeries, int experiment, int runNumber, int nRes, TYPE resType, int nDsp,
			codd.DspGraphBuilder.TYPE dspType, int srcRes, int snkRes, ODDParameters params) {
		this.idSeries = idSeries;
		this.experiment = experiment;
		this.runNumber = runNumber;
		this.nRes = nRes;
		this.resType = resType;
		this.nDsp = nDsp;
		this.dspType = dspType;
		this.srcRes = srcRes;
		this.snkRes = snkRes;
		this.params = params;
	}

	
}
