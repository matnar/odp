import java.io.IOException;

import codd.DspGraphBuilder;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.heuristics.RESODP;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.OptimalSolution;
import codd.placement.ODDBasicModel;
import codd.placement.ODDModel;
import codd.placement.ODDModel.MODE;
import codd.report.Report;
import codd.report.ReportException;


public class EvaluationSimpleHeuristic {

	/* ADDITIONAL PARAMETERS */
	static int SOURCE_PINNED_ON_RES = 0;
	static int SINK_PINNED_ON_RES = 1;
	static int K_NEIGHBORS = 3;
		
	public static void main(String[] args) {

		/* Simple Heuristic: relax, expand, solve */
		/* Evaluation of the Expansion phase */
//		experiment11();
		
		/* Execution on BRITE-generated networks */
		experiment12();

	}

	


	/**
	 * Simple Heuristic: relax, expand, solve
	 *  
	 * Preliminary Test: BRITE-generated network 
	 * 
	 * We pre-generated BRITE networks with 36, 49, 64, 81, 100, 196 nodes
	 */
	private static void experiment12(){
		
		// data stored in exp 112
		
		/**
		 * Relax Y Variables
		 */
		/* ******* PARAMETERS ********** */
		int NRES = 20;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.FULL_MESH;
		//double PERCENTAGE_OCCUPATION 	= 0.5;
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 10; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		/* ******* .PARAMETERS ********** */
		
		NRES = 32; // 20; // 3
		NDSP = 30; // 5
		dspType 				= DspGraphBuilder.TYPE.SEQUENTIAL;
		resType 				= ResGraphBuilder.TYPE.BRITE;
		
		try {
			
			// Model: resultType = TYPE.UNIFORMLY_RANDOM;			
			
			NRES = 36;
//			compileSolveAndReport("nDSP36BriteILP", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			// XXX: valutare con probabilita' in base al valore di x; 
//			K_NEIGHBORS = 1;
//			solveRelaxedExtendAndSolveODP("nDSP36BriteRelaxed+Unif+Prob1kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			K_NEIGHBORS = 5;
			dspType = DspGraphBuilder.TYPE.MULTILEVEL;
			NDSP = 13;
			solveRelaxedExtendAndSolveODP("nDSP36BriteRelaxed+Unif+Prob5kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 9;
//			solveRelaxedExtendAndSolveODP("nDSP36BriteRelaxed+Unif+Prob9kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);

//			NRES = 49;
//			compileSolveAndReport("nDSP49BriteILP", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			// XXX: valutare con probabilita' in base al valore di x; 
//			K_NEIGHBORS = 1;
//			solveRelaxedExtendAndSolveODP("nDSP49BriteRelaxed+Unif+Prob1kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 5;
//			solveRelaxedExtendAndSolveODP("nDSP49BriteRelaxed+Unif+Prob5kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 9;
//			solveRelaxedExtendAndSolveODP("nDSP49BriteRelaxed+Unif+Prob9kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//
//			NRES = 64;
//			compileSolveAndReport("nDSP64BriteILP", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			// XXX: valutare con probabilita' in base al valore di x; 
//			K_NEIGHBORS = 1;
//			solveRelaxedExtendAndSolveODP("nDSP64BriteRelaxed+Unif+Prob1kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 5;
//			solveRelaxedExtendAndSolveODP("nDSP64BriteRelaxed+Unif+Prob5kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 9;
//			solveRelaxedExtendAndSolveODP("nDSP64BriteRelaxed+Unif+Prob9kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//
//			NRES = 81;
//			compileSolveAndReport("nDSP81BriteILP", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			// XXX: valutare con probabilita' in base al valore di x; 
//			K_NEIGHBORS = 1;
//			solveRelaxedExtendAndSolveODP("nDSP81BriteRelaxed+Unif+Prob1kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 5;
//			solveRelaxedExtendAndSolveODP("nDSP81BriteRelaxed+Unif+Prob5kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 9;
//			solveRelaxedExtendAndSolveODP("nDSP81BriteRelaxed+Unif+Prob9kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//
//			NRES = 100;
//			compileSolveAndReport("nDSP100BriteILP", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			// XXX: valutare con probabilita' in base al valore di x; 
//			K_NEIGHBORS = 1;
//			solveRelaxedExtendAndSolveODP("nDSP100BriteRelaxed+Unif+Prob1kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 5;
//			solveRelaxedExtendAndSolveODP("nDSP100BriteRelaxed+Unif+Prob5kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 9;
//			solveRelaxedExtendAndSolveODP("nDSP100BriteRelaxed+Unif+Prob9kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//
//
//			NRES = 196;
//			 compileSolveAndReport("nDSP196BriteILP", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			// XXX: valutare con probabilita' in base al valore di x; 
//			K_NEIGHBORS = 1;
//			solveRelaxedExtendAndSolveODP("nDSP196BriteRelaxed+Unif+Prob1kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 5;
//			solveRelaxedExtendAndSolveODP("nDSP196BriteRelaxed+Unif+Prob5kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
//			K_NEIGHBORS = 9;
//			solveRelaxedExtendAndSolveODP("nDSP196BriteRelaxed+Unif+Prob9kN", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);

			
		} catch (ODDException e) {
			e.printStackTrace();
		}
		
	}
	

	@SuppressWarnings("unused")
	/**
	 * Simple Heuristic: relax, expand, solve
	 *  
	 * Preliminary Test: 30 DSP operators, 32 Res nodes, ANSNET.
	 * Evaluation of the Expansion phase: 
	 * 	- candidate selection 		(hard-coded): model.setMultipleToSingleCandidateMode
	 * 	- expansion candidate set 	(hard-coded): useAllCandidate
	 * 	- expansion # neighbors   	(param)	  	: K_NEIGHBORS
	 */
	private static void experiment11(){
		/**
		 * Relax Y Variables
		 */
		/* ******* PARAMETERS ********** */
		int NRES = 20;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.FULL_MESH;
		//double PERCENTAGE_OCCUPATION 	= 0.5;
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 10; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		/* ******* .PARAMETERS ********** */
		
		NRES = 32; // 20; // 3
		NDSP = 30; // 5
		dspType 				= DspGraphBuilder.TYPE.SEQUENTIAL;
		resType 				= ResGraphBuilder.TYPE.ANSNET;
		
		try {
			
//			compileSolveAndReport("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			
			// XXX: valutare con probabilita' in base al valore di x; 
			K_NEIGHBORS = 0;
			solveRelaxedExtendAndSolveODP("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			K_NEIGHBORS = 1;
			solveRelaxedExtendAndSolveODP("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			K_NEIGHBORS = 2;
			solveRelaxedExtendAndSolveODP("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			K_NEIGHBORS = 3;
			solveRelaxedExtendAndSolveODP("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			K_NEIGHBORS = 4;
			solveRelaxedExtendAndSolveODP("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			K_NEIGHBORS = 5;
			solveRelaxedExtendAndSolveODP("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			K_NEIGHBORS = 6;
			solveRelaxedExtendAndSolveODP("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			K_NEIGHBORS = 7;
			solveRelaxedExtendAndSolveODP("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			K_NEIGHBORS = 8;
			solveRelaxedExtendAndSolveODP("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
			K_NEIGHBORS = 9;
			solveRelaxedExtendAndSolveODP("nDSPSeqYRelaxed", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);

		} catch (ODDException e) {
			e.printStackTrace();
		}

	}
	
	
	/**
	 * Simple Heuristic based on Linear Relaxation of ODP.
	 * 
	 * This heuristic is made of three steps.  
	 * 1. We solve a relaxed version of ODP (with pinned operators);
	 * 		Multiple candidate or a single candidate can be 
	 * 		identified for each operator;
	 * 2. We expand the set of candidate nodes by finding the K-Nearest 
	 * 	  Neighbors using a distance function (in a deterministic
	 * 	  or probabilistic mode);
	 * 3. We solve the ILP of ODP (with pinned operators) where we
	 * 	  restrict the placement of each operator to the set of candidate
	 * 	  nodes
	 */
	private static void solveRelaxedExtendAndSolveODP(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType,
			int NDSP, DspGraphBuilder.TYPE dspType, 
			double RESTRICTION_ON_VRES
			) throws ODDException{
		
		System.out.println("Running " + idSeries + ": experiment: " + experiment + ", run: " + runNumber);
		
		boolean useAllCandidate = false;
		
		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightNetMetric(0);
		params.setWeightCost(0);
		setParameters(params);
		
		RESODP linearRelaxODD = new RESODP(NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES,
				params, SOURCE_PINNED_ON_RES, SINK_PINNED_ON_RES, 
				useAllCandidate, K_NEIGHBORS);

		OptimalSolution solution = linearRelaxODD.solve();

		Report report = new Report(""+experiment, "output");
		try {
			report.write(idSeries, runNumber, linearRelaxODD.getModel(), 
					linearRelaxODD.getDspGraphBuilder(), linearRelaxODD.getApplicationMetricProvider(), 
					linearRelaxODD.getResGraphBuilder(), linearRelaxODD.getResourceMetricProvider(), 
					RESTRICTION_ON_VRES, params, solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
}
	
	
	/**
	 * Optimal DSP Placement
	 * 
	 * Data source and sink are pinned.
	 */
	
	@SuppressWarnings("unused")
	private static void compileSolveAndReport(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType, 
			int NDSP, DspGraphBuilder.TYPE dspType, 
			double RESTRICTION_ON_VRES
			) throws ODDException {
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);
		
		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(0);
		params.setWeightNetMetric(0);
		setParameters(params);

		
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, resType, NRES);
		
		DspGraphBuilder gbuilder = new DspGraphBuilder();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		gbuilder.create(amp, dspType, NDSP);
		if (RESTRICTION_ON_VRES != -1)
			gbuilder.restrictPlacement(rbuilder.getGraph(), RESTRICTION_ON_VRES);
		
		ODDModel model = new ODDBasicModel(gbuilder.getGraph(), rbuilder.getGraph(), params);

		model.compile();
		
		// XXX: pin operators 
		model.pin(0, SOURCE_PINNED_ON_RES);
		model.pin(NDSP - 1, SINK_PINNED_ON_RES);	
		
		
		OptimalSolution solution = model.solve();
		
		for (int i = 0; i < NDSP; i++){
			System.out.print(solution.getPlacement(i) + ", ");
		}
		
		Report report = new Report(""+experiment, "output");
		try {
			report.write(idSeries, runNumber, model, gbuilder, amp, rbuilder, rmp, RESTRICTION_ON_VRES, params, solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}

		System.out.println("\nOptimum: R = " + solution.getOptR());
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}

	
	private static void setParameters(ODDParameters params){
		/* Application Metrics Provider */
		params.setRespTimeMin(1.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		

		params.setAvgBytePerTupleMin(1500.0);
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);
		
		params.setLambdaMin(8.0);
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0.2);
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(4.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		params.setNodeAvailabilityMin(1.0);
		params.setNodeAvailabilityMean(0.0);
		params.setNodeAvailabilityStdDev(0.0);
		
		params.setServiceRate(1.0); // NOT_USED
	}

	
	/* ******* PARAMETERS ********** */
	private static final double WEIGHT_AVAILABILITY = 0.5;
	private static final double WEIGHT_RESP_TIME = 0.5;
	/* ******* .PARAMETERS ********** */
}
