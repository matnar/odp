import java.io.IOException;

import codd.DspGraphBuilder;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.ResVertexMultisetGenerator;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.OptimalSolution;
import codd.placement.ODDBandwidthModel;
import codd.placement.ODDModel;
import codd.placement.ODDModel.MODE;
import codd.placement.ODDReplicatedOperatorsModel;
import codd.report.Report;
import codd.report.ReportException;


public class EvaluationInfQ {
	

	public static void main(String[] args) {


		System.out.println("Optimal DSP Placement library");

//		experiment15();
//		experiment16();
		experiment17();
	}
	
	
	/** 
	 * Session of experiment for InfQ. ReplODP.
	 * 
	 * Evaluating R,C,A,Z with several weights configuration, when 
	 * src and sink are pinned on each pair of computing nodes. 
	 */
	@SuppressWarnings("unused")
	private static void experiment15(){
		
		int NRES = 15;
		int NDSP = 10; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;

		WEIGHT_RESP_TIME = 1;
		WEIGHT_COST = 0;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		try {
			/* Place src and sink on each pair of resources */
			for (int srcRes = 0; srcRes < NRES; srcRes++){
				for (int snkRes = 0; snkRes < NRES; snkRes++){
					if (srcRes == snkRes)
						continue;
					compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
							"_10dsp15res_src"+srcRes + "snk" + snkRes, 1, 1, 
							NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
				}
				System.gc();
			}
			System.gc();
		} catch (ODDException e) {
			e.printStackTrace();
		}


		WEIGHT_RESP_TIME = 0;
		WEIGHT_COST = 1;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		try {
			/* Place src and sink on each pair of resources */
			for (int srcRes = 0; srcRes < NRES; srcRes++){
				for (int snkRes = 0; snkRes < NRES; snkRes++){
					if (srcRes == snkRes)
						continue;
					compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
							"_10dsp15res_src"+srcRes + "snk" + snkRes, 1, 1, 
							NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
				}
				System.gc();

			}
			System.gc();

		} catch (ODDException e) {
			e.printStackTrace();
		}


		WEIGHT_RESP_TIME = 0;
		WEIGHT_COST = 0;
		WEIGHT_AVAILABILITY = 1;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		try {
			/* Place src and sink on each pair of resources */
			for (int srcRes = 0; srcRes < NRES; srcRes++){
				for (int snkRes = 0; snkRes < NRES; snkRes++){
					if (srcRes == snkRes)
						continue;
					compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
							"_10dsp15res_src"+srcRes + "snk" + snkRes, 1, 1, 
							NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
				}
				System.gc();

			}
			System.gc();

		} catch (ODDException e) {
			e.printStackTrace();
		}
		

		WEIGHT_RESP_TIME = 0;
		WEIGHT_COST = 0;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 1;
		System.out.println("\n\n\nReplODP: New session");
		try {
			/* Place src and sink on each pair of resources */
			for (int srcRes = 0; srcRes < NRES; srcRes++){
				for (int snkRes = 0; snkRes < NRES; snkRes++){
					if (srcRes == snkRes)
						continue;
					compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
							"_10dsp15res_src"+srcRes + "snk" + snkRes, 1, 1, 
							NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
				}
				System.gc();

			}
			System.gc();

		} catch (ODDException e) {
			e.printStackTrace();
		}

		WEIGHT_RESP_TIME = 0.25;
		WEIGHT_COST = 0.25;
		WEIGHT_AVAILABILITY = 0.25;
		WEIGHT_NET_METRIC = 0.25;
		System.out.println("\n\n\nReplODP: New session");

		try {
			/* Place src and sink on each pair of resources */
			for (int srcRes = 0; srcRes < NRES; srcRes++){
				for (int snkRes = 0; snkRes < NRES; snkRes++){
					if (srcRes == snkRes)
						continue;
					compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
							"_10dsp15res_src"+srcRes + "snk" + snkRes, 1, 1, 
							NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
				}
				System.gc();
			}
			System.gc();

		} catch (ODDException e) {
			e.printStackTrace();
		}

	}

	
	
	/** 
	 * Session of experiment for InfQ. ReplODP.
	 * 
	 * Evaluating R,C,A,Z with several weights configuration, 
	 * when src and sink are pinned on two predefined computing 
	 * nodes and the utilization imposed by the DSP operator increases.
	 * The utilization increment grows linearly: the bottleneck operators
	 * loads to the computing nodes from 10% to 90%.
	 */
	@SuppressWarnings("unused")
	private static void experiment16(){
		
		int NRES = 15;
		int NDSP = 10; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;

		int srcRes = 0;
		int snkRes = 11;
				
		WEIGHT_RESP_TIME = 1;
		WEIGHT_COST = 0;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate / 5.0 ) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}


		WEIGHT_RESP_TIME = 0;
		WEIGHT_COST = 1;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate / 5.0 ) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}


		WEIGHT_RESP_TIME = 0;
		WEIGHT_COST = 0;
		WEIGHT_AVAILABILITY = 1;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate / 5.0 ) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}
		

		WEIGHT_RESP_TIME = 0;
		WEIGHT_COST = 0;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 1;
		System.out.println("\n\n\nReplODP: New session");
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate / 5.0 ) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}

		WEIGHT_RESP_TIME = 0.25;
		WEIGHT_COST = 0.25;
		WEIGHT_AVAILABILITY = 0.25;
		WEIGHT_NET_METRIC = 0.25;
		System.out.println("\n\n\nReplODP: New session");
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate / 5.0 ) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}
	
	}


	/** 
	 * Session of experiment for InfQ. ReplODP. Multiset cardinality
	 * 
	 * Evaluating R,C with several configuration of weights wr and wc,
	 * when src and sink are pinned on each pair of computing nodes. 
	 * This experiment wants to evaluate the benefits of replication, 
	 * therefore there are two scenario, the former has the multiset cardinality
	 * of at most 2, whereas the latter has cardinality at most 1. 
	 */
	private static void experiment17(){
		
		int NRES = 15;
		int NDSP = 10; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;

		int srcRes = 0;
		int snkRes = 11;
		
		/* ***************************************************************************
		 * MULTISET CARDINALITY: 2
		 * - different combinations of weights 
		 * ***************************************************************************/
		// XXX: to avoid filling up all the memory, run a single set (multiset cardinality) per time
		
		WEIGHT_RESP_TIME = 0.8;
		WEIGHT_COST = 0.2;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		ResVertexMultisetGenerator.MULTISET_CARDINALITY = 2;
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin + "_k_" + ResVertexMultisetGenerator.MULTISET_CARDINALITY, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}
	
		
		WEIGHT_RESP_TIME = 0.5;
		WEIGHT_COST = 0.5;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		ResVertexMultisetGenerator.MULTISET_CARDINALITY = 2;
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin + "_k_" + ResVertexMultisetGenerator.MULTISET_CARDINALITY, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}
	
		
		WEIGHT_RESP_TIME = 0.2;
		WEIGHT_COST = 0.8;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		ResVertexMultisetGenerator.MULTISET_CARDINALITY = 2;
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin + "_k_" + ResVertexMultisetGenerator.MULTISET_CARDINALITY, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}
		
		
		
		/* ***************************************************************************
		 * MULTISET CARDINALITY: 1 
		 * - different combinations of weights 
		 * ***************************************************************************/
		// XXX: to avoid filling up all the memory, run a single set (multiset cardinality) per time
		
		WEIGHT_RESP_TIME = 0.8;
		WEIGHT_COST = 0.2;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		ResVertexMultisetGenerator.MULTISET_CARDINALITY = 1;
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin + "_k_" + ResVertexMultisetGenerator.MULTISET_CARDINALITY, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}
	
		
		WEIGHT_RESP_TIME = 0.5;
		WEIGHT_COST = 0.5;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		ResVertexMultisetGenerator.MULTISET_CARDINALITY = 1;
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin + "_k_" + ResVertexMultisetGenerator.MULTISET_CARDINALITY, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}
	
		
		WEIGHT_RESP_TIME = 0.2;
		WEIGHT_COST = 0.8;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_NET_METRIC = 0;
		System.out.println("\n\n\nReplODP: New session");
		ResVertexMultisetGenerator.MULTISET_CARDINALITY = 1;
		try {
			
			for (int utilization = 1; utilization < 10; utilization++){
				
				lambdaMin = (serviceRate) * 0.1 * utilization; 
				compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
						"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin + "_k_" + ResVertexMultisetGenerator.MULTISET_CARDINALITY, 1, 1, 
						NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
			}
			System.gc();
			
		} catch (ODDException e) {
			e.printStackTrace();
		}
		
	}

	
	
	


	
	private static void compileSolveAndReportReplODP(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType, 
			int NDSP, DspGraphBuilder.TYPE dspType, 
			int srcRes, int snkRes
			) throws ODDException {
		
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);
		
		ODDParameters params = new ODDParameters(MODE.BANDWIDTH, ODDBandwidthModel.MODE.INTERNODE_TRAFFIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);
		
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		rbuilder.create(rmp, resType, NRES);
//		rbuilder.printGraph(true);
		rbuilder.printGraph(false);

		
		DspGraphBuilder gbuilder = new DspGraphBuilder();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		gbuilder.create(amp, dspType, NDSP);
//		if (RESTRICTION_ON_VRES != -1)
//			gbuilder.restrictPlacement(rbuilder.getGraph(), RESTRICTION_ON_VRES);
//		gbuilder.restrictPlacement(rbuilder.getGraph(), 0.3);
		gbuilder.printGraph();
		
		ODDModel model = new ODDReplicatedOperatorsModel(gbuilder.getGraph(), rbuilder.getGraph(), params);

		model.compile();	
		
		model.pin(0, srcRes);
		model.pin(NDSP - 1, snkRes);
		
		OptimalSolution solution = model.solve();
		
		Report report = new Report(""+experiment, "output");
		try {
			double RESTR = -1.0;
			report.write(idSeries, runNumber, model, gbuilder, amp, rbuilder, rmp, RESTR, srcRes, snkRes, params, solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}
	
	
	private static void setParameters(ODDParameters params){

		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		

		params.setAvgBytePerTupleMin(1500.0); // this is byte/ms (i.e., KB/s)
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);

		params.setLambdaMin(lambdaMin); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0.05); // 0.05; // 0.00005; // 0.15; // 0.0005;
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(5.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		params.setNodeAvailabilityMin(.97);			// uniform distribution
		params.setNodeAvailabilityMean(.9999999);	// used as max
		params.setNodeAvailabilityStdDev(0.03);

		params.setServiceRate(serviceRate);			// 100.0; // 15.0; // 100.0;
		
	}
	
	/* ******* PARAMETERS ********** */
	/* ODD Weights */
	private static double WEIGHT_AVAILABILITY = 0;
	private static double WEIGHT_RESP_TIME = 0;
	private static double WEIGHT_COST = 1;
	private static double WEIGHT_NET_METRIC = 0;	
	/* ******* .PARAMETERS ********** */
	
	private static double lambdaMin	= 0.014 / 5.0; // 99.978; // 10.0; // 99.978; // 100.0;	
	private static double serviceRate = 0.02; // 100.0; // 15.0; // 100.0;

}
