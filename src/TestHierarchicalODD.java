import java.io.IOException;

import codd.DspGraphBuilder;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.heuristics.HierarchicalODP;
import codd.model.OptimalSolution;
import codd.placement.ODDModel.MODE;
import codd.report.Report;
import codd.report.ReportException;


public class TestHierarchicalODD {
	
	static int GROUPING_FACTOR = 5;

	public static void main(String[] args) {

		System.out.println("Optimal DSP Placement library");
		
		testClusterBasedODP();
			
	}


	private static void testClusterBasedODP(){
		/* ******* PARAMETERS ********** */
		int NRES = 60;
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 30; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.FULL_MESH;
		
		try {
			compileSolveAndReport("testODP", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES);
		} catch (ODDException e) {
			e.printStackTrace();
		}
	
	}
	
	private static void compileSolveAndReport(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType, 
			int NDSP, DspGraphBuilder.TYPE dspType, 
			double RESTRICTION_ON_VRES
			) throws ODDException {
		
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);
		
		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);
		
		
		// XXX: fix
		snkRes = NDSP - 1;
		snkRes = NDSP/9;
		
//		ODPPrunedSpace clusterBasedODD = new ODPPrunedSpace(NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,
//				srcRes, snkRes);

		HierarchicalODP clusterBasedODD = new HierarchicalODP(NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,
				srcRes, snkRes, GROUPING_FACTOR);

		OptimalSolution solution = clusterBasedODD.solve();
		
		Report report = new Report(""+experiment, "output");
		try {
			report.write(idSeries, runNumber, clusterBasedODD.getModel(), 
					clusterBasedODD.getDspGraphBuilder(), clusterBasedODD.getApplicationMetricProvider(), 
					clusterBasedODD.getResGraphBuilder(), clusterBasedODD.getResourceMetricProvider(), 
					RESTRICTION_ON_VRES, params, solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}


		
	private static void setParameters(ODDParameters params){

		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		

		params.setAvgBytePerTupleMin(1500.0);
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);

		params.setLambdaMin(0.014 / 5.0); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0); // 0.05; // 0.00005; // 0.15; // 0.0005;
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(2.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		// XXX: node availability is generated with a uniform distribution
		// between NodeAvailabilityMin and NodeAvailabilityMean
		params.setNodeAvailabilityMin(.97);
		params.setNodeAvailabilityMean(.99999);
		params.setNodeAvailabilityStdDev(0.0003);
		
		params.setServiceRate(1.0);
		
	}


	/* ******* PARAMETERS ********** */
	/* ODD Weights */
	private static double WEIGHT_AVAILABILITY = 0;
	private static double WEIGHT_RESP_TIME = 1;
	private static double WEIGHT_COST = 0;
	private static double WEIGHT_NET_METRIC = 0;
	/* ******* .PARAMETERS ********** */
	
	private static int srcRes = 0;
	private static int snkRes = 0;
	
}
