import codd.DspGraphBuilder;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.heuristics.*;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.OptimalSolution;
import codd.placement.ODDBandwidthModel;
import codd.placement.ODDBasicModel;
import codd.placement.ODDModel;
import codd.placement.ODDModel.MODE;
import codd.report.Report;
import codd.report.ReportException;

import java.io.IOException;


public class HeuristicComparison {

	private enum ALGORITHM {OPTIMAL, RELAXATION, CLUSTER_BASED, HIERARCHICAL,
		GREEDY_FIRSTFIT, GREEDY_FIRSTFIT_NODISTFX, GREEDY_LOCALSEARCH, TABUSEARCH };

	private static int K_NEIGHBORS = 5;
	private static int GROUPING_FACTOR = 5;
	private static boolean useAllCandidate = false;

	
	public static void main(String[] args) {

		System.out.println("Optimal DSP Placement library");
		
//		experiment18();
		
//		experiment19();

// 		experiment20();

//		fullScriptExperiment20();

		// check vari pinned operators.
//		experiment21();

//		fullScriptExperiment22();

//		fullScriptExperiment23();

		fullScriptExperiment24();

	}

	private static void fullScriptExperiment20(){

		System.out.println("availability");
		WEIGHT_RESP_TIME = 0;
		WEIGHT_AVAILABILITY = 1;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 0;
		experiment20();

		System.out.println("resptime");
		WEIGHT_RESP_TIME = 1;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 0;
		experiment20();

		System.out.println("netmetr");
		WEIGHT_RESP_TIME = 0;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 1;
		experiment20();

		System.out.println("a.r.n");
		WEIGHT_RESP_TIME = 0.33;
		WEIGHT_AVAILABILITY = 0.33;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 0.33;
		experiment20();

	}

	private static void fullScriptExperiment22(){

		System.out.println(" *** Experiment 22 *** ");

		System.out.println("availability");
		WEIGHT_RESP_TIME = 0;
		WEIGHT_AVAILABILITY = 1;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 0;
		experiment22();

		System.out.println("resptime");
		WEIGHT_RESP_TIME = 1;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 0;
		experiment22();

		System.out.println("netmetr");
		WEIGHT_RESP_TIME = 0;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 1;
		experiment22();

		System.out.println("a.r.n");
		WEIGHT_RESP_TIME = 0.33;
		WEIGHT_AVAILABILITY = 0.33;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 0.33;
		experiment22();

	}

	private static void fullScriptExperiment23(){

		System.out.println(" *** Experiment 23 *** ");

		System.out.println("availability");
		WEIGHT_RESP_TIME = 0;
		WEIGHT_AVAILABILITY = 1;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 0;
		experiment23();

		System.out.println("resptime");
		WEIGHT_RESP_TIME = 1;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 0;
		experiment23();

		System.out.println("netmetr");
		WEIGHT_RESP_TIME = 0;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 1;
		experiment23();

		System.out.println("a.r.n");
		WEIGHT_RESP_TIME = 0.33;
		WEIGHT_AVAILABILITY = 0.33;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 0.33;
		experiment23();

	}


	private static void fullScriptExperiment24(){

		System.out.println(" *** Experiment 24 *** ");

		System.out.println("resptime");
		WEIGHT_RESP_TIME = 1;
		WEIGHT_AVAILABILITY = 0;
		WEIGHT_COST = 0;
		WEIGHT_NET_METRIC = 0;
		experiment24();

	}

	/**
	 * Comparison of the following algorithms: 
	 * - ODP: Optimal DSP Placement
	 * 
	 * - HierarchicalODP: Hierarchical resolution of ODP. All computing nodes are grouped 
	 * 					in surrogate nodes (clusters), each with a cumulative capacity. 
	 * 					These surrogate nodes are interconnected to constitute a surrogate
	 * 					graph, where the surrogates nodes are interconnected with surrogate 
	 * 					edges (with cumulative/average QoS attributes of the underlying lower
	 * 					level edges); self-loops are considered as perfect links. 
	 * 					Once ODP is solved on a level, the surrogate graph is unfolded and ODP
	 * 					is solved on the lower level graph, till the real graph is found.
	 * 
	 * - Cluster-based ODP: Groups all the nodes in a hierarchical cluster and solves ODP on
	 * 					the minimum cluster that covers the data source and data sink; if
	 * 					a solution is not found, then the algorithm solves ODP on the cluster
	 * 					of higher level. 
	 * 
	 * - Simple Heuristic: relax ODP, expand the set of candidates with k-Neighbors, solve ODP on
	 * 					the candidates only.
	 *  
	 * Preliminary Test: BRITE-generated networks with 36, 49, 64, 81, 100 nodes.
	 * 
	 * Data source and sink are pinned on the same node (ID: 0).
	 */
	@SuppressWarnings("unused")
	private static void experiment18(){
		
		/* ******* PARAMETERS ********** */
// 		WEIGHT_RESP_TIME = 1;		
//		protected static final double DEFAULT_RMAX 	= 2810.0;
//		protected static final double DEFAULT_RMIN 	= 1230.0;

		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 30; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.BRITE;
		
		srcRes = 0;
		snkRes = 0;

		K_NEIGHBORS = 5;
		GROUPING_FACTOR = 5;

		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);
		params.setRmax(2810.0);
		params.setRmin(1230.0);
		params.setAmax(0.999999);
		params.setAmin(0.005);
		params.setZmax(1.0);
		params.setZmin(0.0);
		params.setCmax(25.0);
		params.setCmin(11.0);
		
		int[] nress = {36, 49, 64, 81, 100};
 		for(int NRES : nress){
			try {
				run("brite"+NRES+"-seqDSP30-sr0sn0-opt", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, false);
				run("brite"+NRES+"-seqDSP30-sr0sn0-hier" + GROUPING_FACTOR, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, false);
				run("brite"+NRES+"-seqDSP30-sr0sn0-clus", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, false);
				run("brite"+NRES+"-seqDSP30-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, false);
				run("brite"+NRES+"-seqDSP30-sr0sn0-greedyff", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, false);
				run("brite"+NRES+"-seqDSP30-sr0sn0-tabusearch", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, false);
			} catch (ODDException e) {
				e.printStackTrace();
			}
 		}


		dspType = DspGraphBuilder.TYPE.FAT;
		for(int NRES : nress){
			try {
				run("brite"+NRES+"-fatDSP30-sr0sn0-opt", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, false);
				run("brite"+NRES+"-fatDSP30-sr0sn0-hier" + GROUPING_FACTOR, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, false);
				run("brite"+NRES+"-fatDSP30-sr0sn0-clus", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, false);
				run("brite"+NRES+"-fatDSP30-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, false);
				run("brite"+NRES+"-fatDSP30-sr0sn0-greedyff", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, false);
				run("brite"+NRES+"-fatDSP30-sr0sn0-tabusearch", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, false);
			} catch (ODDException e) {
				e.printStackTrace();
			}
 		}

		dspType = DspGraphBuilder.TYPE.MULTILEVEL;
		for(int NRES : nress){
			try {
				run("brite"+NRES+"-multilevDSP30-sr0sn0-opt", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, false);
				run("brite"+NRES+"-multilevDSP30-sr0sn0-hier" + GROUPING_FACTOR, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, false);
				run("brite"+NRES+"-multilevDSP30-sr0sn0-clus", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, false);
				run("brite"+NRES+"-multilevDSP30-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, false);
				run("brite"+NRES+"-multilevDSP30-sr0sn0-greedyff", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, false);
				run("brite"+NRES+"-multilevDSP30-sr0sn0-tabusearch", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, false);
			} catch (ODDException e) {
				e.printStackTrace();
			}
 		}
//		dspType = DspGraphBuilder.TYPE.MULTILEVEL;
//		params.setGap(0.02);
//		for(int NRES : nress){
//			try {
//				run("brite"+NRES+"-multilevDSP30-sr0sn0-gap.02-opt", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL);
//				run("brite"+NRES+"-multilevDSP30-sr0sn0-gap.02-hier" + GROUPING_FACTOR, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL);
//				run("brite"+NRES+"-multilevDSP30-sr0sn0-gap.02-clus", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED);
//				run("brite"+NRES+"-multilevDSP30-sr0sn0-gap.02-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION);
//			} catch (ODDException e) {
//				e.printStackTrace();
//			}
// 		}

	}

	/**
	 * Comparison of the following algorithms: 
	 * - ODP: Optimal DSP Placement
	 * 
	 * - HierarchicalODP: Hierarchical resolution of ODP. All computing nodes are grouped 
	 * 					in surrogate nodes (clusters), each with a cumulative capacity. 
	 * 					These surrogate nodes are interconnected to constitute a surrogate
	 * 					graph, where the surrogates nodes are interconnected with surrogate 
	 * 					edges (with cumulative/average QoS attributes of the underlying lower
	 * 					level edges); self-loops are considered as perfect links. 
	 * 					Once ODP is solved on a level, the surrogate graph is unfolded and ODP
	 * 					is solved on the lower level graph, till the real graph is found.
	 * 
	 * - Cluster-based ODP: Groups all the nodes in a hierarchical cluster and solves ODP on
	 * 					the minimum cluster that covers the data source and data sink; if
	 * 					a solution is not found, then the algorithm solves ODP on the cluster
	 * 					of higher level. 
	 * 
	 * - Simple Heuristic: relax ODP, expand the set of candidates with k-Neighbors, solve ODP on
	 * 					the candidates only.
	 *  
	 * Preliminary Test: BRITE-generated networks with 36, 49, 64, 81, 100, 196 nodes.
	 * 
	 * This experiment uses brite2 networks.
	 * 
	 * Data source and sink are pinned on the same node (ID: 0).
	 */
	@SuppressWarnings("unused")
	private static void experiment19(){
		
		/* 
		 * Run1; min R
		 * Run2: max A
		 */
		
		/* ******* PARAMETERS ********** */
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 20; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.BRITE;
		
		srcRes = 0;
		snkRes = 0;

		K_NEIGHBORS = 5;
		GROUPING_FACTOR = 2;
		
		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);
		params.setRmax(2810.0);
		params.setRmin(1230.0);
		params.setAmax(0.999999);
		params.setAmin(0.005);
		params.setZmax(1.0);
		params.setZmin(0.0);
		params.setCmax(25.0);
		params.setCmin(11.0);

		int[] nress = {36, 49, 64, 81, 100};
 		for(int NRES : nress){
			try {
				run("brite"+NRES+"-seqDSP20-sr0sn0-opt", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, false);
				run("brite"+NRES+"-seqDSP20-sr0sn0-hier" + GROUPING_FACTOR, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, false);
				run("brite"+NRES+"-seqDSP20-sr0sn0-clus", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, false);
				run("brite"+NRES+"-seqDSP20-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, false);
				run("brite"+NRES+"-seqDSP20-sr0sn0-greedyff", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, false);
				run("brite"+NRES+"-seqDSP20-sr0sn0-tabusearch", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, false);
			} catch (ODDException e) {
				e.printStackTrace();
			}
 		}


		dspType = DspGraphBuilder.TYPE.FAT;
		for(int NRES : nress){
			try {
				run("brite"+NRES+"-fatDSP20-sr0sn0-opt", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, false);
				run("brite"+NRES+"-fatDSP20-sr0sn0-hier" + GROUPING_FACTOR, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, false);
				run("brite"+NRES+"-fatDSP20-sr0sn0-clus", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, false);
				run("brite"+NRES+"-fatDSP20-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, false);
				run("brite"+NRES+"-fatDSP20-sr0sn0-greedyff", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, false);
				run("brite"+NRES+"-fatDSP20-sr0sn0-tabusearch", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, false);
			} catch (ODDException e) {
				e.printStackTrace();
			}
 		}

		dspType = DspGraphBuilder.TYPE.MULTILEVEL;
		for(int NRES : nress){
			try {
				run("brite"+NRES+"-multilevDSP20-sr0sn0-opt", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, false);
				run("brite"+NRES+"-multilevDSP20-sr0sn0-hier" + GROUPING_FACTOR, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, false);
				run("brite"+NRES+"-multilevDSP20-sr0sn0-clus", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, false);
				run("brite"+NRES+"-multilevDSP20-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, false);
				run("brite"+NRES+"-multilevDSP20-sr0sn0-greedyff", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, false);
				run("brite"+NRES+"-multilevDSP20-sr0sn0-tabusearch", 1, 1, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, false);
			} catch (ODDException e) {
				e.printStackTrace();
			}
 		}

	}

	/**
	 * Comparison of the following algorithms: 
	 * - ODP: Optimal DSP Placement
	 *
	 * - HierarchicalODP: Hierarchical resolution of ODP. All computing nodes are grouped 
	 * 					in surrogate nodes (clusters), each with a cumulative capacity. 
	 * 					These surrogate nodes are interconnected to constitute a surrogate
	 * 					graph, where the surrogates nodes are interconnected with surrogate 
	 * 					edges (with cumulative/average QoS attributes of the underlying lower
	 * 					level edges); self-loops are considered as perfect links. 
	 * 					Once ODP is solved on a level, the surrogate graph is unfolded and ODP
	 * 					is solved on the lower level graph, till the real graph is found.
	 *
	 * - Cluster-based ODP: Groups all the nodes in a hierarchical cluster and solves ODP on
	 * 					the minimum cluster that covers the data source and data sink; if
	 * 					a solution is not found, then the algorithm solves ODP on the cluster
	 * 					of higher level. 
	 *
	 * - Simple Heuristic: relax ODP, expand the set of candidates with k-Neighbors, solve ODP on
	 * 					the candidates only.
	 *
	 * - GreedyLocalSearch First Fit
	 *
	 * - Tabu Search
	 *
	 * This experiment uses brite2 networks with 36, 49, 64, 81, 100 nodes.
	 *
	 * Network-related QoS Metrics. 
	 *
	 * Data source and sink are pinned on the same node (ID: 0).
	 */
	private static void experiment20(){
		
		/*
		 * Run1: min R; Num_Runs = 1 (from experiment19)
		 * Run2: max A; Num_Runs = 1 (from experiment19)
		 * Run3: min Z; Num_Runs = 1
		 * Run4: max A && min R,Z; weights = 0.33; Num_Runs = 5
		 */

		int NUM_RUNS = 5;
		
		/* ******* PARAMETERS ********** */
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 20;
		int[] NRES_SET = {36, 49, 64, 81, 100};
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.BRITE;

		srcRes = 0;
		snkRes = 0;

		K_NEIGHBORS = 5;
		GROUPING_FACTOR = 2;

		ODDParameters params = new ODDParameters(MODE.BANDWIDTH);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);
		params.setBwMode(ODDBandwidthModel.MODE.NETWORK_UTILIZATION);
		params.setTimeLimit(86400.0); // 24h 

		/* ********* Sequential Application *********  */
		params.setRmax(3098.0);			params.setRmin(144.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(304000.0); 		params.setZmin(8000.0);
		params.setCmax(25.0);			params.setCmin(11.0);

//		params.setZmax(1); 		params.setZmin(0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-seqDSP20-sr0sn0-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-hier" + GROUPING_FACTOR, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-clus", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-greedyff", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-tabusearch", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}

			}
		}
		/* ********* **********  **********  ********* */

		/* ********* Fat Application *********  */
		dspType = DspGraphBuilder.TYPE.FAT;

		params.setRmax(247.0);			params.setRmin(49.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(446400.0); 		params.setZmin(52000.0);
		params.setCmax(25.0);			params.setCmin(11.0);

//		params.setZmax(1); 		params.setZmin(0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-fatDSP20-sr0sn0-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-hier" + GROUPING_FACTOR, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-clus", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-greedyff", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-tabusearch", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		/* ********* **********  **********  ********* */

		/* ********* Multilevel Application *********  */
		dspType = DspGraphBuilder.TYPE.MULTILEVEL;

		params.setRmax(410.0);			params.setRmin(74.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(1409200.0); 		params.setZmin(132200.0);
		params.setCmax(25.0);			params.setCmin(11.0);

//		params.setZmax(1); 		params.setZmin(0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {

					run("brite"+NRES+"-multilevDSP20-sr0sn0-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-hier" + GROUPING_FACTOR, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-clus", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-greedyff", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-tabusearch", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		/* ********* **********  **********  ********* */

	}

	/**
	 * This experiment is a follow up of experiment 20.
	 * Comparison of the following algorithms:
	 *  - ODP with bounds on optimality gap 5% and timeout at 300s
	 *  - Greedy First Fit (differently from exp 0-20, this heur. does not perform local search)
	 *  - Greedy First Fit without using the distance function
	 *
	 * This experiment uses brite2 networks with 36, 49, 64, 81, 100 nodes.
	 * Network-related QoS Metrics.
	 * Data source and sink are pinned on the same node (ID: 0).
	 */
	private static void experiment22(){

		/*
		 * Run1: min R; Num_Runs = 1 (from experiment19)
		 * Run2: max A; Num_Runs = 1 (from experiment19)
		 * Run3: min Z; Num_Runs = 1
		 * Run4: max A && min R,Z; weights = 0.33; Num_Runs = 5
		 */

		int NUM_RUNS = 5;

		/* ******* PARAMETERS ********** */
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 20;
		int[] NRES_SET = {36, 49, 64, 81, 100};
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.BRITE;

		srcRes = 0;
		snkRes = 0;

		K_NEIGHBORS = 5;
		GROUPING_FACTOR = 2;

		ODDParameters params = new ODDParameters(MODE.BANDWIDTH);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);
		params.setBwMode(ODDBandwidthModel.MODE.NETWORK_UTILIZATION);
//		params.setTimeLimit(86400.0); // 24h

		/* lODP */
//		params.setGap(0.05);
//		params.setDefineOptimalGap(true);
		params.setTimeLimit(300.0); // 24h

		/* ********* Sequential Application *********  */
		params.setRmax(3098.0);			params.setRmin(144.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(304000.0); 		params.setZmin(8000.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-seqDSP20-sr0sn0-limited-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-real-first-fit", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-real-first-fit-nodistfx", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT_NODISTFX, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}

			}
		}
		/* ********* **********  **********  ********* */

		/* ********* Fat Application *********  */
		dspType = DspGraphBuilder.TYPE.FAT;

		params.setRmax(247.0);			params.setRmin(49.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(446400.0); 		params.setZmin(52000.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-fatDSP20-sr0sn0-limited-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-real-first-fit", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-real-first-fit-nodistfx", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT_NODISTFX, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		/* ********* **********  **********  ********* */

		/* ********* Multilevel Application *********  */
		dspType = DspGraphBuilder.TYPE.MULTILEVEL;

		params.setRmax(410.0);			params.setRmin(74.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(1409200.0); 		params.setZmin(132200.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-multilevDSP20-sr0sn0-limited-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-real-first-fit", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-real-first-fit-nodistfx", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT_NODISTFX, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		/* ********* **********  **********  ********* */

	}

	/**
	 * This experiment is a follow up of experiment 20.
	 * Comparison of the following algorithms:
	 *  - ODP with bounds on optimality gap 5% and timeout at 300s
	 *  - Greedy First Fit (differently from exp 0-20, this heur. does not perform local search)
	 *  - Greedy First Fit without using the distance function
	 *
	 * This experiment uses brite2 networks with 36, 49, 64, 81, 100 nodes.
	 * Network-related QoS Metrics.
	 * Data source and sink are pinned on the same node (ID: 0).
	 */
	private static void experiment23(){

		/*
		 * Run1: min R; Num_Runs = 1 (from experiment19)
		 * Run2: max A; Num_Runs = 1 (from experiment19)
		 * Run3: min Z; Num_Runs = 1
		 * Run4: max A && min R,Z; weights = 0.33; Num_Runs = 5
		 */

		int NUM_RUNS = 5;

		/* ******* PARAMETERS ********** */
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 20;
		int[] NRES_SET = {36, 49, 64, 81, 100};
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.BRITE;

		srcRes = 0;
		snkRes = 0;

		K_NEIGHBORS = 5;
		GROUPING_FACTOR = 2;

		ODDParameters params = new ODDParameters(MODE.BANDWIDTH);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);
		params.setBwMode(ODDBandwidthModel.MODE.NETWORK_UTILIZATION);
//		params.setTimeLimit(86400.0); // 24h

		/* lODP */
//		params.setGap(0.05);
//		params.setDefineOptimalGap(true);
		params.setTimeLimit(300.0); // 24h

		/* ********* Sequential Application *********  */
		params.setRmax(3098.0);			params.setRmin(144.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(304000.0); 		params.setZmin(8000.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-seqDSP20-sr0sn0-localsearch", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-tabusearch", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-real-gff", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-real-first-fit-nodistfx", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT_NODISTFX, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}

			}
		}
		/* ********* **********  **********  ********* */

		/* ********* Fat Application *********  */
		dspType = DspGraphBuilder.TYPE.FAT;

		params.setRmax(247.0);			params.setRmin(49.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(446400.0); 		params.setZmin(52000.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-fatDSP20-sr0sn0-localsearch", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-tabusearch", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-real-gff", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-real-first-fit-nodistfx", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT_NODISTFX, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		/* ********* **********  **********  ********* */

		/* ********* Multilevel Application *********  */
		dspType = DspGraphBuilder.TYPE.MULTILEVEL;

		params.setRmax(410.0);			params.setRmin(74.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(1409200.0); 		params.setZmin(132200.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-multilevDSP20-sr0sn0-localsearch", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-tabusearch", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.TABUSEARCH, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-real-gff", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-real-first-fit-nodistfx", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_FIRSTFIT_NODISTFX, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		/* ********* **********  **********  ********* */

	}

	/**
	 * This experiment is a follow up of experiment 20.
	 * Execution of the replicated run of ODP-based heuristics
	 *
	 * This experiment uses brite2 networks with 36, 49, 64, 81, 100 nodes.
	 * Network-related QoS Metrics.
	 * Data source and sink are pinned on the same node (ID: 0).
	 */
	private static void experiment24(){

		/*
		 * Run1: min R; Num_Runs = 4
		 */

		int NUM_RUNS = 4;

		/* ******* PARAMETERS ********** */
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 20;
		int[] NRES_SET = {36, 49, 64, 81, 100};
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.BRITE;

		srcRes = 0;
		snkRes = 0;

		K_NEIGHBORS = 5;
		GROUPING_FACTOR = 2;

		ODDParameters params = new ODDParameters(MODE.BANDWIDTH);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);
		params.setBwMode(ODDBandwidthModel.MODE.NETWORK_UTILIZATION);
		params.setTimeLimit(86400.0); // 24h

		/* ********* Sequential Application *********  */
		params.setRmax(3098.0);			params.setRmin(144.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(304000.0); 		params.setZmin(8000.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-seqDSP20-sr0sn0-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-hier" + GROUPING_FACTOR, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-clus", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, true);
					run("brite"+NRES+"-seqDSP20-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}

			}
		}
		/* ********* **********  **********  ********* */

		/* ********* Fat Application *********  */
		dspType = DspGraphBuilder.TYPE.FAT;

		params.setRmax(247.0);			params.setRmin(49.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(446400.0); 		params.setZmin(52000.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-fatDSP20-sr0sn0-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-hier" + GROUPING_FACTOR, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-clus", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, true);
					run("brite"+NRES+"-fatDSP20-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		/* ********* **********  **********  ********* */

		/* ********* Multilevel Application *********  */
		dspType = DspGraphBuilder.TYPE.MULTILEVEL;

		params.setRmax(410.0);			params.setRmin(74.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(1409200.0); 		params.setZmin(132200.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		for(int NRES : NRES_SET){
			for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
				try {
					run("brite"+NRES+"-multilevDSP20-sr0sn0-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-hier" + GROUPING_FACTOR, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.HIERARCHICAL, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-clus", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, true);
					run("brite"+NRES+"-multilevDSP20-sr0sn0-Relaxed+Unif+Prob1kN"+K_NEIGHBORS, 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.RELAXATION, true);
				} catch (ODDException e) {
					e.printStackTrace();
				}
			}
		}
		/* ********* **********  **********  ********* */

	}


	/**
	 * Comparison of the following algorithms:
	 * - ODP
	 * - Cluster-based ODP
	 * - GreedyLocalSearch First Fit
	 *
	 * This experiment uses brite2 networks with 16 nodes.
	 *
	 * Multi-objective optimization function: R,A,N
	 *
	 * Data source and sink are pinned on every combination of source/sink
	 *
	 */
	private static void experiment21(){

		/*
		 * Run1: min R; Num_Runs = 1 (from experiment19)
		 * Run2: max A; Num_Runs = 1 (from experiment19)
		 * Run3: min Z; Num_Runs = 1
		 * Run4: max A && min R,Z; weights = 0.33; Num_Runs = 5
		 */

		int NUM_RUNS = 5;

		/* ******* PARAMETERS ********** */
		double RESTRICTION_ON_VRES 		= -1;
		int NDSP = 20;
		int NRES = 16;
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.BRITE;

		K_NEIGHBORS = 5;
		GROUPING_FACTOR = 2;

		ODDParameters params = new ODDParameters(MODE.BASIC);
		params.setWeightAvailability(WEIGHT_AVAILABILITY);
		params.setWeightRespTime(WEIGHT_RESP_TIME);
		params.setWeightCost(WEIGHT_COST);
		params.setWeightNetMetric(WEIGHT_NET_METRIC);
		setParameters(params);
		params.setBwMode(ODDBandwidthModel.MODE.NETWORK_UTILIZATION);
		params.setTimeLimit(86400.0); // 24h

//		/* ********* Sequential Application *********  */
//		params.setRmax(3098.0);			params.setRmin(144.0);
//		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
//		params.setZmax(303800.0); 		params.setZmin(8400.0);
//		params.setCmax(25.0);			params.setCmin(11.0);
//
//		for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
//
//			/* Optimal DSP Placement  */
//			for (srcRes = 0; srcRes < NRES; srcRes++){
//				for (snkRes = 0; snkRes < NRES; snkRes++){
//					try {
//						run("brite"+NRES+"-seqDSP20-sr"+srcRes+"sn"+snkRes+"-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
//					} catch (ODDException e) {	e.printStackTrace(); }
//				}
//			}
//
//
//			/* Cluster-based ODP */
//			for (srcRes = 0; srcRes < NRES; srcRes++){
//				for (snkRes = 0; snkRes < NRES; snkRes++){
//					try {
//						run("brite"+NRES+"-seqDSP20-sr"+srcRes+"sn"+snkRes+"-clus", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, true);
//					} catch (ODDException e) {	e.printStackTrace(); }
//				}
//			}
//
//			/* GreedyLocalSearch First-fit */
//			for (srcRes = 0; srcRes < NRES; srcRes++){
//				for (snkRes = 0; snkRes < NRES; snkRes++){
//					try {
//						run("brite"+NRES+"-seqDSP20-sr"+srcRes+"sn"+snkRes+"-greedyff", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, true);
//					} catch (ODDException e) {	e.printStackTrace(); }
//				}
//			}
//
//		}
//		/* ********* **********  **********  ********* */
//
//		/* ********* Fat Application *********  */
//		dspType = DspGraphBuilder.TYPE.FAT;
//
//		params.setRmax(247.0);			params.setRmin(49.0);
//		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
//		params.setZmax(446400.0); 		params.setZmin(52000.0);
//		params.setCmax(25.0);			params.setCmin(11.0);
//
//		for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
//			/* Optimal DSP Placement  */
//			for (srcRes = 0; srcRes < NRES; srcRes++){
//				for (snkRes = 0; snkRes < NRES; snkRes++){
//					try {
//						run("brite"+NRES+"-fatDSP20-sr"+srcRes+"sn"+snkRes+"-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
//					} catch (ODDException e) {	e.printStackTrace(); }
//				}
//			}
//
//
//			/* Cluster-based ODP */
//			for (srcRes = 0; srcRes < NRES; srcRes++){
//				for (snkRes = 0; snkRes < NRES; snkRes++){
//					try {
//						run("brite"+NRES+"-fatDSP20-sr"+srcRes+"sn"+snkRes+"-clus", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, true);
//					} catch (ODDException e) {	e.printStackTrace(); }
//				}
//			}
//
//			/* GreedyLocalSearch First-fit */
//			for (srcRes = 0; srcRes < NRES; srcRes++){
//				for (snkRes = 0; snkRes < NRES; snkRes++){
//					try {
//						run("brite"+NRES+"-fatDSP20-sr"+srcRes+"sn"+snkRes+"-greedyff", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, true);
//					} catch (ODDException e) {	e.printStackTrace(); }
//				}
//			}
//
//		}
//		/* ********* **********  **********  ********* */

		/* ********* Multilevel Application *********  */
		dspType = DspGraphBuilder.TYPE.MULTILEVEL;

		params.setRmax(410.0);			params.setRmin(74.0);
		params.setAmax(0.9719867357);   params.setAmin(0.5886970777);
		params.setZmax(1409200.0); 		params.setZmin(132200.0);
		params.setCmax(25.0);			params.setCmin(11.0);

		for (int runNumber = 0; runNumber < NUM_RUNS; runNumber++){
			/* Optimal DSP Placement  */
			for (srcRes = 0; srcRes < NRES; srcRes++){
				for (snkRes = 0; snkRes < NRES; snkRes++){
					try {
						run("brite"+NRES+"-multilevDSP20-sr"+srcRes+"sn"+snkRes+"-opt", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.OPTIMAL, true);
					} catch (ODDException e) {	e.printStackTrace(); }
				}
			}


			/* Cluster-based ODP */
			for (srcRes = 0; srcRes < NRES; srcRes++){
				for (snkRes = 0; snkRes < NRES; snkRes++){
					try {
						run("brite"+NRES+"-multilevDSP20-sr"+srcRes+"sn"+snkRes+"-clus", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.CLUSTER_BASED, true);
					} catch (ODDException e) {	e.printStackTrace(); }
				}
			}

			/* GreedyLocalSearch First-fit */
			for (srcRes = 0; srcRes < NRES; srcRes++){
				for (snkRes = 0; snkRes < NRES; snkRes++){
					try {
						run("brite"+NRES+"-multilevDSP20-sr"+srcRes+"sn"+snkRes+"-greedyff", 1, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,ALGORITHM.GREEDY_LOCALSEARCH, true);
					} catch (ODDException e) {	e.printStackTrace(); }
				}
			}
		}
		/* ********* **********  **********  ********* */

	}



	private static void run(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType, 
			int NDSP, DspGraphBuilder.TYPE dspType, 
			double RESTRICTION_ON_VRES, 
			ODDParameters params,
			ALGORITHM algorithm, 
			boolean networkMode
			) throws ODDException {
		
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);
		
		OptimalSolution solution = null;

		if(ALGORITHM.CLUSTER_BASED.equals(algorithm)){

			ODPPrunedSpace placementAlgorithm = new ODPPrunedSpace(NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,
					srcRes, snkRes);

			solution = placementAlgorithm.solve();

			Report report = new Report(""+experiment, "output");
			try {
				report.write(idSeries, runNumber, placementAlgorithm.getModel(), 
						placementAlgorithm.getDspGraphBuilder(), placementAlgorithm.getApplicationMetricProvider(), 
						placementAlgorithm.getResGraphBuilder(), placementAlgorithm.getResourceMetricProvider(), 
						RESTRICTION_ON_VRES, params, solution);
			} catch (ReportException | IOException e) {
				e.printStackTrace();
			}

		} else if (ALGORITHM.HIERARCHICAL.equals(algorithm)){
			
			HierarchicalODP placementAlgorithm = new HierarchicalODP(NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,
					srcRes, snkRes, GROUPING_FACTOR);

			solution = placementAlgorithm.solve();

			Report report = new Report(""+experiment, "output");
			try {
				report.write(idSeries, runNumber, placementAlgorithm.getModel(), 
						placementAlgorithm.getDspGraphBuilder(), placementAlgorithm.getApplicationMetricProvider(), 
						placementAlgorithm.getResGraphBuilder(), placementAlgorithm.getResourceMetricProvider(), 
						RESTRICTION_ON_VRES, params, solution);
			} catch (ReportException | IOException e) {
				e.printStackTrace();
			}

		} else if (ALGORITHM.RELAXATION.equals(algorithm)){
			
			RESODP placementAlgorithm = new RESODP(NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,
					srcRes, snkRes, useAllCandidate, K_NEIGHBORS);

			solution = placementAlgorithm.solve();

			Report report = new Report(""+experiment, "output");
			try {
				report.write(idSeries, runNumber, placementAlgorithm.getModel(), 
						placementAlgorithm.getDspGraphBuilder(), placementAlgorithm.getApplicationMetricProvider(), 
						placementAlgorithm.getResGraphBuilder(), placementAlgorithm.getResourceMetricProvider(), 
						RESTRICTION_ON_VRES, params, solution);
			} catch (ReportException | IOException e) {
				e.printStackTrace();
			}

		} else if (ALGORITHM.GREEDY_FIRSTFIT.equals(algorithm)){

			GreedyFirstfit placementAlgorithm = new GreedyFirstfit(NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,
					srcRes, snkRes);

			solution = placementAlgorithm.solve();

			Report report = new Report(""+experiment, "output");
			try {
				report.write(idSeries, runNumber, null,
						placementAlgorithm.getDspGraphBuilder(), placementAlgorithm.getApplicationMetricProvider(),
						placementAlgorithm.getResGraphBuilder(), placementAlgorithm.getResourceMetricProvider(),
						RESTRICTION_ON_VRES, params, solution);
			} catch (ReportException | IOException e) {
				e.printStackTrace();
			}

		} else if (ALGORITHM.GREEDY_FIRSTFIT_NODISTFX.equals(algorithm)){

			GreedyFirstfit placementAlgorithm = new GreedyFirstfit(NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,
					srcRes, snkRes);

			solution = placementAlgorithm.solveWithoutDistanceFunction();

			Report report = new Report(""+experiment, "output");
			try {
				report.write(idSeries, runNumber, null,
						placementAlgorithm.getDspGraphBuilder(), placementAlgorithm.getApplicationMetricProvider(),
						placementAlgorithm.getResGraphBuilder(), placementAlgorithm.getResourceMetricProvider(),
						RESTRICTION_ON_VRES, params, solution);
			} catch (ReportException | IOException e) {
				e.printStackTrace();
			}

		} else if (ALGORITHM.GREEDY_LOCALSEARCH.equals(algorithm)){

			GreedyLocalSearch placementAlgorithm = new GreedyLocalSearch(NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params,
					srcRes, snkRes);

			solution = placementAlgorithm.solve();

			Report report = new Report(""+experiment, "output");
			try {
				report.write(idSeries, runNumber, null,
						placementAlgorithm.getDspGraphBuilder(), placementAlgorithm.getApplicationMetricProvider(),
						placementAlgorithm.getResGraphBuilder(), placementAlgorithm.getResourceMetricProvider(),
						RESTRICTION_ON_VRES, params, solution);
			} catch (ReportException | IOException e) {
				e.printStackTrace();
			}

		} else if (ALGORITHM.TABUSEARCH.equals(algorithm)){
			
			TabuSearch placementAlgorithm = new TabuSearch(NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params, 
					srcRes, snkRes);

			solution = placementAlgorithm.solve();

			Report report = new Report(""+experiment, "output");
			try {
				report.write(idSeries, runNumber, null,
						placementAlgorithm.getDspGraphBuilder(), placementAlgorithm.getApplicationMetricProvider(), 
						placementAlgorithm.getResGraphBuilder(), placementAlgorithm.getResourceMetricProvider(), 
						RESTRICTION_ON_VRES, params, solution);
			} catch (ReportException | IOException e) {
				e.printStackTrace();
			}
			
		} else if (ALGORITHM.OPTIMAL.equals(algorithm)) {
			
			solveODD(idSeries, experiment, runNumber, NRES, resType, NDSP, dspType, RESTRICTION_ON_VRES, params, srcRes, snkRes, networkMode);  

		} 
		
//		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
//		System.out.println("\n\n");
	}
		
	
	private static void solveODD(
				String idSeries, int experiment, int runNumber, 
				int NRES, ResGraphBuilder.TYPE resType, 
				int NDSP, DspGraphBuilder.TYPE dspType, 
				double RESTRICTION_ON_VRES,
				ODDParameters params, 
				int srcRes, int snkRes, 
				boolean networkMode
				) throws ODDException {
			
			ResGraphBuilder rbuilder = new ResGraphBuilder();
			ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
			rbuilder.create(rmp, resType, NRES);
			
			DspGraphBuilder gbuilder = new DspGraphBuilder();
			ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
			gbuilder.create(amp, dspType, NDSP);
			if (RESTRICTION_ON_VRES != -1)
				gbuilder.restrictPlacement(rbuilder.getGraph(), RESTRICTION_ON_VRES);
			
			ODDModel model = null;
			
			if (!networkMode){
				model =	new ODDBasicModel(gbuilder.getGraph(), rbuilder.getGraph(), params);
			} else {
				params.setBwMode(ODDBandwidthModel.MODE.NETWORK_UTILIZATION);
				model = new ODDBandwidthModel(gbuilder.getGraph(), rbuilder.getGraph(), params);
			}

			model.compile();
			
			for (Integer src : gbuilder.getGraph().getSources()){
				model.pin(src, srcRes);
			}
			for (Integer snk : gbuilder.getGraph().getSinks()){
				model.pin(snk, snkRes);
			}	

			
			OptimalSolution solution = model.solve();
			
			Report report = new Report(""+experiment, "output");
			try {
				report.write(idSeries, runNumber, model, 
						gbuilder, amp,
						rbuilder, rmp, 
						RESTRICTION_ON_VRES, params, solution);
			} catch (ReportException | IOException e) {
				e.printStackTrace();
			}

			model.clean();
	}
	
	private static void setParameters(ODDParameters params){

		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		

		params.setAvgBytePerTupleMin(1500.0);
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);

//		params.setLambdaMin(0.014 / 5.0); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMin(100.0); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0); // 0.05; // 0.00005; // 0.15; // 0.0005;
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(2.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		// XXX: node availability is generated with a uniform distribution
		// between NodeAvailabilityMin and NodeAvailabilityMean
		params.setNodeAvailabilityMin(.97);
		params.setNodeAvailabilityMean(.99999);
		params.setNodeAvailabilityStdDev(0.0003);
		
		params.setServiceRate(1.0);
		
	}


	/* ******* PARAMETERS ********** */
	/* ODD Weights */
	private static double WEIGHT_AVAILABILITY 	= 0;
	private static double WEIGHT_RESP_TIME 		= 0;
	private static double WEIGHT_COST			= 0;
	private static double WEIGHT_NET_METRIC 	= 0;
	/* ******* .PARAMETERS ********** */
	
	private static int srcRes = 0;
	private static int snkRes = 0;
	
}
