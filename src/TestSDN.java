import codd.DspGraphBuilder;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.OptimalSolution;
import codd.placement.*;
import codd.placement.ODDModel.MODE;
import codd.report.Report;
import codd.report.ReportException;

import java.io.IOException;

public class TestSDN {
	
	public static void main(String[] args) {

		System.out.println("Optimal DSP Placement library");

		/* ODP which models the underlying physical network (to be integrated with an SDN controller) */
		testODPSDNModel();

	
	}

	private static void testODPSDNModel(){

		int NRES = 3;
		int NDSP = 3;
		int RESTRICTION_ON_VRES = -1;
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;
		/* the following property allows multi-hop routing 
		 	(i.e., to account for physical links) */
		ResGraphBuilder.TYPE resType = ResGraphBuilder.TYPE.MULTIHOP;
		
		
		/* Compile, solve, and report a test execution of ODP+SDN  model */
		
		System.out.println("Testing ODP + SDN model");
		try {
			ODDParameters params = new ODDParameters(MODE.BASIC);
			params.setWeightAvailability(0);
			params.setWeightRespTime(0);
			params.setWeightCost(1);
			params.setWeightNetMetric(0);
			setParameters(params);

            params.setRmax(100000);
            params.setRmin(8824);
			params.setCmax(3);
			params.setCmin(1);

// 			params.setGap(0.1);

			ResGraphBuilder rbuilder = new ResGraphBuilder();
			ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
			rbuilder.create(rmp, resType, NRES);
	
			// DEBUG
			rbuilder.printGraph(false);
	
			DspGraphBuilder gbuilder = new DspGraphBuilder();
			ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
			gbuilder.create(amp, dspType, NDSP);
			
			// DEBUG
			gbuilder.printGraph();
			
			ODDModel model = new ODDPhysicalLinksModel(gbuilder.getGraph(), rbuilder.getGraph(), params);
			model.compile();	
			OptimalSolution solution = model.solve();

			Report report = new Report("testODPSDN", "output");
			try {
				report.write("testODPSDN",0, model, gbuilder, amp, rbuilder, rmp, RESTRICTION_ON_VRES, params, solution);
			} catch (ReportException | IOException e) {
				e.printStackTrace();
			}

			System.out.println(solution.getUsedNodes());
			System.out.println(solution.getUsedNetworkLinks());
			
			System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
			System.out.println("\n\n");
			
		} catch (ODDException e1) {
			e1.printStackTrace();
		}

	}





	private static void setParameters(ODDParameters params){

		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		

		params.setAvgBytePerTupleMin(1500.0);
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);

		params.setLambdaMin(0.014 / 5.0); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0); // 0.05; // 0.00005; // 0.15; // 0.0005;
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(2.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		params.setNodeAvailabilityMin(.85);
		params.setNodeAvailabilityMean(.95);
		params.setNodeAvailabilityStdDev(0.03);
		
		params.setServiceRate(0.02); // 100.0; // 15.0; // 100.0
		
	}


	/* ******* PARAMETERS ********** */
	/* ODD Weights */
	protected static double WEIGHT_AVAILABILITY = 0;
	protected static double WEIGHT_RESP_TIME = 1;
	/* ******* .PARAMETERS ********** */
	
}
