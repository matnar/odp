import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import codd.DspGraphBuilder;
import codd.ODDException;
import codd.ODDParameters;
import codd.ResGraphBuilder;
import codd.metricsprovider.ApplicationMetricsProvider;
import codd.metricsprovider.ResourceMetricsProvider;
import codd.metricsprovider.SimpleApplicationMetricsProvider;
import codd.metricsprovider.SimpleResourceMetricsProvider;
import codd.model.DspEdge;
import codd.model.DspGraph;
import codd.model.DspPath;
import codd.model.DspVertex;
import codd.model.OptimalSolution;
import codd.model.Pair;
import codd.model.ResourceEdge;
import codd.model.ResourceGraph;
import codd.model.ResourceVertex;
import codd.placement.ODDBandwidthModel;
import codd.placement.ODDModel;
import codd.placement.ODDModel.MODE;
import codd.placement.ODDReplicatedOperatorsModel;
import codd.report.Report;
import codd.report.ReportException;


public class EvaluationPer2016 {
	

	public static void main(String[] args) {


		System.out.println("Optimal DSP Placement library");

		experiment16();
	}
	
	
	
	/** 
	 * Session of experiment for InfQ. ReplODP.
	 * 
	 * Evaluating R,C,A,Z with several weights configuration, 
	 * when src and sink are pinned on two predefined computing 
	 * nodes and the utilization imposed by the DSP operator increases.
	 * The utilization increment grows linearly: the bottleneck operators
	 * loads to the computing nodes from 10% to 90%.
	 */
	private static void experiment16(){
		
		int NRES = 15;
		int NDSP = 10; 
		DspGraphBuilder.TYPE dspType = DspGraphBuilder.TYPE.SEQUENTIAL;

		int srcRes = 0;
		int snkRes = 11;				

		WEIGHT_RESP_TIME = 0.25;
		WEIGHT_COST = 0.25;
		WEIGHT_AVAILABILITY = 0.25;
		WEIGHT_NET_METRIC = 0.25;
		System.out.println("\n\n\nReplODP: New session");
		try {
			
			compileSolveAndReportReplODP("ReplODPSeqAnsnet_Wr"+WEIGHT_RESP_TIME+"c"+WEIGHT_COST+"a"+WEIGHT_AVAILABILITY+"z"+WEIGHT_NET_METRIC+
					"_10dsp15res_src"+srcRes + "snk" + snkRes + "_lambda" + lambdaMin, 1, 1, 
					NRES, ResGraphBuilder.TYPE.ANSNET, NDSP, dspType, srcRes, snkRes);
				
		} catch (ODDException e) {
			e.printStackTrace();
		}
	
	}



	
	private static void compileSolveAndReportReplODP(
			String idSeries, int experiment, int runNumber, 
			int NRES, ResGraphBuilder.TYPE resType, 
			int NDSP, DspGraphBuilder.TYPE dspType, 
			int srcRes, int snkRes
			) throws ODDException {
		
		
		System.out.println("Running " + idSeries + ": experiment: "
				+ experiment + ", run: " + runNumber);
		
		ODDParameters params = new ODDParameters(MODE.BANDWIDTH, ODDBandwidthModel.MODE.INTERNODE_TRAFFIC);
		params.setWeightAvailability(0);
		params.setWeightRespTime(1);
		params.setWeightCost(0);
		params.setWeightNetMetric(0);
		setParameters(params);
		params.setRmax(1000.0);
		params.setRmin(1);
		
		ResGraphBuilder rbuilder = createTUWienInfrastructure();
		ResourceMetricsProvider rmp = new SimpleResourceMetricsProvider(params);
		rbuilder.printGraph(false);
		
		DspGraphBuilder gbuilder = createDebsTopology();
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(dspType, NDSP, params);
		gbuilder.printGraph();
		
		ODDModel model = new ODDReplicatedOperatorsModel(gbuilder.getGraph(), rbuilder.getGraph(), params);
		model.compile();	
		model.pin(0, 0);
		model.pin(7, 0);
		
		OptimalSolution solution = model.solve();
		
		Report report = new Report(""+experiment, "output");
		try {
			double RESTR = -1.0;
			report.write(idSeries, runNumber, model, gbuilder, amp, rbuilder, rmp, RESTR, srcRes, snkRes, params, solution);
		} catch (ReportException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("+ Res time: compilation " + solution.getCompilationTime() + "ms, resolution " + solution.getResolutionTime() +" ms.");
		System.out.println("\n\n");
	}
	
	
	private static void setParameters(ODDParameters params){

		/* Application Metrics Provider */
		params.setRespTimeMin(3.0);
		params.setRespTimeMean(0.0); 
		params.setRespTimeStdDev(0.0); 
		

		params.setAvgBytePerTupleMin(1500.0); // this is byte/ms (i.e., KB/s)
		params.setAvgBytePerTupleMean(0.0); 
		params.setAvgBytePerTupleStdDev(0.0);

		params.setLambdaMin(lambdaMin); // 99.978; // 10.0; // 99.978; // 100.0;
		params.setLambdaMean(0.0);
		params.setLambdaStdDev(0.0);
		params.setLambdaSheddingFactor(0.05); // 0.05; // 0.00005; // 0.15; // 0.0005;
		
		params.setReqResourcesMin(1.0);
		params.setReqResourcesMean(0.0);
		params.setReqResourcesStdDev(0.0);

		params.setCostPerResource(1.0);
		
		
		/* Resource Metrics Provider */
		params.setLinkDelayMin(1.0);
		params.setLinkDelayMean(22.0);
		params.setLinkDelayStdDev(5.0);

		params.setLinkAvailMin(1.0);
		params.setLinkAvailMean(0.0);
		params.setLinkAvailStdDev(0.0);

		params.setLinkBandwidthMin(Double.MAX_VALUE);
		params.setLinkBandwidthMean(0.0);
		params.setLinkBandwidthStdDev(0.0);

		params.setLinkCostPerUnitData(0.02);
		
		params.setNodeAvailResourcesMin(5.0);
		params.setNodeAvailResourcesMean(0.0);
		params.setNodeAvailResourcesStdDev(0.0);

		params.setNodeSpeedupMin(1.0);
		params.setNodeSpeedupMean(0.0);
		params.setNodeSpeedupStdDev(0.0);

		params.setNodeAvailabilityMin(.97);			// uniform distribution
		params.setNodeAvailabilityMean(.9999999);	// used as max
		params.setNodeAvailabilityStdDev(0.03);

		params.setServiceRate(serviceRate);			// 100.0; // 15.0; // 100.0;
		
	}
	
	
	
	private static ResGraphBuilder createTUWienInfrastructure(){
		
		Map<Integer, ResourceVertex> vRes = new HashMap<Integer, ResourceVertex>();
		Map<Pair, ResourceEdge> eRes = new HashMap<Pair, ResourceEdge>();
		
		/* 1. Create ResourceVertex to represent nodes (supervisors, set of worker slots) */
		vRes.put(new Integer(0), new ResourceVertex(0, 12, 1.0, 1.0));
		vRes.put(new Integer(1), new ResourceVertex(1, 12, 1.0, 1.0));
		vRes.put(new Integer(2), new ResourceVertex(2, 12, 1.0, 1.0));
		vRes.put(new Integer(3), new ResourceVertex(3, 12, 1.0, 1.0));
		vRes.put(new Integer(4), new ResourceVertex(4, 12, 1.0, 1.0));
		vRes.put(new Integer(5), new ResourceVertex(5, 12, 1.0, 1.0));
		vRes.put(new Integer(6), new ResourceVertex(6, 12, 1.0, 1.0));
		vRes.put(new Integer(7), new ResourceVertex(7, 12, 1.0, 1.0));
		
		/* 2. Create DspEdges to represent link between executors */
		double bw = Double.MAX_VALUE;
		double av = 1.0;

		eRes.put(new Pair(0, 0), new ResourceEdge(0, 0, bw, 0, av, 1.0));
		eRes.put(new Pair(0, 1), new ResourceEdge(0, 1, bw, 14, av, 1.0));
		eRes.put(new Pair(1, 0), new ResourceEdge(1, 0, bw, 14, av, 1.0));
		eRes.put(new Pair(2, 0), new ResourceEdge(2, 0, bw, 16, av, 1.0));
		eRes.put(new Pair(0, 2), new ResourceEdge(0, 2, bw, 16, av, 1.0));
		eRes.put(new Pair(1, 1), new ResourceEdge(1, 1, bw, 0, av, 1.0));
		eRes.put(new Pair(2, 1), new ResourceEdge(2, 1, bw, 30, av, 1.0));
		eRes.put(new Pair(0, 3), new ResourceEdge(0, 3, bw, 11, av, 1.0));
		eRes.put(new Pair(1, 2), new ResourceEdge(1, 2, bw, 30, av, 1.0));
		eRes.put(new Pair(3, 0), new ResourceEdge(3, 0, bw, 11, av, 1.0));
		eRes.put(new Pair(2, 2), new ResourceEdge(2, 2, bw, 0, av, 1.0));
		eRes.put(new Pair(0, 4), new ResourceEdge(0, 4, bw, 21, av, 1.0));
		eRes.put(new Pair(1, 3), new ResourceEdge(1, 3, bw, 24, av, 1.0));
		eRes.put(new Pair(3, 1), new ResourceEdge(3, 1, bw, 24, av, 1.0));
		eRes.put(new Pair(4, 0), new ResourceEdge(4, 0, bw, 21, av, 1.0));
		eRes.put(new Pair(2, 3), new ResourceEdge(2, 3, bw, 11, av, 1.0));
		eRes.put(new Pair(1, 4), new ResourceEdge(1, 4, bw, 23, av, 1.0));
		eRes.put(new Pair(0, 5), new ResourceEdge(0, 5, bw, 9, av, 1.0));
		eRes.put(new Pair(3, 2), new ResourceEdge(3, 2, bw, 11, av, 1.0));
		eRes.put(new Pair(4, 1), new ResourceEdge(4, 1, bw, 23, av, 1.0));
		eRes.put(new Pair(5, 0), new ResourceEdge(5, 0, bw, 9, av, 1.0));
		eRes.put(new Pair(6, 0), new ResourceEdge(6, 0, bw, 26, av, 1.0));
		eRes.put(new Pair(2, 4), new ResourceEdge(2, 4, bw, 32, av, 1.0));
		eRes.put(new Pair(1, 5), new ResourceEdge(1, 5, bw, 18, av, 1.0));
		eRes.put(new Pair(0, 6), new ResourceEdge(0, 6, bw, 26, av, 1.0));
		eRes.put(new Pair(3, 3), new ResourceEdge(3, 3, bw, 0, av, 1.0));
		eRes.put(new Pair(4, 2), new ResourceEdge(4, 2, bw, 32, av, 1.0));
		eRes.put(new Pair(5, 1), new ResourceEdge(5, 1, bw, 18, av, 1.0));
		eRes.put(new Pair(7, 0), new ResourceEdge(7, 0, bw, 9, av, 1.0));
		eRes.put(new Pair(6, 1), new ResourceEdge(6, 1, bw, 37, av, 1.0));
		eRes.put(new Pair(2, 5), new ResourceEdge(2, 5, bw, 19, av, 1.0));
		eRes.put(new Pair(1, 6), new ResourceEdge(1, 6, bw, 37, av, 1.0));
		eRes.put(new Pair(0, 7), new ResourceEdge(0, 7, bw, 9, av, 1.0));
		eRes.put(new Pair(3, 4), new ResourceEdge(3, 4, bw, 20, av, 1.0));
		eRes.put(new Pair(4, 3), new ResourceEdge(4, 3, bw, 20, av, 1.0));
		eRes.put(new Pair(5, 2), new ResourceEdge(5, 2, bw, 19, av, 1.0));
		eRes.put(new Pair(7, 1), new ResourceEdge(7, 1, bw, 16, av, 1.0));
		eRes.put(new Pair(6, 2), new ResourceEdge(6, 2, bw, 23, av, 1.0));
		eRes.put(new Pair(2, 6), new ResourceEdge(2, 6, bw, 23, av, 1.0));
		eRes.put(new Pair(1, 7), new ResourceEdge(1, 7, bw, 16, av, 1.0));
		eRes.put(new Pair(3, 5), new ResourceEdge(3, 5, bw, 8, av, 1.0));
		eRes.put(new Pair(4, 4), new ResourceEdge(4, 4, bw, 0, av, 1.0));
		eRes.put(new Pair(5, 3), new ResourceEdge(5, 3, bw, 8, av, 1.0));
		eRes.put(new Pair(7, 2), new ResourceEdge(7, 2, bw, 17, av, 1.0));
		eRes.put(new Pair(6, 3), new ResourceEdge(6, 3, bw, 15, av, 1.0));
		eRes.put(new Pair(2, 7), new ResourceEdge(2, 7, bw, 17, av, 1.0));
		eRes.put(new Pair(3, 6), new ResourceEdge(3, 6, bw, 15, av, 1.0));
		eRes.put(new Pair(4, 5), new ResourceEdge(4, 5, bw, 13, av, 1.0));
		eRes.put(new Pair(5, 4), new ResourceEdge(5, 4, bw, 13, av, 1.0));
		eRes.put(new Pair(7, 3), new ResourceEdge(7, 3, bw, 19, av, 1.0));
		eRes.put(new Pair(6, 4), new ResourceEdge(6, 4, bw, 21, av, 1.0));
		eRes.put(new Pair(3, 7), new ResourceEdge(3, 7, bw, 19, av, 1.0));
		eRes.put(new Pair(4, 6), new ResourceEdge(4, 6, bw, 21, av, 1.0));
		eRes.put(new Pair(5, 5), new ResourceEdge(5, 5, bw, 0, av, 1.0));
		eRes.put(new Pair(7, 4), new ResourceEdge(7, 4, bw, 31, av, 1.0));
		eRes.put(new Pair(6, 5), new ResourceEdge(6, 5, bw, 18, av, 1.0));
		eRes.put(new Pair(4, 7), new ResourceEdge(4, 7, bw, 31, av, 1.0));
		eRes.put(new Pair(5, 6), new ResourceEdge(5, 6, bw, 18, av, 1.0));
		eRes.put(new Pair(7, 5), new ResourceEdge(7, 5, bw, 19, av, 1.0));
		eRes.put(new Pair(6, 6), new ResourceEdge(6, 6, bw, 0, av, 1.0));
		eRes.put(new Pair(5, 7), new ResourceEdge(5, 7, bw, 19, av, 1.0));
		eRes.put(new Pair(7, 6), new ResourceEdge(7, 6, bw, 34, av, 1.0));
		eRes.put(new Pair(6, 7), new ResourceEdge(6, 7, bw, 34, av, 1.0));
		eRes.put(new Pair(7, 7), new ResourceEdge(7, 7, bw, 0, av, 1.0));
		

		/* 3. Creating object DspGraph */

		String graphName = "resources";
		ResGraphBuilder a = new ResGraphBuilder(new ResourceGraph(graphName, vRes, eRes));
		return a;
		
	}
	
	
	
	private static DspGraphBuilder createDebsTopology() throws ODDException {
		/* SEQUENTIAL layered topology */
		Map<Integer, DspVertex> vDsp = new HashMap<Integer, DspVertex>();
		Map<Pair, DspEdge> eDsp = new HashMap<Pair, DspEdge>();
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();

		/* Source and sink are not replicated */

		/* 1. Create DspVertex */
		DspVertex v = new DspVertex(0, "datasource", 1, 2.0, 1.0);
		v.setServiceRate(120.0);
		vDsp.put(new Integer(0), v);
		v = new DspVertex(1, "parser", 1, 2.0, 1.0);
		v.setServiceRate(75.0);
		vDsp.put(new Integer(1), v);
		v = new DspVertex(2, "filterByCoordinates", 1, 2.0, 1.0);
		v.setServiceRate(75.0);
		vDsp.put(new Integer(2), v);
		v = new DspVertex(3, "metronome", 1, 2.0, 1.0);
		v.setServiceRate(75.0);
		vDsp.put(new Integer(3), v);
		v = new DspVertex(4, "computeCellID", 1, 2.0, 1.0);
		v.setServiceRate(75.0);
		vDsp.put(new Integer(4), v);
		v = new DspVertex(5, "countByWindow", 1, 2.0, 1.0);
		v.setServiceRate(35.0);
		vDsp.put(new Integer(5), v);
		v = new DspVertex(6, "partialRank", 1, 2.0, 1.0);
		v.setServiceRate(500.0);
		vDsp.put(new Integer(6), v);
		v = new DspVertex(7, "globalrank", 1, 2.0, 1.0);
		v.setServiceRate(300.0);
		vDsp.put(new Integer(7), v);

		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));

		/* 3. Create DspEdges to represent link between executors */
		double alfa = 1.1;
		DspEdge ij = new DspEdge(0, 1, 75, 1500);
		eDsp.put(new Pair(0, 1), ij);
		ij = new DspEdge(1, 2, alfa * 75, 1500);
		eDsp.put(new Pair(1, 2), ij);
		ij = new DspEdge(2, 3, alfa * 75, 1500);
		eDsp.put(new Pair(2, 3), ij);
		ij = new DspEdge(2, 4, alfa * 75, 1500);
		eDsp.put(new Pair(2, 4), ij);
		ij = new DspEdge(3, 5, alfa * 25, 1500);
		eDsp.put(new Pair(3, 5), ij);
		ij = new DspEdge(4, 5, alfa * 10, 1500);
		eDsp.put(new Pair(4, 5), ij);
		ij = new DspEdge(5, 6, alfa * 700, 1500);
		eDsp.put(new Pair(5, 6), ij);
		ij = new DspEdge(6, 7, alfa * 262, 1500);
		eDsp.put(new Pair(6, 7), ij);

		/* 4. Compute Paths */
		ArrayList<DspPath> paths = computePaths(vDsp, eDsp, sourcesIndex);

		/* 5. Identify sinks */
		for (DspPath path : paths) {
			sinksIndex.add(path.getSink());
		}

		/* 5. Creating object DspGraph */
		String graphId = "long-topology";
		DspGraph dspGraph = new DspGraph(graphId, vDsp, eDsp);
		dspGraph.setPaths(paths);

		for (Integer so : sourcesIndex)
			dspGraph.addSource(so);

		for (Integer si : sinksIndex)
			dspGraph.addSink(si);

		return new DspGraphBuilder(dspGraph);
	}

	private static List<Pair> getOutgoingEdges(int currentNodeId, Set<Pair> edges) {
		List<Pair> outgoingEdges = new ArrayList<Pair>();
		for (Pair e : edges)
			if (e.getA() == currentNodeId)
				outgoingEdges.add(e);
		return outgoingEdges;
	}

	private static ArrayList<DspPath> getPathsEndingInNode(ArrayList<DspPath> paths,
			int nodeId) {

		ArrayList<DspPath> output = new ArrayList<DspPath>();
		for (DspPath path : paths) {
			if (path.isSink(nodeId))
				output.add(path);
		}
		return output;
	}

	private static ArrayList<DspPath> computePaths(Map<Integer, DspVertex> vDsp,
			Map<Pair, DspEdge> eDsp, List<Integer> sourcesIndex) {
		/* 3. Compute paths between source and sinks */
		/* Creating paths... */
		List<Integer> frontier = new ArrayList<Integer>();

		/* Create dumb paths from source to source itself */
		ArrayList<DspPath> paths = new ArrayList<DspPath>();
		for (Integer so : sourcesIndex) {
			DspPath path = new DspPath(so);
			paths.add(path);
		}

		if (sourcesIndex.size() > 0) {
			/* Initialize graph exploration variables */
			/* Current node must be a source of the DSP Graph */
			Integer currentNode = sourcesIndex.get(0);
			boolean modified = true;

			do {

				modified = false;
				List<Pair> oEdges = getOutgoingEdges(currentNode, eDsp.keySet());

				/* Update nodes nodeTBV frontier */
				for (Pair p : oEdges) {
					frontier.add(new Integer(p.getB()));
				}

				ArrayList<DspPath> pathsToUpdate = getPathsEndingInNode(paths,
						currentNode);

				for (DspPath pathToUpdate : pathsToUpdate) {

					if (oEdges.isEmpty()) {
						continue;
					}

					paths.remove(pathToUpdate);

					for (Pair oe : oEdges) {
						DspPath newPath = pathToUpdate.clone();
						newPath.add(oe.getB());
						paths.add(newPath);
						modified = true;
					}
				}

				if (frontier.isEmpty()) {
					modified = false;
				} else {
					currentNode = frontier.remove(0);
				}
			} while (modified || !frontier.isEmpty());
		}

		return paths;
	}


	
	/* ******* PARAMETERS ********** */
	/* ODD Weights */
	private static double WEIGHT_AVAILABILITY = 0;
	private static double WEIGHT_RESP_TIME = 1;
	private static double WEIGHT_COST = 0;
	private static double WEIGHT_NET_METRIC = 0;	
	/* ******* .PARAMETERS ********** */
	
	private static double lambdaMin	= 0.014 / 5.0; // 99.978; // 10.0; // 99.978; // 100.0;	
	private static double serviceRate = 0.02; // 100.0; // 15.0; // 100.0;

}
