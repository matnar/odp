# Optimal DSP Placement and Replication 

## Content of the repository 

This repository includes the ILP model and heuristics defined to solve the Placement (and Replication) problem of Data Stream Processing (DSP) applications over geographically distributed computing resources. 

This is a research project, so the code is absolutely not production ready and only aims to show the benefits of approaches described in research papers. 

The repository include ILP models as well as heuristics that contributed to the publication of several papers. Therefore, we strongly encourage you to read the papers before playing with this project. 

This project represents a library for a second repository, which instead focuses on the integration of such ILP models and heuristics within the Apache Storm framework for DSP applications. 

To simplify the exploration and the evalution of this repository content, it includes `main()` files that help to quickly evaluate the ODP/ODRP/EDRP and heuristics included in this library. 

This repository mainly includes the following Integer Linear Programming (ILP) models:

- ODP (Optimal DSP Placement)
- ODRP (Optimal DSP Replication and Placement)
- EDRP (Elastic DSP Replication and Placement)
- Security-aware DSP Placement (SDP)

Placement heuristics included: 

- ODP-PS
- Hierarchical ODP 
- Greedy First-fit
- Greedy Local Search 
- Tabu Search

## Build 

### Requirements 

- Java v8 
- CPLEX v12.6 

### Dependency: CPLEX
First of all, download and install the IBM CPLEX Optimization Studio from the official [IBM website](https://www.ibm.com/products/ilog-cplex-optimization-studio). CPLEX creates different folders within the system, including a "lib" and a "bin" folder. The first contains a "cplex.jar" file, which we are going to locally install on our Maven repository. The secondo contains binary files needed to "cplex.jar", which should be provided while executing our ODP library.  

To install the `lib/cplex.jar` file on the local Maven repositoty, we run the following command: 

```bash 
mvn install:install-file -Dfile=cplex.jar -DgroupId=cplex -DartifactId=cplex -Dversion=12.6 -Dpackaging=jar
```

> ODP 2.0 uses cplex version 12.6. 

### ODP 

ODP is a Maven project. To create a package, you can use the maven command line (or the commands integrated in your favourite IDE). 

To build the project, go to the source folder (where the `pom.xml` file is located) and run the following command:

```bash 
mvn package 
```

This creates a `target` folder with a newly packed `ODD-2.0.jar` file. 


## Execution 

ODP has been designed as a library. Nonetheless, it is possible to play with it and several `Main()` Java classes can be found within this project. 

When the library/program should be executed, you have to inform Java of the location of the (binary of) cplex using the `-Djava.library.path=` parameter. 

As an example, you can run the `HeuristicComparison` as follows:

```bash 
java -Djava.library.path=/cplex/bin/x86-64 -classpath .:./cplex-12.6.jar:./ODD-2.0.jar HeuristicComparison
```

## How to cite 

This repository includes the efforts of published in different papers. We really appreciate if you acknowledge our work by citing the published papers where the ILP model/heuristic has been described. 

- V. Cardellini, V. Grassi, F. Lo Presti, M. Nardelli, "Optimal Operator Placement for Distributed Stream Processing Applications", Proceedings of the 10th ACM International Conference on Distributed and Event-Based Systems (DEBS 2016), pp. 69-80, Irvine, CA, USA, June 2016. doi: 10.1145/2933267.2933312. 

    This paper describes: ODP


- V. Cardellini, V. Grassi, F. Lo Presti, M. Nardelli, "Optimal Operator Replication and Placement for Distributed Stream Processing Systems", ACM SIGMETRICS Performance Evaluation Review, Vol. 44, No. 4, pp. 11-22, May 2017. doi: 10.1145/3092819.3092823. 

    This paper describes: ODRP

- V. Cardellini, F. Lo Presti, M. Nardelli, G. Russo Russo, "Optimal Operator Deployment and Replication for Elastic Distributed Data Stream Processing", Concurrency and Computation: Practice and Experience, Vol. 30, No. 9, May 2018. doi: 10.1002/cpe.4334

    This paper describes: EDRP

- M. Nardelli, V. Cardellini, V. Grassi, F. Lo Presti, "Efficient Operator Placement for Distributed Data Stream Processing Applications", IEEE Transactions on Parallel and Distributed Systems, vol. 30, no. 8, pp. 1753–1767, 2019. doi: 10.1109/TPDS.2019.2896115. 

    This paper describes: ODP, ODP-PS, Hierarchical ODP, Greedy First-fit, Greedy Local Search, Tabu Search

- G. Russo Russo, V. Cardellini, F. Lo Presti, M. Nardelli, "Towards a Security-aware Deployment of Data Streaming Applications in Fog Computing", Fog/Edge Computing For Security, Privacy, and Applications, Advances in Information Security, W. Chang and J. Wu (eds.), Springer, vol. 83, pp. 355–385, 2021. doi: 10.1007/978-3-030-57328-7_14.

    This paper describes: SDP
    
## Authors 

- Matteo Nardelli 
- Gabriele Russo Russo 

## Contribute 

If you want to contribute, you are more than welcome. Feel free to open a pull request to integrate your improvement to this repository. 

If you want to fork the repository, acknowledge this repository and always cite our work. 

## License 
This repository is distributed under the Apache License 2.0.
